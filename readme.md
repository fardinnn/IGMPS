## Integrated Game Maker And Publisher System
In this repository, we develop an application to connect gamers and developers and to simplify processes for both of them. Output games will have a small size, and also every game can use all of the installed technologies.
The software consists of several packages that we provide and several games made with the software.

## Motivation
Game industry revenue was $162 B in 2020. Also, the market trend will rise for the next seven years with the rate of %12, and relatively the number of developers will increase
But both developers and gamers have some unresolved problems such as management problems, high sized games, and expensive and time-consuming processes in game development. so we want to solve all those problems

## Build Status
..?

## Code Style
..?

## Used Technologies
### V.0.0 Built With
* Unity 2020.2
* Visual Studio 2019
* C# 7

## Features
* Low games size (Under 1MB)
* Shared technologies
* Flexible Theme and Environment
* Fast game development
* Direct publishing
* Easy management with game shelf
	
## How To Use


## API Reference


## Tests


## Contribute


## Credits


## License