namespace BG.ShapeProviders
{
    public class ShapeNames
    {
        public const string SimpleSquareCell = "SimpleSquareCell";
        public const string SimpleBoardGamePiece = "SimpleBoardGamePiece";
        public const string SimpleRookPiece = "SimpleRookPiece";
        public const string SimplePawnPiece = "SimplePawnPiece";
        
    }
}
