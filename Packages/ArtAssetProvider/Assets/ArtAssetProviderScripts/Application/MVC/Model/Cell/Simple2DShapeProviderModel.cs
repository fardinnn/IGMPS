﻿using UnityEngine;

namespace BG.ArtAssets
{
    public class Simple2DShapeProviderModel : CellShapeProviderModel
    {
        public Vector2 Size { get; set; }
        public SpriteRenderer SpriteRenderer { get; set; }
    }
}
