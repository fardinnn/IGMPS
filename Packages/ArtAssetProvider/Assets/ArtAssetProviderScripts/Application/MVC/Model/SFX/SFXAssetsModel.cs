﻿using BG.API;
using UnityEngine;

namespace BG.ArtAssets
{
    public abstract class SFXAssetsModel : IModel
    {
        public Transform Parent { get; set; }
    }
}
