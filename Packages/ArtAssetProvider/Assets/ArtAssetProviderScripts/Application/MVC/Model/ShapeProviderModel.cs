﻿using BG.API;
using System.Collections.Generic;
using UnityEngine;

namespace BG.ArtAssets
{
    public abstract class ShapeProviderModel : IModel
    {
        public Transform Parent { get; set; }
        public Options DefaultOptions { get; set; }
    }
}
