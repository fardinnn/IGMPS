﻿using BG.API;
using BG.API.Shapes;
using BG.ArtAssets.Domain;
using System;

namespace BG.ArtAssets
{
    public class Simple2DShapeProviderController : ShapeProviderController<Simple2DShapeProviderView, Simple2DShapeProviderModel>
    {
        public Simple2DShapeProviderController(Simple2DShapeProviderView view = null,
            Simple2DShapeProviderModel model = null) : base(view, model)
        {

        }

        public override IComponentSystem ProvideComponentSystem(Options options)
        {
            throw new NotImplementedException();
        }

        public IComponentSystem ProvideShapeSystem<T>(IShapePackage Package, bool limitedByType, Options options = null) where T : Shape
        {
            Guid systemId = Guid.NewGuid();

            new SetupSimpleShapeSystemHandler().Handle(new SimpleShapeSystemParameters
            {
                SystemId = systemId,
                TargetShapeType = typeof(T),
                TargetPackage = Package,
                LimitedByType = limitedByType
            });

            var shapeProvider = ShapeSystemManager.Get<SimpleShapeSystem>(systemId);
            options?.AlterOn(shapeProvider);
            return shapeProvider;
        }
    }
}