﻿namespace BG.ArtAssets
{
    public abstract class CellShapeProviderController<TView, TModel> : ShapeProviderController<TView, TModel>
        where TView : CellShapeProviderView
        where TModel : CellShapeProviderModel
    {
        public CellShapeProviderController(TView view = null, TModel model = null)
            : base(view, model) { }
    }
}