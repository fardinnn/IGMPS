﻿using BG.API;
using BG.API.Abilities;
using BG.States;
using System;
using UnityEngine;

namespace BG.ArtAssets
{
    public interface ISFXAssetsController : IComponentController
    {

    }

    public abstract class SFXAssetsController<TView, TModel> : IComponentController<TView, TModel>, ISFXAssetsController, IDisposable
        where TView : SFXAssetsView
        where TModel : SFXAssetsModel
    {
        public TView View { get; }
        public TModel Model { get; }

        public Guid Id { get; } = Guid.NewGuid();

        public SFXAssetsController(TView view = null, TModel model = null)
        {
            Model = model ?? CreateDefaultModel();
            View = view ?? CreateDefaultView();
            View.Controller = this;
            if (Model.Parent == null)
                Model.Parent = View.transform;
        }

        protected virtual TModel CreateDefaultModel()
        {
            return (TModel)Activator.CreateInstance(typeof(TModel));
        }

        protected virtual TView CreateDefaultView()
        {
            var view = (Model.Parent != null ? Model.Parent.gameObject :
                (Model.Parent = new GameObject(typeof(TView).Name.Replace("View", "")).transform).gameObject)
                .AddComponent<TView>();
            return view;
        }

        public virtual void SetActivate(bool state)
        {
            View?.gameObject.SetActive(state);
        }

        public virtual void Dispose()
        {
            UnityEngine.Object.Destroy(View.gameObject);
        }


        protected CompositeStatesSet requiredStates;
        public event EventHandler<RequiredStatesEventArgs> RequiredStatesUpdated;
        public void AddStatesGroup(params Enum[] groupStates)
        {
            if (requiredStates.Contains(groupStates)) return;
            requiredStates.Add(groupStates);
            RequiredStatesUpdated?.Invoke(this, new RequiredStatesEventArgs { StatesSet = requiredStates });
        }

        public abstract IComponentSystem ProvideComponentSystem(Options options = null);
    }
}
