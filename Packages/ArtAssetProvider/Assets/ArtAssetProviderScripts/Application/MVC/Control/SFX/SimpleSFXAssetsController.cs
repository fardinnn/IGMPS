﻿using BG.API;
using BG.ArtAssets.Data;
using BG.ArtAssets.Domain;
using System;

namespace BG.ArtAssets
{
    public class SimpleSFXAssetsController : SFXAssetsController<SimpleSFXAssetsView, SimpleSFXAssetsModel>
    {
        SimpleSFXAssetsSystem sfxSystem;

        public SimpleSFXAssetsController(SimpleSFXAssetsView view = null, SimpleSFXAssetsModel model = null) : base(view, model)
        {

        }

        public override IComponentSystem ProvideComponentSystem(Options options = null)
        {
            if (sfxSystem==null)
            {

            }

            if (sfxSystem == null)
            {
                //if (movesFactory.Has(Model.MoveSystemName))
                //{
                //    var factory = movesFactory.Get(Model.MoveSystemName);
                //    moveSystem = factory.Create<SimpleMoveSystem>();
                //}
                //else
                //{
                Guid systemId = Guid.NewGuid();
                SimpleSFXSystemParameters command = Model.ToCommand();
                command.SystemId = systemId;
                command.UnitOfWork = new UnitOfWork(new AddressableDbCache());
                new SetupSimpleSFXSystemHandler().Handle(command);
                sfxSystem = SFXSystemManager.Get<SimpleSFXAssetsSystem>(systemId);
                //}
            }
            options?.AlterOn(sfxSystem);
            return sfxSystem;
        }
    }
}
