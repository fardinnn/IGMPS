﻿using Unity.Mathematics;
using UnityEngine;

namespace BG.ShapeProviders
{
    public class SimpleTemplatePrefab : ShapeDataTemplatePrefab<SimpleTemplatePrefab>
    {
        [SerializeField]
        private Vector3 size = Vector3.one;
        public override float3 Size { get => size; }

    }
}
