﻿using BG.API.Shapes;

namespace BG.ArtAssets.Domain
{
    public abstract class SimpleGridPartTemplate : ShapeTemplate
    {

    }

    public class SimpleSquareCellTemplate : SimpleGridPartTemplate
    {

    }
}
