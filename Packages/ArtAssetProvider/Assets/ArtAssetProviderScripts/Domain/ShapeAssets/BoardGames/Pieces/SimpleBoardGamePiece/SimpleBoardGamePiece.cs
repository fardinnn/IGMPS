﻿using BG.API.Abilities;
using BG.API.Shapes;
using System;
using UnityEngine;

namespace BG.ArtAssets.Domain
{
    public class SimpleBoardGamePiece : PieceShape, IColorable
    {
        [SerializeField] private MeshRenderer meshRenderer;
        public event EventHandler<ColorChangeEventArgs> Colored;

        public void Coloring(object sender, ColorChangeEventArgs e)
        {
            meshRenderer.material.color = e.Color;

            Colored?.Invoke(this, e);
        }

    }
}
