﻿using BG.API;
using System;

namespace BG.ArtAssets.Domain
{
    public abstract class ArtSystemCommand : ICommand
    {
        public Guid Id { get; set; }
        public Guid SystemId { get; set; }
    }
}
