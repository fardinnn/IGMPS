﻿using BG.API;
using BG.API.Shapes;
using System;

namespace BG.ArtAssets.Domain
{
    public class SimpleShapeSystemParameters : ShapeSystemCommand
    {
        public Type TargetShapeType { get; set; }
        public IShapePackage TargetPackage { get; set; }
        public bool LimitedByType { get; set; }
    }

}
