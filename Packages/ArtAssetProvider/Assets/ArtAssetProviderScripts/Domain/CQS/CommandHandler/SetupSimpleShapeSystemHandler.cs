
namespace BG.ArtAssets.Domain
{
    public class SetupSimpleShapeSystemHandler : SetupShapeSystemHandler<SimpleShapeSystemParameters>
    {
        public override void Handle(SimpleShapeSystemParameters command)
        {
            base.Handle(command);

            var system = new SimpleShapeSystem(command);
            system.Id = command.SystemId;
            Manager.Add(system);
        }
    }
}
