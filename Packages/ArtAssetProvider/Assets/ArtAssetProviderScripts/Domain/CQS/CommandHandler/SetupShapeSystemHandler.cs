﻿using BG.API;
using UnityEngine;

namespace BG.ArtAssets.Domain
{
    public abstract class SetupShapeSystemHandler<TCommand> : ICommandHandler<TCommand>
            where TCommand : ShapeSystemCommand
    {
        protected ShapeSystemManager Manager;
        public virtual void Handle(TCommand command)
        {
            Manager = Object.FindObjectOfType<ShapeSystemManager>();
            if (Manager == null)
            {
                new GameObject("ShapeSystemManager").AddComponent<ShapeSystemManager>();
                Manager = Object.FindObjectOfType<ShapeSystemManager>();
            }
        }
    }
}
