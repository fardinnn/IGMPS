﻿using BG.API;
using UnityEngine;

namespace BG.ArtAssets.Domain
{
    public abstract class SetupSFXSystemHandler<TCommand> : ICommandHandler<TCommand>
            where TCommand : SFXSystemCommand
    {
        protected SFXSystemManager Manager;
        public virtual void Handle(TCommand command)
        {
            Manager = Object.FindObjectOfType<SFXSystemManager>();
            if (Manager == null)
            {
                new GameObject("SFXSystemManager").AddComponent<SFXSystemManager>();
                Manager = Object.FindObjectOfType<SFXSystemManager>();
            }
        }
    }
}
