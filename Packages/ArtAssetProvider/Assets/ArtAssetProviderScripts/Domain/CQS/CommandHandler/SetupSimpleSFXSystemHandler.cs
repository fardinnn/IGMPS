﻿
namespace BG.ArtAssets.Domain
{
    public class SetupSimpleSFXSystemHandler : SetupSFXSystemHandler<SimpleSFXSystemParameters>
    {
        public override void Handle(SimpleSFXSystemParameters command)
        {
            base.Handle(command);

            var system = new SimpleSFXAssetsSystem(command);
            system.Id = command.SystemId;
            Manager.Add(system);
        }
    }

}
