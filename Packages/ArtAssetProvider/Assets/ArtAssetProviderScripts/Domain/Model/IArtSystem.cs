﻿using BG.API;
using System;

namespace BG.ArtAssets.Domain
{
    public interface IArtSystem : IComponentSystem
    {
        Guid Id { get; set; }
    }
}
