﻿using BG.API;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace BG.ArtAssets.Domain
{
    public class ArtSystemManager<T1, T2> : Singleton<T1>
        where T1: ArtSystemManager<T1, T2>
        where T2: IArtSystem
    {
        protected static Dictionary<Guid, T2> ArtsSystems;

        protected override void Awake()
        {
            IsDestroyOnLoad = true;
            base.Awake();

            ArtsSystems = new Dictionary<Guid, T2>();
        }

        public void Add(T2 artSystem)
        {
            if (artSystem.Id == default) throw new ArgumentException("artSystem's id in not assigned");
            ArtsSystems.Add(artSystem.Id, artSystem);
        }

        public static T Get<T>(Guid id) where T : class, T2
        {
            if (ArtsSystems == null) throw new NullReferenceException("artsSystems can not be null!");
            if (!ArtsSystems.ContainsKey(id))
                Debug.LogError("There is no art system with the provided id!");
            var target = ArtsSystems[id] as T;
            if (target == null)
                Debug.LogError("Provided type is not compatible with target system id!");
            return target;
        }
    }
}
