﻿using BG.API.Abilities;

namespace BG.ArtAssets.Domain
{
    public interface IShapeSystem : IArtSystem { }
    public interface IShapeSystem<T> : IShapeSystem 
        where T : IShapeSystem<T>
    {   }
}
