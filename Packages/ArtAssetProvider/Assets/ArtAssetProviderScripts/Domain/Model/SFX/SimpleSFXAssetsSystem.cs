﻿using BG.API;
using System;
using System.Collections.Generic;
using BG.States;
using UnityEngine;

namespace BG.ArtAssets.Domain
{
    public class SimpleSFXAssetsSystem : ISFXAssetsSystem<SimpleSFXAssetsSystem>
    {
        public Guid Id { get; set; }

        public bool IsReady { get; private set; }

        private readonly SimpleSFXSystemParameters parameters;


        private readonly IUnitOfWork unitOfWork;

        public SimpleSFXAssetsSystem(SimpleSFXSystemParameters parameters)
        {
            if (parameters.UnitOfWork == null)
                throw new ArgumentNullException("Unit of work can not be null!");
            this.parameters = parameters;
            unitOfWork = parameters.UnitOfWork;

            IsReady = true;
        }

        public void Visit(List<Entity> entities)
        {
            foreach (var entity in entities)
            {
                entity.AddSingleStateActivateAction<RequestSFXChange, RequestSFXChangeEventArgs>(RequestSFXChange.Set, OnRequestSFXSet);
                entity.AddSingleStateActivateAction<RequestSFXChange, RequestSFXChangeEventArgs>(RequestSFXChange.Unset, OnRequestSFXSet);
                entity.AddSingleStateActivateAction<RequestSFXChange, RequestSFXChangeEventArgs>(RequestSFXChange.Delete, OnRequestSFXDelele);
            }
        }

        public async void OnRequestSFXSet(StateGroup states, RequestSFXChangeEventArgs requestSFXChange)
        {
            try
            {
                requestSFXChange.Entity.SetState(SFXChange.Loading, new SFXChangedEventArgs ());
                AudioClip sound = await unitOfWork.SoundFX.Get(requestSFXChange.AssetId, requestSFXChange.ComponentSystem);
                requestSFXChange.Entity.SetState(SFXChange.Loaded, new SFXChangedEventArgs { Sound = sound });
            }
            catch (Exception)
            {
                Debug.LogError($"Asset id: {requestSFXChange.AssetId}, entity: {requestSFXChange.Entity}, unit of work: {unitOfWork}");
                throw;
            }
        }

        public void OnRequestSFXUnset(StateGroup states, RequestSFXChangeEventArgs requestSFXChange)
        {
            unitOfWork.SoundFX.Release(requestSFXChange.AssetId, requestSFXChange.ComponentSystem);
        }

        public void OnRequestSFXDelele(StateGroup states, RequestSFXChangeEventArgs requestSFXChange)
        {
            unitOfWork.SoundFX.Remove(requestSFXChange.AssetId, requestSFXChange.ComponentSystem);
        }



        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}
