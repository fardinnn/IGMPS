﻿namespace BG.ArtAssets.Domain
{
    public interface ISFXAssetsSystem : IArtSystem { }
    public interface ISFXAssetsSystem<T> : ISFXAssetsSystem
        where T : ISFXAssetsSystem<T>
    {   }
}
