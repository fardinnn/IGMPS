using BG.API;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace BG.ArtAssets.Domain
{
    public interface IRepository<T> where T : class
    {
        Task<T> Get(IAssetId id, IComponentSystem component);
        Task<IEnumerable<T>> GetAll(
            IComponentSystem component,
            Expression<Func<T, bool>> filter = null,
            Func<IQueryable<T>, IOrderedQueryable<T>> orederBy = null
            );
        Task<T> GetFirstOrDefault(IComponentSystem component, Expression<Func<T, bool>> filter = null);
        Task Add(T entity, IComponentSystem component);
        Task AddRange(IEnumerable<T> entities, IComponentSystem component);
        void Release(IAssetId id, IComponentSystem component);
        void Release(T entity, IComponentSystem component);
        void Remove(T entity, IComponentSystem component);
        void Remove(IAssetId id, IComponentSystem component);
        void RemoveRange(IEnumerable<T> entities, IComponentSystem component);
    }
}
