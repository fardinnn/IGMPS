using UnityEngine;

namespace BG.ArtAssets.Domain
{
    public interface IMusicRepository : IRepository<AudioClip>
    {

    }
}
