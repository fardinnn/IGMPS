using System;

namespace BG.ArtAssets.Domain
{
    public interface IUnitOfWork : IDisposable
    {
        IMusicRepository Musics { get; }
        ISoundFXRepository SoundFX { get; }
        void Save();
    }
}
