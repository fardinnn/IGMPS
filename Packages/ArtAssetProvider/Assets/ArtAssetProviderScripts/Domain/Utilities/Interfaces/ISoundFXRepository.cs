using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BG.ArtAssets.Domain
{
    public interface ISoundFXRepository : IRepository<AudioClip>
    {

    }
}
