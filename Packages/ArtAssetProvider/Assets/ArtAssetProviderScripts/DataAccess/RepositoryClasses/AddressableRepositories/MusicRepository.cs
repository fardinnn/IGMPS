﻿using BG.API;
using BG.ArtAssets.Domain;
using UnityEngine;

namespace BG.ArtAssets.Data
{
    public class MusicRepository : Repository<AudioClip>, IMusicRepository
    {
        public MusicRepository(IDbCache dbContext) : base(dbContext)
        {

        }

        public AudioClip Get(IAssetId id)
        {
            throw new System.NotImplementedException();
        }

        public void Remove(IAssetId id)
        {
            throw new System.NotImplementedException();
        }
    }
}
