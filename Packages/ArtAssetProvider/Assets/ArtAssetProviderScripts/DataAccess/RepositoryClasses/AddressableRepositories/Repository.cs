﻿using BG.API;
using BG.ArtAssets.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace BG.ArtAssets.Data
{
    public class Repository<T> : IRepository<T> where T : class
    {
        IDbCache dbCache;
        public Repository(IDbCache dbCache)
        {
            this.dbCache = dbCache;
        }

        public async Task Add(T entity, IComponentSystem component)
        {
            await Task.Delay(50);
            throw new NotImplementedException();
            //dbCache.Add(entity, new StringId(), component);
        }

        public async Task AddRange(IEnumerable<T> entities, IComponentSystem component)
        {
            await Task.Delay(50);
            throw new NotImplementedException();
            //foreach (var entity in entities) Add(entity, component);
        }


        public async Task<T> Get(IAssetId id, IComponentSystem component)
        {
            return await dbCache.Get<T>(id, component);
        }

        public async Task<IEnumerable<T>> GetAll(
            IComponentSystem component,
            Expression<Func<T, bool>> filter = null,
            Func<IQueryable<T>, IOrderedQueryable<T>> orederBy = null)
        {
            await Task.Delay(50);
            throw new NotImplementedException();
            //IQueryable<T> query = dbSet;
            //if (filter !=null) query = query.Where(filter);
            //if (orederBy != null) return orederBy(query).ToList();
            //return query.ToList();
        }


        public async Task<T> GetFirstOrDefault(IComponentSystem component, Expression<Func<T, bool>> filter = null)
        {
            await Task.Delay(50);
            throw new NotImplementedException();
            //IQueryable<T> query = dbSet;
            //if (filter != null) query = query.Where(filter);
            //return query.FirstOrDefault();
        }

        public void Remove(T entity, IComponentSystem component)
        {
            throw new NotImplementedException();
        }

        public void Remove(int id, IComponentSystem component)
        {
            throw new NotImplementedException();
        }

        public void Remove(IAssetId id, IComponentSystem component)
        {
            throw new NotImplementedException();
        }

        public void RemoveRange(IEnumerable<T> entities, IComponentSystem component)
        {
            throw new NotImplementedException();
        }

        public void Release(IAssetId id, IComponentSystem component)
        {
            dbCache.Release(id);
        }

        public void Release(T entity, IComponentSystem component)
        {
            dbCache.Release(entity);
        }
    }
}
