﻿using BG.API;
using BG.ArtAssets.Domain;
using UnityEngine;

namespace BG.ArtAssets.Data
{
    public class SoundFXRepository : Repository<AudioClip>, ISoundFXRepository
    {
        public SoundFXRepository(IDbCache dbCache) : base(dbCache)
        {

        }
    }
}
