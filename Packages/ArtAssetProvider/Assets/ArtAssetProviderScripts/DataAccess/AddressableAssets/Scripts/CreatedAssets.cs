using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace BG.ArtAssets.Data
{
    public class CreatedAssets : MonoBehaviour
    {
        [SerializeField] private string label;
        [SerializeField] private string assetName;

        private LoadedAddressableLocations loadedLocations;

        [field: SerializeField] public List<GameObject> Assets { get; } = new List<GameObject>();

        // Start is called before the first frame update
        async void Start()
        {
            await CreateAndWaitUntilCompleted2();
        }

        private async Task CreateAndWaitUntilCompleted2()
        {
            await CreateAddressablesLoader.InitAsset(label, Assets);
            await CreateAddressablesLoader.InitAsset(assetName, Assets);

            foreach (var asset in Assets)
            {
                Debug.Log(asset.name);
            }

            await Task.Delay(2000);

            ClearAssets(Assets[0]);
        }
        private async Task CreateAndWaitUntilCompleted()
        {
            loadedLocations = GetComponent<LoadedAddressableLocations>();

            await Task.Delay(TimeSpan.FromSeconds(1));

            await CreateAddressablesLoader.ByLoadedAddress(loadedLocations.AssetLocations, Assets);

            foreach (var asset in Assets)
            {
                Debug.Log(asset.name);
            }
        }

        private void ClearAssets(GameObject go)
        {
            Addressables.Release(go);
        }
    }
}
