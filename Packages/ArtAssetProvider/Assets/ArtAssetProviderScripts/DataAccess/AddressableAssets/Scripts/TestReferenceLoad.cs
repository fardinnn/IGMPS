using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace BG.ArtAssets.Data
{
    public class TestReferenceLoad : MonoBehaviour
    {
        [SerializeField] private AssetReference sfxRef;
        [SerializeField] private List<AssetReference> allSfxRefs;


        [SerializeField] private List<GameObject> gameObjects = new List<GameObject>();
        // Start is called before the first frame update
        void Start()
        {
            allSfxRefs.Add(sfxRef);

            StartCoroutine(LoadAndWaitUntilComplete());
        }

        private IEnumerator LoadAndWaitUntilComplete()
        {
            yield return AssetRefLoader.CreateAssetsAddToList(allSfxRefs, gameObjects);
        }
    }
}
