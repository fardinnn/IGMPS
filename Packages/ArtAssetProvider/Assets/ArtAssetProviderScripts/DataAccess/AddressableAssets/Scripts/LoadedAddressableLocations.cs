using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.ResourceManagement.ResourceLocations;

namespace BG.ArtAssets.Data
{
    public class LoadedAddressableLocations : MonoBehaviour
    {
        [SerializeField] private string label;

        [SerializeField] public List<IResourceLocation> AssetLocations = new List<IResourceLocation>();

        // Start is called before the first frame update
        async void Start()
        {
            await InitAndWaitUntilLocLoaded();
        }

        private async Task InitAndWaitUntilLocLoaded()
        {
            await AddressableLocationLoader.GetAll(label, AssetLocations);

            foreach (var location in AssetLocations)
            {
                Debug.Log(location.PrimaryKey);
            }
        }
    }
}
