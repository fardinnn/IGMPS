using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace BG.ArtAssets.Data
{
    public static class AssetRefLoader
    {
        public static async Task CreateAssetAddToList<T>(AssetReference reference, List<T> completedObjs)
            where T : Object
        {
            completedObjs.Add(await reference.InstantiateAsync().Task as T);
        }
        public static async Task CreateAssetsAddToList<T>(List<AssetReference> references, List<T> completedObjs)
            where T : Object
        {
            for (int i = 0; i < references.Count; i++)
            {
                completedObjs.Add(await references[i].InstantiateAsync().Task as T);
            }
        }
    }
}
