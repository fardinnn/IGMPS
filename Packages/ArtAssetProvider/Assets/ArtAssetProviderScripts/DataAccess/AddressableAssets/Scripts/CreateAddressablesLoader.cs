using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.ResourceLocations;

namespace BG.ArtAssets.Data
{
    public static class CreateAddressablesLoader
    {
        public static async Task InitAsset<T>(string assetNameOrLabel, List<T> createdObjs)
            where T : Object
        {
            var locations = await Addressables.LoadResourceLocationsAsync(assetNameOrLabel).Task;
            foreach (var location in locations)
            {
                createdObjs.Add(await Addressables.InstantiateAsync(location).Task as T);
            }
        }

        public static async Task ByLoadedAddress<T>(IList<IResourceLocation> loadedLocations, List<T> createObjs)
            where T: Object
        {
            foreach (var location in loadedLocations)
            {
                var obj = await Addressables.InstantiateAsync(location).Task as T;
                createObjs.Add(obj);
            }
        }
    }
}
