﻿using BG.API;
using System;
using System.Collections.Generic;

namespace BG.ArtAssets.Data
{
    public class AssetCacheContainer : Dictionary<Type, Tuple<Dictionary<IAssetId, Object>, Dictionary<Object, IAssetId>>>
    {
        public bool Contains<TEntity>(IAssetId id)
        {
            if (ContainsKey(typeof(TEntity)))
                return this[typeof(TEntity)].Item1.ContainsKey(id);
            return false;
        }

        public bool Contains<TEntity>(TEntity entity)
        {
            if (ContainsKey(typeof(TEntity)))
                return this[typeof(TEntity)].Item2.ContainsKey(entity);
            return false;
        }

        public void Add<TEntity>(TEntity entity, IAssetId assetId)
        {
            if (!ContainsKey(typeof(TEntity)))
            {
                Add(
                    typeof(TEntity),
                    Tuple.Create(
                        new Dictionary<IAssetId, object>(),
                        new Dictionary<object, IAssetId>()));
            }
            if (!this[typeof(TEntity)].Item1.ContainsKey(assetId))
            {
                this[typeof(TEntity)].Item1.Add(assetId, entity);
                this[typeof(TEntity)].Item2.Add(entity, assetId);
            }
        }

        public TEntity Get<TEntity>(IAssetId id)
        {
            return (TEntity)this[typeof(TEntity)].Item1[id];
        }
    }
}