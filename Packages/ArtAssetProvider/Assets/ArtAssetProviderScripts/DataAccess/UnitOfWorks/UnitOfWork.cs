﻿
using BG.API;
using BG.ArtAssets.Domain;
using System.Collections.Generic;

namespace BG.ArtAssets.Data
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly IDbCache db;

        public UnitOfWork(IDbCache db)
        {
            this.db = db;
            Musics = new MusicRepository(this.db);
            SoundFX = new SoundFXRepository(this.db);
        }

        public IMusicRepository Musics { get; private set; }

        public ISoundFXRepository SoundFX { get; private set; }


        public void Dispose()
        {
            db.Dispose();
        }

        public void Save()
        {
            //db.SaveChanges();
        }
    }


    public abstract class AddressableCalls<T>
    {
        protected AddressableDbCache db;
        public AddressableCalls(AddressableDbCache db)
        {
            this.db = db;
        }

        //public virtual T GetSingleAsset(string assetName)
        //{
        //    db.Add<T>(asset);

        //    if (db.Contains<T>(assetName)) return db.Get<T>(assetName);
        //    else
        //    {

        //    }
        //}

        //public virtual List<T> GetLabeledAssets(string lable)
        //{

        //}
    }
}
