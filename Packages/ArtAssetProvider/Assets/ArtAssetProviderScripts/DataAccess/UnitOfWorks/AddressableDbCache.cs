﻿using BG.API;
using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using UnityEngine.AddressableAssets;

namespace BG.ArtAssets.Data
{

    public class AddressableDbCache : IDbCache
    {

        private AssetCacheContainer cachedObjects = new AssetCacheContainer();

        private Dictionary<IAssetId, HashSet<IComponentSystem>> userComponents = new Dictionary<IAssetId, HashSet<IComponentSystem>>();

        private HashSet<IAssetId> newChanges = new HashSet<IAssetId>();

        public void Add<TEntity>(TEntity entity, IAssetId id, IComponentSystem component)
        {
            if (!cachedObjects.Contains(entity))
            {
                newChanges.Add(id);
                cachedObjects.Add(entity, id);
            }
        }

        public bool Contains<TEntity>(IAssetId id)
        {
            return cachedObjects.Contains<TEntity>(id);
        }

        public bool Contains<TEntity>(TEntity entity)
        {
            return cachedObjects.Contains(entity);
        }

        public async Task<TEntity> Get<TEntity>(IAssetId id, IComponentSystem component)
        {
            if (cachedObjects.Contains<TEntity>(id))
            {
                if (!userComponents[id].Contains(component)) userComponents[id].Add(component);
                return cachedObjects.Get<TEntity>(id);
            }

            if (id is StringId stringId)
            {
                TEntity asset = await Addressables.LoadAssetAsync<TEntity>(stringId.Id).Task;
                if (asset == null)
                    throw new MissingFieldException("The provided asset name or label is not correct");

                cachedObjects.Add(asset, stringId);
                if (!userComponents.ContainsKey(id)) userComponents.Add(id, new HashSet<IComponentSystem>());
                userComponents[id].Add(component);

                return asset;
            }
            throw new MissingFieldException("The provided asset id is should be of type StringId or using another type of IDbCache class!");
        }

        public void Release<TEntity>(TEntity entity)
        {
            if (!cachedObjects.ContainsKey(typeof(TEntity)) ||
                !cachedObjects[typeof(TEntity)].Item2.ContainsKey(entity)) return;

            IAssetId id = cachedObjects[typeof(TEntity)].Item2[entity];
            if (userComponents.ContainsKey(id))
            {
                userComponents[id].Clear();
                userComponents.Remove(id);
            }

            cachedObjects[typeof(TEntity)].Item1.Remove(id);
            cachedObjects[typeof(TEntity)].Item2.Remove(entity);

            Addressables.Release(entity);
        }

        public void Release<TEntity>(TEntity entity, IComponentSystem component)
        {
            if (!cachedObjects.ContainsKey(typeof(TEntity)) ||
                !cachedObjects[typeof(TEntity)].Item2.ContainsKey(entity)) return;

            IAssetId id = cachedObjects[typeof(TEntity)].Item2[entity];
            if (userComponents.ContainsKey(id) )
            {
                if(userComponents[id].Contains(component))
                    userComponents[id].Remove(component);

                if (userComponents[id].Count == 0)
                {
                    userComponents.Remove(id);
                    cachedObjects[typeof(TEntity)].Item1.Remove(id);
                    cachedObjects[typeof(TEntity)].Item2.Remove(entity);
                    Addressables.Release(entity);
                }
            }
        }

        public void Dispose()
        {
            newChanges.Clear();

            foreach (var tuple in cachedObjects.Values)
            {
                foreach (var entity in tuple.Item2.Keys)
                    Addressables.Release(entity);

                tuple.Item1.Clear();
                tuple.Item2.Clear();

            }
            foreach (var hashSet in userComponents.Values)
                hashSet.Clear();

            cachedObjects.Clear();
            userComponents.Clear();
        }
    }
}