﻿using BG.API;
using System;
using System.Collections.Generic;

namespace BG.PlayerSystem.Domain
{
    public interface IPlayerSystem
    {
        public Guid Id { get; set; }
        void AddPlayer(string group, Dictionary<Type, IComponentSystem> visitors);
    }
}