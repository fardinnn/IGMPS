﻿using BG.API;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace BG.PlayerSystem.Domain
{

    public class PlayerSystemManager : Singleton<PlayerSystemManager>
    {
        private Dictionary<Guid, IPlayerSystem> playerSystems;


        protected override void Awake()
        {
            IsDestroyOnLoad = true;
            base.Awake();
        }

        public void AddSystem(IPlayerSystem playerSystem)
        {
            if (playerSystems == null) playerSystems = new Dictionary<Guid, IPlayerSystem>();
            if (playerSystem.Id == default) throw new ArgumentException("playerSystem's id in not assigned");
            playerSystems.Add(playerSystem.Id, playerSystem);
        }

        public T Get<T>(Guid id) where T : class, IPlayerSystem
        {
            if (playerSystems == null) throw new NullReferenceException("playerSystems can not be null!");
            if (!playerSystems.ContainsKey(id))
                Debug.LogError("There is no player system with the provided id!");
            var target = playerSystems[id] as T;
            if (target == null)
                Debug.LogError("Provided type is not compatible with target system id!");
            return target;
        }

        public void Clear()
        {
            playerSystems = null;
            GC.Collect();
        }
    }
}