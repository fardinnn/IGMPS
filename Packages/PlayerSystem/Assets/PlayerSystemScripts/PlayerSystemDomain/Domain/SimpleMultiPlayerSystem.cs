﻿using BG.API;
using BG.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace BG.PlayerSystem.Domain
{
    public class SimpleMultiPlayerSystem : IPlayerSystem
    {
        //private static Dictionary<string, List<Entity>> playerEntities = new Dictionary<string, List<Entity>>();
        private SimpleMultiPlayerParameters parameters;

        public SimpleMultiPlayerSystem(SimpleMultiPlayerParameters parameters)
        {
            Id = parameters.SystemId;
            this.parameters = parameters;
        }

        public Guid Id { get; set; }

        public void AddPlayer(string group, Dictionary<Type, IComponentSystem> visitors)
        {
            //if (!playerEntities.ContainsKey(group))
            //    playerEntities.Add(group, new List<Entity>());
            //var newPlayer = new GameObject().AddComponent<Player>();
            //newPlayer.transform.SetParent(parameters.Parent);
            //parameters.PlayerEntityVisitors?
            //    .Select(p => visitors.ContainsKey(p.Key) ? visitors[p.Key] : p.Value)
            //    .ForEach(e => e.Visit(newPlayer));

            //playerEntities[group].Add(newPlayer);
        }
    }
}