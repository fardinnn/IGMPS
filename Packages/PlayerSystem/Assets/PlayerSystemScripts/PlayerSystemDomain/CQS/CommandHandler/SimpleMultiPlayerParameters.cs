﻿using BG.API;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace BG.PlayerSystem.Domain
{
    public class SimpleMultiPlayerParameters : ICommand
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public int NumberOfOtherPlayers { get; set; }
        public int NumberOfPCPlayers { get; set; }
        public Transform Parent { get; set; }
        public Dictionary<Type, IComponentSystem> PlayerEntityVisitors { get; set; }
        public Guid SystemId { get; set; }
    }
}