using BG.API;
using UnityEngine;

namespace BG.PlayerSystem.Domain
{
    public class SimpleMultiPlayerCommandHandler : ICommandHandler<SimpleMultiPlayerParameters>
    {
        public void Handle(SimpleMultiPlayerParameters command)
        {
            var manager = MonoBehaviour.FindObjectOfType<PlayerSystemManager>();
            if (manager == null)
                manager = new GameObject("PlayerSystemManager").AddComponent<PlayerSystemManager>();
            var system = new SimpleMultiPlayerSystem(command);
            manager.AddSystem(system);
        }
    }
}