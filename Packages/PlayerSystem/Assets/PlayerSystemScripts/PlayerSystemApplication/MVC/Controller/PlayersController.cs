using BG.API;
using System;
using UnityEngine;

namespace BG.PlayerSystem
{
    public abstract class PlayersController<TView, TModel> : IController<TView, TModel>, IDisposable
           where TView : PlayersView
           where TModel : PlayersModel
    {
        public TView View { get; }
        public TModel Model { get; }

        public Guid Id { get; } = Guid.NewGuid();

        public virtual void SetActivate(bool state)
        {
            View?.gameObject.SetActive(state);
        }

        public PlayersController(TView view = null, TModel model = null)
        {
            Model = model ?? CreateDefaultModel();
            View = view ?? CreateDefaultView();
            View.Controller = this;
        }

        protected virtual TModel CreateDefaultModel()
        {
            return (TModel)Activator.CreateInstance(typeof(TModel));
        }

        protected virtual TView CreateDefaultView()
        {
            var view = new GameObject(typeof(TView).Name.Replace("View", "")).AddComponent<TView>();
            if (Model.Parent != null) view.transform.SetParent(Model.Parent);
            return view;
        }

        public virtual void Dispose()
        {
            UnityEngine.Object.Destroy(View.gameObject);
        }
    }
}
