using BG.API;
using BG.PlayerSystem.Domain;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace BG.PlayerSystem
{

    public class SimpleMultiPlayerController : PlayersController<SimpleMultiPlayerView, SimpleMultiPlayerModel>
    {
        private IPlayerSystem system;

        public GameObject GetCurrentPlayerView()
        {
            return null;
        }

        public SimpleMultiPlayerController(SimpleMultiPlayerView view = null, SimpleMultiPlayerModel model = null) :
            base(view, model)
        {
            if (model.Parent == null) model.Parent = View.transform;
            var command = model.ToCommand();
            new SimpleMultiPlayerCommandHandler().Handle(command);
            system = MonoBehaviour.FindObjectOfType<PlayerSystemManager>()
                .Get<SimpleMultiPlayerSystem>(model.SystemId);
        }


        public void AddPlayer(string group, List<IComponentSystem> visitors)
        {
            system.AddPlayer(group, visitors.ToDictionary(v => v.GetType(), v => v));
        }

        public void Activate(string playerName)
        {

        }
    }
}
