﻿using BG.API;
using UnityEngine;

namespace BG.PlayerSystem
{
    public abstract class PlayersModel : IModel
    {
        public virtual Transform Parent { get; set; }
        public virtual Transform TargetField { get; set; }
    }
}
