﻿using BG.API;
using BG.PlayerSystem.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BG.PlayerSystem
{
    public class SimpleMultiPlayerModel : PlayersModel
    {
        public Guid SystemId { get; set; } = Guid.NewGuid();
        public int NumberOfOtherPlayers { get; internal set; }
        public int NumberOfPCPlayers { get; internal set; }
        public List<IComponentSystem> PlayerEntityVisitors { get; set; }

        public SimpleMultiPlayerParameters ToCommand()
        {
            return new SimpleMultiPlayerParameters
            {
                SystemId = SystemId,
                Parent = Parent,
                PlayerEntityVisitors = PlayerEntityVisitors.ToDictionary(v => v.GetType(), v => v),
                NumberOfOtherPlayers = NumberOfOtherPlayers,
                NumberOfPCPlayers = NumberOfPCPlayers,
                Id = Guid.NewGuid()
            };
        }
    }
}
