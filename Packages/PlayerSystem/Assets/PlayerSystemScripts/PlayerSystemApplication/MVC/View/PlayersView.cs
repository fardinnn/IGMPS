﻿using BG.API;
using System;
using UnityEngine;

namespace BG.PlayerSystem
{
    public abstract class PlayersView : MonoBehaviour, IView
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public IController<IView, IModel> Controller { get; set; }
        public void Dispose()
        {
            Destroy(gameObject);
        }

    }
}
