﻿using BG.API;

namespace BG.Themes.Domain
{
    public interface IThemeSystem : IComponentSystem
    {
    }
    public interface IThemeSystem<T> : IThemeSystem
        where T : IThemeSystem<T>
    { }

}
