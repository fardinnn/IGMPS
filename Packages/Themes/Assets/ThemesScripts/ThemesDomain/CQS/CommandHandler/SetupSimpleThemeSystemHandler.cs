﻿namespace BG.Themes.Domain
{
    public class SetupSimpleThemeSystemHandler : SetupThemeSystemHandler<SimpleThemeSystemParameters>
    {
        public override void Handle(SimpleThemeSystemParameters command)
        {
            base.Handle(command);

            var system = new SimpleThemeSystem(command);
            system.Id = command.SystemId;
            Manager.Add(system);
        }
    }
}
