﻿using BG.API;
using BG.States;
using System.Collections.Generic;
using System;
using UnityEngine;
using System.Collections;
using System.Linq;
using BG.Extensions;

namespace BG.Themes.Domain
{
    public class SimpleThemeSystem : IThemeSystem<SimpleThemeSystem>
    {
        private Options options;

        public Guid Id { get; set; }

        public bool IsReady { get; private set; }

        SimpleThemeSystemParameters parameters;
        public SimpleThemeSystem(SimpleThemeSystemParameters parameters)
        {
            if (parameters == null) throw new NullReferenceException();
            this.parameters = parameters;
            options = new Options();
            options.AlterOn(this);
            IsReady = true;
        }

        HashSet<Entity> themedEntities = new HashSet<Entity>();

        Task coroutine;
        public void Visit(List<Entity> entities)
        {
            entities.ForEach(entity =>
            {
                entity.AddSingleStateActivateAction<SFXChange, SFXChangedEventArgs>(SFXChange.Loaded, OnSFXSetted);
                if (!themedEntities.Contains(entity)) themedEntities.Add(entity);
            });
            coroutine = new Task(SetSFX());
        }

        IEnumerator SetSFX()
        {
            yield return new WaitForSeconds(5);
            themedEntities.FirstOrDefault()?.SetState(RequestSFXChange.Set, new RequestSFXChangeEventArgs
            {
                AssetId = new StringId { Id = "Click back sound 10" },
                ComponentSystem = this
            });
        }

        private void OnSFXSetted(StateGroup states, SFXChangedEventArgs eventArgs)
        {
            var audioSource = eventArgs.Entity.GetComponent<AudioSource>();
            if (audioSource == null)
                audioSource = eventArgs.Entity.gameObject.AddComponent<AudioSource>();

            audioSource.clip = eventArgs.Sound;
            audioSource.loop = true;
            audioSource.Play();
        }

        private void AlteredOption<T>(T option) where T : Option
        {
            options[typeof(T)] = option;
        }

        
        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}
