using BG.API;
using UnityEngine;

namespace BG.Themes.Domain
{
    public abstract class SetupThemeSystemHandler<TCommand> : ICommandHandler<TCommand>
            where TCommand : ThemeSystemParameters
    {
        protected ThemeSystemManager Manager;
        public virtual void Handle(TCommand command)
        {
            Manager = UnityEngine.Object.FindObjectOfType<ThemeSystemManager>();
            if (Manager == null)
            {
                new GameObject("ThemeSystemManager").AddComponent<ThemeSystemManager>();
                Manager = UnityEngine.Object.FindObjectOfType<ThemeSystemManager>();
            }
        }
    }
}
