using BG.API;
using System;

namespace BG.Themes.Domain
{
    public abstract class ThemeSystemParameters : ICommand
    {
        public Guid Id { get; set; }
        public Guid SystemId { get; set; }
    }
}
