﻿using BG.API;
using BG.Themes.Domain;
using System;

namespace BG.Themes
{
    public class DefaultThemeController : ThemeController<DefaultThemeView, DefaultThemeModel>
    {
        private SimpleThemeSystem themeSystem;
        public DefaultThemeController(DefaultThemeView view = null, DefaultThemeModel model = null) : base(view, model)
        {
        }

        public override IComponentSystem ProvideComponentSystem(Options options = null)
        {
            if (themeSystem == null)
            {
                //if (movesFactory.Has(Model.ThemeSystemName))
                //{
                //    var factory = movesFactory.Get(Model.ThemeSystemName);
                //    moveSystem = factory.Create<SimpleThemeSystem>();
                //}
                //else
                //{
                Guid systemId = Guid.NewGuid();
                var command = Model.ToCommand();
                command.SystemId = systemId;
                new SetupSimpleThemeSystemHandler().Handle(command);
                themeSystem = ThemeSystemManager.Get<SimpleThemeSystem>(systemId);
                //}
            }
            options?.AlterOn(themeSystem);
            return themeSystem;
        }

    }

}
