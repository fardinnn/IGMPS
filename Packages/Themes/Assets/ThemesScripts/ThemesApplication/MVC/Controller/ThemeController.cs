using BG.API;
using BG.States;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BG.Themes
{


    public interface IThemeController : IComponentController
    {
        public Guid SystemId { get; }
    }

    public abstract class ThemeController<TView, TModel> : IComponentController<TView, TModel>, IThemeController, IDisposable
        where TView : ThemeView
        where TModel : ThemeModel
    {
        public TView View { get; }
        public TModel Model { get; }

        public Guid Id { get; } = Guid.NewGuid();

        public Guid SystemId { get; private set; }

        public ThemeController(TView view = null, TModel model = null, CompositeStatesSet requiredStates = null)
        {
            Model = model ?? CreateDefaultModel();
            View = view ?? CreateDefaultView();
            this.requiredStates = requiredStates ?? new CompositeStatesSet();
            View.Controller = this;
        }

        protected CompositeStatesSet requiredStates;
        public void AddStatesGroup(params Enum[] groupStates)
        {
            if (requiredStates.Contains(groupStates)) return;
            requiredStates.Add(groupStates);
        }

        protected virtual TModel CreateDefaultModel()
        {
            return (TModel)Activator.CreateInstance(typeof(TModel));
        }

        protected virtual TView CreateDefaultView()
        {
            var view = (Model.Parent ? Model.Parent.gameObject :
                (Model.Parent = new GameObject(typeof(TView).Name.Replace("View", "")).transform).gameObject)
                .AddComponent<TView>();
            return view;
        }

        public virtual void Dispose()
        {
            UnityEngine.Object.Destroy(View.gameObject);
        }

        public abstract IComponentSystem ProvideComponentSystem(Options options);
    }

}
