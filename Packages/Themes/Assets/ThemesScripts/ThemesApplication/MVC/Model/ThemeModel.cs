﻿using BG.API;
using UnityEngine;

namespace BG.Themes
{
    public interface IThemeModel : IModel { }
    public abstract class ThemeModel : IThemeModel
    {
        public Transform Parent { get; set; }
    }
}
