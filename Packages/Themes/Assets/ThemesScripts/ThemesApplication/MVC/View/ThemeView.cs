﻿using BG.API;
using System;
using UnityEngine;

namespace BG.Themes
{
    public interface IThemeView : IView { }
    public abstract class ThemeView : MonoBehaviour, IThemeView
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public IController<IView, IModel> Controller { get; internal set; }
        public void Dispose()
        {
            Destroy(gameObject);
        }
    }
}
