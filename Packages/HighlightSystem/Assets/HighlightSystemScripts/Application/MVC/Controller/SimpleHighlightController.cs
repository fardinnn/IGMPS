﻿using BG.API;
using BG.HighlightSystem.Domain;
using BG.States;
using System;

namespace BG.HighlightSystem
{

    public class SimpleHighlightController :
        HighlightController<SimpleHighlightView, SimpleHighlightModel>
    {
        private SimpleHighlightSystem highlightSystem;


        public SimpleHighlightController(SimpleHighlightView view = null,
            SimpleHighlightModel model = null, CompositeStatesSet requiredStates = null) : base(view, model, requiredStates)
        {
        }



        public override IComponentSystem ProvideComponentSystem(Options options = null)
        {
            if (highlightSystem == null)
            {
                Guid systemId = Guid.NewGuid();

                new SetUpSimpleHighlightSystemHandler().Handle(new SimpleHighlightSystemCommand
                {
                    RequiredStates = requiredStates,
                    SystemId = systemId,
                    StateColors = Model.StateColors
                });
                highlightSystem = HighlightSystemManager.Get<SimpleHighlightSystem>(systemId);
            }
            options?.AlterOn(highlightSystem);
            return highlightSystem;
        }

        public void BeginHighlight(StateGroup sateGroup, Entity targetObject)
        {
            if (highlightSystem == null) return;
            highlightSystem.Highlight(sateGroup, targetObject);
        }

        public void EndHighlight(StateGroup sateGroup, Entity targetObject)
        {
            if (highlightSystem == null) return;
            highlightSystem.DeHighlight(sateGroup, targetObject);
        }

    }
}
