using BG.API;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BG.HighlightSystem
{
    public abstract class HighlightModel : IModel
    {
        public Transform Parent { get; set; }
        public Options DefaultOptions { get; set; }
    }
}
