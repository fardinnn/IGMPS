﻿using BG.API;
using BG.States;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace BG.HighlightSystem
{
    public class SimpleHighlightModel : HighlightModel
    {
        public Dictionary<StateGroup, Color> StateColors { get; set; }
        public HashSet<Enum> RequiredStates { get; set; }
    }
}
