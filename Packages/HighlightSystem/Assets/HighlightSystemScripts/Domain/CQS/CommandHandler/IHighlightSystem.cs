﻿using BG.API;
using System;

namespace BG.HighlightSystem.Domain
{
    public interface IHighlightSystem : IComponentSystem
    {
        Guid Id { get; set; }
    }

    public interface IHighlightSystem<T> : IHighlightSystem where T : IHighlightSystem<T>
    {

    }
}
