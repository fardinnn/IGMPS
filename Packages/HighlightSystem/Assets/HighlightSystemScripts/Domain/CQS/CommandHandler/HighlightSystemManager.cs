﻿using BG.API;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace BG.HighlightSystem.Domain
{
    public class HighlightSystemManager : Singleton<HighlightSystemManager>
    {
        private static Dictionary<Guid, IHighlightSystem> highlightSystems;

        protected override void Awake()
        {
            IsDestroyOnLoad = true;
            base.Awake();

            highlightSystems = new Dictionary<Guid, IHighlightSystem>();
        }

        public void Add(IHighlightSystem highlightSystem)
        {
            if (highlightSystem.Id == default) throw new ArgumentException("highlightSystem's id in not assigned");
            highlightSystems.Add(highlightSystem.Id, highlightSystem);
        }

        public static T Get<T>(Guid id) where T : class, IHighlightSystem
        {
            if (highlightSystems == null) throw new NullReferenceException("highlightSystems can not be null!");
            if (!highlightSystems.ContainsKey(id))
                Debug.LogError("There is no highlight system with the provided id!");
            var target = highlightSystems[id] as T;
            if (target == null)
                Debug.LogError("Provided type is not compatible with target system id!");
            return target;
        }
    }
}
