﻿namespace BG.HighlightSystem.Domain
{
    public class SetUpSimpleHighlightSystemHandler : SetUpHighlightSystemHandler<SimpleHighlightSystemCommand>
    {
        public override void Handle(SimpleHighlightSystemCommand command)
        {
            base.Handle(command);

            var system = new SimpleHighlightSystem(command);
            system.Id = command.SystemId;
            Manager.Add(system);
        }
    }
}
