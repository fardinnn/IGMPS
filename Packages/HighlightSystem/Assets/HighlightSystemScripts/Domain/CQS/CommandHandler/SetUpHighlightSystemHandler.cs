using BG.API;
using UnityEngine;

namespace BG.HighlightSystem.Domain
{
    public abstract class SetUpHighlightSystemHandler<TCommand> : ICommandHandler<TCommand>
            where TCommand : HighlightSystemCommand
    {
        protected HighlightSystemManager Manager;
        public virtual void Handle(TCommand command)
        {
            Manager = UnityEngine.Object.FindObjectOfType<HighlightSystemManager>();
            if (Manager == null)
            {
                new GameObject("HighlightSystemManager").AddComponent<HighlightSystemManager>();
                Manager = UnityEngine.Object.FindObjectOfType<HighlightSystemManager>();
            }
        }
    }
}
