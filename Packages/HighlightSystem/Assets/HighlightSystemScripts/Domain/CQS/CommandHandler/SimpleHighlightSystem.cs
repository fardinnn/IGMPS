﻿using System;
using BG.API;
using UnityEngine;
using BG.Extensions;
using System.Collections;
using System.Collections.Generic;
using BG.States;

namespace BG.HighlightSystem.Domain
{
    public class SimpleHighlightSystem : IHighlightSystem<SimpleHighlightSystem>//, IColorable
    {

        private Dictionary<StateGroup, Material> stateMaterials;
        private Dictionary<StateGroup, int> stateOrders;
        private Dictionary<Entity, StateGroup> entitiesActiveState
            = new Dictionary<Entity, StateGroup>();
        private Dictionary<Entity, SimpleHighlightableObject> highlightableObjects
            = new Dictionary<Entity, SimpleHighlightableObject>();
        private List<StateGroup> statesInOrder;

        private Options options;

        public Guid Id { get; set; }
        public bool IsReady { get; private set; }

        Task loadMaterialCoroutine;

        private CompositeStatesSet RequiredStates;

        public SimpleHighlightSystem(SimpleHighlightSystemCommand command)
        {
            options = new Options();

            stateOrders = new Dictionary<StateGroup, int>(command.StateColors.Count);
            statesInOrder = new List<StateGroup>(command.StateColors.Count);
            stateMaterials = new Dictionary<StateGroup, Material>(command.StateColors.Count);

            RequiredStates = command.RequiredStates;
            loadMaterialCoroutine = new Task(LoadMaterial(command));
        }
        IEnumerator LoadMaterial(SimpleHighlightSystemCommand command)
        {
            var materialLoader = new HighlightShaderPackage();
            yield return materialLoader.LoadMaterial("SimpleOutlineOnTop");
            TemplateMaterial = materialLoader.HighlightMaterial;
            int count = 1;
            foreach (var stateColor in command.StateColors)
            {
                stateMaterials.Add(stateColor.Key, new Material(TemplateMaterial));
                stateMaterials[stateColor.Key].SetColor("_OutlineColor", stateColor.Value);
                stateOrders.Add(stateColor.Key, count++);
                statesInOrder.Add(stateColor.Key);
            }
            IsReady = true;
        }

        public void Visit(List<Entity> entities)
        {
            entities.ForEach(e =>
            {
                var highlightable = e.GetComponent<SimpleHighlightableObject>();
                if (highlightable == null)
                    highlightable = e.gameObject.AddComponent<SimpleHighlightableObject>();
                highlightable.Entity = e;
                if (!highlightableObjects.ContainsKey(e)) highlightableObjects.Add(e, highlightable);

                if(stateMaterials == null)
                {
                    Debug.LogError("RequiredStates is null.");
                    return;
                }
                foreach (var stateGroup in stateMaterials)
                {
                    AddStateGroupEvents(e, stateGroup.Key);
                }
            });
        }

        public void AddStateGroupEvents(Entity entity, StateGroup stateGroup)
        {
            entity.AddStateActivateAction(stateGroup, OnInsideCommandHighlight);
            entity.AddStateDeactivateAction(stateGroup, OnInsideCommandDeHighlight);
        }

        private void OnInsideCommandHighlight(StateGroup stateGroup, IStateChangedEventArgs e)
        {
            Highlight(stateGroup, e.Entity);
        }
        private void OnInsideCommandDeHighlight(StateGroup stateGroup, IStateChangedEventArgs e)
        {
            DeHighlight(stateGroup, e.Entity);
        }


        private Material TemplateMaterial;
        public void Highlight(StateGroup states, Entity entity) 
        {
            if (!highlightableObjects.ContainsKey(entity))
            {
                return;
            }
            try
            {
                if (!entitiesActiveState.ContainsKey(entity))
                {
                    entitiesActiveState.Add(entity, states);
                    highlightableObjects[entity].Highlight(stateMaterials[states]);
                }
                else
                {
                    if (stateOrders[states] < stateOrders[entitiesActiveState[entity]])
                    {
                        highlightableObjects[entity].DeHighlight(stateMaterials[entitiesActiveState[entity]]);
                        entitiesActiveState[entity] = states;
                        highlightableObjects[entity].Highlight(stateMaterials[states]);
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.LogError($"highlightableObjects: {highlightableObjects.ContainsKey(entity)}, " +
                    $"stateMaterials: {stateMaterials.ContainsKey(states)}, stateOrders: {stateOrders.ContainsKey(states)}" +
                    $" entity:{entity.name}, states:{states} \n\n{ex.Message}");
                throw;
            }
        }

        public void DeHighlight(StateGroup states, Entity entity)
        {
            if (!highlightableObjects.ContainsKey(entity))
            {
                return;
            }
            try
            {
                if (entitiesActiveState.Count == 0 || !entitiesActiveState.ContainsKey(entity))//new StateGroup(SelectState.Selected).IsSubsetOf(states))
                {
                    Debug.LogError($"there is no {states.GetType()} active for {entity.name}");
                    return;
                }

                if (stateOrders[entitiesActiveState[entity]] < stateOrders[states])
                    return;

                highlightableObjects[entity].DeHighlight(stateMaterials[states]);
                entitiesActiveState.Remove(entity);
                if (stateOrders[states] >= statesInOrder.Count)
                    return;

                for (int i = stateOrders[states]; i < statesInOrder.Count; i++)
                {
                    if (entity.IsStateActive(statesInOrder[i]))
                    {
                        entitiesActiveState.Add(entity, statesInOrder[i]);
                        highlightableObjects[entity].Highlight(stateMaterials[statesInOrder[i]]);
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.LogError($"highlightableObjects: {highlightableObjects.ContainsKey(entity)}, " +
                    $"stateMaterials: {stateMaterials.ContainsKey(states)}, stateOrders: {stateOrders.ContainsKey(states)}" +
                    $" entity:{entity.name}, states:{states} \n\n{ex.Message}");
                Debug.LogError($"Un-caught error: {ex.Message}  \n\n entity:{entity.name}, states:{states}");
                throw;
            }
        }



        Task addCoroutine;
        public void AddStateColor(StateGroup states, Color color, int order = -1)
        {
            if (stateMaterials.ContainsKey(states))
            {
                stateMaterials[states].SetColor("_OutlineColor", color);
                return;
            }

            if (loadMaterialCoroutine.Running)
            {
                if (addCoroutine!=null && addCoroutine.Running) return;
                addCoroutine = new Task(AddStateColorEnumerator(states, color, order));
                return;
            }
            DirectAddStateColor(states, color, order);
        }

        IEnumerator AddStateColorEnumerator(StateGroup states, Color color, int order)
        {
            yield return loadMaterialCoroutine;


            DirectAddStateColor(states, color, order);
        }

        private void DirectAddStateColor(StateGroup states, Color color, int order)
        {
            if (stateMaterials.ContainsKey(states))
            {
                stateMaterials[states] = new Material(TemplateMaterial);
                stateMaterials[states].SetColor("_OutlineColor", color);
                if(order != -1)
                {
                    statesInOrder.RemoveAt(stateOrders[states] - 1);
                    statesInOrder.Insert(order - 1, states);
                    stateOrders[states] = order;
                }
            }
            else
            {
                stateMaterials.Add(states, new Material(TemplateMaterial));
                stateMaterials[states].SetColor("_OutlineColor", color);
                if (order == -1)
                {
                    statesInOrder.Add(states);
                    stateOrders.Add(states, statesInOrder.Count);
                }
                else
                {
                    statesInOrder.Insert(order - 1, states);
                    stateOrders.Add(states, order);
                }

                foreach (var item in highlightableObjects)
                {
                    AddStateGroupEvents(item.Key, states);
                    if (item.Key.IsStateActive(states))
                        Highlight(states, item.Key);
                }
            }
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}
