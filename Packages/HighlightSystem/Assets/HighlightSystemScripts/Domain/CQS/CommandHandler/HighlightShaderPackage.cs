﻿using System.Collections;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace BG.HighlightSystem.Domain
{
    public class HighlightShaderPackage
    {
        public Material HighlightMaterial { get; set; }

        public IEnumerator LoadMaterial(string materialName)
        {
            var locationsHandler = Addressables.LoadResourceLocationsAsync(materialName);
            yield return locationsHandler;

            var locations = locationsHandler.Result;

            if (locations.Count > 0)
            {
                var materialHandler = Addressables.LoadAssetAsync<Material>(locations[0]);
                yield return materialHandler;
                if (materialHandler.Status == UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationStatus.Succeeded)
                {
                    HighlightMaterial = materialHandler.Result;
                }
            }
        }
    }
}
