﻿using BG.API;
using System;
using System.Linq;
using UnityEngine;

namespace BG.HighlightSystem.Domain
{
    public class SimpleHighlightableObject : MonoBehaviour
    {
        private MeshRenderer[] meshRenderers;

        public Entity Entity { get; internal set; }

        internal void Highlight(Material material)
        {
            meshRenderers = GetComponentsInChildren<MeshRenderer>();
            for (int i = 0; i < meshRenderers.Length; i++)
            {
                var materials = meshRenderers[i].sharedMaterials.ToList();
                materials.Add(material);
                meshRenderers[i].sharedMaterials = materials.ToArray();
            }
        }

        internal void DeHighlight(Material material)
        {
            meshRenderers = GetComponentsInChildren<MeshRenderer>();
            for (int i = 0; i < meshRenderers.Length; i++)      //????? 
            {
                var materials = meshRenderers[i].sharedMaterials.ToList();
                for (int j = materials.Count-1; j >= 0 ; j--)
                {
                    if (materials[j].name == material.name)
                    {
                        materials.RemoveAt(j);
                        break;
                    }
                }
                meshRenderers[i].sharedMaterials = materials.ToArray();
            }
        }
    }
}
