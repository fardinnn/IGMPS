using BG.API;
using System;

namespace BG.HighlightSystem.Domain
{
    public abstract class HighlightSystemCommand : ICommand
    {
        public Guid Id { get; set; }
    }
}
