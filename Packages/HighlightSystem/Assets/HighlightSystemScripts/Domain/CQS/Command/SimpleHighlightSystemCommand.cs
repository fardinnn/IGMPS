﻿using BG.API;
using BG.States;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace BG.HighlightSystem.Domain
{
    public class SimpleHighlightSystemCommand : HighlightSystemCommand
    {
        public Guid SystemId { get; set; }
        public Dictionary<StateGroup, Color> StateColors { get; set; }
        public CompositeStatesSet RequiredStates { get; set; }
    }
}
