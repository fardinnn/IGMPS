// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Unlit/OutlineOnTop"
{
    Properties
    {
		_OutlineColor ("Outline Color", Color) = (0,0,0,1)
		_Outline ("Outline width", Range (0.0, 10.0)) = .005
    }

    CGINCLUDE
    #include "UnityCG.cginc"
 
    struct appdata {
	    float4 vertex : POSITION;
	    float3 normal : NORMAL;
    };
 
    struct v2f {
	    float4 vertex : POSITION;
	    float4 color : COLOR;
    };
 
    uniform float _Outline;
    uniform float4 _OutlineColor;

    ENDCG

    SubShader {
		//Tags { "Queue" = "Transparent" }
		
		
		Pass {
			Name "BASE"
			Tags{
				"RenderType"= "Transparent"
				"RenderPipeline" = "UniversalPipeline"
			}
			Cull Back
			//Blend  Zero One
			Blend SrcAlpha OneMinusSrcAlpha
			ZTest Always
 
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
			
            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                //float3 norm   = mul ((float3x3)UNITY_MATRIX_IT_MV, v.normal);
	            //float2 offset = TransformViewToProjection(norm.xy);
                
	            //o.vertex.xy += offset * o.vertex.z * _Outline;
	            o.color = _OutlineColor;
                return o;
            }

            half4 frag(v2f i) :COLOR {
				i.color.rgb *= i.color.a;
	            return i.color;
            }
            ENDCG
		}
		Pass {
			Name "OUTLINE"
			Tags { 
                "RenderType" = "Transparent"
                "RenderPipeline" = "UniversalPipeline"
                "LightMode" = "UniversalForward"
			}
			Cull Front
			BlendOp Max
			Blend Zero One
			//Blend  SrcAlpha OneMinusSrcAlpha
			//ZTest Always
 
			// you can choose what kind of blending mode you want for the outline
			//Blend SrcAlpha OneMinusSrcAlpha // Normal
			//Blend One One // Additive
			//Blend One DstColor//OneMinusDstColor // Soft Additive
			//Blend DstColor Zero // Multiplicative
			//Blend DstColor SrcColor // 2x Multiplicative
 
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
 
			v2f vert(appdata v) {
			// just make a copy of incoming vertex data but scaled according to normal direction
			v2f o;
			o.vertex = UnityObjectToClipPos(v.vertex);
 
            float3 norm   = mul ((float3x3)UNITY_MATRIX_IT_MV, v.normal);
	        float2 offset = normalize(TransformViewToProjection(norm.xy));
            float dist = distance(_WorldSpaceCameraPos, mul(unity_ObjectToWorld, v.vertex));
                
	        o.vertex.xy += offset * _Outline * dist / _ScreenParams.xy ;
			o.color = _OutlineColor;
			return o;
			}

            half4 frag(v2f i) :COLOR {
				//i.color.a = 0.0;
				//i.color.rgb *= i.color.a;
	            return i.color;
            }
            ENDCG
		}
 
		
	}
    
	Fallback "Diffuse"
}
