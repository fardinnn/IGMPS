// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Unlit/OutlineOnly"
{
    Properties
    {
		_OutlineColor ("Outline Color", Color) = (0,0,0,1)
		_Outline ("Outline width", Range (0.0, 10.0)) = .005
    }

    CGINCLUDE
    #include "UnityCG.cginc"
 
    struct appdata {
	    float4 vertex : POSITION;
	    float3 normal : NORMAL;
    };
 
    struct v2f {
	    float4 vertex : POSITION;
	    float4 color : COLOR;
    };
 
    uniform float _Outline;
    uniform float4 _OutlineColor;

    ENDCG

    SubShader {
	
		//Tags { "Queue" = "Transparent" }
		Pass {
			Name "OUTLINE"
			Tags { 
                "RenderType" = "Opaque"
                "RenderPipeline" = "UniversalPipeline"
                "LightMode" = "UniversalForward"
			}
			Cull Front
			Blend One Zero
 
			// you can choose what kind of blending mode you want for the outline
			//Blend SrcAlpha OneMinusSrcAlpha // Normal
			//Blend One One // Additive
			//Blend One OneMinusDstColor // Soft Additive
			//Blend DstColor Zero // Multiplicative
			//Blend DstColor SrcColor // 2x Multiplicative
 
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
 
			v2f vert(appdata v) {
			// just make a copy of incoming vertex data but scaled according to normal direction
			v2f o;
			o.vertex = UnityObjectToClipPos(v.vertex);
 
            float3 norm   = mul ((float3x3)UNITY_MATRIX_IT_MV, v.normal);
	        float2 offset = normalize(TransformViewToProjection(norm.xy));
            float dist = distance(_WorldSpaceCameraPos, mul(unity_ObjectToWorld, v.vertex));
                
	        o.vertex.xy += offset * _Outline * dist / _ScreenParams.xy ;
			o.color = _OutlineColor;
			return o;
			}

            half4 frag(v2f i) :COLOR {
	            return i.color;
            }
            ENDCG
		}

		Pass {
			Name "BASE"
			Tags{
				"RenderType"= "Opaque"
				"RenderPipeline" = "UniversalPipeline"
			}
			Cull Front
 
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
			
            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                //float3 norm   = mul ((float3x3)UNITY_MATRIX_IT_MV, v.normal);
	            //float2 offset = TransformViewToProjection(norm.xy);
                
	            //o.vertex.xy += offset * o.vertex.z * _Outline;
	            o.color = _OutlineColor;
                return o;
            }

            half4 frag(v2f i) :COLOR {
	            return i.color;
            }
            ENDCG
		}
 
		// note that a vertex shader is specified here but its using the one above
		
 
 
	}
    
	Fallback "Diffuse"
}
