using BG.API;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BG.EISystem
{
    public abstract class InputController<TView, TModel> : IController<TView, TModel>, IDisposable
        where TView : InputView
        where TModel : InputModel
    {
        public TView View { get; }
        public TModel Model { get; }

        public Guid Id { get; } = Guid.NewGuid();

        public InputController(TView view = null, TModel model = null)
        {
            Model = model ?? CreateDefaultModel();
            View = view ?? CreateDefaultView();
            View.Controller = this;
            if (Model.Parent == null)
                Model.Parent = View.transform;
        }

        protected virtual TModel CreateDefaultModel()
        {
            return (TModel)Activator.CreateInstance(typeof(TModel));
        }

        protected virtual TView CreateDefaultView()
        {
            var view = (Model.Parent != null ? Model.Parent.gameObject :
                (Model.Parent = new GameObject(typeof(TView).Name.Replace("View", "")).transform).gameObject)
                .AddComponent<TView>();
            return view;
        }

        public virtual void SetActivate(bool state)
        {
            View?.gameObject.SetActive(state);
        }

        public virtual void Dispose()
        {
            UnityEngine.Object.Destroy(View.gameObject);
        }
    }
}
