﻿using BG.API;
using UnityEngine;

namespace BG.EISystem
{
    public abstract class InputModel : IModel
    {
        public Transform Parent { get; set; }
    }
}
