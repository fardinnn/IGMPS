﻿using System.Collections.Generic;
using System.Text;
using UnityEngine.InputSystem;

namespace BG.EISystem
{
    public class InputControlSet : HashSet<InputControl>
    {
        public InputControlSet() : base() { }
        public InputControlSet(InputControlSet otherGroup) : base(otherGroup) { }
        public InputControlSet(params InputControl[] controls) : base(controls) { }
        public InputControlSet(IEnumerable<InputControl> controls) : base(controls) { }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;
            var otherSet = (obj as InputControlSet);
            return otherSet.IsSubsetOf(this) && otherSet.Count == this.Count;
        }

        public override int GetHashCode()
        {
            return GetType().GetHashCode();
        }
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("StateGroup values: \n");
            foreach (var item in this)
            {
                sb.Append($"{item.ToString()},  ");
            }
            return sb.ToString();
        }
    }
}