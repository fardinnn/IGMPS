﻿using UnityEngine.InputSystem;

namespace BG.EISystem
{
    public static class InputExtensions
    {
        public static bool IsActive(this InputAction action)
        {
            var result = true;

            for (int i = 0; i < action.controls.Count; i++)
            {
                if (!action.controls[i].IsPressed())
                {
                    result = false;
                }
            }

            return result;
        }
    }
}