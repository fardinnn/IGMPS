﻿using BG.API;
using BG.Extensions;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace BG.EISystem
{

    public class SimpleInputController : InputController<SimpleInputView, SimpleInputModel>
    {
        private InputActionAsset inputActions;
        private Dictionary<string, HashSet<Guid>> groupedMaps;
        private Dictionary<Guid, HashSet<string>> mapsGroups;
        /// <summary>
        /// Keys are actions, 
        /// Values are group names
        /// </summary>
        private Dictionary<InputControlSet, string> activeActions;

        public SimpleInputController(SimpleInputView view = null,
            SimpleInputModel model = null) : base(view, model)
        {
            inputActions = ScriptableObject.CreateInstance<InputActionAsset>();
            groupedMaps = new Dictionary<string, HashSet<Guid>>();
            mapsGroups = new Dictionary<Guid, HashSet<string>>();
            activeActions = new Dictionary<InputControlSet, string>();
            inputActions.Enable();
        }

        public void Add(IInputReceiver inputReceiver)
        {
            if (inputReceiver==null)
            {
                Debug.LogError("'inputReceiver' cannot be null!");
                return;
            }

            InputActionMap inputActionMap = CreateActionMap(inputReceiver);
            if (inputActionMap == null)
            {
                Debug.LogError($"Couldn't find or create 'inputActionMap' for {inputReceiver} with map named: {inputReceiver.InputMapName}");
                return;
            }
            AddMapToGroups(inputReceiver, inputActionMap);

            AssignActionEvents(inputReceiver, inputActionMap);
            AssignCancelEvents(inputReceiver, inputActionMap);

            // Enabling and disabling action maps corresponding to their input receivers' enabling and disabling
            inputReceiver.Disabled += (o, e) => inputActionMap.Disable();
            inputReceiver.Enabled += (o, e) => inputActionMap.Enable();
            
            // enabling input action asset so the new changes can be used
            inputActions.Enable();
        }

        private InputActionMap CreateActionMap(IInputReceiver inputReceiver)
        {
            string mapName = string.IsNullOrEmpty(inputReceiver.InputMapName) ?
                inputReceiver.GetType().Name : inputReceiver.InputMapName;
            InputActionMap inputActionMap = inputActions.FindActionMap(mapName);
            if (inputActionMap == null)
            {
                if (!string.IsNullOrEmpty(inputReceiver.JSONInputs))
                {
                    // Loading default actions from provided JSON
                    inputActions.LoadFromJson(inputReceiver.JSONInputs);
                    inputActionMap = inputActions.FindActionMap(mapName);
                }
                else
                {
                    inputActionMap = new InputActionMap(mapName);
                    inputActions.AddActionMap(inputActionMap);
                }
            }
            return inputActionMap;
        }

        private void AddMapToGroups(IInputReceiver inputReceiver, InputActionMap inputActionMap)
        {
            if (!groupedMaps.ContainsKey(inputReceiver.CoExistGroup))
                groupedMaps.Add(inputReceiver.CoExistGroup, new HashSet<Guid>());
            if (!mapsGroups.ContainsKey(inputActionMap.id))
                mapsGroups.Add(inputActionMap.id, new HashSet<string>());

            if (!groupedMaps[inputReceiver.CoExistGroup].Contains(inputActionMap.id)) groupedMaps[inputReceiver.CoExistGroup].Add(inputActionMap.id);
            if (!mapsGroups[inputActionMap.id].Contains(inputReceiver.CoExistGroup)) mapsGroups[inputActionMap.id].Add(inputReceiver.CoExistGroup);
        }

        private void  AssignActionEvents(IInputReceiver inputReceiver, InputActionMap inputActionMap)
        {
            if (inputReceiver.Actions != null)
            {
                foreach (var actions in inputReceiver.Actions)
                {
                    if (inputActionMap.FindAction(actions.Key) == null)
                        inputActionMap.AddAction(actions.Key);

                    inputActionMap[actions.Key].performed += ctx => PerformAction(ctx, inputReceiver, actions.Key);
                    inputActionMap[actions.Key].canceled += ctx =>
                    {
                        if (inputReceiver.CancelActions != null && inputReceiver.CancelActions.ContainsKey(actions.Key))
                            CancelAction(ctx, inputReceiver, actions.Key);
                        else
                            activeActions.RemoveIfExists(new InputControlSet(ctx.action.controls));
                    };
                }
            }
        }

        private void AssignCancelEvents(IInputReceiver inputReceiver, InputActionMap inputActionMap)
        {
            if (inputReceiver.CancelActions != null)
            {
                foreach (var cancelActions in inputReceiver.CancelActions)
                {
                    if (inputReceiver.Actions != null && inputReceiver.Actions.ContainsKey(cancelActions.Key)) continue;

                    if (inputActionMap.FindAction(cancelActions.Key) == null)
                        inputActionMap.AddAction(cancelActions.Key);

                    inputActionMap[cancelActions.Key].canceled += ctx => CancelAction(ctx, inputReceiver, cancelActions.Key);
                }
            }
        }

        private void PerformAction(InputAction.CallbackContext ctx, IInputReceiver inputReceiver, string actionKey)
        {
            try
            {
                activeActions.AddIfNotExists(new InputControlSet(ctx.action.controls), inputReceiver.CoExistGroup);
                IInputValue value = new UnityInputSystemValue(ctx);
                inputReceiver.Actions[actionKey].Invoke(this, value);
            }
            catch (Exception ex)
            {
                Debug.LogError($"Input Error: {ex.Message} \n\n parameters: {ctx.action.name}, {inputReceiver.GetType().Name}, {actionKey}");
            }
        }

        private void CancelAction(InputAction.CallbackContext ctx, IInputReceiver inputReceiver, string actionKey)
        {
            try
            {
                activeActions.RemoveIfExists(new InputControlSet(ctx.action.controls));
                inputReceiver.CancelActions[actionKey](this, new UnityInputSystemValue(ctx));
            }
            catch (Exception ex)
            {
                Debug.LogError($"Input Error: {ex.Message} \n\n parameters: {ctx.action.name}, {inputReceiver.GetType().Name}, {actionKey}");
            }
        }

        public override void Dispose()
        {
            base.Dispose();
            inputActions.Disable();
        }
    }
}