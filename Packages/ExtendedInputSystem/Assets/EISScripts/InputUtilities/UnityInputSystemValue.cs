using BG.API;
using static UnityEngine.InputSystem.InputAction;

namespace BG.EISystem
{
    public class UnityInputSystemValue : IInputValue
    {
        private CallbackContext ctx;
        public UnityInputSystemValue(CallbackContext callbackContext)
        {
            ctx = callbackContext;
        }
        public T ReadValue<T>() where T : struct
        {
            return ctx.ReadValue<T>();
        }
    }
}
