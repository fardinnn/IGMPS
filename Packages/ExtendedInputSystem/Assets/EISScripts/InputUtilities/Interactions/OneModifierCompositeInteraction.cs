﻿using System;
using UnityEditor;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Controls;

namespace BG.EISystem
{

#if UNITY_EDITOR
    [InitializeOnLoad] // Automatically register in editor.
#endif

    public class OneModifierCompositeInteraction : IInputInteraction
    {
        static OneModifierCompositeInteraction()
        {
            InputSystem.RegisterInteraction<OneModifierCompositeInteraction>();
        }

        public void Reset()
        {
        }

        public void Process(ref InputInteractionContext context)
        {
            if (context.timerHasExpired)
            {
                context.Performed();
                return;
            }


            switch (context.phase)
            {
                case InputActionPhase.Disabled:
                    //Debug.Log("1");
                    break;
                case InputActionPhase.Waiting:
                    //Debug.Log("2");
                    if (((ButtonControl)context.action.controls[0]).isPressed)//(context.ControlIsActuated())
                    {
                        //Debug.Log("2.5");
                        context.Started();
                        context.SetTimeout(float.PositiveInfinity);
                    }
                    break;
                case InputActionPhase.Started:
                    //Debug.Log("3");
                    context.PerformedAndStayPerformed();
                    break;
                case InputActionPhase.Performed:
                    //Debug.Log("4");
                    if (context.ControlIsActuated())
                    {
                        context.PerformedAndStayPerformed();
                    }
                    else
                    {
                        if (!((ButtonControl)context.action.controls[0]).isPressed)
                        {
                            context.Canceled();
                        }

                        ////////// Problematic
                        //if (!context.control.IsPressed())
                        //{
                        //    context.Canceled();
                        //}
                        //else
                        //{
                        //    int currentIndex = 0;
                        //    for (int i = 0; i < context.action.controls.Count; i++)
                        //    {
                        //        if (context.action.controls[i] == context.control)
                        //        {
                        //            currentIndex = i;
                        //            break;
                        //        }
                        //    }
                        //    if (currentIndex == 0 || !((ButtonControl)context.action.controls[currentIndex - 1]).isPressed)
                        //    {
                        //        context.Canceled();
                        //    }
                        //}
                    }
                    break;
                case InputActionPhase.Canceled:
                    //Debug.Log("5");
                    break;
                default: throw new ArgumentOutOfRangeException(nameof(context.phase), context.phase, null);
            }

        }

        [RuntimeInitializeOnLoadMethod]
        private static void Init()
        {
        }
    }

}