﻿using System;
using UnityEditor;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Controls;

namespace BG.EISystem
{
#if UNITY_EDITOR
    [InitializeOnLoad] // Automatically register in editor.
#endif
    public class TwoModifierCompositeInteraction : IInputInteraction
    {
        static TwoModifierCompositeInteraction()
        {
            InputSystem.RegisterInteraction<TwoModifierCompositeInteraction>();
        }

        public void Reset()
        {
        }

        public void Process(ref InputInteractionContext context)
        {
            if (context.timerHasExpired)
            {
                context.Performed();
                return;
            }

            switch (context.phase)
            {
                case InputActionPhase.Disabled:
                    break;
                case InputActionPhase.Waiting:
                    if ((((ButtonControl)context.action.controls[0]).isPressed
                                && ((ButtonControl)context.action.controls[1]).isPressed))// (context.ControlIsActuated())
                    {
                        context.Started();
                        context.SetTimeout(float.PositiveInfinity);
                    }
                    break;
                case InputActionPhase.Started:
                    context.PerformedAndStayPerformed();
                    break;
                case InputActionPhase.Performed:
                    if (context.ControlIsActuated())
                    {
                        context.PerformedAndStayPerformed();
                    }
                    else
                    {
                        if (context.ControlIsActuated())
                        {
                            context.PerformedAndStayPerformed();
                        }
                        else
                        {
                            if (!(((ButtonControl)context.action.controls[0]).isPressed
                                && ((ButtonControl)context.action.controls[1]).isPressed))
                            {
                                context.Canceled();
                            }


                            ////////// Problematic

                            //int currentIndex = 0;
                            //int i = 0;
                            ////try
                            ////{
                            //    for (i = 0; i < context.action.controls.Count; i++)
                            //    {
                            //        if (context.action.controls[i] == context.control)
                            //        {
                            //            currentIndex = i;
                            //            break;
                            //        }
                            //    }
                            //    if (currentIndex < 2 || !(((ButtonControl)context.action.controls[currentIndex - 1]).isPressed
                            //    && ((ButtonControl)context.action.controls[currentIndex - 2]).isPressed))
                            //    {
                            //        context.Canceled();
                            //    }
                            ////}
                            ////catch(Exception ex)
                            ////{

                            ////}
                        }
                    }
                    break;
                case InputActionPhase.Canceled:
                    break;
                default: throw new ArgumentOutOfRangeException(nameof(context.phase), context.phase, null);
            }

        }

        [RuntimeInitializeOnLoadMethod]
        private static void Init()
        {
        }
    }

}