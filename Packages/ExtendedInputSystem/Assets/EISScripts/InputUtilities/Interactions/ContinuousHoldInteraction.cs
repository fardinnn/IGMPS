using System.ComponentModel;
using UnityEngine.InputSystem;
using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;

namespace BG.EISystem
{

#if UNITY_EDITOR
    [InitializeOnLoad] // Automatically register in editor.
#endif

    [DisplayName("Continuous Hold")]
    public class ContinuousHoldInteraction : IInputInteraction
    {

        public float duration;

        public float pressPoint;

        private float durationOrDefault => duration > 0.0 ? duration : InputSystem.settings.defaultHoldTime;
        private float pressPointOrDefault => pressPoint > 0.0 ? pressPoint : 0.0f;//ButtonControl.s_GlobalDefaultButtonPressPoint;

        private double m_TimePressed;

        private Dictionary<Guid, bool> _cancels = new Dictionary<Guid, bool>();
        private Dictionary<Guid, InputInteractionContext> _contexts = new Dictionary<Guid, InputInteractionContext>();
        private CoroutineRunner _gameObject;
        private Dictionary<Guid, InputAction> _actions = new Dictionary<Guid, InputAction>();

        public void Process(ref InputInteractionContext context)
        {

            if (context.timerHasExpired)
            {
                if (!_cancels.ContainsKey(context.action.id))
                {
                    _contexts.Add(context.action.id,context);
                    _actions.Add(context.action.id, context.action);
                    _cancels.Add(context.action.id, false);
                }
                else
                {
                    _contexts[context.action.id] = context;
                    _actions[context.action.id] = context.action;
                    _cancels[context.action.id] = false;
                }
                if (_gameObject ==null)
                    _gameObject = new GameObject().AddComponent<CoroutineRunner>();
                _gameObject.RunCoroutine(RunInteraction);
                return;
            }

            switch (context.phase)
            {
                case InputActionPhase.Waiting:
                    if (context.ControlIsActuated(pressPointOrDefault))
                    {
                        context.PerformedAndStayPerformed();
                        m_TimePressed = context.time;
                        context.Started();
                        context.SetTimeout(durationOrDefault);

                    }
                    break;

                case InputActionPhase.Started:
                    // If we've reached our hold time threshold, perform the hold.
                    // We do this regardless of what state the control changed to.
                    if (context.time - m_TimePressed >= durationOrDefault)
                    {
                        context.PerformedAndStayPerformed();
                    }
                    else if (!context.ControlIsActuated())
                    {
                        // Control is no longer actuated and we haven't performed a hold yet,
                        // so cancel.
                        _cancels[context.action.id] = true;
                        context.Canceled();
                    }
                    break;

                case InputActionPhase.Performed:
                    if (!context.ControlIsActuated(pressPointOrDefault))
                        context.Canceled();
                    break;
            }
        }

        public void Reset()
        {
            m_TimePressed = 0;
            foreach (var action in _actions.Values)
                _cancels[action.id] = true;
        }

        public bool IsCancelled { get; set; }
        public IEnumerator RunInteraction()
        {
            bool canceller = false;
            yield return new WaitUntil(() =>
            {
                canceller = true;
                foreach (var key in _cancels.Keys.ToList())
                {
                    if (_cancels[key]) continue;

                    _contexts[key].PerformedAndStayPerformed();
                    canceller = false;
                }
                return canceller;
            });
        }


#if UNITY_EDITOR
        static ContinuousHoldInteraction()
        {
            InputSystem.RegisterInteraction<ContinuousHoldInteraction>();
        }
#endif
        [RuntimeInitializeOnLoadMethod]
        static void Init() { } // Trigger static constructor.
    }
}
