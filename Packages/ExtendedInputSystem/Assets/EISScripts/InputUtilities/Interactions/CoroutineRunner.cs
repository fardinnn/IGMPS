using System;
using System.Collections;
using UnityEngine;

namespace BG.EISystem
{
    public class CoroutineRunner : MonoBehaviour
    {
        private Coroutine _coroutine;

        public void RunCoroutine(Func<IEnumerator> func)
        {
            _coroutine = StartCoroutine(func.Invoke());
        }


        public void Destroy()
        {
            _coroutine = null;
            Destroy(this.gameObject);
        }

    }
}
