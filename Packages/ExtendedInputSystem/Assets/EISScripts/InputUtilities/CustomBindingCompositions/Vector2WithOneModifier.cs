using UnityEditor;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Layouts;
using UnityEngine.InputSystem.Utilities;

namespace BG.EISystem
{
#if UNITY_EDITOR
    [InitializeOnLoad] // Automatically register in editor.
#endif


    [DisplayStringFormat("{Modifier}+{Position}")]
    public class Vector2WithOneModifier : InputBindingComposite<Vector2>
    {
        [InputControl(layout = "Button")]
        public int Modifier;

        [InputControl(layout = "Vector2")]
        public int Position;

        bool modifierValue;
        public override Vector2 ReadValue(ref InputBindingCompositeContext context)
        {
            modifierValue = context.ReadValueAsButton(Modifier);
            if (modifierValue)
                return context.ReadValue<Vector2, Vector2MagnitudeComparer>(Position);

            return default(Vector2);
        }

        public override float EvaluateMagnitude(ref InputBindingCompositeContext context)
        {
            //if (context.ReadValueAsButton(Modifier))
            //    return 1.0f;
            return modifierValue ? 1.0f : 0.0f;
        }

#if UNITY_EDITOR
        static Vector2WithOneModifier()
        {
            InputSystem.RegisterBindingComposite<Vector2WithOneModifier>();
        }
#endif
        [RuntimeInitializeOnLoadMethod]
        static void Init() { } // Trigger static constructor.
    }

}