﻿using UnityEditor;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Layouts;
using UnityEngine.InputSystem.Utilities;

namespace BG.EISystem
{
#if UNITY_EDITOR
    [InitializeOnLoad] // Automatically register in editor.
#endif

    [DisplayStringFormat("{FirstModifier}+{SecondModifier}+{Position}")]
    public class Vector2WithTwoModifier : InputBindingComposite<Vector2>
    {
        [InputControl(layout = "Button")]
        public int FirstModifier;

        [InputControl(layout = "Button")]
        public int SecondModifier;

        [InputControl(layout = "Vector2")]
        public int Position;
         
        public override Vector2 ReadValue(ref InputBindingCompositeContext context)
        {
            if (context.ReadValueAsButton(FirstModifier) && context.ReadValueAsButton(SecondModifier))
                return context.ReadValue<Vector2, Vector2MagnitudeComparer>(Position);

            return default(Vector2);
        }

        public override float EvaluateMagnitude(ref InputBindingCompositeContext context)
        {
            if (context.ReadValueAsButton(FirstModifier) && context.ReadValueAsButton(SecondModifier))
                return 1.0f;
            return 0.0f;
        }

#if UNITY_EDITOR
        static Vector2WithTwoModifier()
        {
            InputSystem.RegisterBindingComposite<Vector2WithTwoModifier>();
        }
#endif
        [RuntimeInitializeOnLoadMethod]
        static void Init() { } // Trigger static constructor.
    }

}