﻿using UnityEditor;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Layouts;
using UnityEngine.InputSystem.Utilities;

namespace BG.EISystem
{
#if UNITY_EDITOR
    [InitializeOnLoad] // Automatically register in editor.
#endif

    [DisplayStringFormat("{FirstModifier}+{SecondModifier}+{MyDeltaVector}")]
    public class Vector2PolarAngleWithTwoModifier : InputBindingComposite<float>
    {
        [InputControl(layout = "Button")]
        public int FirstModifier;

        [InputControl(layout = "Button")]
        public int SecondModifier;

        [InputControl(layout = "Vector2")]
        public int MyPositionVector;

        [InputControl(layout = "Vector2")]
        public int MyDeltaVector;

        public float CenterWidth = -1;
        public float CenterHeight = -1;

        public override float ReadValue(ref InputBindingCompositeContext context)
        {
            if (!context.ReadValueAsButton(FirstModifier) || !context.ReadValueAsButton(SecondModifier))
                return default(float);

            var position = context.ReadValue<Vector2, Vector2MagnitudeComparer>(MyPositionVector);
            var delta = context.ReadValue<Vector2, Vector2MagnitudeComparer>(MyDeltaVector);

            var Center = new Vector2(CenterWidth == -1 ? Screen.width / 2.0f : CenterWidth,
                CenterHeight == -1 ? Screen.height / 2.0f : CenterHeight);

            var r = position - Center;
            var r_m = r.magnitude;
            var r_n = r / r_m;

            var delta_t = Vector2.Dot(delta, r_n) * r_n;
            var delta_n = delta - delta_t;
            var cross = (delta_n.x * r_n.y - delta_n.y * r_n.x);
            float angle = -Mathf.Sign(cross) * Mathf.Atan2(delta_n.magnitude, r_m) * Mathf.Rad2Deg;

            return angle;
        }

        public override float EvaluateMagnitude(ref InputBindingCompositeContext context)
        {
            if (context.ReadValueAsButton(FirstModifier) && context.ReadValueAsButton(SecondModifier))
                return 1.0f;

            return 0.0f;
        }

#if UNITY_EDITOR
        static Vector2PolarAngleWithTwoModifier()
        {
            InputSystem.RegisterBindingComposite<Vector2PolarAngleWithTwoModifier>();
        }
#endif

        [RuntimeInitializeOnLoadMethod]
        static void Init() { }

    }
}
