using UnityEditor;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Layouts;
using UnityEngine.InputSystem.Utilities;

namespace BG.EISystem
{
#if UNITY_EDITOR
    [InitializeOnLoad] // Automatically register in editor.
#endif

    [DisplayStringFormat("{Modifier}+{Vector2}")]
    public class Vector2SizeBinderWithOneModifier : InputBindingComposite<float>
    {
        [InputControl(layout = "Button")]
        public int Modifier;

        [InputControl(layout = "Vector2")]
        public int Value;

        public bool IsSigned;

        public override float ReadValue(ref InputBindingCompositeContext context)
        {
            if (context.ReadValueAsButton(Modifier))
            {
                var targetVector = context.ReadValue<Vector2, Vector2MagnitudeComparer>(Value);
                if (IsSigned)
                    return targetVector.x + targetVector.y;
                else
                    return targetVector.magnitude;
            }

            return default(float);
        }

        public override float EvaluateMagnitude(ref InputBindingCompositeContext context)
        {
            if (context.ReadValueAsButton(Modifier))
                return 1.0f;
            return 0.0f;
        }

#if UNITY_EDITOR
        static Vector2SizeBinderWithOneModifier()
        {
            InputSystem.RegisterBindingComposite<Vector2SizeBinderWithOneModifier>();
        }
#endif
        [RuntimeInitializeOnLoadMethod]
        static void Init() { } // Trigger static constructor.
    }
}
