﻿using BG.API;
using BG.API.Shapes;
using BG.ShapeProviders.Domain;
using BG.States;
using System;

namespace BG.ShapeProviders
{

    public class Simple3DShapeProviderController :
        ShapeProviderController<Simple3DShapeProviderView, Simple3DShapeProviderModel>
    {
        public Simple3DShapeProviderController(Simple3DShapeProviderView view = null,
            Simple3DShapeProviderModel model = null, CompositeStatesSet requiredStates = null) : base(view, model, requiredStates)
        {

        }

        public override IComponentSystem ProvideComponentSystem(Options options)
        {
            Guid systemId = Guid.NewGuid();

            new CreateSimpleShapeSystemHandler().Handle(new SimpleShapeSystemCommand
            {
                SystemId = systemId
                //TargetShapeType = typeof(T),
                //TargetPackage = Package,
                //LimitedByType = limitedByType
            });

            var shapeProvider = ShapeSystemManager.Get<SimpleShapeSystem>(systemId);
            options?.AlterOn(shapeProvider);
            return shapeProvider;
        }

        public IComponentSystem ProvideShapeSystem<T>(IShapePackage Package, bool limitedByType, Options options = null) where T : Shape
        {
            Guid systemId = Guid.NewGuid();

            new CreateSimpleShapeSystemHandler().Handle(new SimpleShapeSystemCommand
            {
                SystemId = systemId,
                TargetShapeType = typeof(T),
                TargetPackage = Package,
                LimitedByType = limitedByType
            });

            var shapeProvider = ShapeSystemManager.Get<SimpleShapeSystem>(systemId);
            options?.AlterOn(shapeProvider);
            return shapeProvider;
        }


        //public Dictionary<ComponentType, List<IShapeSystem>> ShapeComponents { get; set; }
        //    = new Dictionary<ComponentType, List<IShapeSystem>>();

        //public IShapeSystem GetSystem<TComponentData, TGroupTag>(
        //    TComponentData componentData, TGroupTag groupTag)
        //    where TGroupTag : IComponentData
        //    where TComponentData : IShapeComponentData
        //{
        //    if (typeof(SimpleShapeComponentData) == typeof(TComponentData))
        //    {
        //        var system = World.DefaultGameObjectInjectionWorld.GetOrCreateSystem<SimpleShapeSystem>();
        //        system.AddGroupData(groupTag.GetType());
        //        return system;
        //    }
        //    return null;
        //}

        //public IShapeSystem ProvideSystem<TShapeSystem>(ComponentType shapeGroupTag, Options options = null)
        //    where TShapeSystem : SystemBase, IShapeSystem<TShapeSystem>
        //{
        //    var system = World.DefaultGameObjectInjectionWorld.GetOrCreateSystem<TShapeSystem>();
        //    system.AddGroupData(shapeGroupTag);
        //    options?.AlterOn(system, shapeGroupTag);
        //    //ShapeSystemsGroup.ShapeSystems.Add(system);
        //    return system;
        //}

        //public void Change<T>(ComponentType shapeGroupTag, Options changes)
        //{
        //    var components = ShapeComponents[shapeGroupTag].Where(c => c.GetType() == typeof(T));

        //    foreach (var component in components)
        //        changes.AlterOn(component, shapeGroupTag);
        //}

        //public void Change(ComponentType shapeGroupTag, Options changes)
        //{
        //    foreach (var component in ShapeComponents[shapeGroupTag])
        //        changes.AlterOn(component, shapeGroupTag);
        //}
    }
}