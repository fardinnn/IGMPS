﻿using UnityEngine;

namespace BG.ShapeProviders
{
    public class Simple3DShapeProviderModel : CellShapeProviderModel
    {
        public Vector2 Size { get; set; }
        public MeshRenderer MeshRenderer { get; set; }
        public MeshFilter MeshFilter { get; set; }
        public SpriteRenderer SpriteRenderer { get; set; }
    }
}
