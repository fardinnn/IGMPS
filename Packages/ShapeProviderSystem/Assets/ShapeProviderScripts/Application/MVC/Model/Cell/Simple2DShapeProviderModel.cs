﻿using UnityEngine;

namespace BG.ShapeProviders
{
    public class Simple2DShapeProviderModel : CellShapeProviderModel
    {
        public Vector2 Size { get; set; }
        public SpriteRenderer SpriteRenderer { get; set; }
    }
}
