﻿using BG.API.Shapes;
using BG.ShapeProviders.Domain;

namespace BG.ShapeProviders
{
    public static class ShapePackages
    {
        private static DefaultShapesPackage defaultShapesPackage;
        public static IShapePackage DefaultShapesPackage
        {
            get
            {
                if (defaultShapesPackage == null) defaultShapesPackage = new DefaultShapesPackage();
                return defaultShapesPackage;
            }
        }

    }
}
