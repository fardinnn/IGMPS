﻿using Unity.Mathematics;
using UnityEngine;

namespace BG.ShapeProviders
{
    public abstract class ShapeDataTemplatePrefab : MonoBehaviour
    {
        public abstract float3 Size { get; }
    }
    public abstract class ShapeDataTemplatePrefab<T> : ShapeDataTemplatePrefab where T : ShapeDataTemplatePrefab<T>
    { }

}
