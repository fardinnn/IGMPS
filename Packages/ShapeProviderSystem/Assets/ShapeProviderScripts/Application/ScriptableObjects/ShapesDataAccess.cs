using System;
using System.Linq;
using UnityEngine;

namespace BG.ShapeProviders
{



    [CreateAssetMenu(fileName = "ShapesData", menuName = "Shape provider/Shapes data access")]
    public class ShapesDataAccess : ScriptableObject
    {
        [SerializeField]
        private ShapeDataTemplatePrefab[] shapeDataTemplates;

        [Serializable]
        public struct ShapeAddressing
        {
            [SerializeField]
            public string Name;
            [SerializeField]
            public GameObject Prefab;
        }


        //[SerializeField] private ShapeTemplate[] templatePrefabs;


        //public T GetDataTemplate<T>(string dataName) where T : ShapeDataTemplatePrefab<T>
        //{
        //    var value = (T)(shapeDataTemplates
        //        .Where(st => st.name == dataName && st.GetType() == typeof(T))
        //        .FirstOrDefault());
        //    if (value == null) Debug.LogError($"Not found any shape data with provided name: {dataName}.");
        //    return value;
        //}

        //public ShapeTemplate GetTemplate(string name)
        //{
        //    var prefabs = templatePrefabs.Where(t => t.name == name);
        //    if (prefabs == null) return null;
        //    return prefabs.FirstOrDefault();
        //}

        //public T GetTemplate<T>() where T: ShapeTemplate
        //{
        //    return templatePrefabs.FirstOrDefault(t => t.GetType() == typeof(T)) as T;
        //}
        //public ShapeTemplate GetTemplate(Type type)
        //{
        //    var prefabs = templatePrefabs.Where(t => t.GetType() == type);
        //    if (prefabs == null) return null;
        //    return prefabs.FirstOrDefault();
        //}
    }
}
