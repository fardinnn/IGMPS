﻿using System;
using UnityEngine;
using BG.API.Shapes;
using System.Threading.Tasks;
using System.Collections;

namespace BG.ShapeProviders.Domain
{
    public abstract class ShapesPackage : IShapePackage
    {
        private DefaultShapesPackage defaultShapes;

        public Shape LoadedTemplate { get; protected set; }

        public virtual IEnumerator GetTemplate<T>() where T : Shape
        {
            yield return defaultShapes.GetTemplate<T>();
            LoadedTemplate = defaultShapes.LoadedTemplate;
        }

        public virtual IEnumerator GetTemplate(Type shapeType)
        {
            yield return defaultShapes.GetTemplate(shapeType);
            LoadedTemplate = defaultShapes.LoadedTemplate;
        }

        public virtual IEnumerator GetTemplate(Type shapeType, string shapeName, bool limitedByType = false)
        {
            yield return defaultShapes.GetTemplate(shapeType, shapeName, limitedByType);
            LoadedTemplate = defaultShapes.LoadedTemplate;
        }
    }
}