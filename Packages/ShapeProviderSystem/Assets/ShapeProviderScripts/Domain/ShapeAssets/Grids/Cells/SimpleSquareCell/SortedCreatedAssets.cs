using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.ResourceLocations;

namespace BG.ShapeProviders.Domain
{
    public class SortedCreatedAssets : MonoBehaviour
    {
        [SerializeField] private List<string> labels = new List<string>();

        private void Start()
        {

        }

        private async Task SortWaitToComplete(List<string> labels)
        {
            var locations = await Addressables.LoadResourceLocationsAsync(labels, Addressables.MergeMode.UseFirst).Task;

            foreach (IResourceLocation location in locations)
            {
                await Addressables.InstantiateAsync(location);
            }

        }
    }
}
