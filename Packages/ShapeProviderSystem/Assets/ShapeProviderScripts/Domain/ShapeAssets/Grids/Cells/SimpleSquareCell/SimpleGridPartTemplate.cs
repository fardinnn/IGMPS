﻿using BG.API.Shapes;

namespace BG.ShapeProviders.Domain
{
    public abstract class SimpleGridPartTemplate : ShapeTemplate
    {

    }

    public class SimpleSquareCellTemplate : SimpleGridPartTemplate
    {

    }
}
