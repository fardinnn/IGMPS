﻿using BG.API;
using BG.API.Abilities;
using System;

namespace BG.ShapeProviders
{
    public interface IShapeSystem : IComponentSystem, IResizable
    {
        Guid Id { get; set; }
    }

    public interface IShapeSystem<T> : IShapeSystem 
        where T : IShapeSystem<T>
    {

    }
}
