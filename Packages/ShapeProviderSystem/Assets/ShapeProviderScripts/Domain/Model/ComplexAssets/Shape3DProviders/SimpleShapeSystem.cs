﻿using BG.API;
using BG.API.Abilities;
using BG.API.Shapes;
using BG.Extensions;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BG.ShapeProviders.Domain
{

    public class SimpleShapeSystem : IShapeSystem<SimpleShapeSystem>, IFormable, IColorable
    {
        public event EventHandler<ResizeEventArgs> Resized;
        public event EventHandler<FormingEventArgs> Formed;
        public event EventHandler<ColorChangeEventArgs> Colored;

        private IShapePackage package { get; set; }
        //private bool limiteByType { get; set; }
        private Type targetShapeType { get; set; }
        private string shapeName { get; set; } = string.Empty;

        private HashSet<Transform> targetEntities;
        private HashSet<Shape> entityShapes;
        private Options options;

        public Guid Id { get; set; }

        public bool IsReady { get; private set; }

        public SimpleShapeSystem(SimpleShapeSystemCommand command)
        {

            options = new Options();
            targetEntities = new HashSet<Transform>();
            entityShapes = new HashSet<Shape>();

            options.AlterOn(this);
            IsReady = true; // ??????????

            //package = command.TargetPackage;
            //limiteByType = command.LimitedByType;
            //targetShapeType = command.TargetShapeType;
        }

        public void Visit(List<Entity> entities)
        {
            new Task(AddShapesAsync(entities));
        }

        public void Coloring(object sender, ColorChangeEventArgs e)
        {
            ColorOption option = !options.Contains(typeof(ColorOption)) ? new ColorOption() :
                options[typeof(ColorOption)] as ColorOption;
            option.Value = e.Color;
            AlteredOption(option);
        }

        public void Resizing(object sender, ResizeEventArgs e)
        {
            SizeOption option = !options.Contains(typeof(SizeOption)) ? new SizeOption() :
                options[typeof(SizeOption)] as SizeOption;
            option.Size = e.Size;
            option.KeepAspects = e.KeepAspects;
            AlteredOption(option);
        }

        private void AlteredOption<T>(T option) where T : Option
        {
            options[typeof(T)] = option;
            if (entityShapes == null || entityShapes.Count == 0) return;
            entityShapes.ForEach(e => option.AlterOption(e));
        }

        Task coroutine;
        public void Form(object o, FormingEventArgs e)
        {
            package = e.Package;
            if (package.LoadedTemplate != null && package.LoadedTemplate.name == e.ShapeName) return;
            shapeName = e.ShapeName;
            targetShapeType = e.ShapeType;
            if (coroutine != null && coroutine.Running) coroutine.Stop();
            coroutine = new Task(LoadShapesAsync());
        }

        IEnumerator LoadShapesAsync()
        {
            yield return package.GetTemplate(targetShapeType, shapeName/*, limiteByType*/);

            if (package.LoadedTemplate == null) yield break;
            foreach (var item in entityShapes)
                UnityEngine.Object.Destroy(item.gameObject);
            entityShapes.Clear();
            foreach (var item in targetEntities)
            {
                var shape = UnityEngine.Object.Instantiate(package.LoadedTemplate, item);
                shape.transform.localPosition = Vector3.zero;
                shape.transform.rotation = Quaternion.identity;
                entityShapes.Add(shape);
                options.AlterOn(shape);
            }
        }

        IEnumerator AddShapesAsync(List<Entity> entities)
        {
            if (coroutine != null && coroutine.Running) yield return coroutine;

            for (int i = 0; i < entities.Count; i++)
            {
                if (targetEntities.Contains(entities[i].transform)) continue;
                targetEntities.Add(entities[i].transform);
                if (package.LoadedTemplate != null)
                {
                    var shape = UnityEngine.Object.Instantiate(package.LoadedTemplate, entities[i].transform);
                    shape.transform.localPosition = Vector3.zero;
                    shape.transform.rotation = Quaternion.identity;
                    entityShapes.Add(shape);
                    options.AlterOn(shape);
                }
            }
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}
