﻿using BG.API;
using System;

namespace BG.ShapeProviders.Domain
{
    public abstract class ShapeSystemCommand : ICommand
    {
        public Guid Id { get; set; }
    }

}
