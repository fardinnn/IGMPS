﻿using BG.API;
using UnityEngine;

namespace BG.ShapeProviders.Domain
{
    public class SetUpShapeManagerHandler : ICommandHandler<SetUpShapeManager>
    {
        public void Handle(SetUpShapeManager command)
        {
            new GameObject("CameraManager").AddComponent<ShapeSystemManager>();
        }
    }

}
