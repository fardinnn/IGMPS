﻿using BG.API;
using UnityEngine;

namespace BG.ShapeProviders.Domain
{
    public abstract class CreateShapeSystemHandler<TCommand> : ICommandHandler<TCommand>
            where TCommand : ShapeSystemCommand
    {
        protected ShapeSystemManager Manager;
        public virtual void Handle(TCommand command)
        {
            Manager = UnityEngine.Object.FindObjectOfType<ShapeSystemManager>();
            if (Manager == null)
            {
                new GameObject("ShapeSystemManager").AddComponent<ShapeSystemManager>();
                Manager = UnityEngine.Object.FindObjectOfType<ShapeSystemManager>();
            }
        }
    }

}
