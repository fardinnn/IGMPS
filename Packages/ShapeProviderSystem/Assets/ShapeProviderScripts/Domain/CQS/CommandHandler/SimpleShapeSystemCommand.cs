﻿using BG.API;
using BG.API.Shapes;
using System;

namespace BG.ShapeProviders.Domain
{
    public class SimpleShapeSystemCommand : ShapeSystemCommand
    {
        public Guid SystemId { get; set; }
        public Type TargetShapeType { get; set; }
        public IShapePackage TargetPackage { get; set; }
        public bool LimitedByType { get; set; }
        public ShapeSystemManager Manager { get; internal set; }
    }

}
