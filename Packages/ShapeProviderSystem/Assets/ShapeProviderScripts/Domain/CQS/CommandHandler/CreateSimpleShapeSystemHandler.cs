
namespace BG.ShapeProviders.Domain
{
    public class CreateSimpleShapeSystemHandler : CreateShapeSystemHandler<SimpleShapeSystemCommand>
    {
        public override void Handle(SimpleShapeSystemCommand command)
        {
            base.Handle(command);

            var system = new SimpleShapeSystem(command);
            system.Id = command.SystemId;
            Manager.Add(system);
        }
    }

}
