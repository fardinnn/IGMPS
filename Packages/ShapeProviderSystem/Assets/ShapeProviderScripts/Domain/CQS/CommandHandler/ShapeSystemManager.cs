﻿using BG.API;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace BG.ShapeProviders.Domain
{
    public class ShapeSystemManager : Singleton<ShapeSystemManager>
    {
        private static Dictionary<Guid, IShapeSystem> shapesSystems;

        protected override void Awake()
        {
            IsDestroyOnLoad = true;
            base.Awake();

            shapesSystems = new Dictionary<Guid, IShapeSystem>();
        }

        public void Add(IShapeSystem shapeSystem)
        {
            if (shapeSystem.Id == default) throw new ArgumentException("shapeSystem's id in not assigned");
            shapesSystems.Add(shapeSystem.Id, shapeSystem);
        }

        public static T Get<T>(Guid id) where T : class, IShapeSystem
        {
            if (shapesSystems == null) throw new NullReferenceException("shapesSystems can not be null!");
            if (!shapesSystems.ContainsKey(id))
                Debug.LogError("There is no shape system with the provided id!");
            var target = shapesSystems[id] as T;
            if (target == null)
                Debug.LogError("Provided type is not compatible with target system id!");
            return target;
        }
    }
}
