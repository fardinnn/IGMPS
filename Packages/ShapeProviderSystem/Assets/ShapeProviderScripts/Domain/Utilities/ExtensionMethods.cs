using BG.Extensions;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace BG.ShapeProviders.Domain
{
    public static class ExtensionMethods
    {
        public static HashSet<T> ToHashSet<T>(this IEnumerable<T> self)
        {
            HashSet<T> hashSet = new HashSet<T>();
            self.ForEach(item => { if (!hashSet.Contains(item)) hashSet.Add(item); });
            return hashSet;
        }

        public static void ForEach<T>(this HashSet<T> self, Action<T> action)
        {
            foreach (var item in self)
                action(item);
        }
    }


    public static class IAsyncOperationExtensions
    {

        public static AsyncOperationAwaiter GetAwaiter(this AsyncOperationHandle operation)
        {
            return new AsyncOperationAwaiter(operation);
        }

        public static AsyncOperationAwaiter<T> GetAwaiter<T>(this AsyncOperationHandle<T> operation) where T : UnityEngine.Object
        {
            return new AsyncOperationAwaiter<T>(operation);
        }

        public readonly struct AsyncOperationAwaiter : INotifyCompletion
        {

            readonly AsyncOperationHandle _operation;

            public AsyncOperationAwaiter(AsyncOperationHandle operation)
            {
                _operation = operation;
            }

            public bool IsCompleted => _operation.Status != AsyncOperationStatus.None;

            public void OnCompleted(Action continuation) => _operation.Completed += (op) => continuation?.Invoke();

            public object GetResult() => _operation.Result;

        }

        public readonly struct AsyncOperationAwaiter<T> : INotifyCompletion where T : UnityEngine.Object
        {

            readonly AsyncOperationHandle<T> _operation;

            public AsyncOperationAwaiter(AsyncOperationHandle<T> operation)
            {
                _operation = operation;
            }

            public bool IsCompleted => _operation.Status != AsyncOperationStatus.None;

            public void OnCompleted(Action continuation) => _operation.Completed += (op) => continuation?.Invoke();

            public T GetResult() => _operation.Result;

        }

    }
}
