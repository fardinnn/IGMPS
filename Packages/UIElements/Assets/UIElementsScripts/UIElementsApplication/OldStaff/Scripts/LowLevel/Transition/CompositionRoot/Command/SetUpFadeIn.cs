﻿using UnityEngine;

namespace BG.UIElements.Transition
{
    public class SetUpFadeIn : SetUpTransition<FadeIn>
    {
        public Color Color { get; set; } = Color.black;
        public float Alpha { get; set; } = 1;
    }
}