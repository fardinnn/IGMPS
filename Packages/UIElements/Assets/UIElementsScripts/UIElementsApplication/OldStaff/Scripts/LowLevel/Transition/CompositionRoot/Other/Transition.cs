﻿using System;
using System.Threading.Tasks;
using BG.API;
using BG.API.Abilities;
using BG.UIElements;

namespace BG.UIElements.Transition
{
    public abstract class Transition : UiElement, IPlaySubscriber, IPauseSubscriber
    {
        protected abstract void Visit(IPlayable playable);
        protected abstract void Visit(IPausable pausable);
        public abstract void OnPlayed(object sender, PlayEventArgs e);
        public abstract void OnPaused(object sender, PauseEventArgs e);
    }
}