﻿using BG.UIElements;



namespace BG.UIElements.Transition
{
    public abstract class SetUpTransition<T> : SetUpUiElement<T>
        where T:Transition
    {
        public float Duration { get; set; } = 0.5f;
    }
}