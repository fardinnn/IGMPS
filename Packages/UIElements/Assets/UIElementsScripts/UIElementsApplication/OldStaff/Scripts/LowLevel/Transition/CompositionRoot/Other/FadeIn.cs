﻿using System;
using BG.API.Abilities;

namespace BG.UIElements.Transition
{
    public class FadeIn : Transition
    {
        protected override void Visit(IPlayable playable)
        {
            playable.Played += OnPlayed;
        }

        protected override void Visit(IPausable pausable)
        {
            pausable.Paused += OnPaused;
        }

        public override void OnPlayed(object sender, PlayEventArgs e)
        {
            throw new NotImplementedException();
        }

        public override void OnPaused(object sender, PauseEventArgs e)
        {
            throw new NotImplementedException();
        }
    }
}