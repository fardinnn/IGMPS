﻿using BG.API;

namespace BG.UIElements.Transition
{
    public abstract class SetUpTransitionHandler<TSetUp, TView>  
        : ICommandHandler<TSetUp>
        where TSetUp : SetUpTransition<TView>
        where TView : Transition
    {
        public abstract void Handle(TSetUp command);
    }
}