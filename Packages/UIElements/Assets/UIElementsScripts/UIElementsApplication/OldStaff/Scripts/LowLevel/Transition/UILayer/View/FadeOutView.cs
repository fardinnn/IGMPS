﻿using BG.API;
using System;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace BG.UIElements.Transition
{
    [RequireComponent(typeof(Image))]
    public class FadeOutView : TransitionView, IView
    {
        private Image image;

        void Awake()
        {
            //image = GetComponent<Image>();
        }

        private float duration = 0;
        internal void SetDuration(float duration)
        {
            this.duration = duration;
        }

        internal void SetColor(Color color)
        {
            var c = image.color;
            image.color = new Color(color.r, color.g, color.b, c.a);
        }

        internal void SetAlpha(float alpha)
        {
            var c = image.color;
            image.color = new Color(c.r, c.g, c.b, alpha);
        }
        
        protected override async Task PlayTransition(object o, EventArgs e)
        {
            //CanvasGroup.alpha = 1;
            //float alphaFactor = (1.0f-image.color.a) / duration;
            //while (duration>=0)
            //{
            //    duration -= Time.deltaTime;
            //    SetAlpha(1 - (duration * alphaFactor));
            //    await new WaitForEndOfFrame();
            //}
        }
    }
}