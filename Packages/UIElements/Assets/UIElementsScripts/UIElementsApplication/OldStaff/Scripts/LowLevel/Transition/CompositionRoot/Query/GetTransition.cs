﻿using System;
using BG.API;

namespace BG.UIElements.Transition
{
    public class GetTransition : ISearchIdQuery
    {
        public Guid Id { get; set; }

        public Guid TargetId { get; set; } = Guid.NewGuid();
    }
}