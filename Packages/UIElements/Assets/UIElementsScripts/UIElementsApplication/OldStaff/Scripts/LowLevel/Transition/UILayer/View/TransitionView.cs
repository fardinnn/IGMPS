﻿using System;
using System.Threading.Tasks;
using BG.API;
using UnityEngine;

namespace BG.UIElements.Transition
{
    public abstract class TransitionView : IView//UIView
    {
        public async Task Play(object o, EventArgs e)
        {
            try
            {
                await PlayTransition(o, e);
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                throw;
            }
        }

        protected abstract Task PlayTransition(object o, EventArgs e);
        public Guid Id { get; set; }
        public void Dispose()
        {

        }
    }
}
