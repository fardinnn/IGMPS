﻿using System;
using BG.API;

namespace BG.VisualizerElements.StateView
{
    public class StateViewSetState : ICommand
    {
        public Guid Id { get; set; }
    }
}