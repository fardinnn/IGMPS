﻿using System;
using BG.API;
using UnityEngine;

namespace BG.UIElements.Transition
{
    public class SetUpFadeOut : SetUpTransition<FadeOut>
    {
        public Color Color { get; set; } = Color.black;
        public float Alpha { get; set; } = 0;
    }
}