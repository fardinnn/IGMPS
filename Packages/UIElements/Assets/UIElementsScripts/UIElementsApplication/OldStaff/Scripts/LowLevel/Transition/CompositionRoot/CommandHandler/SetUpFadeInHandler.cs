﻿
using System.Threading.Tasks;
using BG.API;
//using Fardin.Extensions.RectTransforms;
//using Fardin.UITemplate.Transition.UILayer;
using UnityEngine;
using UnityEngine.UI;

namespace BG.UIElements.Transition
{
    public class SetUpFadeInHandler : SetUpTransitionHandler<SetUpFadeIn, FadeIn>
    {
        public override void Handle(SetUpFadeIn command)
        {
            FadeIn fadeIn = new FadeIn();//new GenerateUi(new FadinScheme().GetBytes("Path!?")).Generate<FadeIn>(); //??? generate fadein from file, scheme and in the domain logic processes
            fadeIn.transform.SetParent(command.Parent); //??? should set based on fill type

            //SetTheme(command.Theme);

            //var fadeInRecTransform = new GameObject("FadeIn").AddComponent<RectTransform>();
            //var img = fadeInRecTransform.gameObject.AddComponent<Image>();
            //var fadeIn = fadeInRecTransform.gameObject.AddComponent<FadeIn>();

            //view.SetId(command.ViewId);

            //fadeIn.View.SetColor(command.Color);
            //fadeIn.View.SetAlpha(command.Alpha); 
            //fadeIn.View.SetDuration(command.Duration);

            //fadeIn.GetComponent<RectTransform>().FillParent(command.Parent);
            //fadeIn.View.CanvasGroup.alpha = 0;

            //view.gameObject.SetActive(false);

            //var control = new GameObject("FadeInControl").AddComponent<FadeInControl>();
            //var model = new FadeInModel
            //{
            //    Duration = command.Duration,
            //    Color = command.Color,
            //    Alpha = command.Alpha
            //};
            //var view = new GameObject("FadeInView").AddComponent<FadeInView>();
            //view.SetId(command.TargetId);
            //control.SetUp(view, model);
        }
    }
    

    //namespace UILayer
    //{
    //    internal abstract class TransitionControl<TView, TModel> : 
    //        MonoBehaviour, IControl<TView, TModel>
    //        where TView: TransitionView 
    //        where TModel: TransitionModel
    //    {
    //        protected TView view;
    //        protected TModel model;

    //        public virtual void SetUp(TView view, TModel model)
    //        {
    //            this.view = view;
    //            this.model = model;
    //        }
    //    }

    //    internal abstract class TransitionModel : IModel
    //    {

    //    }

    //    internal class FadeInControl : TransitionControl<FadeInView, FadeInModel>
    //    {
    //        public override void SetUp(FadeInView view, FadeInModel model)
    //        {
    //            base.SetUp(view, model);
    //            view.SetColor(model.Color);
    //            view.SetAlpha(model.Alpha);
    //        }

    //        internal async Task BeginTra()
    //        {
    //            //await 
    //        }
    //    }
    //    internal class FadeInModel : TransitionModel
    //    {
    //        internal float Duration { get; set; } = 3;
    //        internal Color Color { get; set; } = Color.black;
    //        internal float Alpha { get; set; } = 1;
    //    }
    //}

}
