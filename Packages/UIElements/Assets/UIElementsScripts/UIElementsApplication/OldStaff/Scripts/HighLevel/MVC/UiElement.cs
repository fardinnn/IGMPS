﻿using System;
using System.Collections.Generic;
using BG.API;
using UnityEngine;

// ReSharper disable once CheckNamespace
namespace BG.UIElements 
{
    public abstract class UiElement : MonoBehaviour, IView
    {
        public Guid Id { get; set; }
        internal List<UiElement> ChildElements { get; set; }
        
        public virtual void Visit(IController<IView, IModel> controller)
        {
            foreach (var childElement in ChildElements) childElement.Visit(controller);
        }

        public void Dispose()
        {
            Destroy(this.gameObject);
        }
    }
}


