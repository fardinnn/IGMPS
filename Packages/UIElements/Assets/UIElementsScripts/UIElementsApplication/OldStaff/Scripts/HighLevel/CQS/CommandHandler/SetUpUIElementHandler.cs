﻿using System;
using System.Collections;
using System.Collections.Generic;
using BG.API;
using UnityEngine;

namespace BG.UIElements
{
    public abstract class SetUpUiElementHandler<T1, T2> : ICommandHandler<T1> where T1 : SetUpUiElement<T2> 
        where T2 : UiElement
    {
        public abstract void Handle(T1 command);
    }

}
