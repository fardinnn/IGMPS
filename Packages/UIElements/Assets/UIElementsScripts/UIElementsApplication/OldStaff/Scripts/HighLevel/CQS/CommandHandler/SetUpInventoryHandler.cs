﻿namespace BG.UIElements
{
    public abstract class SetUpInventoryHandler<T1, T2> : SetUpUiElementHandler<T1, T2>
        where T1 : SetUpInventory<T2> where T2: Inventory
    {

    }
}
