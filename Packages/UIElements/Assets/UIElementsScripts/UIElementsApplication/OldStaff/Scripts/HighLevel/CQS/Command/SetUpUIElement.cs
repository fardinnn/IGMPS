﻿using System;
using BG.API;
using BG.API.Abilities;
using UnityEngine;

namespace BG.UIElements
{
    public abstract class SetUpUiElement<T> : ICommand where T : UiElement
    {
        public Guid Id { get; set; }
        public Guid ElementId { get; set; }
        public Transform Parent { get; set; }
        //public ITheme Theme { get; set; }
    }
}