﻿using System.Collections.Generic;

namespace BG.UIElements.Serialize
{
    public class UiElementSchema
    {
        public List<IUiComponentSchema> Components { get; }
        public Dictionary<UiElementSchema, int> ChildrenOrdered { get; }
    }
}