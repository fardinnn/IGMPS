﻿public enum Adjustments
{
    FILL,
    STRETCH,
    FIT,
    INTACT,
    TILE,
}
