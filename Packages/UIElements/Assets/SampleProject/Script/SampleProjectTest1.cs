﻿using System;
using System.Linq;
using Fardin.UIElements;
using Fardin.UIElements.Transition;
using UnityEngine;

public class SampleProjectTest1 : MonoBehaviour
{
    [SerializeField] private RectTransform _parent;

    // Start is called before the first frame update
    void Start()
    {
        // Create UIElement under specified parent
        var id = Guid.NewGuid();

        new SetUpFadeInHandler().Handle(new SetUpFadeIn{
            ElementId = id,
            Parent = _parent,
            //fillType = Adjustments.STRETCH,
            //theme = targvetTheme
        });

        //_parent.GetComponentsInChildren<FadeIn>().Where(e => e.Id = id).FirstOrDefault();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
