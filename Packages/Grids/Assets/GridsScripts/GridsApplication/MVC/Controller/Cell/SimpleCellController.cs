﻿using BG.Grids.Domain;

namespace BG.Grids
{
    public class SimpleCellController : CellController<SimpleCellView, SimpleCellModel>
    {
        private SimpleCell _cell;

        public SimpleCellController(SimpleCellView view = null,
            SimpleCellModel model = null) : base(view, model)
        {
            var command = Model.ToCommand();
            command.CellObject = View.transform;
            new SetUpSimpleCellHandler().Handle(command);
            _cell = CellsManager.Get<SimpleCell>(command.SystemId); //???
        }

        public void Hover()
        {

        }

        public void Select()
        {

        }
    }
}
