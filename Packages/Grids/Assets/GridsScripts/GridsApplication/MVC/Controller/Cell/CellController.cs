﻿using BG.API;
using System;
using UnityEngine;

namespace BG.Grids
{

    public abstract class CellController<TView, TModel> : IController<TView, TModel> 
        where TView: CellView
        where TModel: CellModel
    {
        public TView View { get; }
        public TModel Model { get; }

        public Guid Id { get; } = Guid.NewGuid();


        public CellController(TView view = null, TModel model = null)
        {
            Model = model ?? CreateDefaultModel();
            View = view ?? CreateDefaultView();
            View.Controller = this;
        }

        protected virtual TModel CreateDefaultModel()
        {
            return (TModel)Activator.CreateInstance(typeof(TModel));
        }

        protected virtual TView CreateDefaultView()
        {
            var view = (Model.Parent ? Model.Parent.gameObject :
                (Model.Parent = new GameObject(typeof(TView).Name.Replace("View", "")).transform).gameObject)
                .AddComponent<TView>();
            return view;
        }

        public virtual void SetActivate(bool state)
        {
            View?.gameObject.SetActive(state);
        }

        public virtual void Dispose()
        {
            UnityEngine.Object.Destroy(View.gameObject);
        }
    }
}
