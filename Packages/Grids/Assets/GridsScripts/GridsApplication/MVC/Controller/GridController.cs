using BG.API;
using System;
using UnityEngine;

namespace BG.Grids
{
    public interface IGridController : IEntityController
    {
        public Guid SystemId { get; }
        Entity this[int i, int j] { get; }

    }

    public abstract class GridController<TView, TModel> : IEntityController<TView, TModel>, IGridController, IDisposable
        where TView : GridView
        where TModel : GridModel
    {
        public TView View { get; }
        public TModel Model { get; }

        public Guid Id { get; } = Guid.NewGuid();

        public abstract Guid SystemId { get; }

        public abstract Entity this[int i, int j] { get; }

        public GridController(TView view = null, TModel model = null)
        {
            Model = model ?? CreateDefaultModel();
            View = view ?? CreateDefaultView();
            View.Controller = this;
            if (Model.Parent == null)
                Model.Parent = View.transform;
        }

        protected virtual TModel CreateDefaultModel()
        {
            return (TModel)Activator.CreateInstance(typeof(TModel));
        }

        protected virtual TView CreateDefaultView()
        {
            var view = (Model.Parent!=null ? Model.Parent.gameObject :
                (Model.Parent = new GameObject(typeof(TView).Name.Replace("View", "")).transform).gameObject)
                .AddComponent<TView>();
            return view;
        }

        public virtual void SetActivate(bool state)
        {
            View?.gameObject.SetActive(state);
        }

        public virtual void Dispose()
        {
            UnityEngine.Object.Destroy(View.gameObject);
        }

        public abstract void Generate(Options options = null, bool activale = true);
    }
}
