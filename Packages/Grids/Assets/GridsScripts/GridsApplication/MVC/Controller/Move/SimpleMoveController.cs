﻿using BG.API;
using BG.Grids.Domain;
using System;
using System.Collections.Generic;

namespace BG.Grids
{
    public class SimpleMoveController : MoveController<SimpleMoveView, SimpleMoveModel>
    {
        private SimpleMoveSystem moveSystem;
        private MovesFactory movesFactory;
        public SimpleMoveController(SimpleMoveView view = null, SimpleMoveModel model = null) : base(view, model)
        {
            movesFactory = new MovesFactory();
        }

        public override IComponentSystem ProvideComponentSystem(Options options = null)
        {
            if (moveSystem==null)
            {
                //if (movesFactory.Has(Model.MoveSystemName))
                //{
                //    var factory = movesFactory.Get(Model.MoveSystemName);
                //    moveSystem = factory.Create<SimpleMoveSystem>();
                //}
                //else
                //{
                    Guid systemId = Guid.NewGuid();
                    var command = Model.ToCommand();
                    command.SystemId = systemId;
                    new SetupSimpleMoveSystemHandler().Handle(command);
                    moveSystem = MoveSystemsManager.Get<SimpleMoveSystem>(systemId);
                //}
            }
            options?.AlterOn(moveSystem);
            return moveSystem;
        }

    }
}
