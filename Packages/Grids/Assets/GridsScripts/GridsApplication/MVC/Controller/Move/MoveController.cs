using BG.API;
using BG.API.Abilities;
using BG.States;
using System;
using UnityEngine;

namespace BG.Grids
{
    public abstract class MoveController<TView, TModel> : IComponentController<TView, TModel>, IDisposable
        where TView : MoveView
        where TModel : MoveModel
    {
        public TView View { get; }
        public TModel Model { get; }

        public Guid Id { get; } = Guid.NewGuid();


        public MoveController(TView view = null, TModel model = null, CompositeStatesSet requiredStates = null)
        {
            Model = model ?? CreateDefaultModel();
            View = view ?? CreateDefaultView();
            this.requiredStates = requiredStates ?? new CompositeStatesSet();
            View.Controller = this;
        }

        protected CompositeStatesSet requiredStates;
        public event EventHandler<RequiredStatesEventArgs> RequiredStatesUpdated;
        public void AddStatesGroup(params Enum[] groupStates)
        {
            if (requiredStates.Contains(groupStates)) return;
            requiredStates.Add(groupStates);
            RequiredStatesUpdated?.Invoke(this, new RequiredStatesEventArgs { StatesSet = requiredStates });
        }

        protected virtual TModel CreateDefaultModel()
        {
            return (TModel)Activator.CreateInstance(typeof(TModel));
        }

        protected virtual TView CreateDefaultView()
        {
            var view = (Model.Parent ? Model.Parent.gameObject :
                (Model.Parent = new GameObject(typeof(TView).Name.Replace("View", "")).transform).gameObject)
                .AddComponent<TView>();
            return view;
        }

        public virtual void Dispose()
        {
            UnityEngine.Object.Destroy(View.gameObject);
        }

        public abstract IComponentSystem ProvideComponentSystem(Options options);
    }
}
