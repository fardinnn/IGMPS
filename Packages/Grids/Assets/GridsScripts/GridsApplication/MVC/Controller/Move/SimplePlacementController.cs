﻿using BG.API;
using BG.Grids.Domain;
using System;

namespace BG.Grids
{

    public class SimplePlacementController : PlacementController<SimplePlacementView, SimplePlacementModel>, IPlacementController
    {
        SimplePlacementSystem system;
        public SimplePlacementController(SimplePlacementView view = null, SimplePlacementModel model = null) : base(view, model)
        {

        }

        public override IComponentSystem ProvideComponentSystem(Options options = null)
        {
            if (system==null)
            {
                var command = Model.ToCommand();
                command.SystemId = Guid.NewGuid();
                new SetUpSimplePlacementSystemHandler().Handle(command);
                system = PlacementSystemsManager.Get<SimplePlacementSystem>(command.SystemId);
            }
            options?.AlterOn(system);
            return system;
        }
    }
}
