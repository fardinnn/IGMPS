﻿using BG.Grids.Domain;
using BG.API.Abilities;
using BG.API;
using System;

namespace BG.Grids
{
    public class SimpleRectangularGridController : GridController<SimpleRectangularGridView, SimpleRectangularGridModel>
    {
        private SimpleRectangularGrid _grid;

        public SimpleRectangularGridController(SimpleRectangularGridView view = null, 
            SimpleRectangularGridModel model = null): base(view,model)
        {
            var command = Model.ToCommand();
            command.GridObject = View.transform;
            new SetUpSimpleRectangularGridHandler().Handle(command);
            _grid = GridsManager.Get<SimpleRectangularGrid>(command.SystemId);

            if (Model.GenerateOnBegin)
                _grid.Generate();
        }

        public override Entity this[int i, int j]
        {
            get
            {
                if (_grid == null) return null;
                return _grid[i, j];
            }
        }

        public override Guid SystemId => _grid == null ? Guid.Empty : _grid.Id;

        public override void Generate(Options options = null, bool activate = true)
        {
            _grid.Generate();
        }

        public void OnCellShapeChange(object o, ChangeEventArgs e)
        {
            // _grid.UpdateGrid();
        }
    }


}
