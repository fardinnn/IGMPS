﻿using UnityEngine;
using BG.API;
using System;

namespace BG.Grids
{
    public interface IGridView: IView { }
    public abstract class GridView : MonoBehaviour, IGridView
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public IController<IView, IModel> Controller { get; internal set; }
        public void Dispose()
        {
            Destroy(gameObject);
        }
    }
}
