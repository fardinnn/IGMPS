﻿using BG.API;
using BG.Grids.Domain;
using UnityEngine;

namespace BG.Grids
{
    public abstract class CellModel : IModel
    {
        public Transform Parent { get; set; }

    }
}
