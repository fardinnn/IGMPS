﻿using BG.Grids.Domain;
using UnityEngine;

namespace BG.Grids
{
    public class SimpleCellModel : CellModel
    {
        public Vector2 Size { get; set; }
        public Color Color { get; set; }
        //public ICellTemplate Shape { get; set; }

        internal SimpleCellParameters ToCommand()
        {
            return new SimpleCellParameters
            {
                Size = Size
            };
        }
    }
}
