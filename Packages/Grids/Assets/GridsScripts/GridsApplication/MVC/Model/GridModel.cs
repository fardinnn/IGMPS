﻿using BG.API;
using UnityEngine;

namespace BG.Grids
{
    public interface IGridModel : IModel { }

    public abstract class GridModel : IGridModel
    {
        public Transform Parent { get; set; }
    }
}
