﻿using BG.API;
using BG.Grids.Domain;

namespace BG.Grids
{
    public interface IMoveRuleModel : IModel
    {
        IMoveRule ToCommand();
    }
}
