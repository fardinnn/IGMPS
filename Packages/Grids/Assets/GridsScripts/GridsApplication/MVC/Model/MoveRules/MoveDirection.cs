﻿namespace BG.Grids
{
    public enum MoveDirection
    {
        Forward,
        Backward,
        Left,
        Right,
        ForwardLeft,
        ForwardRight,
        BackwardLeft,
        BackwardRight
    }
}
