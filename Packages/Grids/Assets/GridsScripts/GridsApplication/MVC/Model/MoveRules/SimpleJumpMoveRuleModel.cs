﻿using BG.Grids.Domain;
using System.Collections.Generic;
using UnityEngine;

namespace BG.Grids
{
    public class SimpleJumpMoveRuleModel : IMoveRuleModel
    {
        public List<Vector2Int> PotentialMoves { get; set; }

        public IMoveRule ToCommand()
        {
            return new JumpMoveRuleAcceptor { PotentialMoves = PotentialMoves }; // ??????
        }
    }
}
