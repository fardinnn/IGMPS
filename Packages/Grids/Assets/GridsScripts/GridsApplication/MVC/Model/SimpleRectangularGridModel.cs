﻿using BG.API;
using UnityEngine;

namespace BG.Grids
{
    public class SimpleRectangularGridModel : GridModel
    {
        public Vector2Int Size { get; set; }
        public IGridAlgorithmModel GenerateAlgorithm { get; set; }
        public bool GenerateOnBegin { get; set; }
        public bool CellsAffectContents { get; set; }
        public bool CellsGetAffectedByContents { get; set; }
    }
}
