﻿using BG.API;
using BG.API.Abilities;
using BG.Grids.Domain;
using BG.States;
using System.Collections.Generic;

namespace BG.Grids
{
    public class SimplePlacementModel : PlacementModel
    {
        public PlacementStateSet PlacementStates { get; set; }
        public CompositeStatesSet ReplacementStates { get; set; }
        public CompositeStatesSet UnplacingStates { get; set; }
        public GridPositionSet Positions { get; set; }
        public HashSet<StateGroup> CellConstraints { get; set; }

        internal SimplePlacementParameters ToCommand()
        {
            return new SimplePlacementParameters
            {
                PlacementStates = PlacementStates,
                ReplacementStates = ReplacementStates,
                UnplacingStates = UnplacingStates,
                GridSystemId = GridSysetmId,
                Positions = Positions,
                Constraints = CellConstraints
            };
        }
    }

    public class SimpleMoveModel : MoveModel
    {
        public string MoveSystemName { get; set; }
        public CompositeStatesSet CalculateStates { get; set; }
        public CompositeStatesSet SimpleMoveStates { get; set; }
        public CompositeStatesSet ActivatePotentialMoves { get; set; }
        public CompositeStatesSet MoveToCellStates { get; set; }
        public Dictionary<StateGroup, HashSet<IMoveRuleFactory>> StateBasedMoveRules { get; set; }
        public HashSet<StateGroup> StatesToActivatePotentialMove { get; set; }
        public HashSet<StateGroup> StatesToDeactivatePotentialMove { get; set; }
        public IComponentSystem PlacementSystem { get; set; }

        internal SimpleMoveParameters ToCommand()
        {
            var newStateBasedMoveRules = new Dictionary<StateGroup, HashSet<IMoveRule>>();
            foreach (var item in StateBasedMoveRules)
            {
                newStateBasedMoveRules.Add(item.Key, new HashSet<IMoveRule>());
                foreach (var rule in item.Value)
                {
                    newStateBasedMoveRules[item.Key].Add(rule.Create());
                }
            }
            return new SimpleMoveParameters
            {
                CalculateStates = CalculateStates,
                SimpleMoveStates = SimpleMoveStates,
                GridSystemId = GridSysetmId,
                StateBasedMoveRules = newStateBasedMoveRules,
                StatesToActivatePotentialMove = StatesToActivatePotentialMove,
                StatesToDeactivatePotentialMove = StatesToDeactivatePotentialMove,
                PlacementSystem = (PlacementSystem is IPlacementSystem ps) ? ps : null
            };
        }
    }
}
