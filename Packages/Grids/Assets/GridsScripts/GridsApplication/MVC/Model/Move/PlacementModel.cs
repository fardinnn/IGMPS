﻿using BG.API;
using System;
using UnityEngine;

namespace BG.Grids
{
    public abstract class PlacementModel : IModel
    {
        public Transform Parent { get; set; }
        public Options DefaultOptions { get; set; }
        public Guid GridSysetmId { get; set; }
    }
}
