﻿using BG.API;
using BG.Grids.Domain;
using UnityEngine;

namespace BG.Grids
{
    public interface IGridAlgorithmModel
    {
        IGridAlgorithmAcceptor ToCommand();
        Transform Parent { get; set; }
    }
}
