﻿using BG.API;
using BG.Grids.Domain;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace BG.Grids
{
    public class CheckerFillAlgorithmModel : IGridAlgorithmModel
    {
        public Dictionary<HashSet<Enum>, IComponentSystem> Cell1Rules;

        public GameEntitiesVisitors<IComponentSystem> Cell1Systems { get; set; }
        public GameEntitiesVisitors<IComponentSystem> Cell2Systems { get; set; }
        public Vector2 CellSize { get; set; }
        public Transform Parent { get; set; }


        public IGridAlgorithmAcceptor ToCommand()
        {
            return new CheckerFillAlgorithmAcceptor
            {
                Cell1Systems = Cell1Systems,
                Cell2Systems = Cell2Systems,
                CellSize = CellSize,
                Parent = Parent
            };
        }
    }
}
