﻿using BG.API;
using BG.Grids.Domain;
using System.Collections.Generic;
using UnityEngine;

namespace BG.Grids
{
    public class SimpleFillAlgorithmModel : IGridAlgorithmModel
    {
        public GameEntitiesVisitors<IComponentSystem> CellComponents { get; internal set; }
        public Vector2 CellSize { get; set; }
        public Transform Parent { get; set; }

        public IGridAlgorithmAcceptor ToCommand()
        {
            return new SimpleFillAlgorithmAcceptor
            {
                CellComponents = this.CellComponents,
                CellSize = this.CellSize,
                Parent = Parent
            };
        }
    }


}
