﻿using BG.Grids.Domain;

namespace BG.Grids
{
    public static class ModelToCommandExtensions
    {
        public static SimpleRectangularGridParameters ToCommand(this SimpleRectangularGridModel model)
        {
            if (model.GenerateAlgorithm.Parent == null) model.GenerateAlgorithm.Parent = model.Parent;
            return new SimpleRectangularGridParameters
            {
                Algorithm = model.GenerateAlgorithm.ToCommand(),
                Size = model.Size,
                CellsAffectContents = model.CellsAffectContents,
                CellsGetAffectedByContents = model.CellsGetAffectedByContents
            };
        }
    }
}
