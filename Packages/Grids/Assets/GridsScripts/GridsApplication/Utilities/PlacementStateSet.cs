﻿using BG.API.Abilities;
using BG.States;
using System.Collections.Generic;

namespace BG.Grids
{
    public class PlacementStateSet : Dictionary<StateGroup, GridPositionSet>
    {
        public void Add(GridPositionSet positions, params StateGroup[] states)
        {
            for (int i = 0; i < states.Length; i++)
                Add(states[i], positions);
        }
    }
}
