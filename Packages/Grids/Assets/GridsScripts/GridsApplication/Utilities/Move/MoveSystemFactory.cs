﻿using BG.Grids.Domain;
using System.Collections.Generic;

namespace BG.Grids
{
    internal abstract class MoveSystemFactory
    {
        public abstract T Create<T>() where T : IMoveSystem;

        protected abstract IMoveRule CreateState(BeginState state);
        protected abstract IMoveRule CreateState(IdleState state);
        protected abstract IMoveRule CreateState(FreezeState state);
    }
}
