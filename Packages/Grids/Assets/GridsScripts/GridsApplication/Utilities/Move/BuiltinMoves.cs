﻿using BG.Grids.Domain;
using System.Collections.Generic;
using UnityEngine;

namespace BG.Grids
{
    public struct BuiltinMoves
    {
        public const string Pawn = "BuiltinPawn";
        public const string Rook = "BuiltinRook";

        public static readonly IMoveRuleFactory OneForward = new OneForwardMove();
        public static readonly IMoveRuleFactory MaxTwoForward = new MaxTwoForwardMove();
        public static readonly IMoveRuleFactory OneDiagonalForward = new OneDiagonalForwardMove();
        public static readonly IMoveRuleFactory DiagonalUnlimited = new DiagonalUnlimitedMove();
        public static readonly IMoveRuleFactory StraightUnlimited = new StraightUnlimitedMove();
        public static readonly IMoveRuleFactory KnightJump = new KnightJumpMove();
        public static readonly IMoveRuleFactory OneEveryDirection = new OneEveryDirectionMove();
    }

    public interface IMoveRuleFactory
    {
        IMoveRule Create();
    }

    public struct OneForwardMove : IMoveRuleFactory
    {
        private static readonly IMoveRule oneForward = new SimpleDirectedMoveRuleAcceptor
        {
            DirectionSteps = new Dictionary<Vector2Int, int> { { new Vector2Int(1, 0), 1 } }
        };

        public IMoveRule Create()
        {
            return oneForward;
        }
    }
    public struct MaxTwoForwardMove : IMoveRuleFactory
    {
        private static readonly IMoveRule maxTwoForward = new SimpleDirectedMoveRuleAcceptor
        {
            DirectionSteps = new Dictionary<Vector2Int, int> { { new Vector2Int(1, 0), 2 } }
        };

        public IMoveRule Create()
        {
            return maxTwoForward;
        }
    }
    public struct OneDiagonalForwardMove : IMoveRuleFactory
    {
        private static readonly IMoveRule oneDiagonalForward = new SimpleDirectedMoveRuleAcceptor
        {
            DirectionSteps = new Dictionary<Vector2Int, int> { { new Vector2Int(1, 1), 1 }, { new Vector2Int(1, -1), 1 } }
        };

        public IMoveRule Create()
        {
            return oneDiagonalForward;
        }
    }
    public struct DiagonalUnlimitedMove : IMoveRuleFactory
    {
        private static readonly IMoveRule diagonalUnlimited = new SimpleDirectedMoveRuleAcceptor
        {
            DirectionSteps = new Dictionary<Vector2Int, int> {
                { new Vector2Int(1, 1), int.MaxValue },
                { new Vector2Int(-1, 1), int.MaxValue },
                { new Vector2Int(1, -1), int.MaxValue },
                { new Vector2Int(-1, -1), int.MaxValue }
            }
        };

        public IMoveRule Create()
        {
            return diagonalUnlimited;
        }
    }
    public struct StraightUnlimitedMove : IMoveRuleFactory
    {
        private static readonly IMoveRule straightUnlimited = new SimpleDirectedMoveRuleAcceptor
        {
            DirectionSteps = new Dictionary<Vector2Int, int> {
                { new Vector2Int(1, 0), int.MaxValue },
                { new Vector2Int(-1, 0), int.MaxValue },
                { new Vector2Int(0, 1), int.MaxValue },
                { new Vector2Int(0, -1), int.MaxValue }
            }
        };

        public IMoveRule Create()
        {
            return straightUnlimited;
        }
    }
    public struct KnightJumpMove : IMoveRuleFactory
    {
        private static readonly IMoveRule knightJump = new JumpMoveRuleAcceptor
        {
            PotentialMoves = new List<Vector2Int>
            {
                new Vector2Int(2, 1),
                new Vector2Int(2, -1),
                new Vector2Int(1, 2),
                new Vector2Int(1, -2),
                new Vector2Int(-1, 2),
                new Vector2Int(-1, -2),
                new Vector2Int(-2, 1),
                new Vector2Int(-2, -1)
            },
        };

        public IMoveRule Create()
        {
            return knightJump;
        }
    }
    public struct OneEveryDirectionMove : IMoveRuleFactory
    {
        private static readonly IMoveRule oneEveryDirection = new SimpleDirectedMoveRuleAcceptor
        {
            DirectionSteps = new Dictionary<Vector2Int, int>
            {
                {new Vector2Int(0, 1), 1},
                {new Vector2Int(1, 1), 1},
                {new Vector2Int(1, 0), 1},
                {new Vector2Int(1, -1), 1},
                {new Vector2Int(0, -1), 1},
                {new Vector2Int(-1, -1), 1},
                {new Vector2Int(-1, 0), 1},
                {new Vector2Int(-1, 1), 1}
            }
        };

        public IMoveRule Create()
        {
            return oneEveryDirection;
        }
    }
    
}
