﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace BG.Grids
{
    internal class MovesFactory
    {
        private static Dictionary<string, Type> factories;
        private static Dictionary<string, MoveSystemFactory> cachedFactories;
        internal MovesFactory()
        {
            if (factories==null)
            {
                factories = new Dictionary<string, Type>();
                var type = typeof(MoveSystemFactory);
                var factoryTypes = type.Assembly.GetTypes().Where(t => t.IsAssignableFrom(type) && !t.IsAbstract && !t.IsInterface);
                foreach (var factory in factoryTypes)
                {
                    factories.Add(factory.Name.Replace("factory", ""), factory);
                }
            }
        }

        internal bool Has(string typeName)
        {
            Debug.Log(typeName);
            Debug.Log(factories.Count);
            return factories.ContainsKey(typeName);
        }

        internal MoveSystemFactory Get(string typeName)
        {
            if (!Has(typeName)) return null;
            if (cachedFactories.ContainsKey(typeName)) return cachedFactories[typeName];
            var newFactory = Activator.CreateInstance(factories[typeName]) as MoveSystemFactory;
            cachedFactories.Add(typeName, newFactory);
            return newFactory;
        }
    }
}
