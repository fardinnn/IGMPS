﻿using BG.Grids.Domain;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace BG.Grids
{
    internal class BuiltinPawnFactory : MoveSystemFactory
    {
        public override T Create<T>()
        {
            ConditionalMoves pawnMoves = new ConditionalMoves();
            pawnMoves.Add(new IdleState(), CreateState(new IdleState()));
            pawnMoves.Add(new BeginState(), CreateState(new BeginState()));
            pawnMoves.Add(new BeginState(), CreateState(new BeginState()));

            var systemId = Guid.NewGuid();
            new SetupSimpleMoveSystemHandler().Handle(new SimpleMoveParameters
            {
                SystemId = systemId,
                Moves = pawnMoves
            });
            throw new Exception();
            //return MoveSystemsManager.Get<SimpleMoveSystem>(systemId) as T;
        }

        protected override IMoveRule CreateState(IdleState state)
        {
            return BuiltinMoves.OneForward.Create();
            //return new SimpleDirectedMoveRuleAcceptor
            //{
            //    DirectionSteps = new List<IMove>
            //    {
            //        new SimpleDirectedMove
            //        {
            //            Direction = new Vector2Int(0, 1),
            //            StepCount = 1
            //        }
            //    }
            //};
        }

        protected override IMoveRule CreateState(BeginState state)
        {
            return BuiltinMoves.MaxTwoForward.Create();
            //return new SimpleDirectedMoveRuleAcceptor
            //{
            //    DirectionSteps = new List<IMove>
            //    {
            //        new SimpleDirectedMove
            //        {
            //            Direction = Domain.MoveDirection.Forward,
            //            StepCount = 2
            //        }
            //    }
            //};
        }

        protected override IMoveRule CreateState(FreezeState state)
        {
            return null;
            //return new SimpleDirectedMoveRuleAcceptor
            //{
            //    DirectionSteps = new List<IMove>
            //    {
            //        new SimpleDirectedMove
            //        {
            //            Direction = Domain.MoveDirection.Forward,
            //            StepCount = 0
            //        }
            //    }
            //};
        }
    }
}
