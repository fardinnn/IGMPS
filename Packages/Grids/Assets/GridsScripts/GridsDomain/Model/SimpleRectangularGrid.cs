using System;
using System.Collections.Generic;
using UnityEngine;

namespace BG.Grids.Domain
{
    public class SimpleRectangularGrid : GridSystem
    {
        public override Vector2Int Size => new Vector2Int(cells.GetLength(0), cells.GetLength(1));

        private Cell[,] cells;

        public override Cell this[int i, int j]
        {
            get
            {
                if (cells == null) throw new ArgumentNullException("Board 'cells' container is null!");
                if (cells.GetLength(0) < i) throw new ArgumentOutOfRangeException($"Cells has less rows than provided 'i={i}'");
                if (cells.GetLength(1) < j) throw new ArgumentOutOfRangeException($"Cells has less columns than provided 'j={j}'");
                return cells[i, j];
            }
        }

        private readonly SimpleRectangularGridParameters parameters;

        //public Dictionary<HashSet<Enum>, IGameEntitiesVisitor> Cell1Rules;

        private HashSet<Enum> States;

        public SimpleRectangularGrid(SimpleRectangularGridParameters parameters)
        {
            //Cell1Rules = new Dictionary<HashSet<Enum>, IGameEntitiesVisitor>();
            States = new HashSet<Enum>();
            this.parameters = parameters;
        }
        IGridAlgorithm gridAlgorithm;

        public override void Generate() /////////////////////////
        {
            gridAlgorithm = new SimpleRectangularAlgorithmVisitor(
                parameters.Size.y,
                parameters.Size.x,
                parameters.CellsAffectContents,
                parameters.CellsGetAffectedByContents
                );
            parameters.Algorithm.Accept(gridAlgorithm);
            cells = gridAlgorithm.Cells;
            foreach (Cell cell in cells)
                cell.GridSystem = this;
            //Cell1Rules
        }
    }
}
