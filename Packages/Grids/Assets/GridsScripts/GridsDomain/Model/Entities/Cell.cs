
using BG.API;
using BG.API.Abilities;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace BG.Grids.Domain
{
    public class Cell : ContainerEntity, IActivable
    {
        public int Row { get; set; }
        public int Column { get; set; }

        public Vector2 Size { get; set; }
        public GridSystem GridSystem { get; set; }
        protected Dictionary<Entity, Vector2Int> entitiesDirection = new Dictionary<Entity, Vector2Int>();
        public Vector2Int Direction { get; set; } = new Vector2Int(0, 1);

        public void Add(Entity entity, Vector2Int entityDirection)
        {
            base.Add(entity);
            entitiesDirection.Add(entity, entityDirection);
        }

        public void ChangeDirection(Entity entity, Vector2Int entityDirection)
        {
            if (Entities.Contains(entity))
            {
                if (entitiesDirection.ContainsKey(entity)) entitiesDirection[entity] = entityDirection;
                else entitiesDirection.Add(entity, entityDirection);
            }
        }

        public Vector2Int GetDirection(Entity entity)
        {
            return entitiesDirection.ContainsKey(entity) ? entitiesDirection[entity] : new Vector2Int(0, 1);
        }

        //public Dictionary<Type, HashSet<IAbility>> Abilities { get; set; }

        //public void UpdateOptions(Options options)
        //{
        //    foreach (var item in Abilities)
        //    {
        //        options.AlterOn(Abilities);
        //    }
        //}

        public event EventHandler<ActivateEventArgs> Activated;


        public void Activate(object sender, ActivateEventArgs e)
        {
            Activated?.Invoke(this, new ActivateEventArgs { });
        }

        public Vector2Int GridPosition => new Vector2Int(Row, Column);
    }
}
