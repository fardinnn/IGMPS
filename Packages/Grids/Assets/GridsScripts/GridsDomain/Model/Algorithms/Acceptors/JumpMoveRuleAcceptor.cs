﻿using System.Collections.Generic;
using UnityEngine;

namespace BG.Grids.Domain
{
    public class JumpMoveRuleAcceptor : IMoveRule
    {
        public List<Vector2Int> PotentialMoves { get; set; }
        public void Accept(IMoveAlgorithm visitor)
        {
            visitor.Visit(this);
        }
    }
}
