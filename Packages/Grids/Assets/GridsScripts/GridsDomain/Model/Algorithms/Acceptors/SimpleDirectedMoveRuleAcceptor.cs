﻿using System.Collections.Generic;
using UnityEngine;

namespace BG.Grids.Domain
{
    public class SimpleDirectedMoveRuleAcceptor : IMoveRule
    {
        public Dictionary<Vector2Int, int> DirectionSteps { get; set; }

        public void Accept(IMoveAlgorithm visitor)
        {
            visitor.Visit(this);
        }
    }
}
