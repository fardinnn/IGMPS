﻿using BG.API;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace BG.Grids.Domain
{
    public class CheckerFillAlgorithmAcceptor : IGridAlgorithmAcceptor
    {
        public GameEntitiesVisitors<IComponentSystem> Cell1Systems { get; set; }
        public GameEntitiesVisitors<IComponentSystem> Cell2Systems { get; set; }
        public Vector2 CellSize { get; set; }
        public Transform Parent { get; set; }

        public void Accept(IGridAlgorithm visitor)
        {
            visitor.Visit(this);
        }
    }
}
