﻿using BG.API;

namespace BG.Grids.Domain
{
    public interface IGridAlgorithmAcceptor : IAcceptor<IGridAlgorithm> { }
}
