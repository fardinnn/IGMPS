﻿using BG.States;
using UnityEngine;
using BG.Extensions;
using System.Collections.Generic;
using BG.API;

namespace BG.Grids.Domain
{
    public class ActivatePotentialMoveCells : IMoveAlgorithm
    {
        private readonly GridSystem grid;
        private readonly Vector2Int activeCell;
        private readonly Vector2Int pieceDirection;

        private HashSet<Cell> readyCells;

        // Lightweight pattern event args
        private PlaceChagedEventArgs placeChagedEventArgs = new PlaceChagedEventArgs();

        public ActivatePotentialMoveCells(GridSystem grid, Vector2Int activeCell, Vector2Int pieceDirection)
        {
            this.grid = grid;
            this.activeCell = activeCell;
            this.pieceDirection = pieceDirection;
            readyCells = new HashSet<Cell>();
        }

        public void Dispose()
        {
            foreach (var readyCell in readyCells)
            {
                if(readyCell.IsStateActive(PlaceState.Ready)) readyCell.SetState(PlaceState.NotReady, placeChagedEventArgs);
            }
            readyCells.Clear();
        }

        public void Visit(SimpleDirectedMoveRuleAcceptor acceptor)
        {
            foreach (var move in acceptor.DirectionSteps)
            {
                Vector2Int worldDirection = move.Key.LocalToWorld(pieceDirection);

                for (int i = 1; i <= move.Value; i++)
                {
                    Vector2Int cellPos = new Vector2Int(activeCell.x + worldDirection.x * i, activeCell.y + worldDirection.y * i);
                    if (cellPos.IsLimitedTo(new Vector2Int(grid.Size.x-1,grid.Size.y-1) ,inclusive: true))
                    {
                        var targetCell = grid[cellPos.x, cellPos.y];
                        if (!(targetCell.IsStateActive(PlaceState.Ready)|| targetCell.IsStateActive(PlaceState.IsOccupying)))
                        {
                            targetCell.SetState(PlaceState.Ready, placeChagedEventArgs);
                            readyCells.Add(targetCell);
                        }
                    }
                    else break;
                }
            }
        }

        public void Visit(JumpMoveRuleAcceptor acceptor)
        {
            foreach (Vector2Int move in acceptor.PotentialMoves)
            {
                Vector2Int worldMove = move.LocalToWorld(pieceDirection);
                var targetPos = new Vector2Int(activeCell.x + worldMove.x, activeCell.y + worldMove.y);
                if (targetPos.IsLimitedTo(new Vector2Int(grid.Size.x - 1, grid.Size.y - 1), inclusive: true))
                {
                    Cell cell = grid[targetPos.x , targetPos.y];
                    cell.SetState(PlaceState.Ready, placeChagedEventArgs);
                }
            }
            
        }
    }
}
