﻿using BG.API;
using System;

namespace BG.Grids.Domain
{
    public interface IMoveAlgorithm : IVisitor, IDisposable
    {
        void Visit(SimpleDirectedMoveRuleAcceptor acceptor);
        void Visit(JumpMoveRuleAcceptor acceptor);
    }
}
