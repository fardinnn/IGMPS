﻿using UnityEngine;

namespace BG.Grids.Domain
{
    public class DeactivatePotentialMoveCells : IMoveAlgorithm
    {
        private readonly GridSystem _grid;
        private readonly Vector2Int _activeCell;

        public DeactivatePotentialMoveCells(GridSystem grid, Vector2Int activeCell)
        {
            _grid = grid;
            _activeCell = activeCell;
        }

        public void Dispose()
        {
            throw new System.NotImplementedException();
        }

        public void Visit(SimpleDirectedMoveRuleAcceptor acceptor)
        {
            //_grid.HighlightCell(Vector2Int cellPosition);
            //_grid.HighlightCells(List<Vector2Int> cellPosition);
            //_grid.LocateCell();
        }

        public void Visit(JumpMoveRuleAcceptor acceptor)
        {
            throw new System.NotImplementedException();
        }
    }
}
