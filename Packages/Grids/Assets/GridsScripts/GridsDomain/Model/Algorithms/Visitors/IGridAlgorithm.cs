using BG.API;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BG.Grids.Domain
{
    public interface IGridAlgorithm : IVisitor
    {
        void Visit(SimpleFillAlgorithmAcceptor acceptor);
        void Visit(CheckerFillAlgorithmAcceptor acceptor);
        Cell[,] Cells { get; set; }
    }
}
