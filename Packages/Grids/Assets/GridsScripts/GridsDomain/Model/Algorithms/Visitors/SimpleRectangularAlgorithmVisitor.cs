﻿using UnityEngine;
using System.Collections.Generic;
using BG.Extensions;
using BG.API;
using System.Collections;
using System.Linq;

namespace BG.Grids.Domain
{


    public class SimpleRectangularAlgorithmVisitor : IGridAlgorithm
    {
        private int _row;
        private int _column;

        public Cell[,] Cells { get; set; }


        private readonly bool cellsGetAffectedByContents;
        private readonly bool cellsAffectContents;
        
        public SimpleRectangularAlgorithmVisitor(int row, int column, bool cellsAffectContents, bool cellsGetAffectedByContents)
        {
            _row = row;
            _column = column;
            Cells = new Cell[row, column];
            this.cellsGetAffectedByContents = cellsGetAffectedByContents;
            this.cellsAffectContents = cellsAffectContents;
        }

        public List<Task> coroutines = new List<Task>();
        public void Visit(SimpleFillAlgorithmAcceptor acceptor)
        {
            List<Entity> cells = new List<Entity>();
            for (int i = 0; i < _row; i++)
            {
                var rowPosition = (i - ((_row - 1) / 2.0f)) * acceptor.CellSize.x;
                for (int j = 0; j < _column; j++)
                {
                    Cells[i, j] = new GameObject($"{i}-{j}").AddComponent<Cell>();
                    Cells[i, j].AffectContents = cellsAffectContents;
                    Cells[i, j].GetAffectedByContents = cellsGetAffectedByContents;

                    Cells[i, j].transform.SetParent(acceptor.Parent);
                    Cells[i, j].transform.position = new Vector3
                        ((j - ((_column - 1) / 2.0f)) * acceptor.CellSize.y, 0, rowPosition);

                    Cells[i, j].Row = i;
                    Cells[i, j].Column = j;
                    Cells[i, j].Size = acceptor.CellSize;

                    //Cells[i, j].SetInitialStates(acceptor.CellComponents.InitialStates);
                    cells.Add(Cells[i, j]);
                }
            }

            foreach (var stateSetter in acceptor.CellComponents.InitialStates)
            {
                stateSetter.SetState(cells);
            } 
            //acceptor.CellComponents.ForEach(c => c.Visit(cells));

            coroutines.Add(new Task(VisitComponents(acceptor.CellComponents, cells)));
            StaticBatchingUtility.Combine(cells.Select(c => c.gameObject).ToArray(), acceptor.Parent.gameObject);
            acceptor.Parent.gameObject.SetActive(false);
            acceptor.Parent.gameObject.SetActive(true);
        }

        public void Visit(CheckerFillAlgorithmAcceptor acceptor)
        {
            int[] counter = new int[] { 0, 0 };

            List<Entity> cells1 = new List<Entity>();
            List<Entity> cells2 = new List<Entity>();

            for (int i = 0; i < _row; i++)
            {
                var rowPosition = (i - ((_row - 1) / 2.0f)) * acceptor.CellSize.y;
                for (int j = 0; j < _column; j++)
                {
                    Cells[i, j] = new GameObject($"{i}-{j}").AddComponent<Cell>();
                    Cells[i, j].AffectContents = cellsAffectContents;
                    Cells[i, j].GetAffectedByContents = cellsGetAffectedByContents;

                    Cells[i, j].transform.SetParent(acceptor.Parent);
                    Cells[i, j].transform.position = new Vector3
                        ((j - ((_column - 1) / 2.0f)) * acceptor.CellSize.y, 0, rowPosition);

                    Cells[i, j].Row = i;
                    Cells[i, j].Column = j;
                    Cells[i, j].Size = acceptor.CellSize;

                    if ((i + j) % 2 == 0)
                    {
                        //Cells[i, j].SetInitialStates(acceptor.Cell1Systems.InitialStates);
                        cells1.Add(Cells[i, j]);
                    }
                    else
                    {
                        //Cells[i, j].SetInitialStates(acceptor.Cell2Systems.InitialStates);
                        cells2.Add(Cells[i, j]);
                    }
                }
            }

            foreach (var stateSetter in acceptor.Cell1Systems.InitialStates)
            {
                stateSetter.SetState(cells1);
            }

            foreach (var stateSetter in acceptor.Cell2Systems.InitialStates)
            {
                stateSetter.SetState(cells2);
            }

            coroutines.Add(new Task(VisitComponents(acceptor.Cell1Systems, cells1)));
            coroutines.Add(new Task(VisitComponents(acceptor.Cell2Systems, cells2)));
        }



        IEnumerator VisitComponents(GameEntitiesVisitors<IComponentSystem> components, List<Entity> entities)
        {
            foreach (var component in components)
            {
                yield return new WaitUntil(() => component.IsReady);
                component.Visit(entities);
            }
        }
    }
}
