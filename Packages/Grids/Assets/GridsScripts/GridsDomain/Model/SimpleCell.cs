﻿using BG.API.Abilities;
using System;

namespace BG.Grids.Domain
{
    public class SimpleCell : ICellSystem, IActivable
    {
        public Guid Id { get; set; }

        private readonly SimpleCellParameters _parameters;

        public event EventHandler<ActivateEventArgs> Activated;

        public SimpleCell(SimpleCellParameters parameters)
        {
            _parameters = parameters;
        }

        public void Activate(object sender, ActivateEventArgs e)
        {
            Activated?.Invoke(this, new ActivateEventArgs { });
        }
    }
}
