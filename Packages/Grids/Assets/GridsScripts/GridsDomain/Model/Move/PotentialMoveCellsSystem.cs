﻿using BG.API;
using System;
using System.Collections.Generic;
using UnityEngine;

using BG.Extensions;
using System.Collections;
using BG.States;

namespace BG.Grids.Domain
{
    internal class PotentialMoveCellsSystem : IComponentSystem
    {
        public bool IsReady => throw new NotImplementedException();

        public Guid Id { get; set; }

        HashSet<Entity> containeeEntities;
        HashSet<Cell> targetCells;
        CompositeStatesSet MoveStates;

        public PotentialMoveCellsSystem()
        {
            containeeEntities = new HashSet<Entity>();
            targetCells = new HashSet<Cell>();
        }

        public void Visit(List<Entity> entities)
        {
            entities.ForEach(e =>
            {
                if(e is Cell cell)
                {
                    targetCells.Add(cell);
                    foreach (var stateMove in MoveStates)
                    {
                        cell.AddStateActivateAction(stateMove, MoveEntitiesToTheCell);
                    }
                }
            });
        }

        Task moveTransition;
        public void MoveEntitiesToTheCell(StateGroup stateGroup, IStateChangedEventArgs e)
        {
            if (moveTransition.Running)
            {
                Debug.LogError("moveTransition is running");
                return;
            }
            if(e.Entity is Cell cell)
                moveTransition = new Task(TransitEntitiesTo(cell));
        }

        IEnumerator TransitEntitiesTo(Cell cell)
        {
            bool allInPlace = false;

            float nearLimit = Mathf.Min(cell.Size.x, cell.Size.y) / 20.0f;
            float maxDistanceDelta = nearLimit / 10.0f;
            nearLimit *= nearLimit;

            while (!allInPlace)
            {
                allInPlace = true;
                containeeEntities.ForEach(e =>
                {
                    if ((e.transform.position - cell.transform.position).sqrMagnitude > nearLimit)
                    {
                        e.transform.position = Vector3.MoveTowards(e.transform.position, cell.transform.position, maxDistanceDelta);
                        allInPlace = false;
                    }
                });
                yield return new WaitForFixedUpdate();
            }
        }

        public void Dispose()
        {
            targetCells.ForEach(e =>
            {
                foreach (var stateGroup in MoveStates)
                    e.Dispose(stateGroup, MoveEntitiesToTheCell);
            });
        }
    }
}