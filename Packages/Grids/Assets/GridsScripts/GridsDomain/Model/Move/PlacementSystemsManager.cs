﻿using BG.API;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace BG.Grids.Domain
{
    public class PlacementSystemsManager : Singleton<PlacementSystemsManager>
    {
        private static Dictionary<Guid, IPlacementSystem> placementSystems;

        protected override void Awake()
        {
            IsDestroyOnLoad = true;
            base.Awake();

            placementSystems = new Dictionary<Guid, IPlacementSystem>();
        }

        public void Add(IPlacementSystem placementSystem)
        {
            if (placementSystem.Id == default) throw new ArgumentException("placementSystem's id in not assigned");
            placementSystems.Add(placementSystem.Id, placementSystem);
        }


        public static IPlacementSystem Get(Guid id)
        {
            if (placementSystems == null) throw new NullReferenceException("placementSystems can not be null!");
            if (!placementSystems.ContainsKey(id))
                Debug.LogError("There is no placement system with the provided id!");
            return placementSystems[id];
        }

        public static T Get<T>(Guid id) where T : class, IPlacementSystem
        {
            if (placementSystems == null) throw new NullReferenceException("placementSystems can not be null!");
            if (!placementSystems.ContainsKey(id))
                Debug.LogError("There is no placement system with the provided id!");
            var target = placementSystems[id] as T;
            if (target == null)
                Debug.LogError("Provided type is not compatible with target system id!");
            return target;
        }
    }
}
