﻿using BG.API;
using BG.API.Abilities;
using System;
using System.Collections.Generic;
using UnityEngine;
using BG.Extensions;
using BG.States;
using BG.API.Exceptions;

namespace BG.Grids.Domain
{
    public class SimplePlacementSystem : IPlacementSystem
    {
        public Guid Id { get; set; }
        public bool IsReady { get; private set; }

        public event EventHandler<GridPlacingEventArgs> Placed;

        // Lightweight pattern event args
        private LinearTransitionChangedEventArgs transitionChagedEventArgs = new LinearTransitionChangedEventArgs();
        private PlacementStateChagedEventArgs placementChagedEventArgs = new PlacementStateChagedEventArgs();
        private SelectStateChagedEventArgs selectStateChaged = new SelectStateChagedEventArgs();
        private HoverStateChagedEventArgs hoverStateChaged = new HoverStateChagedEventArgs();


        private SimplePlacementParameters parameters;
        private GridSystem gridSystem;
        public SimplePlacementSystem(SimplePlacementParameters parameters)
        {
            this.parameters = parameters ?? throw new ArgumentNullException($"'parameters' argument can not be null!");
            gridSystem = GridsManager.Get(parameters.GridSystemId)?? throw new IdNotFoundException($"Grid system with provided id is not exists!"); ;
            TakeUnfilledCells();
            IsReady = true;
        }

        Dictionary<StateGroup, HashSet<Cell>> UnfilledCells = new Dictionary<StateGroup, HashSet<Cell>>();
        private void TakeUnfilledCells()
        {
            foreach (var positionSet in parameters.PlacementStates)
            {
                if (!UnfilledCells.ContainsKey(positionSet.Key)) UnfilledCells.Add(positionSet.Key, new HashSet<Cell>());
                foreach (var position in positionSet.Value)
                {
                    var cell = gridSystem[position.Item1, position.Item2];
                    if (cell.IsStateActive(ContainerEntityState.Empty))
                        UnfilledCells[positionSet.Key].Add(cell);

                    cell.Dispose<ContainerEntityState, ContainerEntityStateChagedEventArgs>(ContainerEntityState.Empty, CellFilled);
                    cell.Dispose<ContainerEntityState, ContainerEntityStateChagedEventArgs>(ContainerEntityState.Empty, CellUnfilled);
                    cell.AddSingleStateActivateAction<ContainerEntityState, ContainerEntityStateChagedEventArgs>(ContainerEntityState.Empty, CellUnfilled);
                    cell.AddSingleStateDeactivateAction<ContainerEntityState, ContainerEntityStateChagedEventArgs>(ContainerEntityState.Empty, CellFilled);
                }
            }
        }

        private void CellFilled(StateGroup stateGroup, ContainerEntityStateChagedEventArgs cellChangedArgs)
        {
            Cell cell = null;
            StateGroup targetStates = null;
            try
            {
                cell = (Cell)cellChangedArgs.Entity;
                foreach (StateGroup states in UnfilledCells.Keys)
                {
                    targetStates = states;
                    if (UnfilledCells[states].Contains(cell)) UnfilledCells[states].Remove(cell);
                }
            }
            catch (Exception ex)
            {
                Debug.LogError($"cell: {cell != null}, UnfilledCells: {UnfilledCells != null}," +
                $"states:{targetStates != null && UnfilledCells[targetStates] != null} \n\n{ex.Message}");
                throw;
            }
        }

        private void CellUnfilled(StateGroup stateGroup, ContainerEntityStateChagedEventArgs cellChangedArgs)
        {
            Cell cell = null;
            KeyValuePair<StateGroup, GridPositionSet> targetPositionSet;
            try
            {
                cell = (Cell)cellChangedArgs.Entity;
                Tuple<int, int> cellPosition = new Tuple<int, int>(cell.Row, cell.Column);
                foreach (var positionSet in parameters.PlacementStates)
                {
                    targetPositionSet = positionSet;
                    if (positionSet.Value.Contains(cellPosition) && !UnfilledCells[positionSet.Key].Contains(cell))
                        UnfilledCells[positionSet.Key].Add(cell);
                }
            }
            catch (Exception ex)
            {
                Debug.LogError($"cell: {cell != null}, PlacementStates: {parameters.PlacementStates != null}," +
                $"states:{targetPositionSet.Key != null && UnfilledCells[targetPositionSet.Key] != null}," +
                $"targetPositionSet:{targetPositionSet.Value != null} \n\n{ex.Message}");
                throw;
            }
        }

        public void Visit(List<Entity> entities)
        {
            entities.ForEach(entity =>
            {
                foreach (var stateGroup in parameters.PlacementStates)
                {
                    entity.AddStateActivateAction(stateGroup.Key, OnProceduralCommandPlacement);
                }
                entity.AddSingleStateActivateAction<DragAndDropState, DragAndDropChagedEventArgs>(DragAndDropState.Drop, OnDropPlacement);
            });
        }

        private void OnProceduralCommandPlacement(StateGroup stateGroup, IStateChangedEventArgs e)
        {
            Cell cell = null;
            try
            {
                foreach (var position in parameters.PlacementStates[stateGroup])
                {
                    if (!gridSystem[position.Item1, position.Item2].IsStateActive(ContainerEntityState.Full))
                    {
                        cell = gridSystem[position.Item1, position.Item2];
                        break;
                    }
                }
                if (cell == null) throw new NullReferenceException("All specified grid cell positions had been occupied!");

                BeginTransition(e, cell);
            }
            catch (Exception ex)
            {
                Debug.LogError($"cell: {cell != null}, PlacementStates: {parameters.PlacementStates != null}," +
                    $"PlacementStates Contains: {parameters.PlacementStates.ContainsKey(stateGroup)} \n\n{ex.Message}");
                throw;
            }
        }
        private void OnDropPlacement(StateGroup stateGroup, DragAndDropChagedEventArgs e)
        {
            try
            {
                bool acceptsStateConstraints = CheckTargetPlaceConstraints(e);

                if (acceptsStateConstraints)
                {
                    if (e.TargetRegion.Entity is Cell cell1)
                        BeginTransition(e, cell1);
                    else if (e.Entity.Parent is Cell cell2)
                        BeginTransition(e, cell2);
                }
                else
                {
                    if (e.Entity.Parent is Cell cell)
                        BeginTransition(e, cell);
                }
            }
            catch (Exception ex)
            {
                Debug.LogError(ex.Message);
                throw;
            }
        }

        private bool CheckTargetPlaceConstraints(DragAndDropChagedEventArgs e)
        {
            if (e.TargetRegion == null) return false;
            if (parameters.Constraints == null) return true;
            bool isPlaceable = true;
            foreach (var states in parameters.Constraints)
            {
                if (e.TargetRegion.Entity.IsStateActive(states))
                {
                    isPlaceable = true;
                    break;
                }
                isPlaceable = false;
            }

            return isPlaceable;
        }

        private void BeginTransition(IStateChangedEventArgs e, Cell cell)
        {
            if (cell == null) throw new NullReferenceException("Cell can not be null!");

            DisableEntity(e.Entity);
            e.Entity.Parent = cell;
            EnableEntity(e.Entity);

            e.Entity.AddSingleStateActivateAction<TransitionState, LinearTransitionChangedEventArgs>
                (TransitionState.End, OnTransitionEnd);
            float nearLimit = Mathf.Min(cell.Size.x, cell.Size.y) / 20.0f;
            transitionChagedEventArgs.BeginPosition = e.Entity.transform.position;
            transitionChagedEventArgs.EndPosition = cell.transform.position;
            transitionChagedEventArgs.Threshold = nearLimit * nearLimit;
            e.Entity.SetState(TransitionState.Begin, transitionChagedEventArgs);
        }

        private void EnableEntity(Entity entity)
        {
            entity.SetState(SelectState.Selected, selectStateChaged);
            entity.SetState(HoverState.Entered, hoverStateChaged);
        }
        private void DisableEntity(Entity entity)
        {
            entity.SetState(SelectState.Unselected, selectStateChaged);
            entity.SetState(HoverState.Exited, hoverStateChaged);
        }

        public void OnTransitionEnd(StateGroup states, LinearTransitionChangedEventArgs e)
        {
            try
            {
                placementChagedEventArgs.TargetPlace = e.Entity.Parent;
                e.Entity.SetState(PlacementState.Placed, placementChagedEventArgs);

                e.Entity.Dispose<TransitionState, LinearTransitionChangedEventArgs>
                    (TransitionState.End, OnTransitionEnd);
            }
            catch (Exception ex) 
            {
                Debug.LogError($"placementChagedEventArgs: {placementChagedEventArgs != null} \n\n {ex.Message}");
                throw;
            }
        }


        //private PlacementStageState placementStage;
        //private void AssignDroppableValues(Entity entity, Cell cell)
        //{
        //    IDroppable droppable = entity.GetComponent<IDroppable>();
        //    if (droppable == null) return;
        //    IDroppableRegion droppableRegion = cell.GetComponent<IDroppableRegion>();
        //    if (droppableRegion == null) return;

        //    droppable.DroppableRegion = droppableRegion;
        //    droppableRegion.DroppedEntity = droppable;

        //    Enum activePlacementState = cell.GetActiveState(typeof(PlacementState));
        //    Placed?.Invoke(this, new GridPlacingEventArgs
        //    {
        //        Entity = entity,
        //        GridSystemId = cell.GridSystem.Id,
        //        PlacementStage = placementStage,
        //        Position = cell.GridPosition,
        //        PlacementState = activePlacementState != null ? (PlacementState)activePlacementState : PlacementState.NotPlaced
        //    });
        //}

        //public void Placing(object sender, OnGridPlacingEventArgs e)
        //{
            //var placingOption = new GridPlacementOption { GridSystemId = e.GridSystemId, PlacementStates = e.PlacementStates, Positions = e.Positions };

            //foreach (var cellPosition in e.Positions)
            //{
            //    var cell = gridSystem[cellPosition.Item1, cellPosition.Item2];
            //    if (!cell.IsStateActive(ContainerEntityState.Full))
            //    {
            //        cell.AddItem(entities)
            //    }
            //}

            //placingOption.AlterOption()

        //    Placed?.Invoke(this, e);
        //}

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public Cell CellOf(Entity entity)
        {
            //if(entitiesCell.ContainsKey(entity)) return entitiesCell[entity];
            return null;
        }
    }
}
