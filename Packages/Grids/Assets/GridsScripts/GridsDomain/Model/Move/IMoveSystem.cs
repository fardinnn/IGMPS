﻿using BG.API;
using System;

namespace BG.Grids.Domain
{
    public interface IMoveSystem : IComponentSystem
    {
        Guid Id { get; set; }
    }
}
