﻿using BG.API;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace BG.Grids.Domain
{
    public class MoveSystemsManager : Singleton<MoveSystemsManager>
    {
        private static Dictionary<Guid, IMoveSystem> moveSystems;

        protected override void Awake()
        {
            IsDestroyOnLoad = true;
            base.Awake();

            moveSystems = new Dictionary<Guid, IMoveSystem>();
        }

        public void Add(IMoveSystem moveSystem)
        {
            if (moveSystem.Id == default) throw new ArgumentException("moveSystem's id in not assigned");
            moveSystems.Add(moveSystem.Id, moveSystem);
        }

        public static IMoveSystem Get(Guid id)
        {
            if (moveSystems == null) throw new NullReferenceException("moveSystems can not be null!");
            if (!moveSystems.ContainsKey(id))
                Debug.LogError("There is no move system with the provided id!");
            return moveSystems[id];
        }

        public static T Get<T>(Guid id) where T : class, IMoveSystem
        {
            if (moveSystems == null) throw new NullReferenceException("moveSystems can not be null!");
            if (!moveSystems.ContainsKey(id))
                Debug.LogError("There is no move system with the provided id!");
            var target = moveSystems[id] as T;
            if (target == null)
                Debug.LogError("Provided type is not compatible with target system id!");
            return target;
        }
    }
}
