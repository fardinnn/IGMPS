﻿using BG.API;
using System;
using UnityEngine;
using BG.States;
using System.Collections.Generic;

namespace BG.Grids.Domain
{
    public class SimpleMoveSystem : IMoveSystem
    {
        private SimpleMoveParameters command;
        // Lightweight pattern event args
        private MoveChagedEventArgs moveChagedEventArgs = new MoveChagedEventArgs();

        public SimpleMoveSystem(SimpleMoveParameters command)
        {
            this.command = command;
            IsReady = true;
        }

        public Guid Id { get; set; }
        public bool IsReady { get; private set; }

        public void Visit(List<Entity> entities)
        {
            entities.ForEach(entity =>
            {
                entity.SetState(MoveState.NotMoved, moveChagedEventArgs);

                foreach (var state in command.StatesToActivatePotentialMove)
                {
                    entity.AddStateActivateAction(state, OnActivateStateMove);
                    entity.AddStateDeactivateAction(state, OnDeactivateStateMove);
                }

                entity.AddStateActivateAction(new StateGroup(PlacementStageState.Begin), OnBeginPlacement);
                entity.AddStateDeactivateAction(new StateGroup(PlacementStageState.Begin), OnChangedPlacement);
            });
        }

        HashSet<IMoveAlgorithm> cellsActivators = new HashSet<IMoveAlgorithm>();
        public void OnActivateStateMove(StateGroup stateGroup, IStateChangedEventArgs e)
        {
            try
            {
                if(e.Entity.Parent is Cell cell)
                {
                    //Cell entitiesCell = command.PlacementSystem.CellOf(e.Entity);
                    //if (entitiesCell == null)
                    //{
                    //    Debug.LogError("Piece should be placed in a cell to activate moves!");
                    //    return;
                    //}
                    foreach (var stateMoves in command.StateBasedMoveRules)
                    {
                        if (e.Entity.IsStateActive(stateMoves.Key))
                        {
                            foreach (var moveRule in stateMoves.Value)
                            {
                                var moveAlgorithm = new ActivatePotentialMoveCells(cell.GridSystem,
                                    cell.GridPosition,
                                    cell.GetDirection(e.Entity));
                                moveRule.Accept(moveAlgorithm);
                                cellsActivators.Add(moveAlgorithm);
                            }
                        }
                    }
                }
                
            }
            catch (Exception ex)
            {
                Debug.LogError($"Un caught error: {ex.Message}");
            }
        }
         
        public void OnDeactivateStateMove(StateGroup stateGroup, IStateChangedEventArgs e)
        {
            try
            {
                foreach (IMoveAlgorithm cellsActivator in cellsActivators)
                {
                    cellsActivator.Dispose();
                }
                cellsActivators.Clear();
            }
            catch (Exception ex)
            {
                Debug.LogError($"Un caught error: {ex.Message}");
            }
        }

        private void OnBeginPlacement(StateGroup stateGroup, IStateChangedEventArgs e)
        {
            e.Entity.SetState(MoveState.NotMoved, moveChagedEventArgs);
        }

        private void OnChangedPlacement(StateGroup stateGroup, IStateChangedEventArgs e)
        {
            e.Entity.SetState(MoveState.Moved, moveChagedEventArgs);
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}