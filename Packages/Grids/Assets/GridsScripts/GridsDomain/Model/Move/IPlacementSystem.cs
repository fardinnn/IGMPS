﻿using BG.API;
using BG.API.Abilities;
using BG.States;
using System;

namespace BG.Grids.Domain
{
    public interface IPlacementSystem : IComponentSystem
    {
        Guid Id { get; set; }
        Cell CellOf(Entity entity);
        event EventHandler<GridPlacingEventArgs> Placed;
    }
}
