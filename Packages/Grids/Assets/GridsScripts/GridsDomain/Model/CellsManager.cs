﻿using BG.API;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace BG.Grids.Domain
{
    public class CellsManager : Singleton<CellsManager>
    {
        private static Dictionary<Guid, ICellSystem> _cells;

        protected override void Awake()
        {
            IsDestroyOnLoad = true;
            base.Awake();

            _cells = new Dictionary<Guid, ICellSystem>();
        }

        public void Add(ICellSystem cell)
        {
            if (cell.Id == default) throw new ArgumentException("Cell's id in not assigned");
            _cells.Add(cell.Id, cell);
        }

        public static T Get<T>(Guid id) where T : class, ICellSystem
        {
            if (_cells == null) throw new NullReferenceException("_cells can not be null!");
            if (!_cells.ContainsKey(id))
                Debug.LogError("There is no cell with the provided id!");
            var target = _cells[id] as T;
            if (target == null)
                Debug.LogError("Provided type is not compatible with target system id!");
            return target;
        }
    }
}
