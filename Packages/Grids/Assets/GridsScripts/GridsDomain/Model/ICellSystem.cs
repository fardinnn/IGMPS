﻿using System;

namespace BG.Grids.Domain
{
    public interface ICellSystem
    {
        Guid Id { get; set; }
    }
}
