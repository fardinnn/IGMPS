﻿using System;
using UnityEngine;

namespace BG.Grids.Domain
{

    public abstract class GridSystem
    {
        public Guid Id { get; set; }
        public abstract Cell this[int i, int j] { get; }
        public abstract Vector2Int Size { get; }

        public virtual void Generate()
        {

        }
    }
}
