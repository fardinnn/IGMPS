﻿using BG.API;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace BG.Grids.Domain
{

    public class GridsManager : Singleton<GridsManager>
    {
        private static Dictionary<Guid, GridSystem> _grids;

        protected override void Awake()
        {
            IsDestroyOnLoad = true;
            base.Awake();

            _grids = new Dictionary<Guid, GridSystem>();
        }

        public void Add(GridSystem grid)
        {
            if (grid.Id == default) throw new ArgumentException("Grid's id in not assigned");
            _grids.Add(grid.Id, grid);
        }

        public static GridSystem Get(Guid id)
        {
            if (_grids == null) throw new NullReferenceException("_grids can not be null!");
            if (!_grids.ContainsKey(id))
                Debug.LogError("There is no Grid with the provided id!");
            return _grids[id];
        }

        public static T Get<T>(Guid id) where T : GridSystem
        {
            if (_grids == null) throw new NullReferenceException("_grids can not be null!");
            if (!_grids.ContainsKey(id))
                Debug.LogError("There is no Grid with the provided id!");
            var target = _grids[id] as T;
            if (target == null)
                Debug.LogError("Provided type is not compatible with target system id!");
            return target;
        }
    }
}
