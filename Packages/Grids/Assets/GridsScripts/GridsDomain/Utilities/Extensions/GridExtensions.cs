using UnityEngine;

namespace BG.Grids.Domain
{
    public static class GridExtensions
    {
        public static Vector2Int LocalToWorld(this Vector2Int localDirection, Vector2Int forward)
        {
            var right = forward.GetRight();
            return new Vector2Int(localDirection.x * (forward.x + right.x), localDirection.y * (forward.y + right.y));
        }


        public static Vector2Int GetRight(this Vector2Int forwardDirection)
        {
            if (forwardDirection.x == 0) return forwardDirection.y > 0 ? new Vector2Int(1, 0) : new Vector2Int(-1, 0);
            if (forwardDirection.y == 0) return forwardDirection.x > 0 ? new Vector2Int(0, -1) : new Vector2Int(0, 1);
            return new Vector2Int(forwardDirection.y > 0 ? 1 : -1, forwardDirection.x > 0 ? -1 : 1);
        }

    }
}
