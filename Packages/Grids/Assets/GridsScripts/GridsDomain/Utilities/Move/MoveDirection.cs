﻿namespace BG.Grids.Domain
{
    public enum MoveDirection
    {
        Forward,
        Backward,
        Left,
        Right,
        ForwardLeft,
        ForwardRight,
        BackwardLeft,
        BackwardRight
    }
}
