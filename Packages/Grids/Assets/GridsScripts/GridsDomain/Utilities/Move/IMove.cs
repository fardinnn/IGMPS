﻿using BG.API;
using System.Collections.Generic;
using UnityEngine;

namespace BG.Grids.Domain
{
    public interface IMove
    {
        List<Cell> GetPotentialPositions(GridSystem grid, Vector2Int startPosition);
    }
}
