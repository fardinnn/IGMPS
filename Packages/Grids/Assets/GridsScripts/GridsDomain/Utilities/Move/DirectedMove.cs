using BG.API;
using System.Collections.Generic;
using UnityEngine;

namespace BG.Grids.Domain
{
    public struct SimpleDirectedMove : IMove
    {
        public int StepCount { get; set; }
        public Vector2Int Direction { get; set; }

        public List<Cell> GetPotentialPositions(GridSystem grid, Vector2Int startPosition)
        {
            throw new System.NotImplementedException();
        }
    }

    //public struct JumpingDirectedMove : IMove
    //{
    //    public int StepCount { get; set; }
    //    public MoveDirection Direction { get; set; }

    //    public List<Cell> GetPotentialPositions(IGridSystem grid, Vector2Int startPosition)
    //    {
    //        throw new System.NotImplementedException();
    //    }
    //}

}
