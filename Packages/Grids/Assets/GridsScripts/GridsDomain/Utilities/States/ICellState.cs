﻿namespace BG.Grids.Domain
{
    public interface ICellState
    {

    }

    public struct BeginState : ICellState
    {

    }
    public struct IdleState : ICellState
    {

    }
    public struct FreezeState : ICellState
    {

    }
}
