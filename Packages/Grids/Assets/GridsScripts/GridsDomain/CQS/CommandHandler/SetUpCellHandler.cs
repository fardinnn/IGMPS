﻿using BG.API;
using UnityEngine;

namespace BG.Grids.Domain
{
    public abstract class SetUpCellHandler<TCommandParameter> : ICommandHandler<TCommandParameter> 
        where TCommandParameter : CellCommandParameters
    {
        protected CellsManager Manager;
        public virtual void Handle(TCommandParameter command)
        {
            Manager = Object.FindObjectOfType<CellsManager>();
            if (Manager == null)
            {
                new GameObject("CellsManager").AddComponent<CellsManager>();
                Manager = Object.FindObjectOfType<CellsManager>();
            }
        }
    }
}
