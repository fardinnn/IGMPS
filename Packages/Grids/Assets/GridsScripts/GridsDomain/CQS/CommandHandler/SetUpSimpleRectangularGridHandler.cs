namespace BG.Grids.Domain
{
    public class SetUpSimpleRectangularGridHandler : SetUpGridHandler<SimpleRectangularGridParameters>
    {
        public override void Handle(SimpleRectangularGridParameters command)
        {
            //Null checks
            base.Handle(command);

            var grid = new SimpleRectangularGrid(command);
            grid.Id = command.SystemId;
            Manager.Add(grid);

            //Initialize
        }
    }
}
