﻿namespace BG.Grids.Domain
{
    public class SetUpSimplePlacementSystemHandler : SetUpPlacementCommandHandler<SimplePlacementParameters>
    {
        public override void Handle(SimplePlacementParameters command)
        {
            base.Handle(command);

            var system = new SimplePlacementSystem(command);
            system.Id = command.SystemId;
            Manager.Add(system);
        }
    }
}
