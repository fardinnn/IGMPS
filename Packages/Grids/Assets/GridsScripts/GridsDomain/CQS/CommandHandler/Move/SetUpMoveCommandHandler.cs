using BG.API;
using System.Collections;
using UnityEngine;

namespace BG.Grids.Domain
{
    public abstract class SetupMoveCommandHandler<TCommand> : ICommandHandler<TCommand>
            where TCommand : MoveCommandParameters
    {
        protected MoveSystemsManager Manager;
        public virtual void Handle(TCommand command)
        {
            Manager = UnityEngine.Object.FindObjectOfType<MoveSystemsManager>();
            if (Manager == null)
            {
                new GameObject("MoveSystemManager").AddComponent<MoveSystemsManager>();
                Manager = UnityEngine.Object.FindObjectOfType<MoveSystemsManager>();
            }
        }
    }
}
