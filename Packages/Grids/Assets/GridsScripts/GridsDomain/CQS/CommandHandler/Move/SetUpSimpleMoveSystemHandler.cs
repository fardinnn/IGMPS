﻿
namespace BG.Grids.Domain
{
    public class SetupSimpleMoveSystemHandler : SetupMoveCommandHandler<SimpleMoveParameters>
    {
        public override void Handle(SimpleMoveParameters command)
        {
            base.Handle(command);

            var system = new SimpleMoveSystem(command);
            system.Id = command.SystemId;
            Manager.Add(system);
        }
    }
}
