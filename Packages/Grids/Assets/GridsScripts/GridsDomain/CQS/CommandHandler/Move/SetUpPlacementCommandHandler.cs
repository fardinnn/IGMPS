﻿using BG.API;
using UnityEngine;

namespace BG.Grids.Domain
{
    public abstract class SetUpPlacementCommandHandler<TCommand> : ICommandHandler<TCommand>
           where TCommand : PlacementCommandParameters
    {
        protected PlacementSystemsManager Manager;
        public virtual void Handle(TCommand command)
        {
            Manager = UnityEngine.Object.FindObjectOfType<PlacementSystemsManager>();
            if (Manager == null)
            {
                new GameObject("PlacementSystemManager").AddComponent<PlacementSystemsManager>();
                Manager = UnityEngine.Object.FindObjectOfType<PlacementSystemsManager>();
            }
        }
    }
}
