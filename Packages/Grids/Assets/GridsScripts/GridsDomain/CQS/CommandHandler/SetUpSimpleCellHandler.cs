﻿namespace BG.Grids.Domain
{
    public class SetUpSimpleCellHandler : SetUpCellHandler<SimpleCellParameters>
    {
        public override void Handle(SimpleCellParameters command)
        {
            //Null checks
            base.Handle(command);

            var cell = new SimpleCell(command);
            cell.Id = command.SystemId;
            Manager.Add(cell);

            //Initialize
        }
    }
}
