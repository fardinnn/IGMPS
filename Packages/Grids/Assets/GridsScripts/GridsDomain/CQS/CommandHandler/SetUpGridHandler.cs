﻿using BG.API;
using UnityEngine;

namespace BG.Grids.Domain
{
    public abstract class SetUpGridHandler<TCommandParameter> : ICommandHandler<TCommandParameter> 
        where TCommandParameter : GridCommandParameters
    {
        protected GridsManager Manager;
        public virtual void Handle(TCommandParameter command)
        {
            Manager = Object.FindObjectOfType<GridsManager>();
            if (Manager == null)
            {
                new GameObject("GridsManager").AddComponent<GridsManager>();
                Manager = Object.FindObjectOfType<GridsManager>();
            }
        }
    }
}
