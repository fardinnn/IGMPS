﻿using UnityEngine;

namespace BG.Grids.Domain
{
    public class SimpleCellParameters : CellCommandParameters
    {
        public Vector2 Size { get; set; }
    }

    
}
