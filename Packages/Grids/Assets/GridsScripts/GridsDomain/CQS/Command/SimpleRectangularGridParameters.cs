using UnityEngine;

namespace BG.Grids.Domain
{
    public class SimpleRectangularGridParameters : GridCommandParameters
    {
        public Vector2Int Size { get; set; }
        public IGridAlgorithmAcceptor Algorithm { get; set; }
        public bool CellsGetAffectedByContents { get; set; }
        public bool CellsAffectContents { get; set; }
    }

    
}
