﻿using BG.API;
using System;
using UnityEngine;

namespace BG.Grids.Domain
{
    public abstract class GridCommandParameters : ICommand
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public Guid SystemId { get; set; } = Guid.NewGuid();
        public Transform GridObject { get; set; }
    }
}
