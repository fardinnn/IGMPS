﻿using BG.API;
using BG.API.Abilities;
using BG.States;
using System.Collections.Generic;

namespace BG.Grids.Domain
{
    public class SimplePlacementParameters : PlacementCommandParameters
    {
        public Dictionary<StateGroup, GridPositionSet> PlacementStates { get; set; }
        public CompositeStatesSet ReplacementStates { get; set; }
        public CompositeStatesSet UnplacingStates { get; set; }
        public GridPositionSet Positions { get; set; }
        public HashSet<StateGroup> Constraints { get; set; }
        //public ITransitionChangedEventArgs TransitionType { get; set; }
    }
}
