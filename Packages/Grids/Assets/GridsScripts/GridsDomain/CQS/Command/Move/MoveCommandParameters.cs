using BG.API;
using System;

namespace BG.Grids.Domain
{
    public abstract class MoveCommandParameters : ICommand
    {
        public Guid Id { get; set; }
        public Guid SystemId { get; set; }
        public Guid GridSystemId { get; set; }
    }
}
