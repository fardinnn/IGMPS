﻿using BG.API;
using BG.States;
using System.Collections.Generic;

namespace BG.Grids.Domain
{

    public class SimpleMoveParameters : MoveCommandParameters
    {
        public string Name { get; set; }
        public ConditionalMoves Moves { get; set; }
        public CompositeStatesSet CalculateStates { get; set; }
        public CompositeStatesSet SimpleMoveStates { get; set; }
        public Dictionary<StateGroup, HashSet<IMoveRule>> StateBasedMoveRules { get; set; }
        public HashSet<StateGroup> StatesToActivatePotentialMove { get; set; }
        public HashSet<StateGroup> StatesToDeactivatePotentialMove { get; set; }
        public IPlacementSystem PlacementSystem { get; set; }
    }
}
