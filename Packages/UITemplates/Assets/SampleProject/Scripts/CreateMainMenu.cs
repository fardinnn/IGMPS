﻿using System;
using System.Collections.Generic;
using System.Linq;
using Fardin.UITemplates.MainMenu;
using UnityEngine;

public class CreateMainMenu : MonoBehaviour
{
    [SerializeField] private RectTransform _parent;
    // Start is called before the first frame update
    void Start()
    {
        var id = Guid.NewGuid();
        new SetUpSimpleMainMenuHandler().Handle(new SetUpSimpleMainMenu
        {
            Parent = _parent,
            ButtonList = new Dictionary<string, Action<object, EventArgs>>
            {
                ["New Game"] = (o, e) => { },
                ["Continue Game"] = (o, e) => { },
            },
            InstanceId = id
        });

        //var mainMenu = FindObjectsOfType<SimpleMainMenu>().Where(m => m.Id == id).First();

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
