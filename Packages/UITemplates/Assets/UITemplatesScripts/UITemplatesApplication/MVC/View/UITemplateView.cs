﻿using BG.API;
using System;
using UnityEngine;

namespace BG.UITemplates
{
    public interface IUITemplateView : IView { }
    public abstract class UITemplateView : MonoBehaviour, IUITemplateView
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public IController<IView, IModel> Controller { get; internal set; }
        public void Dispose()
        {
            Destroy(gameObject);
        }
    }
}
