﻿using BG.API;
using UnityEngine;

namespace BG.UITemplates
{
    public interface IUITemplateModel : IModel { }
    public abstract class UITemplateModel : IUITemplateModel
    {
        public Transform Parent { get; set; }
    }
}
