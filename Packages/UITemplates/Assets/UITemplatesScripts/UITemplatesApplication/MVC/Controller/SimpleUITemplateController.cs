﻿using BG.API;
using BG.UITemplates.Domain;
using System;

namespace BG.UITemplates
{
    public class SimpleUITemplateController : UITemplateController<SimpleUITemplateView, SimpleUITemplateModel>
    {
        private SimpleUITemplateSystem uiTemplateSystem;

        public SimpleUITemplateController(SimpleUITemplateView view = null,
            SimpleUITemplateModel model = null) : base(view, model)
        {
            var command = Model.ToCommand();
            new SetUpSimpleUITemplateHandler().Handle(command);
            uiTemplateSystem = UITemplatesManager.Get<SimpleUITemplateSystem>(command.SystemId);
        }

        public override IDynamicView GetTemplate()
        {
            return uiTemplateSystem.GetTemplate();
        }

        public override Guid SystemId => uiTemplateSystem == null ? Guid.Empty : uiTemplateSystem.Id;

        public override void Generate(Options options = null, bool activate = true)
        {
            uiTemplateSystem.Generate();
        }

    }
}
