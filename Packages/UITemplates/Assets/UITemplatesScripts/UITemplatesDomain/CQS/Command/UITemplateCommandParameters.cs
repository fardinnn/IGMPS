﻿using BG.API;
using System;

namespace BG.UITemplates.Domain
{
    public abstract class UITemplateCommandParameters : ICommand
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public Guid SystemId { get; set; } = Guid.NewGuid();
    }
}
