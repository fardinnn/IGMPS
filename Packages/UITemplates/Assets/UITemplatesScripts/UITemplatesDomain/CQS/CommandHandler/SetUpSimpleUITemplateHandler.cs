﻿namespace BG.UITemplates.Domain
{
    public class SetUpSimpleUITemplateHandler : SetUpUITemplateHandler<SimpleUITemplateParameters>
    {
        public override void Handle(SimpleUITemplateParameters command)
        {
            //Null checks
            base.Handle(command);

            var grid = new SimpleUITemplateSystem(command);
            grid.Id = command.SystemId;
            Manager.Add(grid);

            //Initialize
        }
    }
}
