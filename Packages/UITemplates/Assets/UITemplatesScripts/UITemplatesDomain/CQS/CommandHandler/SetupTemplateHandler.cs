using BG.API;
using UnityEngine;

namespace BG.UITemplates.Domain
{
    public abstract class SetUpUITemplateHandler<TCommandParameter> : ICommandHandler<TCommandParameter> 
        where TCommandParameter : UITemplateCommandParameters
    {
        protected UITemplatesManager Manager;
        public virtual void Handle(TCommandParameter command)
        {
            Manager = Object.FindObjectOfType<UITemplatesManager>();
            if (Manager == null)
            {
                new GameObject("UITemplatesManager").AddComponent<UITemplatesManager>();
                Manager = Object.FindObjectOfType<UITemplatesManager>();
            }
        }
    }
}
