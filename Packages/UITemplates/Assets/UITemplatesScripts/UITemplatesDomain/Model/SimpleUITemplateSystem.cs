﻿using BG.API;
using System;
using System.Collections.Generic;

namespace BG.UITemplates.Domain
{
    public class SimpleUITemplateSystem : UITemplateSystem
    {
        private readonly SimpleUITemplateParameters parameters;

        //public Dictionary<HashSet<Enum>, IGameEntitiesVisitor> Cell1Rules;

        private HashSet<Enum> States;

        public SimpleUITemplateSystem(SimpleUITemplateParameters parameters)
        {
            //Cell1Rules = new Dictionary<HashSet<Enum>, IGameEntitiesVisitor>();
            States = new HashSet<Enum>();
            this.parameters = parameters;
        }

        public override IDynamicView GetTemplate()
        {
            throw new NotImplementedException();
        }

        public override void Generate()
        {
            throw new NotImplementedException();
        }
    }
}
