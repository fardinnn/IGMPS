﻿using BG.API;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace BG.UITemplates.Domain
{
    public class UITemplatesManager : Singleton<UITemplatesManager>
    {
        private static Dictionary<Guid, UITemplateSystem> _uiTemplates;

        protected override void Awake()
        {
            IsDestroyOnLoad = true;
            base.Awake();

            _uiTemplates = new Dictionary<Guid, UITemplateSystem>();
        }

        public void Add(UITemplateSystem uiTemplate)
        {
            if (uiTemplate.Id == default) throw new ArgumentException("UITemplate's id in not assigned");
            _uiTemplates.Add(uiTemplate.Id, uiTemplate);
        }

        public static UITemplateSystem Get(Guid id)
        {
            if (_uiTemplates == null) throw new NullReferenceException("_uiTemplates can not be null!");
            if (!_uiTemplates.ContainsKey(id))
                Debug.LogError("There is no UITemplate with the provided id!");
            return _uiTemplates[id];
        }

        public static T Get<T>(Guid id) where T : UITemplateSystem
        {
            if (_uiTemplates == null) throw new NullReferenceException("_uiTemplates can not be null!");
            if (!_uiTemplates.ContainsKey(id))
                Debug.LogError("There is no UITemplate with the provided id!");
            var target = _uiTemplates[id] as T;
            if (target == null)
                Debug.LogError("Provided type is not compatible with target system id!");
            return target;
        }
    }
}
