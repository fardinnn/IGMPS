﻿using BG.API.Abilities;
using System;

namespace BG.UITemplates.Domain
{
    public class PlayButton : UIElement, IPlayable, IPlaySubscriber
    {
        public event EventHandler<PlayEventArgs> Played;

        public void OnPlayed(object sender, PlayEventArgs e)
        {
            throw new System.NotImplementedException();
        }

        public void Play(object sender, PlayEventArgs e)
        {
            throw new NotImplementedException();
        }
    }
}
