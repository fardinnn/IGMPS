﻿using System;
using System.Collections.Generic;
using BG.API.Abilities;
using UnityEngine;

namespace BG.UITemplates.Domain
{
    public class SimpleIntro : Intro, IPlayable, ISkippable, IPausable
    {
        public RectTransform Background { get; set; }
        public RectTransform Foreground { get; set; }

        //internal SimpleIntroModel Model { get; set; }

        public event EventHandler<PauseEventArgs> Paused;

        public void Pause(object o, PauseEventArgs e)
        {
            throw new System.NotImplementedException();
        }

        public event EventHandler<PlayEventArgs> Played;

        public async void Play(object o, PlayEventArgs e)
        {
            //await new WaitForEndOfFrame();
        }

        public event EventHandler<SkipEventArgs> Skipped;

        public void Skip(object o, SkipEventArgs e)
        {
            throw new System.NotImplementedException();
        }

        public override void Dispose()
        {
            throw new NotImplementedException();
        }

        public override IEnumerable<TAbility> GetAbilities<TAbility>()
        {
            throw new NotImplementedException();
        }

        public override IEnumerable<TSubscriber> GetSubscribers<TSubscriber>()
        {
            throw new NotImplementedException();
        }
    }
}