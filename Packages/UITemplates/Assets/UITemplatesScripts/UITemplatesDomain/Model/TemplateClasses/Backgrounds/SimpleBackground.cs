﻿using System;
using System.Collections.Generic;
using BG.API.Abilities;

namespace BG.UITemplates.Domain
{
    public class SimpleBackground : Background, IPausable, IPlayable
    {
        public event EventHandler<PauseEventArgs> Paused;

        public void Pause(object o, PauseEventArgs e)
        {
            throw new System.NotImplementedException();
        }

        public event EventHandler<PlayEventArgs> Played;

        public void Play(object o, PlayEventArgs e)
        {
            throw new System.NotImplementedException();
        }

        public override void Dispose()
        {
            throw new NotImplementedException();
        }

        public override IEnumerable<TAbility> GetAbilities<TAbility>()
        {
            throw new NotImplementedException();
        }

        public override IEnumerable<TSubscriber> GetSubscribers<TSubscriber>()
        {
            throw new NotImplementedException();
        }
    }
}