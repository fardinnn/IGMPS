﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace BG.UITemplates.Domain
{
    public class SimpleMainMenu : MainMenuTemplate
    {
        public event EventHandler OnOptionsButtonClick;
        public event EventHandler OnCreditsButtonClick;
        public event EventHandler OnExitButtonClick   ;

        public RectTransform OptionsParent;
        public RectTransform InventoryParent;

        public override void Dispose()
        {
            throw new NotImplementedException();
        }

        public override IEnumerable<TAbility> GetAbilities<TAbility>()
        {
            throw new NotImplementedException();
        }

        public override IEnumerable<TSubscriber> GetSubscribers<TSubscriber>()
        {
            throw new NotImplementedException();
        }
    }
}