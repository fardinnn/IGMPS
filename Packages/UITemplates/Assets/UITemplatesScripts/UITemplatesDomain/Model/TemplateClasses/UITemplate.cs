﻿using BG.API;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace BG.UITemplates.Domain
{
    public abstract class UITemplate : MonoBehaviour, IDynamicView
    {
        public Guid Id { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public abstract void Dispose();

        public abstract IEnumerable<TAbility> GetAbilities<TAbility>();

        public abstract IEnumerable<TSubscriber> GetSubscribers<TSubscriber>();
    }
}