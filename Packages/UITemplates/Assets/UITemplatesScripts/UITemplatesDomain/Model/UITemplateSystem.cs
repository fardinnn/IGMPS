﻿using BG.API;
using System;

namespace BG.UITemplates.Domain
{
    public abstract class UITemplateSystem
    {
        public Guid Id { get; set; }

        public abstract IDynamicView GetTemplate();

        public abstract void Generate();
    }
}
