﻿
/****************************************/
/*   Using Function Oriented Paradigm   */
/****************************************/


using BG.CameraSystems.Domain;
using UnityEngine;

namespace BG.CameraSystems
{
    internal static class ModelToCommandExtensions
    {
        /****************************************/
        /*      Model To Command Converters     */
        /****************************************/

        internal static SimplePerspectiveCameraParameters ToCommand(this SimplePerspectiveCameraModel model)
        {
            return new SimplePerspectiveCameraParameters
            {
                FocusObject = new GameObject("FocusObject")
                .transform.GetComponentValues(model.TargetField, model.Parent),
                FieldOfView = model.FieldOfView,

                FreezeRotation = model.FreezeRotation.ToDomain(),
                FreezeZoom = model.FreezeZoom,
                FreezeMove = model.FreezeMove.ToDomain(),

                DefaultEulerAngles = model.DefaultEulerAngles,
                DefaultZoomScale = model.DefaultZoomScale,
                DefaultRelativePosition = model.DefaultRelativePosition,

                RotationConstraintsType = model.RotationConstraintsType.ToDomain(),
                DefaultEulerAnglesRange = model.DefaultEulerAnglesRange.ToDomain(),
                ZoomConstraintsType = model.ZoomConstraintsType.ToDomain(),
                DefaultZoomRange = model.DefaultZoomRange.ToDomain(),
                MoveConstraintsType = model.MoveConstraintsType.ToDomain(),
                DefaultMoveRange = model.DefaultMoveRange.ToDomain(),

                Parent = model.Parent,
                HasRayCaster = model.HasRayCaster,
                CameraTag = model.CameraTag,

                Components = model.Components
            };
        }

        internal static SimpleTopViewCameraParameters ToCommand(this SimpleTopViewCameraModel model)
        {
            return new SimpleTopViewCameraParameters
            {
                FocusObject = new GameObject("FocusObject")
                .transform.GetComponentValues(model.TargetField, model.Parent),
                Size = model.Size,

                FreezeRotation = model.FreezeRotation,
                FreezeZoom = model.FreezeZoom,
                FreezeMove = model.FreezeMove.ToDomain(),

                DefaultEulerAngles = model.DefaultEulerAngles,
                DefaultZoomScale = model.DefaultZoomScale,
                DefaultRelativePosition = model.DefaultRelativePosition,

                RotationConstraintsType = model.RotationConstraintsType.ToDomain(),
                DefaultRotationRange = model.DefaultEulerAnglesRange.ToDomain(),
                ZoomConstraintsType = model.ZoomConstraintsType.ToDomain(),
                DefaultZoomRange = model.DefaultZoomRange.ToDomain(),
                MoveConstraintsType = model.MoveConstraintsType.ToDomain(),
                DefaultMoveRange = model.DefaultMoveRange.ToDomain(),

                Parent = model.Parent,
                HasRayCaster = model.HasRayCaster,
                CameraTag = model.CameraTag,

                Components = model.Components
            };
        }






        /****************************************/
        /*         Converters To Domain         */
        /****************************************/

        private static Domain.Bool2 ToDomain(this Bool2 bool2)
        {
            return new Domain.Bool2 { X = bool2.X, Y = bool2.Y };
        }

        private static Domain.Bool3 ToDomain(this Bool3 bool3)
        {
            return new Domain.Bool3 { X = bool3.X, Y = bool3.Y, Z = bool3.Z };
        }

        private static Domain.ParameterSettingType ToDomain(this ParameterSettingType parameterSettingType)
        {
            return (Domain.ParameterSettingType)((int)parameterSettingType);
        }

        private static Domain.Range<T> ToDomain<T>(this Range<T> range)
        {
            return new Domain.Range<T> { Min = range.Min, Max = range.Max };
        }
    }
}
