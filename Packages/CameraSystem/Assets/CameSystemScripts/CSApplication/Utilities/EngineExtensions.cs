﻿
/****************************************/
/*   Using Function Oriented Paradigm   */
/****************************************/


using UnityEngine;

namespace BG.CameraSystems
{
    internal static class EngineExtensions
    {
        public static Transform GetComponentValues(this Transform current, Transform target, Transform parent = null)
        {
            target.CopyTo(current);
            if (parent != null) current.SetParent(parent);
            return current;
        }
        public static void CopyTo(this Transform current, Transform target)
        {
            target.position = current.position;
            target.rotation = current.rotation;
            var targetParent = target.parent;
            target.SetParent(current.parent);
            target.localScale = current.localScale;
            target.SetParent(targetParent);
        }
    }
}
