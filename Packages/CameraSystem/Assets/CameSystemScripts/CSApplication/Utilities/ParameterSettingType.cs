﻿namespace BG.CameraSystems
{
    public enum ParameterSettingType
    {
        Default,
        Automatic,
        Manual
    }
}
