﻿namespace BG.CameraSystems
{
    public struct Bool3
    {
        public bool X;
        public bool Y;
        public bool Z;

        public static implicit operator Bool3(bool value)
        {
            return new Bool3()
            {
                X = value,
                Y = value,
                Z = value
            };
        }
    }
}
