﻿namespace BG.CameraSystems
{
    public struct Range<T>
    {
        public T Min;
        public T Max;
    }
}
