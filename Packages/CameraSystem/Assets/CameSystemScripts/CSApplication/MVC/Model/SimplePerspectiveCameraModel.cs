﻿using BG.API;
using BG.CameraSystems.Domain;
using UnityEngine;

namespace BG.CameraSystems
{
    public class SimplePerspectiveCameraModel : CameraModel
    {
        public float FieldOfView { get; set; } = 45.0f;

        public Bool2 FreezeRotation { get; set; } = false;
        public bool FreezeZoom { get; set; } = false;
        public Bool2 FreezeMove { get; set; } = false;

        public Vector3 DefaultEulerAngles { get; set; } = Vector3.zero;
        public float DefaultZoomScale { get; set; } = 1.0f;
        public Vector3 DefaultRelativePosition { get; set; } = Vector3.zero;
         
        public ParameterSettingType RotationConstraintsType { get; set; } = ParameterSettingType.Default;
        public Range<Vector3> DefaultEulerAnglesRange { get; set; } = new Range<Vector3> { Min = Vector3.zero, Max = Vector3.one * 360 };
        public ParameterSettingType ZoomConstraintsType { get; set; } = ParameterSettingType.Default;
        public Range<float> DefaultZoomRange { get; set; } = new Range<float> { Min = 0.1f, Max = 1.55f };
        public ParameterSettingType MoveConstraintsType { get; set; } = ParameterSettingType.Default;
        public Range<Vector3> DefaultMoveRange { get; set; } = new Range<Vector3> { Min = -10 * Vector3.one, Max = 10 * Vector3.one };

    }
}
