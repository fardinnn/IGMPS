﻿using BG.API;
using UnityEngine;

namespace BG.CameraSystems
{
    public abstract class CameraModel : IModel
    {
        public virtual Transform Parent { get; set; }
        public virtual Transform TargetField { get; set; }
        public bool HasRayCaster { get; set; }
        public string CameraTag { get; set; } = "MainCamera";
        public GameEntitiesVisitors<IComponentSystem> Components { get; set; }
    }
}
