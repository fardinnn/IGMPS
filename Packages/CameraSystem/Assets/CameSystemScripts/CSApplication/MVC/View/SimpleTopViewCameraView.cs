﻿using UnityEngine;

namespace BG.CameraSystems
{
    public class SimpleTopViewCameraView : CameraView
    {

        protected override void OnDisable()
        {
            (Controller as SimpleTopViewCameraController)?.OnDisable();
        }

        protected override void OnEnable()
        {
            (Controller as SimpleTopViewCameraController)?.OnEnable();
        }
    }
}
