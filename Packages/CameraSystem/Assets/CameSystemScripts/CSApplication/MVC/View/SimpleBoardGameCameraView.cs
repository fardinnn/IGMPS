using BG.API;
using System;
using UnityEngine;

namespace BG.CameraSystems
{
    public class SimpleBoardGameCameraView : MonoBehaviour, IView
    {
        public Guid Id { get; set; }
        public event EventHandler<Activate2DCameraEventArgs> OnActivate2DCamera;
        public event EventHandler<Activate3DCameraEventArgs> OnActivate3DCamera;

        public void Activate2DCamera()
        {
            OnActivate2DCamera?.Invoke(this, null);
        }

        public void Activate3DCamera()
        {
            OnActivate3DCamera?.Invoke(this, null);
        }
        public void Dispose()
        {
            Destroy(gameObject);
        } 
    }
}
