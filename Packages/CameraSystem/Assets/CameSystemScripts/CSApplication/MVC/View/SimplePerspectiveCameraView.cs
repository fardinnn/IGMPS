﻿using UnityEngine;

namespace BG.CameraSystems
{
    public class SimplePerspectiveCameraView : CameraView
    {
        protected override void OnDisable()
        {
            (Controller as SimplePerspectiveCameraController)?.OnDisable();
        }

        protected override void OnEnable()
        {
            (Controller as SimplePerspectiveCameraController)?.OnEnable();
        }
    }
}
