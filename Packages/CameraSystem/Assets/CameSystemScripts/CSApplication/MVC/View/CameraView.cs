﻿using BG.API;
using System;
using UnityEngine;

namespace BG.CameraSystems
{
    public abstract class CameraView : MonoBehaviour, IView
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public ICameraController Controller { get; set; }

        protected abstract void OnEnable();

        protected abstract void OnDisable();

        public void Dispose()
        {
            Destroy(gameObject);
        }
    }
}
