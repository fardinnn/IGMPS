﻿using BG.API;
using BG.CameraSystems.Domain;
using BG.EISystem;
using BG.Extensions;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.InputSystem;

namespace BG.CameraSystems
{
    public class SimplePerspectiveCameraController : CameraController<SimplePerspectiveCameraView, SimplePerspectiveCameraModel>,
        IInputReceiver
    {
        private SimplePerspectiveCameraSystem _cameraSystem;


        public SimplePerspectiveCameraController(SimplePerspectiveCameraView view = null,
            SimplePerspectiveCameraModel model =null)
            :base(view, model)
        {
            InitializeInputs();
        }

        public override void Generate(Options options = null, bool activate = true)
        {
            // Debug.Log("THIS RUNNED");
            var command = Model.ToCommand();
            new SetUpSimplePerspectiveCameraHandler().Handle(command);
            _cameraSystem = CameraSystemManager.Get<SimplePerspectiveCameraSystem>(command.SystemId);
            if (!activate)
                Model.Parent.gameObject.SetActive(false);
        }



        /****************************************/
        /*             SetUp Inputs             */
        /****************************************/

        public event EventHandler Enabled;
        public event EventHandler Disabled;
        public string InputMapName { get; private set; }
        public Dictionary<string, EventHandler<IInputValue>> Actions { get; private set; }
        public Dictionary<string, EventHandler<IInputValue>> CancelActions { get; private set; }
        public string JSONInputs { get; private set; }
        public string CoExistGroup { get; set; } = "Cameras";


        private Task rotateCoroutine;
        private Task panCoroutine;
        private Task zoomCoroutine;

        private void InitializeInputs()
        {
            InputMapName = "Simple Perspective Camera";
            Actions = new Dictionary<string, EventHandler<IInputValue>>
            {
                {"Rotate_Actuators",  (object o, IInputValue e)=>ContinuousRotate() },
                {"Pan_Actuators",  (object o, IInputValue e)=>ContinuousPan() },
                {"Zoom_Actuators",  (object o, IInputValue e)=>ContinuousZoom() },
                {"Rotate_Buttons",  (object o, IInputValue e)=>Rotate(e.ReadValue<Vector2>()) },
                {"Pan_Buttons",  (object o, IInputValue e)=>Pan(0.005f * e.ReadValue<Vector2>()) },
                {"Zoom_Buttons",  (object o, IInputValue e)=>Zoom(e.ReadValue<float>()) }
            };
            CancelActions = new Dictionary<string, EventHandler<IInputValue>>
            {
                {"Pan_Actuators",  (object o, IInputValue e)=>EndPan() },
                {"Rotate_Actuators",  (object o, IInputValue e)=>EndRotate() },
                {"Zoom_Actuators",  (object o, IInputValue e)=>EndZoom() },
            };

            JSONInputs = new SimplePerspectiveCameraInput().SimplePerspectiveCamera.Get().ToJson();//DefaultInputs.SimplePerspectiveCamera;


            rotateCoroutine = new Task(RotateCoroutine(), false);
            panCoroutine = new Task(PanCoroutine(), false);
            zoomCoroutine = new Task(ZoomCoroutine(), false);
        }


        private bool isEnable = true;
        public override void OnEnable()
        {
            isEnable = true;
            Enabled?.Invoke(this, null);

        }

        public override void OnDisable()
        {
            isEnable = false;
            Disabled?.Invoke(this, null);
            //if (rotateCoroutine!=null)
            //{
            //    if (rotateCoroutine.Running) rotateCoroutine.Stop();
            //    rotateCoroutine = null;
            //}
        }


        private Pointer currentPointer;
        private Vector2 rotateLastPosition;
        private void ContinuousRotate()
        {
            try
            {
                if (!isEnable || rotateCoroutine.Running) return;
                currentPointer = Pointer.current;
                rotateLastPosition = currentPointer.position.ReadValue();
                rotateCoroutine.Start();
            }
            catch (Exception ex)
            {
                Debug.LogError($"Catch Error: {ex.Message}");
            }
        }
        private Vector2 panLastPosition;
        private void ContinuousPan()
        {
            try
            {
                if (!isEnable || panCoroutine.Running) return;
                currentPointer = Pointer.current;
                panLastPosition = currentPointer.position.ReadValue();
                panCoroutine.Start();
            }
            catch (Exception ex)
            {
                Debug.LogError($"Catch Error: {ex.Message}");
            }
        }
        private Vector2 zoomLastPosition;
        private void ContinuousZoom()
        {
            try
            {
                if (!isEnable || zoomCoroutine.Running) return;
                currentPointer = Pointer.current;
                zoomLastPosition = currentPointer.position.ReadValue();
                zoomCoroutine.Start();
            }
            catch (Exception ex)
            {
                Debug.LogError($"Catch Error: {ex.Message}");
            }
        }

        private Vector2 rotateFactor = new Vector2(10.0f,-5.0f);
        private Vector2 rotateCurrentPosition;
        IEnumerator RotateCoroutine()
        {
            while (isEnable)
            {
                rotateCurrentPosition = currentPointer.position.ReadValue();
                Vector2 rotateValue = (rotateCurrentPosition - rotateLastPosition);
                Rotate(new Vector2(rotateFactor.x * rotateValue.x, rotateFactor.y * rotateValue.y));
                rotateLastPosition = rotateCurrentPosition;
                yield return null;
            }
        }

        private float panFactor = -0.005f;
        private Vector2 panCurrentPosition;
        IEnumerator PanCoroutine()
        {
            while (isEnable)
            {
                panCurrentPosition = currentPointer.position.ReadValue();
                Pan(panFactor*(panCurrentPosition - panLastPosition));
                panLastPosition = panCurrentPosition;
                currentPointer.position.ReadValue();
                yield return null;
            }
        }

        private float zoomFactor = 0.5f;
        private Vector2 zoomCurrentPosition;
        IEnumerator ZoomCoroutine()
        {
            while (isEnable)
            {
                zoomCurrentPosition = currentPointer.position.ReadValue();
                Vector2 zoomDelta = zoomCurrentPosition - zoomLastPosition;
                Zoom(zoomFactor*(zoomDelta.x + zoomDelta.y));
                zoomLastPosition = zoomCurrentPosition;
                currentPointer.position.ReadValue();

                yield return null;
            }
        }

        private void EndRotate()
        {
            try
            {
                rotateCoroutine.Stop();
            }
            catch (Exception ex)
            {
                Debug.LogError($"Catch Error: {ex.Message}");
            }
        }

        private void EndPan()
        {
            try
            {
                panCoroutine.Stop();
            }
            catch (Exception ex)
            {
                Debug.LogError($"Catch Error: {ex.Message}");
            }
        }

        private void EndZoom()
        {
            try
            {
                zoomCoroutine.Stop();
            }
            catch (Exception ex)
            {
                Debug.LogError($"Catch Error: {ex.Message}");
            }
        }



        private void Rotate(Vector2 deltaAngles)
        {
            if (!isEnable) return;
            if (deltaAngles == default) return;
            _cameraSystem?.OnRotateCamera(deltaAngles);
        }
        private void Pan(Vector2 deltaPosition)
        {
            if (!isEnable) return;
            if (deltaPosition == default) return;
            _cameraSystem?.OnPanCamera(Vector3.up, deltaPosition);
        }
        private void Zoom(float zoomDelta)
        {
            if (!isEnable) return;
            if (zoomDelta == default) return;
            _cameraSystem?.OnZoomCamera(zoomDelta);
        }


    }
}
