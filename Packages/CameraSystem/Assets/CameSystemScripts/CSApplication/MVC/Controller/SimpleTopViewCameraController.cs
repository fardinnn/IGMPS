﻿using BG.API;
using BG.CameraSystems.Domain;
using BG.EISystem;
using BG.Extensions;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace BG.CameraSystems
{
    public class SimpleTopViewCameraController : CameraController<SimpleTopViewCameraView, SimpleTopViewCameraModel>,
        IInputReceiver
    {

        private SimpleTopViewCameraSystem _cameraSystem;

        public event EventHandler Enabled;
        public event EventHandler Disabled;

        public SimpleTopViewCameraController(SimpleTopViewCameraView view = null, SimpleTopViewCameraModel model=null) :
            base(view, model)
        {
            InitializeInputs();
        }

        public override void Generate(Options options = null, bool activate = true)
        {
            var command = Model.ToCommand();
            new SetUpSimpleTopViewCameraHandler().Handle(command);
            _cameraSystem = CameraSystemManager.Get<SimpleTopViewCameraSystem>(command.SystemId);

            if (!activate)
                Model.Parent.gameObject.SetActive(false);
        }



        /****************************************/
        /*             SetUp Inputs             */
        /****************************************/
        public string InputMapName { get; private set; }
        public Dictionary<string, EventHandler<IInputValue>> Actions { get; private set; }
        public Dictionary<string, EventHandler<IInputValue>> CancelActions { get; private set; }
        public string JSONInputs { get; private set; }
        public string CoExistGroup { get; set; } = "Cameras";

        private Task rotateCoroutine;
        private Task panCoroutine;
        private Task zoomCoroutine;

        private void InitializeInputs()
        {
            InputMapName = "Simple Top View Camera";
            Actions = new Dictionary<string, EventHandler<IInputValue>>
            {
                {"Rotate_Actuators",  (object o, IInputValue e)=>ContinuousRotate() }, 
                {"Pan_Actuators",  (object o, IInputValue e)=>ContinuousPan() },
                {"Zoom_Actuators",  (object o, IInputValue e)=>ContinuousZoom() },
                {"Pan_Buttons",  (object o, IInputValue e)=>Pan(0.005f * e.ReadValue < Vector2 >()) },
                {"Rotate_Buttons",  (object o, IInputValue e)=>Rotate(e.ReadValue<float>()) },
                {"Zoom_Buttons",  (object o, IInputValue e)=>Zoom(e.ReadValue<float>()) }
            };
            CancelActions = new Dictionary<string, EventHandler<IInputValue>>
            {
                {"Pan_Actuators",  (object o, IInputValue e)=>EndPan() },
                {"Rotate_Actuators",  (object o, IInputValue e)=>EndRotate() },
                {"Zoom_Actuators",  (object o, IInputValue e)=>EndZoom() },
            };

            JSONInputs = new SimpleTopViewCameraInput().SimpleTopViewCamera.Get().ToJson(); //DefaultInputs.SimpleTopViewCamera;

            rotateCoroutine = new Task(RotateCoroutine(), false);
            panCoroutine = new Task(PanCoroutine(), false);
            zoomCoroutine = new Task(ZoomCoroutine(), false);
        }

        private bool isEnable = true;
        public override void OnEnable()
        {
            isEnable = true;
            Enabled?.Invoke(this, null);

        }

        public override void OnDisable()
        {
            isEnable = false;
            Disabled?.Invoke(this, null);
            //if (rotateCoroutine != null)
            //{
            //    if (rotateCoroutine.Running) rotateCoroutine.Stop();
            //    rotateCoroutine = null;
            //}
        }




        private Pointer currentPointer;
        private Vector2 rotateLastPosition;
        private void ContinuousRotate()
        {
            try
            {
                if (!isEnable || rotateCoroutine.Running) return;
                currentPointer = Pointer.current;
                rotateLastPosition = currentPointer.position.ReadValue();
                rotateCoroutine.Start();
            }
            catch (Exception ex)
            {
                Debug.LogError($"Catch Error: {ex.Message}");
            }
        }
        private Vector2 panLastPosition;
        private void ContinuousPan()
        {
            try
            {
                if (!isEnable || panCoroutine.Running) return;
                currentPointer = Pointer.current;
                panLastPosition = currentPointer.position.ReadValue();
                panCoroutine.Start();
            }
            catch (Exception ex)
            {
                Debug.LogError($"Catch Error: {ex.Message}");
            }
        }
        private Vector2 zoomLastPosition;
        private void ContinuousZoom()
        {
            try
            {
                if (!isEnable || zoomCoroutine.Running) return;
                currentPointer = Pointer.current;
                zoomLastPosition = currentPointer.position.ReadValue();
                zoomCoroutine.Start();
            }
            catch (Exception ex)
            {
                Debug.LogError($"Catch Error: {ex.Message}");
            }
        }

        private float rotateFactor = 1.0f;
        private Vector2 rotateCurrentPosition;
        IEnumerator RotateCoroutine()
        {
            while (isEnable)
            {
                rotateCurrentPosition = currentPointer.position.ReadValue();
                Vector2 rotateValue = (rotateCurrentPosition - rotateLastPosition);
                float angle = rotateValue.ToDeltaAngle(rotateLastPosition, Screen.width / 2.0f, Screen.height / 2.0f);
                Rotate(rotateFactor * angle);
                rotateLastPosition = rotateCurrentPosition;
                yield return null;
            }
        }

        private float panFactor = -0.005f;
        private Vector2 panCurrentPosition;
        IEnumerator PanCoroutine()
        {
            while (isEnable)
            {
                panCurrentPosition = currentPointer.position.ReadValue();
                Pan(panFactor * (panCurrentPosition - panLastPosition));
                panLastPosition = panCurrentPosition;
                currentPointer.position.ReadValue();
                yield return null;
            }
        }

        private float zoomFactor = 0.5f;
        private Vector2 zoomCurrentPosition;
        IEnumerator ZoomCoroutine()
        {
            while (isEnable)
            {
                zoomCurrentPosition = currentPointer.position.ReadValue();
                Vector2 zoomDelta = zoomCurrentPosition - zoomLastPosition;
                Zoom(zoomFactor * (zoomDelta.x + zoomDelta.y));
                zoomLastPosition = zoomCurrentPosition;
                currentPointer.position.ReadValue();

                yield return null;
            }
        }

        private void EndRotate()
        {
            try
            {
                rotateCoroutine.Stop();
            }
            catch (Exception ex)
            {
                Debug.LogError($"Catch Error: {ex.Message}");
            }
        }

        private void EndPan()
        {
            try
            {
                panCoroutine.Stop();
            }
            catch (Exception ex)
            {
                Debug.LogError($"Catch Error: {ex.Message}");
            }
        }

        private void EndZoom()
        {
            try
            {
                zoomCoroutine.Stop();
            }
            catch (Exception ex)
            {
                Debug.LogError($"Catch Error: {ex.Message}");
            }
        }



        private void Rotate(float angle)
        {
            if (!isEnable) return;
            if (angle == default) return;
            _cameraSystem?.OnCameraRotate(angle);
        }
        private void Pan(Vector2 displacement)
        {
            if (!isEnable) return;
            if (displacement == default) return;
            _cameraSystem?.OnPanCamera(Vector3.up, displacement);

        }
        private void Zoom(float zoomScale)
        {
            if (!isEnable) return;
            if (zoomScale == default) return;
            _cameraSystem?.OnZoomCamera(zoomScale);
        }
    }
}
