using BG.API;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace BG.CameraSystems
{
    public class SimpleMultiCameraController : CameraController<SimpleMultiCameraView, SimpleMultiCameraModel>
    {
        private Dictionary<Guid, ICameraController> _controllers{ get; }

        public CameraView GetActiveCameraView()
        {
            return _activeCamera.View;
        }
        private dynamic _activeCamera { get; set; }

        public SimpleMultiCameraController(SimpleMultiCameraView view = null, SimpleMultiCameraModel model = null) :
            base(view, model)
        {
            _controllers = new Dictionary<Guid, ICameraController>();
        }

        private Guid defaultActiveCameraControllerId;
        public void AddCamera<TView>(TView view, bool defaultActive = false)
            where TView: CameraView
        {
            view.transform.SetParent(View.transform); // ????
            _controllers.Add(view.Controller.Id, view.Controller);
            //Debug.Log($"view.Controller.Id: {view.Controller.Id}");
            if (defaultActive)
            {
                defaultActiveCameraControllerId = view.Controller.Id;
                //Activate(view.Controller.GetType());
            }
        }

        public void Activate(Guid controllerId)
        {
            if (!_controllers.ContainsKey(controllerId)) throw new ArgumentException("Provided camera controller type is not created yet!");
            if ((_activeCamera != null) && (controllerId == _activeCamera.Id)) return;

            _controllers[controllerId].SetActivate(true);
            _activeCamera?.SetActivate(false);
            _activeCamera = _controllers[controllerId];
        }

        public override void OnEnable() { }
        public override void OnDisable() { }

        public override void Generate(Options options = null, bool activate = true)
        {
            foreach (var item in _controllers)
            {
                if (item.Value is IEntityController entityController)
                {
                    entityController.Generate(options, item.Key == defaultActiveCameraControllerId);
                }

            }
        }
    }

    public class SimpleMultiCameraView : CameraView
    {

        protected override void OnDisable()
        {
            Controller?.OnDisable();
        }

        protected override void OnEnable()
        {
            Controller?.OnEnable();
        }
    }

    public class SimpleMultiCameraModel : CameraModel
    {

    }
}
