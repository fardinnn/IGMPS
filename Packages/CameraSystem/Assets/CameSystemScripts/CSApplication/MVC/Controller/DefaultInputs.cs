﻿namespace BG.CameraSystems
{
    internal static class DefaultInputs
    {
        public const string SimplePerspectiveCamera = @"
            {
                ""maps"":[
                    {
                    ""name"" : ""Simple Perspective Camera"",
                    ""actions"": [
                        {
                            ""name"": ""Rotate_Values"", ""type"": ""Value""
                        },
                        {
                            ""name"": ""Zoom_Values"", ""type"": ""Value""
                        },
                        {
                            ""name"": ""Pan_Values"", ""type"": ""Value""
                        },
                        {
                            ""name"": ""Zoom_Buttons"", ""type"": ""Button""
                        },
                        {
                            ""name"": ""Rotate_Buttons"", ""type"": ""Button""
                        },
                        {
                            ""name"": ""Pan_Buttons"", ""type"": ""Button""
                        }
                    ],
                    ""bindings"": [
                        {
                            ""name"": ""Vector 2 With One Modifier"",
                            ""path"": ""Vector2WithOneModifier"",
                            ""interactions"": """",
                            ""processors"": ""ScaleVector2(x=20,y=-20)"",
                            ""groups"": """",
                            ""action"": ""Rotate_Values"",
                            ""isComposite"": true,
                            ""isPartOfComposite"": false
                        },
                        {
                            ""name"": ""Modifier"",
                            ""path"": ""<Mouse>/leftButton"",
                            ""interactions"": """",
                            ""processors"": """",
                            ""groups"": """",
                            ""action"": ""Rotate_Values"",
                            ""isComposite"": false,
                            ""isPartOfComposite"": true
                        },
                        {
                            ""name"": ""Position"",
                            ""path"": ""<Mouse>/delta"",
                            ""interactions"": """",
                            ""processors"": """",
                            ""groups"": """",
                            ""action"": ""Rotate_Values"",
                            ""isComposite"": false,
                            ""isPartOfComposite"": true
                        },
                        {
                            ""name"": """",
                            ""path"": ""<Mouse>/scroll/y"",
                            ""interactions"": """",
                            ""processors"": ""Scale(factor=-0.05)"",
                            ""groups"": """",
                            ""action"": ""Zoom_Values"",
                            ""isComposite"": false,
                            ""isPartOfComposite"": false
                        },
                        {
                            ""name"": ""Vector 2 Size Binder With One Modifier"",
                            ""path"": ""Vector2SizeBinderWithOneModifier(IsSigned=true)"",
                            ""interactions"": """",
                            ""processors"": """",
                            ""groups"": """",
                            ""action"": ""Zoom_Values"",
                            ""isComposite"": true,
                            ""isPartOfComposite"": false
                        },
                        {
                            ""name"": ""Modifier"",
                            ""path"": ""<Mouse>/rightButton"",
                            ""interactions"": """",
                            ""processors"": """",
                            ""groups"": """",
                            ""action"": ""Zoom_Values"",
                            ""isComposite"": false,
                            ""isPartOfComposite"": true
                        },
                        {
                            ""name"": ""MyVector"",
                            ""path"": ""<Mouse>/delta"",
                            ""interactions"": """",
                            ""processors"": ""ScaleVector2(x=0.2,y=-0.2)"",
                            ""groups"": """",
                            ""action"": ""Zoom_Values"",
                            ""isComposite"": false,
                            ""isPartOfComposite"": true
                        },
                        {
                            ""name"": ""Vector 2 With One Modifier"",
                            ""path"": ""Vector2WithOneModifier"",
                            ""interactions"": """",
                            ""processors"": ""ScaleVector2(x=-0.005,y=-0.005)"",
                            ""groups"": """",
                            ""action"": ""Pan_Values"",
                            ""isComposite"": true,
                            ""isPartOfComposite"": false
                        },
                        {
                            ""name"": ""Modifier"",
                            ""path"": ""<Mouse>/middleButton"",
                            ""interactions"": """",
                            ""processors"": """",
                            ""groups"": """",
                            ""action"": ""Pan_Values"",
                            ""isComposite"": false,
                            ""isPartOfComposite"": true
                        },
                        {
                            ""name"": ""Position"",
                            ""path"": ""<Mouse>/delta"",
                            ""interactions"": """",
                            ""processors"": """",
                            ""groups"": """",
                            ""action"": ""Pan_Values"",
                            ""isComposite"": false,
                            ""isPartOfComposite"": true
                        },
                        {
                            ""name"": ""2D Vector"",
                            ""path"": ""2DVector"",
                            ""interactions"": ""ContinuousHold(duration=0.5,pressPoint=0.4)"",
                            ""processors"": ""Scale(factor=0.5)"",
                            ""groups"": """",
                            ""action"": ""Pan_Buttons"",
                            ""isComposite"": true,
                            ""isPartOfComposite"": false
                        },
                        {
                            ""name"": ""up"",
                            ""path"": ""<Keyboard>/w"",
                            ""interactions"": """",
                            ""processors"": """",
                            ""groups"": """",
                            ""action"": ""Pan_Buttons"",
                            ""isComposite"": false,
                            ""isPartOfComposite"": true
                        },
                        {
                            ""name"": ""down"",
                            ""path"": ""<Keyboard>/s"",
                            ""interactions"": """",
                            ""processors"": """",
                            ""groups"": """",
                            ""action"": ""Pan_Buttons"",
                            ""isComposite"": false,
                            ""isPartOfComposite"": true
                        },
                        {
                            ""name"": ""left"",
                            ""path"": ""<Keyboard>/a"",
                            ""interactions"": """",
                            ""processors"": """",
                            ""groups"": """",
                            ""action"": ""Pan_Buttons"",
                            ""isComposite"": false,
                            ""isPartOfComposite"": true
                        },
                        {
                            ""name"": ""right"",
                            ""path"": ""<Keyboard>/d"",
                            ""interactions"": """",
                            ""processors"": """",
                            ""groups"": """",
                            ""action"": ""Pan_Buttons"",
                            ""isComposite"": false,
                            ""isPartOfComposite"": true
                        },
                        {
                            ""name"": ""Keyboard"",
                            ""path"": ""2DVector"",
                            ""interactions"": ""ContinuousHold(duration=0.5,pressPoint=0.4)"",
                            ""processors"": ""ScaleVector2(x=45,y=45)"",
                            ""groups"": """",
                            ""action"": ""Rotate_Buttons"",
                            ""isComposite"": true,
                            ""isPartOfComposite"": false
                        },
                        {
                            ""name"": ""up"",
                            ""path"": ""<Keyboard>/upArrow"",
                            ""interactions"": """",
                            ""processors"": """",
                            ""groups"": """",
                            ""action"": ""Rotate_Buttons"",
                            ""isComposite"": false,
                            ""isPartOfComposite"": true
                        },
                        {
                            ""name"": ""down"",
                            ""path"": ""<Keyboard>/downArrow"",
                            ""interactions"": """",
                            ""processors"": """",
                            ""groups"": """",
                            ""action"": ""Rotate_Buttons"",
                            ""isComposite"": false,
                            ""isPartOfComposite"": true
                        },
                        {
                            ""name"": ""left"",
                            ""path"": ""<Keyboard>/leftArrow"",
                            ""interactions"": """",
                            ""processors"": """",
                            ""groups"": """",
                            ""action"": ""Rotate_Buttons"",
                            ""isComposite"": false,
                            ""isPartOfComposite"": true
                        },
                        {
                            ""name"": ""right"",
                            ""path"": ""<Keyboard>/rightArrow"",
                            ""interactions"": """",
                            ""processors"": """",
                            ""groups"": """",
                            ""action"": ""Rotate_Buttons"",
                            ""isComposite"": false,
                            ""isPartOfComposite"": true
                        }
                    ]
                }
            ]
        }";

        public const string SimpleTopViewCamera = @"{
            ""maps"": [
                {
                    ""name"": ""Simple Top View Camera"",
                    ""id"": ""b36209df-e086-4f03-9949-e16676dd643a"",
                    ""actions"": [
                        {
                            ""name"": ""Rotate_Values"",
                            ""type"": ""Value"",
                            ""id"": ""f1602e74-4058-4624-8447-f36a167b5351"",
                            ""expectedControlType"": """",
                            ""processors"": ""Scale"",
                            ""interactions"": """"
                        },
                        {
                            ""name"": ""Rotate_Buttons"",
                            ""type"": ""Button"",
                            ""id"": ""29aeb9c6-9437-49a3-bbd2-7249bef5854f"",
                            ""expectedControlType"": ""Button"",
                            ""processors"": """",
                            ""interactions"": """"
                        },
                        {
                            ""name"": ""Zoom_Values"",
                            ""type"": ""Value"",
                            ""id"": ""033db0d9-ed37-410d-ba9c-86f83643b4ff"",
                            ""expectedControlType"": """",
                            ""processors"": """",
                            ""interactions"": """"
                        },
                        {
                            ""name"": ""Zoom_Buttons"",
                            ""type"": ""PassThrough"",
                            ""id"": ""204de1ac-07f3-402a-ab82-9ca02a7afb9b"",
                            ""expectedControlType"": """",
                            ""processors"": """",
                            ""interactions"": """"
                        },
                        {
                            ""name"": ""Pan_Values"",
                            ""type"": ""Value"",
                            ""id"": ""8120fa08-dab2-4923-b370-b2e1a723c0de"",
                            ""expectedControlType"": """",
                            ""processors"": """",
                            ""interactions"": """"
                        },
                        {
                            ""name"": ""Pan_Buttons"",
                            ""type"": ""Button"",
                            ""id"": ""50462be9-661e-463c-95dc-20432f18e91e"",
                            ""expectedControlType"": ""Button"",
                            ""processors"": """",
                            ""interactions"": """"
                        }
                    ],
                    ""bindings"": [
                        {
                            ""name"": """",
                            ""id"": ""bb0f582f-5fb4-46c6-a3e9-9aec1f79112a"",
                            ""path"": ""<Mouse>/scroll/y"",
                            ""interactions"": """",
                            ""processors"": ""Scale(factor=-0.05)"",
                            ""groups"": """",
                            ""action"": ""Zoom_Values"",
                            ""isComposite"": false,
                            ""isPartOfComposite"": false
                        },
                        {
                            ""name"": ""Vector 2 Size Binder With One Modifier"",
                            ""id"": ""db643b60-a34f-4f49-8142-7eaf2970228d"",
                            ""path"": ""Vector2SizeBinderWithOneModifier(IsSigned=true)"",
                            ""interactions"": """",
                            ""processors"": """",
                            ""groups"": """",
                            ""action"": ""Zoom_Values"",
                            ""isComposite"": true,
                            ""isPartOfComposite"": false
                        },
                        {
                            ""name"": ""Modifier"",
                            ""id"": ""f73d44e8-30d1-46d1-b261-d61c88aa0b20"",
                            ""path"": ""<Mouse>/rightButton"",
                            ""interactions"": """",
                            ""processors"": """",
                            ""groups"": """",
                            ""action"": ""Zoom_Values"",
                            ""isComposite"": false,
                            ""isPartOfComposite"": true
                        },
                        {
                            ""name"": ""MyVector"",
                            ""id"": ""464915f7-e071-483d-8910-5661bde72566"",
                            ""path"": ""<Mouse>/delta"",
                            ""interactions"": """",
                            ""processors"": ""Scale"",
                            ""groups"": """",
                            ""action"": ""Zoom_Values"",
                            ""isComposite"": false,
                            ""isPartOfComposite"": true
                        },
                        {
                            ""name"": ""Vector 2 With One Modifier"",
                            ""id"": ""391c4669-4eb7-40f0-a32b-95f12ed322bf"",
                            ""path"": ""Vector2WithOneModifier"",
                            ""interactions"": """",
                            ""processors"": ""ScaleVector2(x=-0.004,y=-0.004)"",
                            ""groups"": """",
                            ""action"": ""Pan_Values"",
                            ""isComposite"": true,
                            ""isPartOfComposite"": false
                        },
                        {
                            ""name"": ""Modifier"",
                            ""id"": ""8ea6add4-0739-46f9-8f39-f98e3bd13e41"",
                            ""path"": ""<Mouse>/middleButton"",
                            ""interactions"": """",
                            ""processors"": """",
                            ""groups"": """",
                            ""action"": ""Pan_Values"",
                            ""isComposite"": false,
                            ""isPartOfComposite"": true
                        },
                        {
                            ""name"": ""Position"",
                            ""id"": ""4c202c88-a02c-4dce-b23e-25a37e952f5d"",
                            ""path"": ""<Mouse>/delta"",
                            ""interactions"": """",
                            ""processors"": """",
                            ""groups"": """",
                            ""action"": ""Pan_Values"",
                            ""isComposite"": false,
                            ""isPartOfComposite"": true
                        },
                        {
                            ""name"": ""Mouse"",
                            ""id"": ""a45e1f63-ce84-4b31-a0ae-88f0f52ef298"",
                            ""path"": ""Vector2PolarAngleWithOneModifier"",
                            ""interactions"": """",
                            ""processors"": ""Scale(factor=0.67)"",
                            ""groups"": """",
                            ""action"": ""Rotate_Values"",
                            ""isComposite"": true,
                            ""isPartOfComposite"": false
                        },
                        {
                            ""name"": ""Modifier"",
                            ""id"": ""1add8a9b-e892-413c-9435-e0f4a85462bd"",
                            ""path"": ""<Mouse>/leftButton"",
                            ""interactions"": """",
                            ""processors"": """",
                            ""groups"": """",
                            ""action"": ""Rotate_Values"",
                            ""isComposite"": false,
                            ""isPartOfComposite"": true
                        },
                        {
                            ""name"": ""MyPositionVector"",
                            ""id"": ""1eb55ad6-d499-48d3-8c98-5d7412da5d91"",
                            ""path"": ""<Mouse>/position"",
                            ""interactions"": """",
                            ""processors"": """",
                            ""groups"": """",
                            ""action"": ""Rotate_Values"",
                            ""isComposite"": false,
                            ""isPartOfComposite"": true
                        },
                        {
                            ""name"": ""MyDeltaVector"",
                            ""id"": ""52e17cdd-4704-4e4f-adf9-8925422e6e3d"",
                            ""path"": ""<Mouse>/delta"",
                            ""interactions"": """",
                            ""processors"": """",
                            ""groups"": """",
                            ""action"": ""Rotate_Values"",
                            ""isComposite"": false,
                            ""isPartOfComposite"": true
                        },
                        {
                            ""name"": ""1D Axis"",
                            ""id"": ""6304cc8c-51a5-4704-bfb5-4ec64c014107"",
                            ""path"": ""1DAxis"",
                            ""interactions"": ""ContinuousHold(duration=0.1,pressPoint=0.3)"",
                            ""processors"": """",
                            ""groups"": """",
                            ""action"": ""Rotate_Buttons"",
                            ""isComposite"": true,
                            ""isPartOfComposite"": false
                        },
                        {
                            ""name"": ""negative"",
                            ""id"": ""c3025cc8-dc37-4089-87fa-ed15c753dc4b"",
                            ""path"": ""<Keyboard>/leftArrow"",
                            ""interactions"": """",
                            ""processors"": """",
                            ""groups"": """",
                            ""action"": ""Rotate_Buttons"",
                            ""isComposite"": false,
                            ""isPartOfComposite"": true
                        },
                        {
                            ""name"": ""positive"",
                            ""id"": ""9cf99018-8599-496e-b166-15d37cb80f91"",
                            ""path"": ""<Keyboard>/rightArrow"",
                            ""interactions"": """",
                            ""processors"": """",
                            ""groups"": """",
                            ""action"": ""Rotate_Buttons"",
                            ""isComposite"": false,
                            ""isPartOfComposite"": true
                        },
                        {
                            ""name"": ""2D Vector"",
                            ""id"": ""da30489e-b5d7-4687-b910-472cdd08abde"",
                            ""path"": ""2DVector"",
                            ""interactions"": ""ContinuousHold(duration=0.1,pressPoint=0.2)"",
                            ""processors"": """",
                            ""groups"": """",
                            ""action"": ""Pan_Buttons"",
                            ""isComposite"": true,
                            ""isPartOfComposite"": false
                        },
                        {
                            ""name"": ""up"",
                            ""id"": ""086cc1af-62e6-44c5-9600-8e914b498fed"",
                            ""path"": ""<Keyboard>/w"",
                            ""interactions"": """",
                            ""processors"": """",
                            ""groups"": """",
                            ""action"": ""Pan_Buttons"",
                            ""isComposite"": false,
                            ""isPartOfComposite"": true
                        },
                        {
                            ""name"": ""down"",
                            ""id"": ""fdfb7dd0-a349-433e-88d1-25c2b64c6be1"",
                            ""path"": ""<Keyboard>/s"",
                            ""interactions"": """",
                            ""processors"": """",
                            ""groups"": """",
                            ""action"": ""Pan_Buttons"",
                            ""isComposite"": false,
                            ""isPartOfComposite"": true
                        },
                        {
                            ""name"": ""left"",
                            ""id"": ""f62dd064-cc90-42a9-b43b-872a7d598fff"",
                            ""path"": ""<Keyboard>/a"",
                            ""interactions"": """",
                            ""processors"": """",
                            ""groups"": """",
                            ""action"": ""Pan_Buttons"",
                            ""isComposite"": false,
                            ""isPartOfComposite"": true
                        },
                        {
                            ""name"": ""right"",
                            ""id"": ""93949c6c-dcec-4980-9ce9-91416199e38d"",
                            ""path"": ""<Keyboard>/d"",
                            ""interactions"": """",
                            ""processors"": """",
                            ""groups"": """",
                            ""action"": ""Pan_Buttons"",
                            ""isComposite"": false,
                            ""isPartOfComposite"": true
                        }
                    ]
                }
            ]
            }";
    }
}
