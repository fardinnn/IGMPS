﻿using BG.API;
using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace BG.CameraSystems
{
    public interface ICameraController : IEntityController
    {
        Guid Id { get; }
        void OnEnable();
        void OnDisable();
        void SetActivate(bool v);
    }

    public abstract class CameraController<TView, TModel> : IEntityController<TView, TModel>, ICameraController, IDisposable
        where TView : CameraView
        where TModel : CameraModel
    {
        public TView View { get; }
        public TModel Model { get; }

        public Guid Id { get; } = Guid.NewGuid();

        public virtual void SetActivate(bool state)
        {
            View?.gameObject.SetActive(state);
        }

        public CameraController(TView view = null, TModel model = null)
        {
            Model = model ?? CreateDefaultModel();
            View = view ?? CreateDefaultView();
            View.Controller = this;
        }

        protected virtual TModel CreateDefaultModel()
        {
            return (TModel)Activator.CreateInstance(typeof(TModel));
        }

        protected virtual TView CreateDefaultView()
        {
            var view = new GameObject(typeof(TView).Name.Replace("View", "")).AddComponent<TView>();
            if (Model.Parent == null) Model.Parent = view.transform;
            return view;
        }

        public abstract void OnEnable();

        public abstract void OnDisable();


        public virtual void Dispose()
        {
            UnityEngine.Object.Destroy(View.gameObject);
        }

        public abstract void Generate(Options options = null, bool activate = true);
    }
}
