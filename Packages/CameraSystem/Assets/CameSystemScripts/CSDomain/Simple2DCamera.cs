using System;

namespace BG.CameraSystems.Domain
{
    public class Simple2DCamera : ICameraSystem
    {

        #region Construct class
        private readonly SimpleCameraParameters _parameters;
        public Simple2DCamera(SimpleCameraParameters parameters)
        {

        }

        public Guid Id { get; set; }

        public event EventHandler<RotateAroundAxisEventArgs> CameraRotated;
        public event EventHandler<PanEventArgs> CameraPanned;
        public event EventHandler<ZoomEventArgs> CameraZoomed;
        #endregion
    }
}
