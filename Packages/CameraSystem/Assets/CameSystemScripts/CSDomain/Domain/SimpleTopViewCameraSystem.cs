﻿using BG.API;
using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace BG.CameraSystems.Domain
{
    public class SimpleTopViewCameraSystem : ICameraSystem
    {
        public Guid Id { get; set; }

        public event EventHandler<RotateAroundAxisEventArgs> CameraRotated;
        public event EventHandler<PanEventArgs> CameraPanned;
        public event EventHandler<ZoomEventArgs> CameraZoomed;

        private readonly SimpleTopViewCameraParameters _parameters;

        private CameraEntity camera;

        public SimpleTopViewCameraSystem(SimpleTopViewCameraParameters parameters)
        {
            _parameters = parameters ?? throw new ArgumentNullException("SimplePerspectiveCameraParameters can not be null!");
            _zoomScale = _parameters.DefaultZoomScale;
            GenerateCamera();
        }

        private void GenerateCamera()
        {
            camera = new GameObject("SimplePerspectiveCameraEntity").AddComponent<CameraEntity>();
            camera.CreateCamera();
            if (_parameters.HasRayCaster)
                camera.gameObject.AddComponent<PhysicsRaycaster>();
            if (!string.IsNullOrEmpty(_parameters.CameraTag)) camera.gameObject.tag = _parameters.CameraTag;
            camera.transform.SetParent(_parameters.Parent);

            camera.Camera.orthographic = true;
            camera.Camera.orthographicSize = _parameters.Size;
            camera.transform.position = _parameters.FocusObject.position + _parameters.DefaultRelativePosition;
            camera.transform.RotateAround(_parameters.FocusObject.position, Vector3.up, _parameters.DefaultEulerAngles.y);
            camera.transform.LookAt(_parameters.FocusObject);

            SetupComponents(_parameters.Components);
        }

        private void SetupComponents(GameEntitiesVisitors<IComponentSystem> components)
        {
            if (components == null) return;

            for (int i = 0; i < components.Count; i++)
                components[i].Visit(new System.Collections.Generic.List<Entity> { camera });
        }


        public void OnCameraRotate(float deltaPositions)
        {
            if (_parameters.FreezeRotation) return;
            RotateCamera(deltaPositions);

            CameraRotated?.Invoke(this, new RotateAroundAxisEventArgs 
            { HorizontalRotation = deltaPositions, VerticalRotation = deltaPositions });
        }

        private void RotateCamera(float deltaPositions)
        {
            var rotAngle = Time.deltaTime * _parameters.RotateSpeed * deltaPositions;
            var targetAngle = (camera.transform.eulerAngles.y + rotAngle);
            if (targetAngle < _parameters.DefaultRotationRange.Min.y
                || targetAngle > _parameters.DefaultRotationRange.Max.y)
                return;
            camera.transform.Rotate(Vector3.up, deltaPositions, Space.World);
        }

        public void OnPanCamera(Vector3 normal, Vector2 displacement)
        {
            if (_parameters.FreezeMove.X && _parameters.FreezeMove.Y) return;
            PanCamera(displacement);
            CameraPanned?.Invoke(this, new PanEventArgs { });
        }

        private void PanCamera(Vector2 displacement)
        {
            var upward = camera.transform.up * displacement.y * camera.Camera.orthographicSize;
            var right = camera.transform.right * displacement.x * camera.Camera.orthographicSize;

            var d = right + upward;

            var newPostion = (_parameters.FocusObject.position +
                             new Vector3(_parameters.FreezeMove.X ? 0 : d.x, d.y,
                            _parameters.FreezeMove.Y ? 0 : d.z))
                            .LimitTo(_parameters.DefaultMoveRange);

            var deltaMove = newPostion - _parameters.FocusObject.position;
            _parameters.FocusObject.position = newPostion;
            camera.transform.position += deltaMove;
        }

        private float _zoomScale;
        public void OnZoomCamera(float zoomDelta)
        {
            if (_parameters.FreezeZoom) return;
            ZoomCamera(zoomDelta);
            CameraZoomed?.Invoke(this, new ZoomEventArgs { });
        }

        private void ZoomCamera(float zoomStep)
        {

            var newZoomScale = _zoomScale * (1 + zoomStep * _parameters.ZoomSpeed * Time.deltaTime);
            _zoomScale = newZoomScale.LimitTo(_parameters.DefaultZoomRange);
            camera.Camera.orthographicSize = _parameters.Size * _zoomScale;
        }
    }
}
