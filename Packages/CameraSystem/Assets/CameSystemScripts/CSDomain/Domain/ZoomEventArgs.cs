﻿using System;


namespace BG.CameraSystems.Domain
{
    public class ZoomEventArgs : EventArgs
    {
        public float ZoomScale { get; set; }
    }
}
