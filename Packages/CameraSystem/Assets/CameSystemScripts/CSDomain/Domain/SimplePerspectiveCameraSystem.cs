using BG.API;
using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace BG.CameraSystems.Domain
{
    public class CameraEntity : Entity
    {
        public Camera Camera { get; set; }

        internal void CreateCamera()
        {
            Camera = gameObject.AddComponent<Camera>();
        }
    }


    public class SimplePerspectiveCameraSystem : ICameraSystem
    {
        public event EventHandler<RotateAroundAxisEventArgs> CameraRotated;
        public event EventHandler<PanEventArgs> CameraPanned;
        public event EventHandler<ZoomEventArgs> CameraZoomed;

        private readonly SimplePerspectiveCameraParameters _parameters;


        private CameraEntity camera;

        public SimplePerspectiveCameraSystem(SimplePerspectiveCameraParameters parameters)
        {
            _parameters = parameters ?? throw new ArgumentNullException("SimplePerspectiveCameraParameters can not be null!");
            _zoomScale = _parameters.DefaultZoomScale;
            GenerateCamera();
        }


        private void GenerateCamera()
        {
            camera = new GameObject("SimplePerspectiveCameraEntity").AddComponent<CameraEntity>();
            camera.CreateCamera();
            if (_parameters.HasRayCaster)
            {
                // Debug.Log("RayCasteraDDED");
                camera.gameObject.AddComponent<PhysicsRaycaster>();
            }
            if (!string.IsNullOrEmpty(_parameters.CameraTag)) camera.gameObject.tag = _parameters.CameraTag;
            camera.transform.SetParent(_parameters.Parent);

            camera.Camera.orthographic = false;
            camera.Camera.fieldOfView = _parameters.FieldOfView;
            camera.transform.position = _parameters.FocusObject.position + _parameters.DefaultRelativePosition;
            camera.transform.RotateAround(_parameters.FocusObject.position, Vector3.up, _parameters.DefaultEulerAngles.y);
            camera.transform.LookAt(_parameters.FocusObject);

            SetupComponents(_parameters.Components);
        }

        private void SetupComponents(GameEntitiesVisitors<IComponentSystem> components)
        {
            if (components == null) return;

            for (int i = 0; i < components.Count; i++)
                components[i].Visit(new System.Collections.Generic.List<Entity> { camera });
        }


        #region Camera transformations

        private Vector3 focusPoint
        {
            get => _parameters.FocusObject.position; set
            {
                _parameters.FocusObject.position = value;
                camera.transform.position = value + _parameters.DefaultRelativePosition;
            }
        }

        public Guid Id { get; set; }

        private Vector3 _startPostion = Vector3.zero;
        private Vector3 _displacement = Vector3.zero;

        public void OnRotateCamera(Vector2 deltaAngles)
        {
            if (_parameters.RotateSpeed <= 0.01f || (_parameters.FreezeRotation.Y && _parameters.FreezeRotation.X)) return;
            if (!_parameters.FreezeRotation.Y) RotateAroundY(deltaAngles.x);
            if (!_parameters.FreezeRotation.X) RotateAroundX(deltaAngles.y);
            camera.transform.LookAt(_parameters.FocusObject);
            CameraRotated?.Invoke(this, new RotateAroundAxisEventArgs { HorizontalRotation = deltaAngles.y, VerticalRotation = deltaAngles.x });
        }

        private void RotateAroundY(float deltaAngle)
        {
            var rotAngle = Time.deltaTime * _parameters.RotateSpeed * deltaAngle;
            var finalAngle = (camera.transform.eulerAngles.y + rotAngle);
            if ((finalAngle < _parameters.DefaultEulerAnglesRange.Min.y && rotAngle < 0) &&
                (finalAngle > _parameters.DefaultEulerAnglesRange.Max.y && rotAngle > 0))
                return;
            camera.transform.RotateAround(focusPoint, Vector3.up, rotAngle);
        }

        private void RotateAroundX(float rotationAngle)
        {
            var rotAngle = Time.deltaTime * _parameters.RotateSpeed * rotationAngle;
            var finalAngle = camera.transform.localEulerAngles.x + rotAngle;
            if ((finalAngle < _parameters.DefaultEulerAnglesRange.Min.x && rotAngle < 0) ||
                (finalAngle > _parameters.DefaultEulerAnglesRange.Max.x && rotAngle > 0))
                return;
            camera.transform.RotateAround(focusPoint, camera.transform.right, rotAngle);
        }

        public void OnPanCamera(Vector3 normal, Vector2 deltaPosition)
        {
            if (_parameters.FreezeMove.X && _parameters.FreezeMove.Y) return;
            var deltaMove = PanCamera(normal, deltaPosition);
            CameraPanned?.Invoke(this, new PanEventArgs { Displacement = deltaMove, Normal = normal });
        }

        private Vector2 PanCamera(Vector3 normal, Vector2 deltaPosition)
        {
            var forward = camera.transform.forward * deltaPosition.y /
                Mathf.Abs(Mathf.Cos(camera.transform.localEulerAngles.x * Mathf.Deg2Rad));
            var right = camera.transform.right * deltaPosition.x;
            var tangents = forward - Vector3.Dot(forward, normal) * normal;

            var screenFactor = _parameters.DefaultRelativePosition.magnitude * Mathf.Tan(Mathf.Deg2Rad * camera.Camera.fieldOfView / 2.0f);
            var d = screenFactor * (right + tangents);

            var newPostion = (_parameters.FocusObject.position +
                             new Vector3(_parameters.FreezeMove.X ? 0 : d.x, d.y,
                            _parameters.FreezeMove.Y ? 0 : d.z))
                            .LimitTo(_parameters.DefaultMoveRange);

            var deltaMove = newPostion - _parameters.FocusObject.position;
            _parameters.FocusObject.position = newPostion;
            camera.transform.position += deltaMove;
            return deltaMove;
        }

        private float _zoomScale;
        public void OnZoomCamera(float zoomDelta)
        {
            if (_parameters.FreezeZoom) return;
            ZoomCamera(zoomDelta);
            CameraZoomed?.Invoke(this, new ZoomEventArgs { ZoomScale = zoomDelta });
        }

        private void ZoomCamera(float zoomDelta)
        {
            var newZoomScale = _zoomScale * (1 + zoomDelta * _parameters.ZoomSpeed * Time.deltaTime);
            _zoomScale = newZoomScale.LimitTo(_parameters.DefaultZoomRange);

            camera.Camera.fieldOfView = _parameters.FieldOfView * _zoomScale;
        }
        #endregion
    }
}
