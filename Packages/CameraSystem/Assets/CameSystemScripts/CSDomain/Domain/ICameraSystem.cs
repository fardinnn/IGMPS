﻿using System;

namespace BG.CameraSystems.Domain
{
    public interface ICameraSystem
    {
        Guid Id { get; set; }

        event EventHandler<RotateAroundAxisEventArgs> CameraRotated;
        event EventHandler<PanEventArgs> CameraPanned;
        event EventHandler<ZoomEventArgs> CameraZoomed;
    }
}
