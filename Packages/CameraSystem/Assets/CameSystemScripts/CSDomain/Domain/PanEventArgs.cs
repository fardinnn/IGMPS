﻿using System;
using UnityEngine;


namespace BG.CameraSystems.Domain
{
    public class PanEventArgs : EventArgs
    {
        public Vector2 Displacement { get; set; }
        public Vector3 Normal { get; set; }
    }
}
