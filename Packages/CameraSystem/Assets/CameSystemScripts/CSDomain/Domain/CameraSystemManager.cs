﻿using BG.API;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace BG.CameraSystems.Domain
{
    public class CameraSystemManager : Singleton<CameraSystemManager>
    {
        private static Dictionary<Guid,ICameraSystem> camerasSystems;
        
        protected override void Awake()
        {
            IsDestroyOnLoad = true;
            base.Awake();

            camerasSystems = new Dictionary<Guid, ICameraSystem>();
        }

        public void Add(ICameraSystem cameraSystem)
        {
            if (cameraSystem.Id == default) throw new ArgumentException("cameraSystem's id in not assigned");
            camerasSystems.Add(cameraSystem.Id, cameraSystem);
        }

        public static T Get<T>(Guid id) where T: class, ICameraSystem
        {
            if (camerasSystems == null) throw new NullReferenceException("_camerasSystems can not be null!");
            if (!camerasSystems.ContainsKey(id))
                Debug.LogError("There is no camera system with the provided id!");
            var target = camerasSystems[id] as T;
            if (target ==null)
                Debug.LogError("Provided type is not compatible with target system id!");
            return target;
        }
    }
}
