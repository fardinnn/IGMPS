﻿using System;
using UnityEngine;

namespace BG.CameraSystems.Domain
{
    public class RotateAroundAxisEventArgs : EventArgs
    {
        public float HorizontalRotation { get; set; }
        public float VerticalRotation { get; set; }
    }
}
