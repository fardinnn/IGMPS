﻿using System;
using UnityEngine;

namespace BG.CameraSystems.Domain
{
    public class SimpleCameraParameters : CameraParameters
    {
        public Transform CameraParent { get; set; }
        public Camera Camera { get; set; }
        public Transform TargetField { get; set; }
        public Vector3 RelativePosition { get; set; }
        public Transform FocusObject { get; set; }

        public int CullingMask { get; set; }
        public float FieldOfView { get; set; }
        public float NearClipPlan { get; set; }
        public float FarClipPlan { get; set; }
        public float Size { get; set; }

        public float MoveSpeed { get; set; }
        public float RotateSpeed { get; set; }
        public float ZoomSpeed { get; set; }
    }
}
