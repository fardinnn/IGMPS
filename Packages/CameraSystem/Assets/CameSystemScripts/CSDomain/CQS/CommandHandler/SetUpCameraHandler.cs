﻿using BG.API;
using UnityEngine;

namespace BG.CameraSystems.Domain
{
    public abstract class SetUpCameraHandler<TCommand> : ICommandHandler<TCommand> where TCommand : CameraCommandParameters
    {
        protected CameraSystemManager Manager;
        public virtual void Handle(TCommand command)
        {
            Manager = UnityEngine.Object.FindObjectOfType<CameraSystemManager>();
            if (Manager == null)
            {
                new GameObject("CameraManager").AddComponent<CameraSystemManager>();
                Manager = UnityEngine.Object.FindObjectOfType<CameraSystemManager>();
            }
        }
    }
}
