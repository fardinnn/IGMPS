﻿using BG.API;
using System;
using UnityEngine;

namespace BG.CameraSystems.Domain
{
    public class SetUpCameraManagerHandler : ICommandHandler<SetUpCameraManager>
    {
        public void Handle(SetUpCameraManager command)
        {
            new GameObject("CameraManager").AddComponent<CameraSystemManager>();
        }
    }
}
