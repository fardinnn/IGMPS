﻿using System;
using UnityEngine;

namespace BG.CameraSystems.Domain
{
    public class SetUpSimplePerspectiveCameraHandler : SetUpCameraHandler<SimplePerspectiveCameraParameters>
    {
        public override void Handle(SimplePerspectiveCameraParameters command)
        {
            if (command.FocusObject ==null) throw new ArgumentNullException("Focus point can not be null!");
            base.Handle(command);

            var cameraSystem = new SimplePerspectiveCameraSystem(command);
            cameraSystem.Id = command.SystemId;
            Manager.Add(cameraSystem);
        }
    }

    public class SetUpSimpleTopViewCameraHandler : SetUpCameraHandler<SimpleTopViewCameraParameters>
    {
        public override void Handle(SimpleTopViewCameraParameters command)
        {
            if (command.FocusObject == null) throw new ArgumentNullException("Focus point can not be null!");

            base.Handle(command);

            var cameraSystem = new SimpleTopViewCameraSystem(command);
            cameraSystem.Id = command.SystemId;
            Manager.Add(cameraSystem);

        }
    }
}
