﻿using BG.API;
using System;
using UnityEngine;

namespace BG.CameraSystems.Domain
{
    public class SimplePerspectiveCameraParameters : CameraCommandParameters
    {
        public Transform TargetField { get; set; }
        public Transform FocusObject { get; set; }
        public float FieldOfView { get; set; } = 45.0f;

        public float MoveSpeed { get; set; } = 1.0f;
        public float RotateSpeed { get; set; } = 1.0f;
        public float ZoomSpeed { get; set; } = 1.0f;


        public Bool2 FreezeRotation { get; set; } = false;
        public bool FreezeZoom { get; set; } = false;
        public Bool2 FreezeMove { get; set; } = false;

        public Vector3 DefaultEulerAngles { get; set; } = Vector3.zero;
        public float DefaultZoomScale { get; set; } = 1.0f;
        public Vector3 DefaultRelativePosition { get; set; } = Vector3.zero;

        public ParameterSettingType RotationConstraintsType { get; set; } = ParameterSettingType.Default;
        public Range<Vector3> DefaultEulerAnglesRange { get; set; } = new Range<Vector3> { Min = Vector3.zero, Max = Vector3.one * 360 };
        public ParameterSettingType ZoomConstraintsType { get; set; } = ParameterSettingType.Default;
        public Range<float> DefaultZoomRange { get; set; } = new Range<float> { Min = 0.1f, Max = 1.9f };
        public ParameterSettingType MoveConstraintsType { get; set; } = ParameterSettingType.Default;
        public Range<Vector3> DefaultMoveRange { get; set; } = new Range<Vector3> { Min = -10 * Vector3.one, Max = 10 * Vector3.one };
        public bool HasRayCaster { get; set; }
        public string CameraTag { get; set; }
        public Transform Parent { get; set; }
        public GameEntitiesVisitors<IComponentSystem> Components { get; set; }
    }
}