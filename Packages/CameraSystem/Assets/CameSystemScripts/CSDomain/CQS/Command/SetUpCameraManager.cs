﻿using BG.API;
using System;

namespace BG.CameraSystems.Domain
{
    public class SetUpCameraManager : ICommand
    {
        public Guid Id { get; set; } = Guid.NewGuid();
    }
}
