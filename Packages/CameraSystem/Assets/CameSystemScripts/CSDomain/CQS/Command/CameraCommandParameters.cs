﻿using BG.API;
using System;

namespace BG.CameraSystems.Domain
{
    public abstract class CameraCommandParameters : ICommand
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public Guid SystemId { get; set; } = Guid.NewGuid();
    }
}
