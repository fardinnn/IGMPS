﻿using UnityEngine;


namespace BG.CameraSystems.Domain
{
    public static class LimitRanges
    {
        public static float LimitTo(this float target, Range<float> range)
        {
            return target < range.Min ? range.Min :
                target > range.Max ? range.Max : target;
        }

        public static Vector2 LimitTo(this Vector2 target, Range<Vector2> range)
        {
            return new Vector2(target.x < range.Min.x ? range.Min.x :
                target.x > range.Max.x ? range.Max.x : target.x,
                target.y < range.Min.y ? range.Min.y :
                target.y > range.Max.y ? range.Max.y : target.y
                );
        }
        public static Vector3 LimitTo(this Vector3 target, Range<Vector3> range)
        {
            return new Vector3(target.x < range.Min.x ? range.Min.x :
                target.x > range.Max.x ? range.Max.x : target.x,
                target.y < range.Min.y ? range.Min.y :
                target.y > range.Max.y ? range.Max.y : target.y,
                target.z < range.Min.z ? range.Min.z :
                target.z > range.Max.z ? range.Max.z : target.z
                );
        }
    }


}
