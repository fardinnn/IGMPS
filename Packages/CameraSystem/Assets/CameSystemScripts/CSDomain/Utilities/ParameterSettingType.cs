﻿namespace BG.CameraSystems.Domain
{
    public enum ParameterSettingType
    {
        Default,
        Automatic,
        Manual
    }
}
