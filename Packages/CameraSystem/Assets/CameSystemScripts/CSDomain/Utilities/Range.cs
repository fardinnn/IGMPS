﻿namespace BG.CameraSystems.Domain
{
    public struct Range<T>
    {
        public T Min;
        public T Max;
    }
}
