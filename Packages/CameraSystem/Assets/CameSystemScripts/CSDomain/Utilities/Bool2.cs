﻿namespace BG.CameraSystems.Domain
{
    public struct Bool2
    {
        public bool X;
        public bool Y;

        public static implicit operator Bool2(bool value)
        {
            return new Bool2()
            {
                X = value,
                Y = value
            };
        }
    }
}
