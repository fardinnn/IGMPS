﻿using BG.API;
using BG.API.Abilities;
using BG.Extensions;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BG.BoardGamePieces.Domain
{
    public class SimplePieceSystem : IPieceSystem  
    {
        public Guid Id { get; set; }

        private readonly SimplePieceParameters parameters;

        public event EventHandler<FormingEventArgs> Formed;

        public SimplePieceSystem(SimplePieceParameters parameters)
        {
            this.parameters = parameters ?? throw new ArgumentNullException("SimplePieceParameters can not be null!");
            Id = parameters.SystemId;
            Generate();
        }

        public void UnSelect()
        {
            parameters.Renderer.sharedMaterial = parameters.DefaultMateral;
            HighLight(Color.red);
        }

        public void Select()
        {
            var meshrenderer = parameters.GameObject.GetComponent<MeshRenderer>();
            meshrenderer.sharedMaterial = parameters.HighlightMaterial;
            HighLight(Color.red);

            //CalculateNextMoves
        }
        
        public void HighLight(Color color)
        {

        }

        public void Move(Vector3 position)
        {

        }

        public void Form(object o, FormingEventArgs e)
        {

            Formed?.Invoke(this, e);
        }

        // ...

        Piece pieceEntity;
        public List<Task> coroutines = new List<Task>();

        public void Generate()
        {
            
            pieceEntity = new GameObject($"BoardGamePiece").AddComponent<Piece>();

            pieceEntity.transform.SetParent(parameters.Parent);
            pieceEntity.transform.position = Vector3.zero;


            coroutines.Add(new Task(VisitComponents(parameters.Components, new List<Entity> { pieceEntity })));

        }


        IEnumerator VisitComponents(GameEntitiesVisitors<IComponentSystem> components, List<Entity> entities)
        {
            foreach (var component in components)
            {
                yield return new WaitUntil(() => component.IsReady);
                var comp = component;
                component.Visit(entities);
            }
            foreach (var cachedState in parameters.Components.InitialStates)
            {
                cachedState.SetState(entities);
            }
            
            //foreach (var pieceEntity in entities)
            //{
            //    pieceEntity.SetInitialStates(parameters.Components.InitialStates);
            //}
        }
    }
}
