﻿using BG.API;
using BG.API.Abilities;
using System;

namespace BG.BoardGamePieces.Domain
{
    public class Piece : Entity, IActivable
    {

        //public Dictionary<Type, HashSet<IAbility>> Abilities { get; set; }


        //public void UpdateOptions(Options options)
        //{
        //    foreach (var item in Abilities)
        //    {
        //        options.AlterOn(Abilities);
        //    }
        //}

        public event EventHandler<ActivateEventArgs> Activated;


        public void Activate(object sender, ActivateEventArgs e)
        {
            Activated?.Invoke(this, new ActivateEventArgs { });
        }

    }
}
