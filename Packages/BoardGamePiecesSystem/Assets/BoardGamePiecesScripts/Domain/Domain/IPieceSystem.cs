﻿using System;

namespace BG.BoardGamePieces.Domain
{
    public interface IPieceSystem
    {
        Guid Id { get; set; }
    }
}
