using BG.API;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BG.BoardGamePieces.Domain
{
    public class PieceSystemManager : Singleton<PieceSystemManager>
    {
        private static Dictionary<Guid, IPieceSystem> _piecesSystems;

        protected override void Awake()
        {
            IsDestroyOnLoad = true;
            base.Awake();

            _piecesSystems = new Dictionary<Guid, IPieceSystem>();
        }

        public void Add(IPieceSystem pieceSystem)
        {
            if (pieceSystem.Id == default) throw new ArgumentException("IPieceSystem's id in not assigned");
            _piecesSystems.Add(pieceSystem.Id, pieceSystem);
        }

        public static T Get<T>(Guid id) where T : class, IPieceSystem
        {
            if (_piecesSystems == null) throw new NullReferenceException("_piecesSystems can not be null!");
            if (!_piecesSystems.ContainsKey(id))
                Debug.LogError("There is no piece system with the provided id!");
            var target = _piecesSystems[id] as T;
            if (target == null)
                Debug.LogError("Provided type is not compatible with target system id!");
            return target;
        }
    }
}
