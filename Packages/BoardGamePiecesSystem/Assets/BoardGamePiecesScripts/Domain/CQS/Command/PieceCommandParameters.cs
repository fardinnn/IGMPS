﻿using BG.API;
using System;
using UnityEngine;

namespace BG.BoardGamePieces.Domain
{
    public abstract class PieceCommandParameters : ICommand
    {
        public Guid Id { get; set; }

        public Guid SystemId { get; set; }
        public Transform Parent { get; set; }
    }
}
