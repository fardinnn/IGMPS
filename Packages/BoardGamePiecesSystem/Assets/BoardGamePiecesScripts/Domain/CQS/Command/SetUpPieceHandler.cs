﻿using BG.API;
using UnityEngine;

namespace BG.BoardGamePieces.Domain
{
    public abstract class SetUpPieceHandler<TCommandParameter> : ICommandHandler<TCommandParameter> where TCommandParameter : PieceCommandParameters
    {
        protected PieceSystemManager Manager;
        public virtual void Handle(TCommandParameter command)
        {
            Manager = Object.FindObjectOfType<PieceSystemManager>();
            if (Manager == null)
            {
                new GameObject("GridsManager").AddComponent<PieceSystemManager>();
                Manager = Object.FindObjectOfType<PieceSystemManager>();
            }
        }
    }
}
