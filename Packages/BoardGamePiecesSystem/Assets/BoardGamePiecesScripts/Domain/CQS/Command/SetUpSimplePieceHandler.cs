﻿namespace BG.BoardGamePieces.Domain
{
    public class SetUpSimplePieceHandler : SetUpPieceHandler<SimplePieceParameters>
    {
        public override void Handle(SimplePieceParameters command)
        {
            //Null checks
            base.Handle(command);

            var pieceSystem = new SimplePieceSystem(command);
            pieceSystem.Id = command.SystemId;
            Manager.Add(pieceSystem);

            //Initialize
        }
    }
}
