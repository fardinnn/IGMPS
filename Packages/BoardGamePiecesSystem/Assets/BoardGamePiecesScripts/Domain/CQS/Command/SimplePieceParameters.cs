﻿using BG.API;
using UnityEngine;

namespace BG.BoardGamePieces.Domain
{
    public class SimplePieceParameters : PieceCommandParameters
    {
        public GameObject GameObject { get; set; }
        public MeshRenderer Renderer { get; internal set; }
        public Material HighlightMaterial { get; internal set; }
        public Material DefaultMateral { get; internal set; }
        public GameEntitiesVisitors<IComponentSystem> Components { get; set; }
    }
}
