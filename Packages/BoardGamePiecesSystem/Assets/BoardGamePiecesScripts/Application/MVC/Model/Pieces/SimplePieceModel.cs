﻿using BG.API;
using BG.BoardGamePieces.Domain;
using BG.Grids;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace BG.BoardGamePieces
{
    public class SimplePieceModel : PieceModel
    {
        public IPieceState State { get; set; }
        public Guid PlayerId { get; set; }
        public Transform View3D { get; set; }
        public Texture2D View2D { get; set; }
        public Dictionary<IPieceState, IMoveRuleModel> MoveRules { get; set; }
        public PieceStateController StateController { get; set; }
        public Vector2Int PositionOnBoard { get; set; }
        public GameEntitiesVisitors<IComponentSystem> Components { get; set; }


        public SimplePieceParameters ToCommand()
        {
            return new SimplePieceParameters
            {
                Components = Components,
                Parent = Parent
            };
        }
    }
}
