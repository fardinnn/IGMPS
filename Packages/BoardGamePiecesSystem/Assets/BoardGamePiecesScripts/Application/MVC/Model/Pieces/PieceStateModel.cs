﻿using BG.API;
using System.Collections.Generic;

namespace BG.BoardGamePieces
{
    public class PieceStateModel : IModel
    {
        public List<IPieceState> States { get; set; }
    }
}
