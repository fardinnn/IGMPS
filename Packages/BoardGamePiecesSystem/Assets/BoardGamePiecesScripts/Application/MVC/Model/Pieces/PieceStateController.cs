﻿using System;
using System.Collections.Generic;

namespace BG.BoardGamePieces
{
    public class PieceStateController
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public Dictionary<string, IPieceState> States { get; set; }


        public IPieceState this[string stateName]
        {
            get { return States[stateName]; }
        }

    }
}
