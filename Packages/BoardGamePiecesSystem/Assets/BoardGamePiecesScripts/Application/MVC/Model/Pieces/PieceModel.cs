﻿using BG.API;
using UnityEngine;

namespace BG.BoardGamePieces
{
    public abstract class PieceModel : IModel
    {
        public Transform Parent { get; internal set; }
    }
}
