using System;
using System.Collections.Generic;

namespace BG.BoardGamePieces
{
    public class SimplePlayerPiecesModel : PiecesCollectionModel
    {
        public List<PieceView> Pieces { get; set; }
        public Guid PlayerId { get; set; }
    }
}
