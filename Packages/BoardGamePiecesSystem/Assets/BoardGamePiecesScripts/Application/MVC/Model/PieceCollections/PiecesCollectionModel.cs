﻿using BG.API;
using UnityEngine;

namespace BG.BoardGamePieces
{
    public abstract class PiecesCollectionModel : IModel
    {
        public Transform Parent { get; internal set; }
    }
}
