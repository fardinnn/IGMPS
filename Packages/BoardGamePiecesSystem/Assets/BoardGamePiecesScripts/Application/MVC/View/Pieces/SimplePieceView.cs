﻿namespace BG.BoardGamePieces
{
    public class SimplePieceView : PieceView
    {
        protected override void OnDisable()
        {
            (Controller as SimplePieceController)?.OnEnable();
        }

        protected override void OnEnable()
        {
            (Controller as SimplePieceController)?.OnDisable();
        }
    }
}
