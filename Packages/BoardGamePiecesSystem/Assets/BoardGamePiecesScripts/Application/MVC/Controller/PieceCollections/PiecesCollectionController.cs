﻿using BG.API;
using System;
using UnityEngine;

namespace BG.BoardGamePieces
{
    public abstract class PiecesCollectionController<TView, TModel> : IController<TView, TModel>
        where TView : PiecesCollectionView
        where TModel : PiecesCollectionModel
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public TView View { get; set; }
        public TModel Model { get; set; }

        public virtual void SetActivate(bool state)
        {
            View?.gameObject.SetActive(state);
        }

        public PiecesCollectionController(TView view = null, TModel model = null)
        {
            Model = model ?? CreateDefaultModel();
            View = view ?? CreateDefaultView();
            View.Controller = this;
        }

        protected virtual TModel CreateDefaultModel()
        {
            return (TModel)Activator.CreateInstance(typeof(TModel));
        }

        protected virtual TView CreateDefaultView()
        {
            var view = new GameObject(typeof(TView).Name.Replace("View", "")).AddComponent<TView>();
            if (Model.Parent != null) view.transform.SetParent(Model.Parent);
            return view;
        }

        public virtual void Dispose()
        {
            UnityEngine.Object.Destroy(View.gameObject);
        }
    }
}
