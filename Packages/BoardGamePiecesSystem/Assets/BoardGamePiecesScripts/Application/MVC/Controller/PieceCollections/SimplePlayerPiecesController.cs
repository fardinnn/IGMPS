using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BG.BoardGamePieces
{
    public class SimplePlayerPiecesController : PiecesCollectionController<SimplePlayerPiecesView, SimplePlayerPiecesModel>
    {
        private Dictionary<Type, dynamic> _pieceControllers { get; }


        public SimplePlayerPiecesController(SimplePlayerPiecesView view = null,
            SimplePlayerPiecesModel model = null) : base(view, model)
        {
            _pieceControllers = new Dictionary<Type, dynamic>();
        }

        public void AddPiece<TView>(TView view)
            where TView : PieceView
        {
            view.transform.SetParent(View.transform); // ????
            _pieceControllers.Add(view.Controller.GetType(), view.Controller);
            Activate(view.Controller.GetType());
        }

        private dynamic _activeCamera { get; set; }
        public PieceView GetActiveCameraView()
        {
            return _activeCamera.View;
        }
        public void Activate(Type controllerType)
        {
            if (!_pieceControllers.ContainsKey(controllerType)) throw new ArgumentException("Provided camera controller type is not created yet!");
            if ((_activeCamera != null) && (_pieceControllers[controllerType].Id == _activeCamera.Id)) return;

            _pieceControllers[controllerType].SetActivate(true);
            _activeCamera?.SetActivate(false);
            _activeCamera = _pieceControllers[controllerType];
        }
    }
}
