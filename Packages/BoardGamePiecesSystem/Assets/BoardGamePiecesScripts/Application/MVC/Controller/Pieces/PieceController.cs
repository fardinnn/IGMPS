﻿using BG.API;
using System;
using UnityEngine;

namespace BG.BoardGamePieces
{
    public abstract class PieceController<TView, TModel> : IController<TView, TModel>
        where TView : PieceView
        where TModel : PieceModel
    {
        public Guid Id { get; set; } = Guid.NewGuid();

        public TView View { get; set; }

        public TModel Model { get; set; }

        public virtual void SetActivate(bool state)
        {
            View?.gameObject.SetActive(state);
        }

        public PieceController(TView view = null, TModel model = null)
        {
            Model = model ?? CreateDefaultModel();
            View = view ?? CreateDefaultView();
            View.Controller = this;
        }

        protected virtual TModel CreateDefaultModel()
        {
            return (TModel)Activator.CreateInstance(typeof(TModel));
        }

        protected virtual TView CreateDefaultView()
        {
            var view = new GameObject(typeof(TView).Name.Replace("View", "")).AddComponent<TView>();
            if (Model.Parent != null) view.transform.SetParent(Model.Parent);
            return view;
        }


        public abstract void OnEnable();

        public abstract void OnDisable();


        public virtual void Dispose()
        {
            UnityEngine.Object.Destroy(View.gameObject);
        }
    }
}
