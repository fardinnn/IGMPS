﻿using BG.BoardGamePieces.Domain;
using System;
using System.Numerics;

namespace BG.BoardGamePieces
{
    public class SimplePieceController : PieceController<SimplePieceView, SimplePieceModel>
    {
        private SimplePieceSystem _pieceSystem;


        public SimplePieceController(SimplePieceView view = null,
            SimplePieceModel model = null)
            : base(view, model)
        {
            //InitializeInputs();
            var command = model.ToCommand();
            command.SystemId = Guid.NewGuid();
            new SetUpSimplePieceHandler().Handle(command);
            _pieceSystem = PieceSystemManager.Get<SimplePieceSystem>(command.SystemId);
        }


        public void Select()
        {

        }

        public void Move(Vector3 position)
        {

        }

        //public void Lose
        //public void Change
        //public void Win
        //public void DropOut
        //public void SetTrue

        /****************************************/
        /*             SetUp Inputs             */
        /****************************************/
        //private SimplePerspectiveCameraInput controls;
        //private void InitializeInputs()
        //{
        //    controls = new SimplePerspectiveCameraInput();
        //    controls.Enable();

        //    controls.Map1.Rotate_Values.performed += ctx => Rotate(ctx.ReadValue<Vector2>());
        //    controls.Map1.Pan_Values.performed += ctx => Pan(ctx.ReadValue<Vector2>());
        //    controls.Map1.Zoom_Values.performed += ctx => Zoom(ctx.ReadValue<float>());
        //    controls.Map1.Rotate_Buttons.performed += ctx => Rotate(ctx.ReadValue<Vector2>());
        //    controls.Map1.Pan_Buttons.performed += ctx => Pan(ctx.ReadValue<Vector2>());
        //    controls.Map1.Zoom_Buttons.performed += ctx => Zoom(ctx.ReadValue<float>());
        //}

        public override void OnEnable()
        {
        //    controls.Enable();
        }

        public override void OnDisable()
        {
        //    controls.Disable();
        }
    }
}
