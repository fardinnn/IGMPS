﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace BG.Extensions
{
    public static class MathExtensions
    {
        public static float ToDeltaAngle(this Vector2 delta, Vector2 position, float centerWidth = -1, float centerHeight = -1)
        {
            Vector2 center = new Vector2(centerWidth == -1 ? Screen.width / 2.0f : centerWidth,
                centerHeight == -1 ? Screen.height / 2.0f : centerHeight);
            Vector2 r = position - center;
            float r_m = r.magnitude;
            Vector2 r_n = r / r_m;

            Vector2 delta_t = Vector2.Dot(delta, r_n) * r_n;
            Vector2 delta_n = delta - delta_t;
            float cross = (delta_n.x * r_n.y - delta_n.y * r_n.x);
            return (-Mathf.Sign(cross) * Mathf.Atan2(delta_n.magnitude, r_m) * Mathf.Rad2Deg);
        }

        public static float ToPolarAngle(this Vector2 vector)
        {
            return 0;
        }

        private static Dictionary<Type, object> defaultValues = new Dictionary<Type, object>
            {
                { typeof(int), default(int) },
                { typeof(float), default(float) },
                { typeof(bool), default(bool) },
                { typeof(Vector2), default(Vector2) },
                { typeof(Vector3), default(Vector3) }
            };
        public static Dictionary<Type, object> Default { get => defaultValues; }

        public static bool IsLimitedTo(this Vector2Int self, Vector2Int max, Vector2Int min = default, bool inclusive = false)
        {
            return inclusive? (self.x <= max.x && self.y <= max.y && self.x >= min.x && self.y >= min.y) :
                (self.x < max.x && self.y < max.y && self.x > min.x && self.y > min.y);
        }
    }
}