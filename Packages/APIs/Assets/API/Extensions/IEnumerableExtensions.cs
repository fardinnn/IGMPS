using BG.API;
using BG.States;
using System;
using System.Collections.Generic;

namespace BG.Extensions
{


    public static class IEnumerableExtensions 
    {
        public static IEnumerable<T> ForEach<T>(this IEnumerable<T> collection, Action<T> action)
        {
            foreach (var item in collection)
                action(item);

            return collection;
        }

        public static void Add<T>(this List<T> collection, IEnumerable<T> newItems)
        {
            foreach (T item in newItems)
                collection.Add(item);
        }

        public static HashSet<T> AddIfNotExist<T>(this HashSet<T> hs, T item)
        {
            if (!hs.Contains(item))
                hs.Add(item);
            return hs;
        }

        public static Dictionary<T1,T2> AddIfNotExist<T1, T2>(this Dictionary<T1, T2> dc, T1 item1, T2 item2)
        {
            if (!dc.ContainsKey(item1))
                dc.Add(item1, item2);
            return dc;
        }

        public static HashSet<T> AddIfNotExist<T>(this HashSet<T> hs, IEnumerable<T> items)
        {
            items.ForEach(i =>
            {
                if (!hs.Contains(i))
                    hs.Add(i);
            });
            return hs;
        }
        public static HashSet<T> AddIfNotExist<T>(this HashSet<T> hs, params T[] items)
        {
            items.ForEach(i =>
            {
                if (!hs.Contains(i))
                    hs.Add(i);
            });
            return hs;
        }

        public static HashSet<T> ToHashSet<T>(this IEnumerable<T> ts)
        {
            HashSet<T> hs = new HashSet<T>();
            foreach (var item in ts)
            {
                if (!hs.Contains(item))
                    hs.Add(item);
            }
            return hs;
        }
        public static StateGroup ToStateGroup(this IEnumerable<Enum> ts)
        {
            StateGroup sg = new StateGroup();
            foreach (var item in ts)
            {
                if (!sg.Contains(item))
                    sg.Add(item);
            }
            return sg;
        }

        public static void RemoveIfExists<TKey, TValue>(this Dictionary<TKey, TValue> self, TKey key)
        {
            if (self.ContainsKey(key)) self.Remove(key);
        }
        public static void AddIfNotExists<TKey, TValue>(this Dictionary<TKey, TValue> self, TKey key, TValue value)
        {
            if (!self.ContainsKey(key)) self.Add(key, value);
        }
    }
}