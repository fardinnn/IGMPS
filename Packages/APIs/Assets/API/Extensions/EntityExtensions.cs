﻿using BG.API;
using UnityEngine;

namespace BG.Extensions
{
    public static class EntityExtensions
    {
        public static void SetLayer<TComponent>(this Entity entity, int layer, bool includeInactive = false) where TComponent: Component
        {
            foreach (TComponent component in entity.GetComponentsInChildren<TComponent>(includeInactive))
                component.gameObject.layer = layer;
        }

        public static void SetLayer(this Entity entity, int layer, bool includeInactive = false)
        {
            foreach (Transform transform in entity.GetComponentsInChildren<Transform>(includeInactive))
                transform.gameObject.layer = layer;
        }
    }
}