﻿
namespace BG.API
{
    public interface IAspectCommandHandler<TAspectCommand> : ICommandHandler<TAspectCommand>
        where TAspectCommand : IActionCommand
    { }
}