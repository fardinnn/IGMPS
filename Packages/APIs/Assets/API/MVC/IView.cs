﻿
// ReSharper disable once CheckNamespace

using System;
using System.Collections.Generic;

namespace BG.API
{
    public interface IView: IDisposable
    {
        Guid Id { get; set; }
    }

    public interface IDynamicView : IView
    {
        IEnumerable<TAbility> GetAbilities<TAbility>();
        IEnumerable<TSubscriber> GetSubscribers<TSubscriber>();
    }
}