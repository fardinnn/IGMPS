﻿// ReSharper disable once CheckNamespace

using System;

// ReSharper disable once CheckNamespace IdentifierTypo
namespace BG.API
{
    public interface IController
    {
        Guid Id { get; }
    }
    public interface IController<out TView, out TModel> : IController where TView : IView where TModel : IModel
    {
        TView View { get; }
        TModel Model { get; }
    }

    public interface IComponentController : IController
    {
        void AddStatesGroup(params Enum[] groupStates);
        IComponentSystem ProvideComponentSystem(Options options = null);
    }

    public interface IComponentController<out TView, out TModel> : IController<TView, TModel>, IComponentController
        where TView : IView where TModel : IModel
    {
    }

    public interface IEntityController : IController
    {
        void Generate(Options options = null, bool activate = true);
    }

    public interface IEntityController<out TView, out TModel> : IController<TView, TModel>, IEntityController
        where TView : IView where TModel : IModel
    { }
}