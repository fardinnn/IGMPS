﻿
// ReSharper disable once CheckNamespace

using System;
using System.Collections.Generic;
using UnityEngine;

namespace BG.API
{
    public class EmptyView : MonoBehaviour, IDynamicView
    {
        public Guid Id { get; set; }

        public void Dispose()
        {
            Destroy(gameObject);
        }

        public IEnumerable<TAbility> GetAbilities<TAbility>()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<TSubscriber> GetSubscribers<TSubscriber>()
        {
            throw new NotImplementedException();
        }
    }
}