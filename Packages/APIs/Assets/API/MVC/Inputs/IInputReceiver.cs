﻿// ReSharper disable once CheckNamespace

using System;
using System.Collections.Generic;

// ReSharper disable once CheckNamespace IdentifierTypo
namespace BG.API
{
    public interface IInputReceiver
    {
        public string InputMapName { get; }
        /// <summary>
        /// Keys are the action names
        /// Values are target actions
        /// </summary>
        Dictionary<string, EventHandler<IInputValue>> Actions { get; }
        Dictionary<string, EventHandler<IInputValue>> CancelActions { get; }
        /// <summary>
        /// Keys are the action names
        /// Values are inputs in JSON format
        /// </summary>
        string JSONInputs { get; }

        event EventHandler Enabled;
        event EventHandler Disabled;

        string CoExistGroup { get; set; }
    }
}