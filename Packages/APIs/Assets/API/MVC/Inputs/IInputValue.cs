﻿// ReSharper disable once CheckNamespace


// ReSharper disable once CheckNamespace IdentifierTypo
namespace BG.API
{
    public interface IInputValue
    {
        T ReadValue<T>() where T : struct;
    }

}