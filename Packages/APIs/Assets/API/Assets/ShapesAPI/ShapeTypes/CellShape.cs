using BG.API.Abilities;
using System;
using UnityEngine;

namespace BG.API.Shapes
{
    public abstract class Shape : MonoBehaviour, IResizable
    {
        public event EventHandler<ResizeEventArgs> Resized;
        [SerializeField] private Vector3 TemplateSize = Vector3.one;

        public virtual void Resizing(object sender, ResizeEventArgs e)
        {
            var scaleFactor = new Vector3();
            if (TemplateSize.x == 0 || TemplateSize.y == 0 || TemplateSize.z == 0) throw new ArithmeticException($"Template size for group '{this}' has zero size component");
            if (e.KeepAspects)
            {
                scaleFactor = new Vector3(1.0f, 1.0f, 1.0f) * Mathf.Min(new float[] { e.Size.x / TemplateSize.x
                , e.Size.y / TemplateSize.y
                , e.Size.z / TemplateSize.z});
            }
            else
            {
                scaleFactor = new Vector3(e.Size.x / TemplateSize.x,
                    e.Size.y / TemplateSize.y,
                    e.Size.z / TemplateSize.z);
            }
            transform.localScale = scaleFactor;
            Resized?.Invoke(this, e);
        }
    }
    public abstract class CellShape : Shape
    {

    }
}
