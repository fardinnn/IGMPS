﻿using System;
using System.Collections;
using System.Threading.Tasks;

namespace BG.API.Shapes
{
    public interface IShapePackage
    {
        Shape LoadedTemplate { get; }
        IEnumerator GetTemplate<T>() where T : Shape;
        IEnumerator GetTemplate(Type shapeType);
        IEnumerator GetTemplate(Type shapeType, string shapeName, bool limitedByType = false);
    }
}
