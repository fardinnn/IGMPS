using System;
using UnityEngine;

namespace BG.API.Audios
{
    public interface ISFX
    {
        event EventHandler<SFXEventArgs> Played;
        event EventHandler<SFXEventArgs> Paused;
        event EventHandler<SFXEventArgs> Stopped;
        public void Play();
        public void Pause();
        public void Stop();
    }

    public abstract class SFX : Component, ISFX
    {
        public int Variant { get; set; }

        public AudioSource audioSource { get; set; }

        public event EventHandler<SFXEventArgs> Played;
        public event EventHandler<SFXEventArgs> Paused;
        public event EventHandler<SFXEventArgs> Stopped;
        public void Play()
        {
            Played?.Invoke(this, null);
        }

        public void Pause()
        {
            Paused?.Invoke(this, null);
        }

        public void Stop()
        {
            Stopped?.Invoke(this, null);
        }
    }


    public class SFXEventArgs : EventArgs
    {
        public SFX SFX { get; set; }
    }

    public class WoodImpact : SFX { }
    public class DoorKnock : SFX { }
    public class GunShoot : SFX { }

}
