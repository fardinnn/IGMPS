using System;
using UnityEngine;

namespace BG.API.Audios
{


    public abstract class Music : MonoBehaviour
    {
        public int Variant { get; set; }

        public event EventHandler Played;
        public event EventHandler Paused;
        public event EventHandler Stopped;
        public void Play()
        {
            Played?.Invoke(this, null);
        }

        public void Pause()
        {
            Paused?.Invoke(this, null);
        }

        public void Stop()
        {
            Stopped?.Invoke(this, null);
        }
    }

    public class SimpleMusic : Music { }
}
