﻿using System;
using System.Threading.Tasks;

namespace BG.API
{
    public delegate Task AsyncEventHandler(object o, EventArgs e);
}