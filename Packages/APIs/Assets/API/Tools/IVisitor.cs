﻿// ReSharper disable once IdentifierTypo CheckNamespace

namespace BG.API
{
    public interface IVisitor
    {

    }


    public interface IAcceptor<T> where T : IVisitor
    {
        void Accept(T visitor);
    }
}