﻿using System;

namespace BG.API
{
    public class Empty : ICommand, IQuery
    {
        public Guid Id { get; set; }
    }
}