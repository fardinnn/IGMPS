﻿using System;
using System.Collections.Generic;

namespace BG.API
{

    public interface IComponentSystem : IDisposable
    {
        void Visit(List<Entity> entities);
        bool IsReady { get; }
        Guid Id { get; set; }
    }
}