﻿using System;
using UnityEngine;

namespace BG.API
{
    public class IdentificationClass : MonoBehaviour
    {
        public Guid Id { get; set; }
    }
}
