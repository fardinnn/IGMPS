using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;

namespace BG.API
{
    public interface IDbCache : IDisposable
    {
        bool Contains<TEntity>(IAssetId id);
        bool Contains<TEntity>(TEntity entity);
        void Add<TEntity>(TEntity entity, IAssetId id, IComponentSystem component);
        Task<TEntity> Get<TEntity>(IAssetId id, IComponentSystem component);
        void Release<TEntity>(TEntity entity);
        void Release<TEntity>(TEntity entity, IComponentSystem component);
    }

    public interface IDbSet<T> :IQueryable<T>
        where T : class
    {
        void Add(T entity);
        T Find(int id);
        void Remove(T entity);
        void RemoveRange(IEnumerable<T> entities);
    }
}
