﻿// ReSharper disable once IdentifierTypo CheckNamespace
using BG.States;
using System;
using System.Collections.Generic;

namespace BG.API
{
    public class GameEntitiesVisitors<T> : List<T>
        where T : IComponentSystem
    {
        public HashSet<IStateSetter> InitialStates { get; set; }

        public GameEntitiesVisitors(): base()
        {
            InitialStates = new HashSet<IStateSetter>();
        }

        public void Add(IStateSetter initialState)
        {
            InitialStates.Add(initialState);
        }

        public void Add<T1, T2> (T1 state, T2 stateChangeEventArgs)
        where T1 : Enum
        where T2 : class, IStateChangedEventArgs<T1, T2>
        {
            InitialStates.Add(new CacheState<T1, T2>(state, stateChangeEventArgs));
        }

        public void Add(params T[] items)
        {
            for (int i = 0; i < items.Length; i++)
                base.Add(items[i]);
        }
        //public FixedString32 GroupTag;
        //private List<T> visitors = new List<T>();

        //public void VisitAll(NativeArray<Entity> entities)
        //{
        //    visitors.ForEach(v => v.Visit(GroupTag, entities));
        //}

        //public GameEntitiesVisitors()
        //{
        //    GroupTag = "dafault";
        //}
        //public GameEntitiesVisitors(FixedString32 groupTag)
        //{
        //    GroupTag = groupTag;
        //}


        //public int Count => visitors.Count;

        //public bool IsReadOnly => ((ICollection<T>)visitors).IsReadOnly;

        //public void Add(T item)
        //{
        //    visitors.Add(item);
        //}

        //public void Clear()
        //{
        //    visitors.Clear();
        //}

        //public bool Contains(T item)
        //{
        //    return visitors.Contains(item);
        //}

        //public void CopyTo(T[] array, int arrayIndex)
        //{
        //    visitors.CopyTo(array, arrayIndex);
        //}

        //public bool Remove(T item)
        //{
        //    return visitors.Remove(item);
        //}

        //public IEnumerator<T> GetEnumerator()
        //{
        //   return visitors.GetEnumerator();
        //}

        //IEnumerator IEnumerable.GetEnumerator()
        //{
        //    yield return visitors.GetEnumerator();
        //}
    }
}