﻿using System;
using System.Collections.Generic;

// ReSharper disable once CheckNamespace IdentifierTypo
namespace BG.API
{
    public class InstanceContainer<TBase>
    {
        private readonly Dictionary<Type, TBase> _instances;
        public InstanceContainer()
        {
            _instances = new Dictionary<Type, TBase>();
        }

        public T Get<T>() where T : TBase
        {
            return (T)_instances[typeof(T)];
        }

        public void Set<T>(T value) where T : TBase
        {
            _instances.Add(typeof(T), value);
        }
    }
}