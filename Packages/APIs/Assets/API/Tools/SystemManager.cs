﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace BG.API
{
    public abstract class SystemManager<T1, T2> : Singleton<T1>
        where T1 : SystemManager<T1, T2>
        where T2 : IComponentSystem
    {
        protected static Dictionary<Guid, T2> Systems;

        protected override void Awake()
        {
            IsDestroyOnLoad = true;
            base.Awake();

            Systems = new Dictionary<Guid, T2>();
        }

        public void Add(T2 system)
        {
            if (system.Id == default) throw new ArgumentException("system's id in not assigned");
            Systems.Add(system.Id, system);
        }

        public static T Get<T>(Guid id) where T : class, T2
        {
            if (Systems == null) throw new NullReferenceException("systems can not be null!");
            if (!Systems.ContainsKey(id))
                Debug.LogError("There is no system with the provided id!");
            var target = Systems[id] as T;
            if (target == null)
                Debug.LogError("Provided type is not compatible with target system id!");
            return target;
        }
    }
}