﻿//using System.Linq;
//using UnityEngine;

//namespace Fardin.API
//{
//    public abstract class FindAccessPoint<TQuery, TResult> : IQueryHandler<TQuery, TResult>
//        where TQuery : ISearchIdQuery
//        where TResult: AccessPoint
//    {
//        public TResult Query(TQuery parameters)
//        {
//            return SearchForId(parameters);
//        }

//        protected virtual TResult SearchForId(TQuery parameters)
//        {
//            var founded = Object.FindObjectsOfType<TResult>().Where(t => t.Id == parameters.TargetId);
//            if (founded.Count() == 0)
//            {
//                Debug.LogError($"Did not found target object of type={typeof(TResult)}! with id={parameters.TargetId}");
//                return null;
//            }

//            var transition = founded.First();
//            return transition;
//        }
//    }
//}