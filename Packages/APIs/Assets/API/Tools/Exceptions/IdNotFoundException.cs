﻿using System;

namespace BG.API.Exceptions
{
    public class IdNotFoundException : Exception
    {
        public IdNotFoundException() : base() { }
        public IdNotFoundException(string message) : base(message) { }
        public IdNotFoundException(string message, Exception innerException) : base(message, innerException) { }
    }
}
