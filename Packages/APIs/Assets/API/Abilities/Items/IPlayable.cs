﻿using System;
using System.Threading.Tasks;

namespace BG.API.Abilities
{
    public interface IPlayable : IAbility
    {
        event EventHandler<PlayEventArgs> Played;
        void Play(object sender, PlayEventArgs e);
    }

    public interface IPlaySubscriber : ISubscriber
    {
        void OnPlayed(object sender, PlayEventArgs e);
    }
    
    public class PlayEventArgs : EventArgs
    {

    }
}