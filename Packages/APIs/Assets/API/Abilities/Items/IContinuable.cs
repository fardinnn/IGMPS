﻿using System;

// ReSharper disable once CheckNamespace
namespace BG.API.Abilities
{
    public interface IContinuable : IAbility
    {
        event EventHandler<ContinueEventArgs> Continued;
        void Continue(object sender, ContinueEventArgs e);
    }
    public interface IContinueSubscriber : ISubscriber
    {
        void OnContinued(object sender, ContinueEventArgs e);
    }
    public class ContinueEventArgs : EventArgs
    {

    }
}