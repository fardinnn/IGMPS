﻿using System;

// ReSharper disable once CheckNamespace
namespace BG.API.Abilities
{
    public interface IAddableItem<T> : IAbility
    {
        event EventHandler<AddItemEventArgs<T>> ItemAdded;
        void AddItem(object sender, AddItemEventArgs<T> e);
    }
    public interface IAddItemSubscriber : ISubscriber
    {
        void OnItemAdded<T>(object sender, AddItemEventArgs<T> e);
    }
    public class AddItemEventArgs<T> : EventArgs
    {
        public T Item { get; }

        public AddItemEventArgs(T item)
        {
            Item = item;
        }
    }
}