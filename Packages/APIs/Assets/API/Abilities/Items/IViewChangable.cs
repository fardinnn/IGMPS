﻿using BG.States;
using System;

// ReSharper disable once CheckNamespace
namespace BG.API.Abilities
{
    public interface IViewChangable : IAbility
    {
        event EventHandler<ViewChangeEventArgs> ViewChanged;
        void Change(object sender, ViewChangeEventArgs e);
    }
    public interface IViewChangeSubscriber : ISubscriber
    {
        void OnViewChanged<T>(object sender, ViewChangeEventArgs e);
    }
    public class ViewChangeEventArgs : EventArgs
    {
        public ViewAngleState ViewAngleState { get; set; } // Very Important
    }
}