﻿using System;

// ReSharper disable once CheckNamespace
namespace BG.API.Abilities
{
    public interface IFailable : IAbility
    {
        event EventHandler<FailEventArgs> Failed;
        void Fail(object sender, FailEventArgs e);
    }

    public interface IFailSubscriber : ISubscriber
    {
        void OnFailed(object sender, FailEventArgs e);
    }
    public class FailEventArgs : EventArgs
    {

    }
}