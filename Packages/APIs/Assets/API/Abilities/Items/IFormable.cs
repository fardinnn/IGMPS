﻿using BG.API.Shapes;
using System;
using Unity.Collections;
using UnityEngine;

// ReSharper disable once CheckNamespace
namespace BG.API.Abilities
{
    public interface IFormable : IAbility
    {
        event EventHandler<FormingEventArgs> Formed;
        void Form(object o, FormingEventArgs e);
    }

    public interface IFormingSubscriber : ISubscriber
    {
        void OnFormed(object sender, FormingEventArgs e);
    }

    public class FormingEventArgs : EventArgs
    {
        public string ShapeName { get; set; }
        public IShapePackage Package { get; set; }
        public Type ShapeType { get; set; }
        //public ComponentType GroupTag { get; set; }
    }
}