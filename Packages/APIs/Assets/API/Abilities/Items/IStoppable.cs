﻿using System;

namespace BG.API.Abilities
{
    public interface IStoppable : IAbility
    {
        event EventHandler<StopEventArgs> Stopped;
        void Stop(object o, StopEventArgs e);
    }

    public interface IStopSubscriber : ISubscriber
    {
        void OnStopped(object sender, StopEventArgs e);
    }

    public class StopEventArgs : EventArgs
    {

    }

}