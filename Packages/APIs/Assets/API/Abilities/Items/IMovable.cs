﻿using System;

namespace BG.API.Abilities
{
    public interface IMovable : IAbility
    {
        event EventHandler<MoveEventArgs> Moved;
        void Move(object sender, MoveEventArgs e);
    }

    public interface IMoveSubscriber : ISubscriber
    {
        void OnMoved(object sender, MoveEventArgs e);
    }
    public class MoveEventArgs : EventArgs
    {

    }
}