﻿using System;

// ReSharper disable once CheckNamespace
namespace BG.API.Abilities
{
    public interface IRemovableItem : IAbility
    {
        event EventHandler<RemoveItemEventArgs> ItemRemoved;
        void RemoveItem(object o, RemoveItemEventArgs e);
    }
    public interface IRemoveItemSubscriber : ISubscriber
    {
        void OnItemRemoved(object sender, RemoveItemEventArgs e);
    }
    public class RemoveItemEventArgs : EventArgs
    {

    }
}