﻿using System;

// ReSharper disable once CheckNamespace
namespace BG.API.Abilities
{
    public interface IAddableDirectory : IAbility
    {
        event EventHandler<AddDirectoryEventArgs> DirectoryAdded;
        void AddDirectory(object sender, AddDirectoryEventArgs e);
    }
    public interface IAddDirectorySubscriber : ISubscriber
    {
        void OnDirectoryAdded(object sender, AddDirectoryEventArgs e);
    }
    public class AddDirectoryEventArgs : EventArgs
    {
        public string Directory { get; set; }
    }

}