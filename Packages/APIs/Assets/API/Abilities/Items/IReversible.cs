﻿using System;

// ReSharper disable once CheckNamespace
namespace BG.API.Abilities
{
    public interface IReversible : IAbility
    {
        event EventHandler<ReverseEventArgs> Reversed;
        void Reverse(object o, ReverseEventArgs e);
    }
    public interface IReverseSubscriber : ISubscriber
    {
        void OnReversed(object sender, ReverseEventArgs e);
    }
    public class ReverseEventArgs : EventArgs
    {

    }
}