﻿using System;

// ReSharper disable once CheckNamespace
namespace BG.API.Abilities
{
    public interface IDroppableRegion : IAbility
    {
        Entity Entity { get; set; }
        IDroppable DroppedEntity { get; set; }

        void DragExit();
        void DragEnter();
        void DropIn(IDroppable DroppedEntity, bool select = true);
        void TakenOut(Entity entity);
        void DragFailed();
    }
    public interface IDropSubscriber : ISubscriber
    {
        void OnDropped(object sender, DropEventArgs e);
    }
    public class DropEventArgs : EventArgs
    {
        public Entity DroppedEntity { get; set; }
        public Entity DropRegionEntity { get; set; }
    }


    public interface IDroppable : IAbility
    {
        Entity Entity { get; set; }
        IDroppableRegion DroppableRegion { get; set; }
    }

}