﻿using System;
using Unity.Collections;

// ReSharper disable once CheckNamespace
namespace BG.API.Abilities
{
    public interface IScalable : ITransientAbility
    {
        event EventHandler<ScaleEventArgs> Scaled;
        void Scale(object o, ScaleEventArgs e);
    }

    public interface IScaleSubscriber : ISubscriber
    {
        void OnScaled(object sender, ScaleEventArgs e);
    }

    public class ScaleEventArgs : EventArgs
    {
        public float ScaleFactor { get; set; }
        //public ComponentType GroupTag { get; internal set; }
    }

}