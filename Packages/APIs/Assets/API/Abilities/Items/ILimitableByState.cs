﻿using BG.States;
using System;

namespace BG.API.Abilities
{
    public interface ILimitableByState : IAbility
    {
        event EventHandler<StateEventArgs> LimitedByState;
        void LimitByState(object o, StateEventArgs e);
    }
    public interface LimitByStateSubscriber : ISubscriber
    {
        void OnSubscribe(object sender, StateEventArgs e);
    }
    public class RequiredStatesEventArgs : EventArgs
    {
        public CompositeStatesSet StatesSet { get; set; }
    }
}