﻿// ReSharper disable once CheckNamespace
namespace BG.API.Abilities
{
    public interface ISpeedChangeable : IChangeable<SpeedChangeEventArgs> { }
    public interface ISpeedChangeSubscriber : IChangeSubscriber<SpeedChangeEventArgs> { }
    public class SpeedChangeEventArgs : ChangeEventArgs
    {
    }
}