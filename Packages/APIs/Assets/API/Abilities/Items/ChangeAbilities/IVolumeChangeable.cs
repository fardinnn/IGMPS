﻿// ReSharper disable once CheckNamespace
namespace BG.API.Abilities
{
    public interface IVolumeChangeable : IChangeable<VolumeChangeEventArgs> { }
    public interface IVolumeChangeSubscriber : IChangeSubscriber<VolumeChangeEventArgs> { }
    public class VolumeChangeEventArgs : ChangeEventArgs { }
}