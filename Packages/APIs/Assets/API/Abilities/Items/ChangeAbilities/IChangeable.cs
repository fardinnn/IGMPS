﻿using System;

// ReSharper disable once CheckNamespace
namespace BG.API.Abilities
{
    public interface IChangeable<T> : IAbility where T: ChangeEventArgs
    {
        event EventHandler<T> Changed;
        void Change(object sender, T e);
    }
    public interface IChangeSubscriber<T> : ISubscriber where T : ChangeEventArgs
    {
        void OnChanged(object sender, T e);
    }
    public class ChangeEventArgs : EventArgs
    {
        public float Value { get; set; }
    }
}