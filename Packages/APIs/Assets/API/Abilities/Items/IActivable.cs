﻿using System;

// ReSharper disable once CheckNamespace
namespace BG.API.Abilities
{
    public interface IActivable : IAbility
    {
        event EventHandler<ActivateEventArgs> Activated;
        void Activate(object sender, ActivateEventArgs e);
    }
    public interface IActivateSubscriber : ISubscriber
    {
        void OnActivated(object sender, ActivateEventArgs e);
    }
    public class ActivateEventArgs : EventArgs
    {

    }
}