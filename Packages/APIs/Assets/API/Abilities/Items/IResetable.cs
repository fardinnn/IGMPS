﻿using System;

// ReSharper disable once CheckNamespace
namespace BG.API.Abilities
{
    public interface IResetable : IAbility
    {
        event EventHandler<ResetEventArgs> Resetting;
        void Reset(object o, ResetEventArgs e);
    }
    public interface IResetSubscriber : ISubscriber
    {
        void OnResetting(object sender, ResetEventArgs e);
    }
    public class ResetEventArgs : EventArgs
    {

    }
}