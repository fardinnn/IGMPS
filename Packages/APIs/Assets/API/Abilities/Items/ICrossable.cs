﻿using System;

// ReSharper disable once CheckNamespace
namespace BG.API.Abilities
{
    public interface ICrossable : IAbility
    {
        event EventHandler<CrossEventArgs> Crossed;
        void Cross(object sender, CrossEventArgs e);
    }
    public interface ICrossSubscriber : ISubscriber
    {
        void OnCrossed(object sender, CrossEventArgs e);
    }
    public class CrossEventArgs : EventArgs
    {

    }
}