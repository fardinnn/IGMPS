﻿using System;

// ReSharper disable once CheckNamespace
namespace BG.API.Abilities
{
    public interface IRollable : ITransientAbility
    {
        event EventHandler<RollEventArgs> Rolled;
        void Roll(object o, RollEventArgs e);
    }
    public interface IRollSubscriber : ISubscriber
    {
        void OnRolled(object sender, RollEventArgs e);
    }

    public class RollEventArgs : EventArgs
    {

    }

}