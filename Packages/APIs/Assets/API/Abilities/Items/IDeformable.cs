﻿using System;

// ReSharper disable once CheckNamespace
namespace BG.API.Abilities
{
    public interface IDeformable<T> : IAbility where T: DeformEventArgs
    {
        event EventHandler<T> Deformed;
        void Deform(object sender, T e);
    }
    public interface IDeformSubscriber<T> : ISubscriber where T : DeformEventArgs
    {
        void OnObjectDeformed(object sender, T e);
    }
    public class DeformEventArgs : EventArgs
    {

    }
}