﻿using System;

namespace BG.API.Abilities
{
    public interface ISerializable : ITransientAbility
    {
        event EventHandler<SerializeEventArgs> Serialized;
        void Serialize(object sender, SerializeEventArgs e);
    }

    public interface ISerializeSubscriber : ISubscriber
    {
        void OnSerialized(object sender, out SerializeEventArgs e);
    }

    public class SerializeEventArgs : EventArgs
    {

    }
}