﻿using System;

namespace BG.API.Abilities
{
    public interface ISteppable : IAbility
    {
        event EventHandler<StepEventArgs> Stepped;
        void Step(object o, StepEventArgs e);
    }
    public interface IStepSubscriber : ISubscriber
    {
        void OnStepped(object sender, StepEventArgs e);
    }
    public class StepEventArgs : EventArgs
    {
        public int StepCount { get; set; }
    }
}