﻿using System;

// ReSharper disable once CheckNamespace
namespace BG.API.Abilities
{
    public interface IRotatable : ITransientAbility
    {
        event EventHandler<RotateEventArgs> Rotated;
        void Rotate(object o, RotateEventArgs e);
    }
    public interface IRotateSubscriber : ISubscriber
    {
        void OnRotated(object sender, RotateEventArgs e);
    }
    public class RotateEventArgs : EventArgs
    {

    }
}