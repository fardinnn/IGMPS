﻿using System;

namespace BG.API.Abilities
{
    public interface ISkippable : IAbility
    {
        event EventHandler<SkipEventArgs> Skipped;
        void Skip(object sender, SkipEventArgs e);
    }

    public interface SkipSubscriber : ISubscriber
    {
        void OnSkipped(object sender, SkipEventArgs e);
    }

    public class SkipEventArgs : EventArgs
    {

    }
}