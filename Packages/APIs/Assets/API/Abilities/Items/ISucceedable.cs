﻿using System;

namespace BG.API.Abilities
{
    public interface ISucceedable : IAbility
    {
        event EventHandler<SucceedEventArgs> Succeeded;
        void Succeed(object sender, SucceedEventArgs e);
    }

    public interface ISucceedSubscriber : ISubscriber
    {
        void OnSucceeded(object sender, SucceedEventArgs e);
    }

    public class SucceedEventArgs : EventArgs
    {

    }

}