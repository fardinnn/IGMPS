﻿using System;

// ReSharper disable once CheckNamespace
namespace BG.API.Abilities
{
    public interface IHighlightable : IAbility
    {
        event EventHandler<HighlightEventArgs> Highlighted;
        void Highlight(object sender, HighlightEventArgs e);
    }
    public interface IHighlightSubscriber : ISubscriber
    {
        void OnHighlighted(object sender, HighlightEventArgs e);
    }
    public class HighlightEventArgs : EventArgs
    {

    }
}