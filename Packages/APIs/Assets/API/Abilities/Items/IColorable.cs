﻿using System;
using Unity.Collections;
using UnityEngine;

// ReSharper disable once CheckNamespace
namespace BG.API.Abilities
{
    public interface IColorable : IAbility
    {
        event EventHandler<ColorChangeEventArgs> Colored;
        void Coloring(object sender, ColorChangeEventArgs e);
    }
    public interface IColorChangeSubscriber : ISubscriber
    {
        void OnColorChanged(object sender, ColorChangeEventArgs e);
    }
    public class ColorChangeEventArgs : EventArgs
    {
        public int Layer { get; set; }
        public Color Color { get; set; }
        //public ComponentType GroupTag { get; internal set; }
    }
}