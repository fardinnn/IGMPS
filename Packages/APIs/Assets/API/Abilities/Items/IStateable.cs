﻿using BG.States;
using System;

namespace BG.API.Abilities
{
    public interface IStateable : IAbility
    {
        event EventHandler<StateEventArgs> StateSetted;
        void SetState(object o, StateEventArgs e);
    }
    public interface StateSubscriber : ISubscriber
    {
        void OnSubscribe(object sender, StateEventArgs e);
    }
    public class StateEventArgs : EventArgs
    {
        public StateGroup States { get; set; }
    }
}