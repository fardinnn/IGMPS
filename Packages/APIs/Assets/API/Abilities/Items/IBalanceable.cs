﻿using System;

namespace BG.API.Abilities
{
    public interface IBalanceable : IAbility
    {
        event EventHandler<BalanceEventArgs> Balancing;
        void Balance(object o, BalanceEventArgs e);
    }
    public interface IBalanceSubscriber : ISubscriber
    {
        void OnBalancing(object sender, BalanceEventArgs e);
    }

    public class BalanceEventArgs : EventArgs
    {

    }
}