﻿using System;
using UnityEngine.EventSystems;

// ReSharper disable once CheckNamespace
namespace BG.API.Abilities
{
    public interface ISelectable : IAbility
    {
        event EventHandler<SelectEventArgs> Selected;
        void Select(object sender, SelectEventArgs e);
    }

    public interface ISelecetSubscriber : ISubscriber
    {
        void OnSelected(object sender, SelectEventArgs e);
    }
    public class SelectEventArgs : EventArgs
    {
        public Entity Entity { get; set; }
        public PointerEventData.InputButton InputModule { get; set; }
    }
}