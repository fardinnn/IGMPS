﻿using System;
using UnityEngine;

namespace BG.API.Abilities
{
    public interface IResizable : IAbility
    {
        event EventHandler<ResizeEventArgs> Resized;
        void Resizing(object sender, ResizeEventArgs e);
    }
    public interface IResizingSubscriber : ISubscriber
    {
        void OnResizeSetted(object sender, ResizeEventArgs e);
    }
    public class ResizeEventArgs : EventArgs
    {
        public Vector3 Size { get; set; } = Vector3.one;
        public bool KeepAspects { get; set; } = false;

        //public ComponentType GroupTag { get; internal set; }
    }
}