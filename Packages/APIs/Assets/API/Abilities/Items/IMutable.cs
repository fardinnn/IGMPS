﻿using System;

namespace BG.API.Abilities
{
    public interface IMutable : IAbility
    {
        event EventHandler<MuteEventArgs> Muted;
        void Mute(object o, MuteEventArgs e);
    }
    public interface IMuteSubscriber : ISubscriber
    {
        void OnMuted(object sender, MuteEventArgs e);
    }
    public class MuteEventArgs : EventArgs
    {

    }
}