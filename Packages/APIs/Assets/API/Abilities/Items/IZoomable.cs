﻿using System;

namespace BG.API.Abilities
{
    public interface IZoomable : ITransientAbility
    {
        event EventHandler<ZoomedEventArgs> Zoomed;
        void Zoom(object sender, ZoomedEventArgs e);
    }

    public interface IZoomSubscriber : ISubscriber
    {
        void OnZoomed(object sender, ZoomedEventArgs e);
    }

    public class ZoomedEventArgs : EventArgs
    {

    }
}