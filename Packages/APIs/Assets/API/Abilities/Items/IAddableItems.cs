﻿using System;

// ReSharper disable once CheckNamespace
namespace BG.API.Abilities
{
    public interface IAddableItems<T> : IAbility
    {
        event EventHandler<AddItemsEventArgs<T>> ItemsAdded;
        void AddItems(object sender, AddItemsEventArgs<T> e);
    }
    public interface IAddItemsSubscriber : ISubscriber
    {
        void OnItemsAdded<T>(object sender, AddItemsEventArgs<T> e);
    }
    public class AddItemsEventArgs<T> : EventArgs
    {
        public T[] Items { get; set; }
    }
}