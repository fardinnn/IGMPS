﻿using System;

namespace BG.API.Abilities
{
    public interface IRemovable : IAbility
    {
        void Remove(object o, RemoveEventArgs e);
    }
    public class RemoveEventArgs : EventArgs
    {

    }
}