﻿using BG.States;
using System;
using UnityEngine;

namespace BG.API.Abilities
{
    public interface IOnGridPlaceable : IAbility
    {
        event EventHandler<GridPlacingEventArgs> Placed;
        void Placing(object sender, GridPlacingEventArgs e);
        void PlacingAll(object sender, OnGridPlacingGroupEventArgs e);
    }
    public interface IOnGridPlacingSubscriber : ISubscriber
    {
        void OnPlaced(object sender, GridPlacingEventArgs e);
    }
    public class GridPlacingEventArgs : EventArgs
    {
        public Entity Entity { get; set; }
        public Vector2Int Position { get; set; }
        public Guid GridSystemId { get; set; }
        public PlacementState PlacementState { get; set; }   // Very Important
        public PlacementStageState PlacementStage { get; set; }   // Very Important
    }

    public class OnGridPlacingGroupEventArgs : EventArgs
    {
        public Entity Entity { get; set; }
        public GridPositionSet Positions { get; set; }
        public Guid GridSystemId { get; set; }
        public CompositeStatesSet PlacementState { get; set; }
        public PlacementStageState PlacementStage { get; set; }   // Very Important
    }
}