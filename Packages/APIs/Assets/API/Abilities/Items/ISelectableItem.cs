﻿using System;

// ReSharper disable once CheckNamespace
namespace BG.API.Abilities
{
    public interface ISelectableItem : IAbility
    {
        event EventHandler<SelectItemEventArgs> ItemSelected;
        void SelectItem(object sender, SelectItemEventArgs e);
    }

    public interface ISelectItemSubscriber : ISubscriber
    {
        void OnItemSelected(object sender, SelectEventArgs e);
    }

    public class SelectItemEventArgs : EventArgs
    {

    }
}