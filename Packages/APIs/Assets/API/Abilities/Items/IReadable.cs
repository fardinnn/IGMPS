﻿using System;

// ReSharper disable once CheckNamespace
namespace BG.API.Abilities
{
    public interface IReadable : ITransientAbility
    {
        event EventHandler<ReadEventArgs> ReadingBegan;
        void Read(object sender, ReadEventArgs e);
    }

    public interface IReadSubscriber : ISubscriber
    {
        void OnReadingBegan(object sender, ReadEventArgs e);
    }
    public class ReadEventArgs : EventArgs
    {
        
    }
}