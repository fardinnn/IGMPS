﻿using System;
using UnityEngine;

// ReSharper disable once CheckNamespace
namespace BG.API.Abilities
{
    public interface IClickable : IAbility
    {
        event EventHandler<ClickEventArgs> Clicked0;
        event EventHandler<ClickEventArgs> Clicked1;
        event EventHandler<ClickEventArgs> Clicked2;
        event EventHandler<ClickEventArgs> ClickedUp0;
        event EventHandler<ClickEventArgs> ClickedUp1;
        event EventHandler<ClickEventArgs> ClickedUp2;
        event EventHandler<ClickEventArgs> ClickedDown0;
        event EventHandler<ClickEventArgs> ClickedDown1;
        event EventHandler<ClickEventArgs> ClickedDown2;

        void OnClick0(object sender, ClickEventArgs e);
        void OnClick1(object sender, ClickEventArgs e);
        void OnClick2(object sender, ClickEventArgs e);
        void OnPointerDown0(object sender, ClickEventArgs e);
        void OnPointerDown1(object sender, ClickEventArgs e);
        void OnPointerDown2(object sender, ClickEventArgs e);
        void OnPointerUp0(object sender, ClickEventArgs e);
        void OnPointerUp1(object sender, ClickEventArgs e);
        void OnPointerUp2(object sender, ClickEventArgs e);
    }

    public interface IClickSubscriber : ISubscriber
    {
        void OnClicked(object sender, ClickEventArgs e);
    }
    public class ClickEventArgs : EventArgs
    {
        public Vector3 Position { get; set; }
        public Entity ClickedEntity { get; set; }
    }
}