﻿using System;
using UnityEngine;

// ReSharper disable once CheckNamespace
namespace BG.API.Abilities
{
    public interface IHoverable : IAbility
    {
        event EventHandler<HoverEventArgs> Hovered;
        event EventHandler<HoverEventArgs> Entered;
        event EventHandler<HoverEventArgs> Exited;
        void Hover(object sender, HoverEventArgs e);
        void Enter(object sender, HoverEventArgs e);
        void Exit(object sender, HoverEventArgs e);
    }
    public interface IHoverSubscriber : ISubscriber
    {
        void OnHovered(object sender, HoverEventArgs e);
    }
    public class HoverEventArgs : EventArgs
    {
        public Vector3 Position { get; set; }
        public Entity Entity { get; set; }
    }
}