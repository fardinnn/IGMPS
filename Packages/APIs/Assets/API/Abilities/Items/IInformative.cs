﻿using System;

// ReSharper disable once CheckNamespace
namespace BG.API.Abilities
{
    public interface IInformative : IAbility
    {
        event EventHandler<GetInformationEventArgs> GotInformation;
        void GetInformation(object sender, GetInformationEventArgs e);
    }
    public interface IGetInformationSubscriber : ISubscriber
    {
        void OnGotInformation(object sender, GetInformationEventArgs e);
    }
    public class GetInformationEventArgs : EventArgs
    {

    }
}