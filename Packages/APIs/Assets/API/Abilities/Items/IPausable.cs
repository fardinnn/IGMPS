﻿using System;

// ReSharper disable once CheckNamespace
namespace BG.API.Abilities
{
    public interface IPausable : IAbility
    {
        event EventHandler<PauseEventArgs> Paused;
        void Pause(object o, PauseEventArgs e);
    }
    public interface IPauseSubscriber : ISubscriber
    {
        void OnPaused(object sender, PauseEventArgs e);
    }

    public class PauseEventArgs : EventArgs
    {

    }
}