﻿using System;
using UnityEngine;

// ReSharper disable once CheckNamespace
namespace BG.API.Abilities
{
    public interface IDraggable : IDroppable
    {
        void OnBeginDrag();
        void OnDragging(IDroppableRegion droppableRegion);
        void OnEndDrag(IDroppableRegion droppableRegion);
    }
    public interface IDragSubscriber : ISubscriber
    {
        void OnDragged(object sender, DragEventArgs e);
    }
    public class DragEventArgs : EventArgs
    {
        public IDroppableRegion DropRegion { get; set; }
        public Entity Entity { get; set; }
    }
}