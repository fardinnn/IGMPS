﻿using System;

// ReSharper disable once CheckNamespace
namespace BG.API.Abilities
{
    public interface IEndable : IAbility
    {
        event EventHandler<EndEventArgs> Ended;
        void End(object sender, EndEventArgs e);
    }

    public interface IEndSubscriber : ISubscriber
    {
        void OnEnded(object sender, EndEventArgs e);
    }
    public class EndEventArgs : EventArgs
    {

    }

}