﻿using System;

// ReSharper disable once CheckNamespace
namespace BG.API.Abilities
{
    public interface IBeginnable : IAbility
    {
        event EventHandler<BeginEventArgs> Begined;
        void Begin(object sender, BeginEventArgs e);
    }

    public interface IBeginSubscriber : ISubscriber
    {
        void OnBegined(object sender, BeginEventArgs e);
    }
    public class BeginEventArgs : EventArgs
    {

    }
}