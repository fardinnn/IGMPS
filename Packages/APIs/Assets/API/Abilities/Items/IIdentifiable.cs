﻿using System;
using Unity.Collections;

// ReSharper disable once CheckNamespace
namespace BG.API.Abilities
{
    public interface IIdentifiable : IAbility
    {
        event EventHandler<IdentityChangeEventArgs> IdentityChanged;
        void ChangeIdentity(object sender, IdentityChangeEventArgs e);
    }
    public interface IIdentityChangeSubscriber : ISubscriber
    {
        void OnIdentityChanged(object sender, IdentityChangeEventArgs e);
    }
    public class IdentityChangeEventArgs : EventArgs
    {
        public string Name { get; set; }
        public Guid Id { get; set; }
        //public ComponentType GroupTag { get; internal set; }
    }
}