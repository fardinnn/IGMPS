﻿using BG.States;

namespace BG.API.Abilities
{
    public class ViewOption : Option
    {
        public ViewAngleState ViewAngleState { get; set; }
        protected override void Alter(dynamic entity/*, ComponentType groupTag*/)
        {
            if (entity is IViewChangable)
                (entity as IViewChangable).Change(this, new ViewChangeEventArgs { ViewAngleState = ViewAngleState/*, GroupTag = groupTag*/ });
        }
    }
}
