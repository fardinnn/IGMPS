﻿using System.Collections;
using UnityEngine;

namespace BG.API.Abilities
{
    public class SizeOption : Option
    {
        public Vector3 Size { get; set; } = Vector3.one;
        public bool KeepAspects { get; set; } = false;
        protected override void Alter(dynamic entity/*, ComponentType groupTag*/)
        {
            if (entity is IResizable)
                (entity as IResizable).Resizing(this, new ResizeEventArgs { Size = Size, KeepAspects = KeepAspects/*, GroupTag = groupTag*/ });
        }
    }
}
