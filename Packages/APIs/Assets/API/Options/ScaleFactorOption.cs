﻿using Unity.Collections;

namespace BG.API.Abilities
{
    public class ScaleFactorOption : Option
    {
        public float ScaleFactor { get; set; }
        protected override void Alter(dynamic entity/*, ComponentType groupTag*/)
        {
            if (entity is IScalable)
                (entity as IScalable).Scale(this, new ScaleEventArgs { ScaleFactor = ScaleFactor/*, GroupTag = groupTag*/ });
        }
    }
}
