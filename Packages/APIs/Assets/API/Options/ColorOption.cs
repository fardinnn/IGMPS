﻿using Unity.Collections;
using UnityEngine;

namespace BG.API.Abilities
{
    public class ColorOption : Option
    {
        public Color Value { get; set; }
        protected override void Alter(dynamic entity/*, ComponentType groupTag*/)
        {
            if (entity is IColorable)
                (entity as IColorable).Coloring(this, new ColorChangeEventArgs { Color = Value/*, GroupTag = groupTag*/ });
        }
    }
}
