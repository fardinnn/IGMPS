﻿using System;
using Unity.Collections;

namespace BG.API.Abilities
{
    public class IdentityOption : Option
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        protected override void Alter(dynamic entity/*, ComponentType groupTag*/)
        {
            if (entity is IIdentifiable)
                (entity as IIdentifiable).ChangeIdentity(this, new IdentityChangeEventArgs { Name = Name, Id = Id/*, GroupTag = groupTag*/ });
        }
    }
}
