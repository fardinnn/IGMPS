﻿using BG.States;
using System;

namespace BG.API.Abilities
{
    public class GridPlacementOption : Option
    {
        public GridPositionSet Positions { get; set; }
        public Guid GridSystemId { get; set; }
        public CompositeStatesSet PlacementStates { get; set; }
        protected override void Alter(dynamic entity)
        {
            if (entity is IOnGridPlaceable placeable)
                placeable.PlacingAll(this, new OnGridPlacingGroupEventArgs
                {
                    Positions = Positions,
                    GridSystemId = GridSystemId,
                    PlacementState = PlacementStates
                });
        }
    }
}
