using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.Collections;

namespace BG.API
{
    public class Options : IEnumerable<Option>
    {
        private Dictionary<Type, Option> _items;

        public Option this[Type optionType]
        {
            get
            {
                if (!_items.ContainsKey(optionType)) return null;
                return _items[optionType];
            }
            set
            {
                if (_items.ContainsKey(optionType))
                    _items[optionType] = value;
                else if (typeof(Option).IsAssignableFrom(optionType) && value.GetType() == optionType)
                    _items.Add(optionType, value);
                else throw new ArrayTypeMismatchException($"The type '{optionType}' is not and option type or '{value.GetType()}' is not of type '{optionType}'");
            }
        }

        public Options()
        {
            _items = new Dictionary<Type, Option>();
        }
        protected Options(Options options)
        {
            _items = new Dictionary<Type, Option>(options.Count);
            foreach (var item in options)
                _items.Add(item.GetType(), item);
        }

        protected Options(ICollection<Option> items)
        {
            _items = items.ToDictionary(i=>i.GetType(), i=>i);
        }


        public int Count => _items.Count;


        public void Add(Option item)
        {
            _items.Add(item.GetType(), item);
        }

        public void Replace(Option option)
        {
            var optionType = option.GetType();
            if (_items.ContainsKey(optionType))
                _items[optionType] = option;
        }

        public void Clear()
        {
            _items.Clear();
        }

        public bool Contains(Option item)
        {
            return _items[item.GetType()] == item;
        }

        public bool Contains(Type optionType)
        {
            return _items.ContainsKey(optionType);
        }

        public void CopyTo(ref Options options)
        {
            options = new Options(this);
        }

        public IEnumerator<Option> GetEnumerator()
        {
            return _items.Values.GetEnumerator();
        }

        public bool Remove(Option item)
        {
            return _items.Remove(item.GetType());
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _items.GetEnumerator();
        }

        public void AlterOn(dynamic ability/*, ComponentType groupTag*/)
        {
            if (_items == null) return;
            foreach (var option in _items)
            {
                var op = option;
                option.Value.AlterOption(ability/*, groupTag*/);
            }
        }

        public Options MergeWith(Options options)
        {
            if (options == null)
                return this;

            foreach (var op in options)
            {
                if (!Contains(op))
                    Add(op);
            }

            return this;
        }

        public static Options operator +(Options options1, Options options2)
        {
            return options1 == null ? new Options(options2) : options1.MergeWith(options2);
        }
    }
}
