﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace BG.API.Abilities
{
    public class GridPositionSet : HashSet<Tuple<int, int>>
    {
        private int gridRowSize;
        private int gridColumnSize;
        public GridPositionSet(int gridRowSize, int gridColumnSize)
        {
            this.gridRowSize = gridRowSize;
            this.gridColumnSize = gridColumnSize;
        }

        public GridPositionSet Add(int row, int column)
        {
            if (row >= gridRowSize || column >= gridColumnSize || row < 0 || column < 0)
            {
                Debug.LogError($"row or column ({row}, {column}) is out of range!");
                return this;
            }
            AddIfNotExist(row, column);
            return this;
        }
        private void AddIfNotExist(int row, int column)
        {
            Tuple<int, int> position = new Tuple<int, int>(row, column);
            if (!Contains(position))
                Add(position);
        }

        //HashSet<Tuple<int,int>>
        public GridPositionSet AddRowRange(int column, int begin, int end, bool inculsive = false)
        {
            end = inculsive ? end + 1 : end;
            if (end < begin || end > gridRowSize || column >= gridColumnSize || begin < 0 || column < 0)
            {
                Debug.LogError($"the range(column = {column}, begin = {begin}, end = {end}) is not currect!");
                return this;
            }

            for (int i = begin; i < end; i++)
                AddIfNotExist(i, column);
            return this;
        }

        public GridPositionSet AddColumnRange(int row, int begin, int end, bool inculsive = false)
        {
            end = inculsive ? end + 1 : end;
            if (end < begin || row >= gridRowSize || end > gridColumnSize || begin < 0 || row < 0)
            {
                Debug.LogError($"the range(row = {row}, begin = {begin}, end = {end}) is not currect!");
                return this;
            }

            for (int i = begin; i < end; i++)
                AddIfNotExist(row, i);
            return this;
        }

        public GridPositionSet AddGridRange(int rowBegin, int rowEnd, int columnBegin, int columnEnd, bool inculsive = false)
        {
            rowEnd = inculsive ? rowEnd + 1 : rowEnd;
            columnEnd = inculsive ? columnEnd + 1 : columnEnd;

            if (rowEnd < rowBegin || columnEnd < columnBegin || rowEnd > gridRowSize || columnEnd > gridColumnSize || rowBegin < 0 || columnBegin < 0)
            {
                Debug.LogError($"the range(rowBegin = {rowBegin}, rowEnd = {rowEnd}, columnBegin = {columnBegin}, columnEnd = {columnEnd}) is not currect!");
                return this;
            }

            for (int i = rowBegin; i < rowEnd; i++)
            {
                for (int j = columnBegin; j < columnEnd; j++)
                {
                    AddIfNotExist(i, j);
                }
            }
            return this;
        }

        public GridPositionSet AddRowRange(int begin, int end, bool inculsive = false)
        {
            end = inculsive ? end + 1 : end;

            if (end < begin || end > gridRowSize ||  begin < 0)
            {
                Debug.LogError($"the range(begin = {begin}, end = {end}) is not currect!");
                return this;
            }

            for (int i = 0; i < gridColumnSize; i++)
                for (int j = begin ; j < end; j++)
                    AddIfNotExist(j, i);
            return this;
        }

        public GridPositionSet AddColumnRange(int begin, int end, bool inculsive = false)
        {
            end = inculsive ? end + 1 : end;

            if (end < begin || end > gridRowSize || begin < 0)
            {
                Debug.LogError($"the range(begin = {begin}, end = {end}) is not currect!");
                return this;
            }

            for (int i = 0; i < gridRowSize; i++)
                for (int j = begin; j < end; j++)
                    AddIfNotExist(i, j);
            return this;
        }
    }
}
