﻿
using BG.API.Shapes;
using System;
using Unity.Collections;
using UnityEngine;

namespace BG.API.Abilities
{
    public class FormingOption : Option
    {
        public string ShapeName { get; set; }
        public IShapePackage Package { get; set; }
        public Type ShapeType { get; set; }

        protected override void Alter(dynamic entity/*, ComponentType groupTag*/)
        {
            if (entity is IFormable)
                (entity as IFormable).Form(this, new FormingEventArgs
                {
                    ShapeName = ShapeName,
                    Package = Package,
                    ShapeType = ShapeType/*, GroupTag = groupTag*/
                });
        }
    }
}
