﻿using Unity.Collections;
using UnityEngine;

namespace BG.API.Abilities
{
    public class ParentingOption : Option
    {
        public Transform Parent { get; set; }
        protected override void Alter(dynamic ability/*, ComponentType groupTag*/)
        {
            //if (ability is MonoBehaviour)
            //    (ability as MonoBehaviour).transform.SetParent(Parent);
        }
    }
}
