﻿using Unity.Collections;
using UnityEngine;

namespace BG.API.Abilities
{
    public class LocalPositionOption : Option
    {
        public Vector3 Position { get; set; }
        protected override void Alter(dynamic entity/*, ComponentType groupTag*/)
        {
            //if (entity is MonoBehaviour)
            //    (entity as MonoBehaviour).transform.localPosition = Position;
        }
    }
}
