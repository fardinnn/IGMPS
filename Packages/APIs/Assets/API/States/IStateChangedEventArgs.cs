﻿using BG.API;
using BG.States;
using System;

namespace BG.States
{
    public enum BroadcastDirection : byte
    {
        NotBroadcast = 0,
        Downcast = 1,
        Upcast = 2,
        BothDirection = 3
    }

    public interface IStateChangedEventArgs
    {
        Entity Entity { get; set; }
        Enum NewState { get; set; }
        Enum PreviousState { get; set; }
        public BroadcastDirection BroadcastDirection { get; set; }
    }

    public interface IStateChangedEventArgs<T1, T2> : IStateChangedEventArgs
        where T1 : Enum
        where T2 : IStateChangedEventArgs<T1, T2>
    {
        T2 CreateCopy(Entity entity);
        void CopyTo(T2 target);
    }
}