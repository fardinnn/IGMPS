﻿using BG.API;
using System;
using System.Collections.Generic;

namespace BG.States
{
    public interface IStateSetter
    {
        void SetState(Entity entity);
        void SetState(IEnumerable<Entity> entities);
        void TakeCopyOf(IStateChangedEventArgs stateChanged);
    }

    public class CacheState<T1, T2> : IStateSetter
        where T1: Enum
        where T2: class, IStateChangedEventArgs<T1,T2>
    {
        T1 state;
        T2 eventArgs;
        public CacheState(T1 state, T2 e)
        {
            this.state = state;
            eventArgs = e;
        }

        public void SetState(Entity entity)
        {
            entity.SetState(state, eventArgs);
            EquateState();
        }


        public void SetState(IEnumerable<Entity> entities)
        {
            foreach (Entity entity in entities)
            {
                entity.SetState(state, eventArgs);
                EquateState();
            }
        }

        public void TakeCopyOf(IStateChangedEventArgs stateEventArgs)
        {
            if (stateEventArgs is T2 newArgs)
            {
                newArgs.CopyTo(eventArgs);
                EquateState();
            }
        }
        private void EquateState()
        {
            if (!state.Equals(eventArgs.NewState)) state = (T1)eventArgs.NewState;
        }
    }
}
