using BG.API;
using BG.API.Abilities;
using System;
using UnityEngine;

namespace BG.States
{

    public class ViewAngleChagedEventArgs : IStateChangedEventArgs<ViewAngleState, ViewAngleChagedEventArgs>
    {
        public Enum NewState { get; set; }
        public Enum PreviousState { get; set; }
        public Entity Entity { get; set; }
        public BroadcastDirection BroadcastDirection { get; set; } = BroadcastDirection.Downcast;

        public void CopyTo(ViewAngleChagedEventArgs target)
        {
            target.NewState = NewState;
            target.PreviousState = PreviousState;
            target.BroadcastDirection = BroadcastDirection;
        }

        public ViewAngleChagedEventArgs CreateCopy(Entity entity)
        {
            return new ViewAngleChagedEventArgs
            {
                NewState = NewState,
                PreviousState = PreviousState,
                Entity =entity,
                BroadcastDirection = BroadcastDirection
            };
        }
    }


    public class HoverStateChagedEventArgs : IStateChangedEventArgs<HoverState, HoverStateChagedEventArgs>
    {
        public Enum NewState { get; set; }
        public Enum PreviousState { get; set; }
        public Entity Entity { get; set; }
        public BroadcastDirection BroadcastDirection { get; set; } = BroadcastDirection.BothDirection;

        public void CopyTo(HoverStateChagedEventArgs target)
        {
            target.NewState = NewState;
            target.PreviousState = PreviousState;
            target.BroadcastDirection = BroadcastDirection;
        }
        public HoverStateChagedEventArgs CreateCopy(Entity entity)
        {
            return new HoverStateChagedEventArgs
            {
                NewState = NewState,
                PreviousState = PreviousState,
                Entity = entity,
                BroadcastDirection = BroadcastDirection
            };
        }
    }
    public class SelectStateChagedEventArgs : IStateChangedEventArgs<SelectState, SelectStateChagedEventArgs>
    {
        public Enum NewState { get; set; }
        public Enum PreviousState { get; set; }
        public Entity Entity { get; set; }
        public BroadcastDirection BroadcastDirection { get; set; } = BroadcastDirection.BothDirection;

        public void CopyTo(SelectStateChagedEventArgs target)
        {
            target.NewState = NewState;
            target.PreviousState = PreviousState;
            target.BroadcastDirection = BroadcastDirection;
        }

        public SelectStateChagedEventArgs CreateCopy(Entity entity)
        {
            return new SelectStateChagedEventArgs
            {
                NewState = NewState,
                PreviousState = PreviousState,
                Entity = entity,
                BroadcastDirection = BroadcastDirection
            };
        }
    }
    public class PointerChagedEventArgs : IStateChangedEventArgs<PointerState, PointerChagedEventArgs>
    {
        public Enum NewState { get; set; }
        public Enum PreviousState { get; set; }
        public Entity Entity { get; set; }
        public BroadcastDirection BroadcastDirection { get; set; } = BroadcastDirection.Downcast;

        public void CopyTo(PointerChagedEventArgs target)
        {
            target.NewState = NewState;
            target.PreviousState = PreviousState;
            target.BroadcastDirection = BroadcastDirection;
        }
        public PointerChagedEventArgs CreateCopy(Entity entity)
        {
            return new PointerChagedEventArgs
            {
                NewState = NewState,
                PreviousState = PreviousState,
                Entity = entity,
                BroadcastDirection = BroadcastDirection
            };
        }
    }
    public class DragAndDropChagedEventArgs : IStateChangedEventArgs<DragAndDropState, DragAndDropChagedEventArgs>
    {
        public Enum NewState { get; set; }
        public Enum PreviousState { get; set; }
        public Entity Entity { get; set; }
        public BroadcastDirection BroadcastDirection { get; set; } = BroadcastDirection.Downcast; 
        public IDroppableRegion TargetRegion { get; set; }

        public void CopyTo(DragAndDropChagedEventArgs target)
        {
            target.NewState = NewState;
            target.PreviousState = PreviousState;
            target.BroadcastDirection = BroadcastDirection;
            target.TargetRegion = TargetRegion;
        }

        public DragAndDropChagedEventArgs CreateCopy(Entity entity)
        {
            return new DragAndDropChagedEventArgs
            {
                NewState = NewState,
                PreviousState = PreviousState,
                Entity = entity,
                BroadcastDirection = BroadcastDirection,
                TargetRegion = TargetRegion
            };
        }
    }
    public class DroppableRegionChagedEventArgs : IStateChangedEventArgs<DroppableRegionState, DroppableRegionChagedEventArgs>
    {
        public Enum NewState { get; set; }
        public Enum PreviousState { get; set; }
        public Entity Entity { get; set; }
        public BroadcastDirection BroadcastDirection { get; set; } = BroadcastDirection.Downcast;

        public void CopyTo(DroppableRegionChagedEventArgs target)
        {
            target.NewState = NewState;
            target.PreviousState = PreviousState;
            target.BroadcastDirection = BroadcastDirection;
        }
        public DroppableRegionChagedEventArgs CreateCopy(Entity entity)
        {
            return new DroppableRegionChagedEventArgs
            {
                NewState = NewState,
                PreviousState = PreviousState,
                Entity = entity,
                BroadcastDirection = BroadcastDirection
            };
        }
    }
    public class PlaceChagedEventArgs : IStateChangedEventArgs<PlaceState, PlaceChagedEventArgs>
    {
        public Enum NewState { get; set; }
        public Enum PreviousState { get; set; }
        public Entity Entity { get; set; }
        public BroadcastDirection BroadcastDirection { get; set; } = BroadcastDirection.Downcast;

        public void CopyTo(PlaceChagedEventArgs target)
        {
            target.NewState = NewState;
            target.PreviousState = PreviousState;
            target.BroadcastDirection = BroadcastDirection;
        }
        public PlaceChagedEventArgs CreateCopy(Entity entity)
        {
            return new PlaceChagedEventArgs
            {
                NewState = NewState,
                PreviousState = PreviousState,
                Entity = entity,
                BroadcastDirection = BroadcastDirection
            };
        }
    }
    public class MoveChagedEventArgs : IStateChangedEventArgs<MoveState, MoveChagedEventArgs>
    {
        public Enum NewState { get; set; }
        public Enum PreviousState { get; set; }
        public Entity Entity { get; set; }
        public BroadcastDirection BroadcastDirection { get; set; } = BroadcastDirection.Downcast;

        public void CopyTo(MoveChagedEventArgs target)
        {
            target.NewState = NewState;
            target.PreviousState = PreviousState;
            target.BroadcastDirection = BroadcastDirection;
        }

        public MoveChagedEventArgs CreateCopy(Entity entity)
        {
            return new MoveChagedEventArgs
            {
                NewState = NewState,
                PreviousState = PreviousState,
                Entity = entity,
                BroadcastDirection = BroadcastDirection
            };
        }
    }
    public class PlacementStateChagedEventArgs : IStateChangedEventArgs<PlacementState, PlacementStateChagedEventArgs>
    {
        public Enum NewState { get; set; }
        public Enum PreviousState { get; set; }
        public Entity Entity { get; set; }
        public BroadcastDirection BroadcastDirection { get; set; } = BroadcastDirection.Downcast;
        public IContainerEntity TargetPlace { get; set; }

        public void CopyTo(PlacementStateChagedEventArgs target)
        {
            target.NewState = NewState;
            target.PreviousState = PreviousState;
            target.BroadcastDirection = BroadcastDirection;
            target.TargetPlace = TargetPlace;
        }
        public PlacementStateChagedEventArgs CreateCopy(Entity entity)
        {
            return new PlacementStateChagedEventArgs
            {
                NewState = NewState,
                PreviousState = PreviousState,
                Entity = entity,
                BroadcastDirection = BroadcastDirection,
                TargetPlace = TargetPlace
            };
        }
    }
    public class PlacementStageStateChagedEventArgs : IStateChangedEventArgs<PlacementStageState, PlacementStageStateChagedEventArgs>
    {
        public Enum NewState { get; set; }
        public Enum PreviousState { get; set; }
        public Entity Entity { get; set; }
        public BroadcastDirection BroadcastDirection { get; set; } = BroadcastDirection.Downcast;

        public void CopyTo(PlacementStageStateChagedEventArgs target)
        {
            target.NewState = NewState;
            target.PreviousState = PreviousState;
            target.BroadcastDirection = BroadcastDirection;
        }
        public PlacementStageStateChagedEventArgs CreateCopy(Entity entity)
        {
            return new PlacementStageStateChagedEventArgs
            {
                NewState = NewState,
                PreviousState = PreviousState,
                Entity = entity,
                BroadcastDirection = BroadcastDirection
            };
        }
    }
    public class ContainerEntityStateChagedEventArgs : IStateChangedEventArgs<ContainerEntityState, ContainerEntityStateChagedEventArgs>
    {
        public Enum NewState { get; set; }
        public Enum PreviousState { get; set; }
        public Entity Entity { get; set; }
        public BroadcastDirection BroadcastDirection { get; set; } = BroadcastDirection.Downcast;

        public void CopyTo(ContainerEntityStateChagedEventArgs target)
        {
            target.NewState = NewState;
            target.PreviousState = PreviousState;
            target.BroadcastDirection = BroadcastDirection;
        }
        public ContainerEntityStateChagedEventArgs CreateCopy(Entity entity)
        {
            return new ContainerEntityStateChagedEventArgs
            {
                NewState = NewState,
                PreviousState = PreviousState,
                Entity = entity,
                BroadcastDirection = BroadcastDirection
            };
        }
    }
    public class ReserveChagedEventArgs : IStateChangedEventArgs<ReserveState, ReserveChagedEventArgs>
    {
        public Enum NewState { get; set; }
        public Enum PreviousState { get; set; }
        public Entity Entity { get; set; }
        public BroadcastDirection BroadcastDirection { get; set; } = BroadcastDirection.Downcast;

        public void CopyTo(ReserveChagedEventArgs target)
        {
            target.NewState = NewState;
            target.PreviousState = PreviousState;
            target.BroadcastDirection = BroadcastDirection;
        }
        public ReserveChagedEventArgs CreateCopy(Entity entity)
        {
            return new ReserveChagedEventArgs
            {
                NewState = NewState,
                PreviousState = PreviousState,
                Entity = entity,
                BroadcastDirection = BroadcastDirection
            };
        }
    }
    public class SafetyChagedEventArgs : IStateChangedEventArgs<SafetyState, SafetyChagedEventArgs>
    {
        public Enum NewState { get; set; }
        public Enum PreviousState { get; set; }
        public Entity Entity { get; set; }
        public BroadcastDirection BroadcastDirection { get; set; } = BroadcastDirection.Downcast;

        public void CopyTo(SafetyChagedEventArgs target)
        {
            target.NewState = NewState;
            target.PreviousState = PreviousState;
            target.BroadcastDirection = BroadcastDirection;
        }

        public SafetyChagedEventArgs CreateCopy(Entity entity)
        {
            return new SafetyChagedEventArgs
            {
                NewState = NewState,
                PreviousState = PreviousState,
                Entity = entity,
                BroadcastDirection = BroadcastDirection
            };
        }
    }

    public interface ITransitionChangedEventArgs<T> : IStateChangedEventArgs<TransitionState, T>
        where T : ITransitionChangedEventArgs<T>
    {
    }

    public class LinearTransitionChangedEventArgs : ITransitionChangedEventArgs<LinearTransitionChangedEventArgs>
    {
        public Enum NewState { get; set; }
        public Enum PreviousState { get; set; }
        public Entity Entity { get; set; }
        public BroadcastDirection BroadcastDirection { get; set; } = BroadcastDirection.Downcast;

        public Vector3 BeginPosition { get; set; }
        public Vector3 EndPosition { get; set; }
        public float Threshold { get; set; }

        public void CopyTo(LinearTransitionChangedEventArgs target)
        {
            target.NewState = NewState;
            target.PreviousState = PreviousState;
            target.BroadcastDirection = BroadcastDirection;
            target.BeginPosition = BeginPosition;
            target.EndPosition = EndPosition;
            target.Threshold = Threshold;
        }

        public LinearTransitionChangedEventArgs CreateCopy(Entity entity)
        {
            return new LinearTransitionChangedEventArgs
            {
                NewState = NewState,
                PreviousState = PreviousState,
                Entity = entity,
                BroadcastDirection = BroadcastDirection,
                BeginPosition = BeginPosition,
                EndPosition = EndPosition,
                Threshold = Threshold
            };
        }
    }

    public class LockStateChangedEventArgs : IStateChangedEventArgs<LockState, LockStateChangedEventArgs>
    {
        public Enum NewState { get; set; }
        public Enum PreviousState { get; set; }
        public Entity Entity { get; set; }
        public BroadcastDirection BroadcastDirection { get; set; } = BroadcastDirection.Downcast;

        public void CopyTo(LockStateChangedEventArgs target)
        {
            target.NewState = NewState;
            target.PreviousState = PreviousState;
            target.BroadcastDirection = BroadcastDirection;
        }

        public LockStateChangedEventArgs CreateCopy(Entity entity)
        {
            return new LockStateChangedEventArgs
            {
                NewState = NewState,
                PreviousState = PreviousState,
                Entity = entity,
                BroadcastDirection = BroadcastDirection
            };
        }
    }


    public class GameStateChangedEventArgs : IStateChangedEventArgs<GameState, GameStateChangedEventArgs>
    {
        public Enum NewState { get; set; }
        public Enum PreviousState { get; set; }
        public Entity Entity { get; set; }
        public BroadcastDirection BroadcastDirection { get; set; } = BroadcastDirection.Downcast;

        public void CopyTo(GameStateChangedEventArgs target)
        {
            target.NewState = NewState;
            target.PreviousState = PreviousState;
            target.BroadcastDirection = BroadcastDirection;
        }

        public GameStateChangedEventArgs CreateCopy(Entity entity)
        {
            return new GameStateChangedEventArgs
            {
                NewState = NewState,
                PreviousState = PreviousState,
                Entity = entity,
                BroadcastDirection = BroadcastDirection
            };
        }
    }

}
