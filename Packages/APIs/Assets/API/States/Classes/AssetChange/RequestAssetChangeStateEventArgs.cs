﻿using BG.API;
using System;


namespace BG.States
{
    public abstract class RequestAssetChangeEventArgs<T1, T2> : IStateChangedEventArgs<T1, T2>
        where T1 : Enum
        where T2 : RequestAssetChangeEventArgs<T1, T2>
    {
        public Enum NewState { get; set; }
        public Enum PreviousState { get; set; }
        public Entity Entity { get; set; }
        public BroadcastDirection BroadcastDirection { get; set; } = BroadcastDirection.NotBroadcast;
        public IAssetId AssetId { get; set; }
        public IComponentSystem ComponentSystem { get; set; }
        public virtual void CopyTo(T2 target)
        {
            target.NewState = NewState;
            target.PreviousState = PreviousState;
            target.BroadcastDirection = BroadcastDirection;
            target.AssetId = AssetId;
        }

        public abstract T2 CreateCopy(Entity entity);
    }

    public class RequestSFXChangeEventArgs : RequestAssetChangeEventArgs<RequestSFXChange, RequestSFXChangeEventArgs>
    {
        public override RequestSFXChangeEventArgs CreateCopy(Entity entity)
        {
            return new RequestSFXChangeEventArgs
            {
                NewState = NewState,
                PreviousState = PreviousState,
                Entity = Entity,
                BroadcastDirection = BroadcastDirection,
                AssetId = AssetId,
            };
        }
    }


}
