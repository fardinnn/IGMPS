﻿namespace BG.API
{
    public interface IAssetId { }

    public interface IAssetId<T> : IAssetId
    {
        T Id { get; set; }
    }

    public struct StringId : IAssetId<string>
    {
        public string Id { get; set; }
    }

    public struct IntId : IAssetId<int>
    {
        public int Id { get; set; }
    }

}
