﻿using BG.API;
using System;
using UnityEngine;

namespace BG.States
{
    public class SFXChangedEventArgs : IStateChangedEventArgs<SFXChange, SFXChangedEventArgs>
    {
        public Enum NewState { get; set; }
        public Enum PreviousState { get; set; }
        public Entity Entity { get; set; }
        public BroadcastDirection BroadcastDirection { get; set; } = BroadcastDirection.NotBroadcast;

        public AudioClip Sound { get; set; }

        public void CopyTo(SFXChangedEventArgs target)
        {
            target.NewState = NewState;
            target.PreviousState = PreviousState;
            target.BroadcastDirection = BroadcastDirection;
            target.Sound = Sound;
        }

        public SFXChangedEventArgs CreateCopy(Entity entity)
        {
            return new SFXChangedEventArgs
            {
                NewState = NewState,
                PreviousState = PreviousState,
                Entity = Entity,
                BroadcastDirection = BroadcastDirection,
                Sound = Sound
            };
        }
    }
}
