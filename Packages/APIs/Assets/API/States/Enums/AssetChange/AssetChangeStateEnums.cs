﻿namespace BG.States
{
    public enum RequestSFXChange : byte
    {
        None,
        Set,
        Unset,
        Delete
    }
    public enum SFXChange : byte
    {
        Loading,
        Loaded,
        Deleted,
    }
}
