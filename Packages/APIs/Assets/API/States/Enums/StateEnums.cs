﻿namespace BG.States
{
    public enum ViewAngleState : byte
    {
        None,
        TopDown,
        Perspective,
        ForwardView,
        SideView
    }

    public enum HoverState : byte
    {
        NotEntered,
        Entered,
        Exited
    }
    public enum SelectState : byte
    {
        NotSelected,
        Selected,
        Unselected
    }

    public enum PointerState : byte
    {
        PointerNotWorked,
        PointerDown,
        PointerReleased
    }

    public enum DragAndDropState : byte
    {
        NotDragged,
        DragBeginned,
        Dragging, 
        Drop
    }

    public enum DroppableRegionState : byte
    {
        DragEnter,
        DragExit,
        DropIn,
        TakenOut
    }

    public enum PlaceState : byte
    {
        NotReady,
        Ready,
        IsOccupying
    }


    public enum MoveState : byte
    {
        NotMoved,
        IsMoving,
        Moved
    }

    public enum PlacementState : byte
    {
        NotPlaced,
        Placed,
        Unplaced
    }

    public enum PlacementStageState : byte
    {
        Begin,
        Middle,
        Final
    }

    public enum ContainerEntityState : byte
    {
        Empty,
        Full,
        NotFullNotEmpty
    }

    public enum ReserveState : byte
    {
        Reserved,
        NotReserved
    }

    public enum SafetyState : byte
    {
        Safe,
        Endangered
    }

    public enum GameState : byte
    {
        Waiting,
        Beginning,
        BeginPassed
    }

    public enum TransitionState: byte
    {
        Begin,
        Running,
        End
    }

    public enum LockState : byte
    {
        Locked,
        Unlocked
    }

}
