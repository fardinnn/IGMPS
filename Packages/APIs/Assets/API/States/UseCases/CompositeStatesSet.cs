﻿using BG.Extensions;
using System;
using System.Collections.Generic;

namespace BG.States
{

    public class CompositeStatesSet : HashSet<StateGroup>
    {
        public bool Contains(IEnumerable<Enum> statesGroup)
        {
            return base.Contains(statesGroup.ToStateGroup());
        }
        public void Add(params Enum[] statesCombination)
        {
            Add(new StateGroup(statesCombination));
        }
    }
}