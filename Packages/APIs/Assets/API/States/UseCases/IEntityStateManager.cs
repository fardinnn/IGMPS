﻿using System;

namespace BG.States
{
    public interface IEntityStateManager
    {
        void SetInitialStates(StateGroup templateStates);
        void SetStateAsChild<T1, T2>(T1 state, T2 eventArgs) where T1: Enum where T2 : class, IStateChangedEventArgs<T1, T2>;
        void SetState<T1, T2>(T1 state, T2 eventArgs) where T1 : Enum where T2 : class, IStateChangedEventArgs<T1, T2>;
        void SetState(Enum state, IStateChangedEventArgs eventArgs, Type flag);
        bool IsStateActive(StateGroup stateGroup);
        bool IsStateActive(Enum state);
        Enum GetActiveState(Type stateType);
        void AddStateActivateAction(StateGroup states, StateEventHandler<IStateChangedEventArgs> action);
        void AddStateDeactivateAction(StateGroup states, StateEventHandler<IStateChangedEventArgs> action);
        void AddSingleStateActivateAction<T1, T2>(T1 state, StateEventHandler<T2> action) where T1 : Enum where T2 : class, IStateChangedEventArgs<T1, T2>;
        void AddSingleStateDeactivateAction<T1, T2>(T1 state, StateEventHandler<T2> action) where T1 : Enum where T2 : class, IStateChangedEventArgs<T1, T2>;
        void Dispose(StateGroup states, StateEventHandler<IStateChangedEventArgs> action);
        void Dispose<T1, T2>(T1 state, StateEventHandler<T2> action) where T1 : Enum where T2 : class, IStateChangedEventArgs<T1, T2>;
        event StateEventHandler<IStateChangedEventArgs> StateChanged;
    }
}