﻿using BG.API;
using BG.States;
using System;
using System.Collections.Generic;

namespace BG.States
{
    // Rquires Memory Optimization

    public class ContainerEntityStateManager : EntityStatesManager
    {
        protected new ContainerEntity entity { get; set; }

        public ContainerEntityStateManager(ContainerEntity entity) : base(entity)
        {
            this.entity = entity;
        }

        internal void SetStateAsParent<T1, T2>(T1 state, T2 eventArgs)
            where T1 : Enum
            where T2 : class, IStateChangedEventArgs<T1, T2>
        {
            if (!cachedStates.ContainsKey(typeof(T2))) cachedStates.Add(typeof(T2), 
                new CacheState<T1,T2>(state, eventArgs.CreateCopy(entity)));
            else cachedStates[typeof(T2)].TakeCopyOf(eventArgs);

            cachedStates[typeof(T2)].SetState((Entity)entity);
        }

        public override void SetState<T1, T2>(T1 state, T2 eventArgs)
        {
            base.SetState(state, eventArgs);
            if ((eventArgs.BroadcastDirection & BroadcastDirection.Downcast) == BroadcastDirection.Downcast)
            {
                foreach (var entity in entity.Entities)
                    if (!entity.IsStateActive(state)) entity.SetStateAsChild(state, eventArgs);
            }
        }

    }

}