﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace BG.States
{
    public class StateGroup : ISet<Enum>
    {
        private HashSet<Enum> states;
        private long hashCode;

        public int Count => states.Count;
        public bool IsReadOnly => false;

        public StateGroup() : base()
        {
            states = new HashSet<Enum>();
            hashCode = typeof(StateGroup).GetHashCode();
        }
        public StateGroup(StateGroup states) {
            this.states = new HashSet<Enum>(states);
            hashCode = typeof(StateGroup).GetHashCode();
            foreach (var item in states)
                hashCode += item.GetType().GetHashCode() + item.GetHashCode();
        }
        public StateGroup(params Enum[] states)
        {
            this.states = new HashSet<Enum>(states);
            foreach (var item in states)
                hashCode += item.GetType().GetHashCode() + item.GetHashCode();
        }

        public T GetState<T>() where T : Enum
        {
            foreach (var item in this)
                if (item is T result) return result;
            return default(T);
        }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;
            var otherSet = (obj as StateGroup);
            return otherSet.IsSubsetOf(states) && otherSet.Count == this.Count;
        }

        public override int GetHashCode()
        {
            return (int)hashCode;
        }


        

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("StateGroup values: \n");
            foreach (var item in this)
            {
                sb.Append($"{item.ToString()},  ");
            }
            return sb.ToString();
        }

        public bool Add(Enum item)
        {
            if (states.Add(item))
            {
                hashCode += item.GetType().GetHashCode() + item.GetHashCode();
                return true;
            }
            return false;
        }

        public void ExceptWith(IEnumerable<Enum> other)
        {
            throw new NotImplementedException();
        }

        public void IntersectWith(IEnumerable<Enum> other)
        {
            throw new NotImplementedException();
        }

        public bool IsProperSubsetOf(IEnumerable<Enum> other)
        {
            return states.IsProperSubsetOf(other);
        }

        public bool IsProperSupersetOf(IEnumerable<Enum> other)
        {
            return states.IsProperSupersetOf(other);
        }

        public bool IsSubsetOf(IEnumerable<Enum> other)
        {
            return states.IsSubsetOf(other);
        }

        public bool IsSupersetOf(IEnumerable<Enum> other)
        {
            return states.IsSupersetOf(other);
        }

        public bool Overlaps(IEnumerable<Enum> other)
        {
            return states.Overlaps(other);
        }

        public bool SetEquals(IEnumerable<Enum> other)
        {
            throw new NotImplementedException();
        }

        public void SymmetricExceptWith(IEnumerable<Enum> other)
        {
            throw new NotImplementedException();
        }

        public void UnionWith(IEnumerable<Enum> other)
        {
            throw new NotImplementedException();
        }

        void ICollection<Enum>.Add(Enum item)
        {
            Add(item);
        }

        public void Clear()
        {
            hashCode = typeof(StateGroup).GetHashCode();
            states.Clear();
        }

        public bool Contains(Enum item)
        {
            return states.Contains(item);
        }

        public void CopyTo(Enum[] array, int arrayIndex)
        {
            states.CopyTo(array, arrayIndex);
        }

        public bool Remove(Enum item)
        {
            if (states.Remove(item))
            {
                hashCode -= item.GetType().GetHashCode() + item.GetHashCode();
                return true;
            }
            return false;
        }

        public IEnumerator<Enum> GetEnumerator()
        {
            return states.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            yield return states.GetEnumerator();
        }
    }
}