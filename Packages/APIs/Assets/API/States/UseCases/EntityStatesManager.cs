﻿using BG.API;
using BG.States;
using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;


namespace BG.States
{
    // Rquires Memory Optimization
     
    public class EntityStatesManager : IEntityStateManager
    {
        private Dictionary<Type, Enum> activeStates = new Dictionary<Type, Enum>();
        private StateGroup activeStatesSet = new StateGroup();
        protected virtual Entity entity { get; set; }

        public EntityStatesManager(Entity entity)
        {
            this.entity = entity;
        }

        public void SetInitialStates(StateGroup templateStates)
        {
            if (templateStates == null)
            {
                Debug.LogError("States hashset is null!");
                return;
            }

            //activeStatesSet = new StateGroup();
            //activeStates = new Dictionary<Type, Enum>(templateStates.Count);



            foreach (var state in templateStates)
            {
                // SetState(state);          ///////       Very Important
            }
        }


        private event StateEventHandler<IStateChangedEventArgs> stateChanged;
        private static object objectLock = new object();

        public event StateEventHandler<IStateChangedEventArgs> StateChanged
        {
            add
            {
                lock (objectLock)
                {
                    stateChanged -= value;
                    stateChanged += value;
                }
            }
            remove
            {
                lock (objectLock)
                {
                    stateChanged -= value;
                }
            }
        }

        protected Dictionary<Type, IStateSetter> cachedStates = new Dictionary<Type, IStateSetter>();
        public virtual void SetStateAsChild<T1, T2>(T1 state, T2 eventArgs)
            where T1 : Enum
            where T2 : class, IStateChangedEventArgs<T1, T2>
        {
            if (!cachedStates.ContainsKey(typeof(T2))) cachedStates.Add(typeof(T2),
                new CacheState<T1, T2>(state, eventArgs.CreateCopy(entity)));
            else cachedStates[typeof(T2)].TakeCopyOf(eventArgs);

            cachedStates[typeof(T2)].SetState(entity);
        }

        public virtual void SetState<T1, T2>(T1 state, T2 eventArgs)
            where T1 : Enum
            where T2 : class, IStateChangedEventArgs<T1, T2>
        {
            eventArgs.Entity = entity;
            eventArgs.NewState = state;
            if (activeStates.ContainsKey(typeof(T1)))
            {
                if (IsStateActive(state)) return;
                else singleStateDeactivated.Invoke((T1)activeStates[typeof(T1)], eventArgs);
            }
            SetState(state, eventArgs, typeof(T1));
            singleStateActivated.Invoke(state, eventArgs);

            if (entity.Parent != null && (eventArgs.BroadcastDirection & BroadcastDirection.Upcast) == BroadcastDirection.Upcast)
            {
                entity.Parent.SetStateAsParent(state, eventArgs); ///????
            }
        }

        public void SetState(Enum state, IStateChangedEventArgs eventArgs, Type flag)
        {
            try
            {
                if (state.Equals(SelectState.Selected))
                {
                    Debug.Log("IsSelected");
                }
                eventArgs.Entity = entity;
                eventArgs.NewState = state;

                if (activeStates.ContainsKey(flag))
                {
                    if (activeStates[flag].Equals(state)) return;
                    eventArgs.PreviousState = activeStates[flag];
                    activeStates[flag] = state;
                    var previousStateSet = new StateGroup(activeStatesSet);
                    activeStatesSet.Remove(eventArgs.PreviousState);
                    RunDeactiveStateEvents(eventArgs, previousStateSet);
                }
                else
                {
                    activeStates.Add(flag, state);
                }

                activeStatesSet.Add(state);

                RunActiveStateEvents(eventArgs);

                // Just For Show
                GenerateActiveStatesString();
            }
            catch (Exception ex)
            {
                Debug.LogError($"Error Message: {ex.Message} \n\n parameters: state={state}, entity: {entity.name}");
                throw;
            }
        }



        private void RunDeactiveStateEvents(IStateChangedEventArgs eventArgs, StateGroup previousStateSet)
        {
            foreach (var stateGroup in StateDeactivated.Keys)
                if (stateGroup.Contains(eventArgs.PreviousState) && stateGroup.IsSubsetOf(previousStateSet))
                    StateDeactivated[stateGroup]?.Invoke(stateGroup, eventArgs);
        }

        private void RunActiveStateEvents(IStateChangedEventArgs eventArgs)
        {
            foreach (var stateGroup in StateActivated.Keys)
                if (stateGroup.Contains(eventArgs.NewState) && stateGroup.IsSubsetOf(activeStatesSet))
                    StateActivated[stateGroup]?.Invoke(stateGroup, eventArgs);
        }


        public bool IsStateActive(StateGroup stateGroup)
        {
            return stateGroup.IsSubsetOf(activeStatesSet);
        }

        public bool IsStateActive(Enum state)
        {
            return activeStatesSet.Contains(state);
        }

        public Enum GetActiveState(Type stateType)
        {
            return activeStates.ContainsKey(stateType) ? activeStates[stateType] : null;
        }


        private Dictionary<StateGroup, EventElement<IStateChangedEventArgs>> StateActivated { get; set; } =
            new Dictionary<StateGroup, EventElement<IStateChangedEventArgs>>();
        public virtual void AddStateActivateAction(StateGroup states, StateEventHandler<IStateChangedEventArgs> action)
        {
            if (!StateActivated.ContainsKey(states))
                StateActivated.Add(states, new EventElement<IStateChangedEventArgs>());
            StateActivated[states] += action;
        }

        private Dictionary<StateGroup, EventElement<IStateChangedEventArgs>> StateDeactivated { get; set; } =
            new Dictionary<StateGroup, EventElement<IStateChangedEventArgs>>();
        public virtual void AddStateDeactivateAction(StateGroup states, StateEventHandler<IStateChangedEventArgs> action)
        {
            if (!StateDeactivated.ContainsKey(states))
                StateDeactivated.Add(states, new EventElement<IStateChangedEventArgs>());
            StateDeactivated[states] += action;
        }

        private SingleStateEvents singleStateActivated { get; set; } = new SingleStateEvents();
        public virtual void AddSingleStateActivateAction<T1, T2>(T1 state, StateEventHandler<T2> action)
            where T1 : Enum
            where T2 : class, IStateChangedEventArgs<T1, T2>
        {
            singleStateActivated.Add(state, action);
        }

        private SingleStateEvents singleStateDeactivated { get; set; } = new SingleStateEvents();
        public virtual void AddSingleStateDeactivateAction<T1, T2>(T1 state, StateEventHandler<T2> action)
            where T1 : Enum
            where T2 : class, IStateChangedEventArgs<T1, T2>
        {
            singleStateDeactivated.Add(state, action);
        }

        public void Dispose(StateGroup states, StateEventHandler<IStateChangedEventArgs> action)
        {
            if (StateActivated.ContainsKey(states)) StateActivated[states] -= action;
            if (StateDeactivated.ContainsKey(states)) StateDeactivated[states] -= action;
        }
        public void Dispose<T1, T2>(T1 state, StateEventHandler<T2> action)
            where T1 : Enum
            where T2 : class, IStateChangedEventArgs<T1, T2>
        {
            singleStateActivated.Remove(state, action);
            singleStateDeactivated.Remove(state, action);
        }



        // Just For Show
        private void GenerateActiveStatesString()
        {
            var sb = new StringBuilder();
            foreach (var item in activeStates)
            {
                sb.Append(item.Value.ToString()).Append(" \n");
            }
            entity.ActiveStates = sb.ToString();
        }
    }
}