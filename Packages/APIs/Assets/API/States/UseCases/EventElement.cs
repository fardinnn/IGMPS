﻿using System;
using UnityEngine;

namespace BG.States
{

    public delegate void StateEventHandler<TEventArgs>(StateGroup states, TEventArgs e);

    public interface IEventElement
    {

    }

    public class EventElement<TEventArgs>: IEventElement
    {
        protected event StateEventHandler<TEventArgs> MyEvent;

        public void Invoke(StateGroup o, TEventArgs e)
        {
            MyEvent?.Invoke(o, e);
        }

        public static EventElement<TEventArgs> operator +(EventElement<TEventArgs> MyElement, StateEventHandler<TEventArgs> kDelegate)
        {
            MyElement.MyEvent += kDelegate;
            return MyElement;
        }

        public static EventElement<TEventArgs> operator -(EventElement<TEventArgs> MyElement, StateEventHandler<TEventArgs> kDelegate)
        {
            MyElement.MyEvent -= kDelegate;
            return MyElement;
        }
    }
}