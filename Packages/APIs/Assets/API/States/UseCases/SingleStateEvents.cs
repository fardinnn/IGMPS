﻿using System;
using System.Collections.Generic;

namespace BG.States
{
    public class SingleStateEvents
    {
        private Dictionary<Enum, IEventElement> eventElements;

        public SingleStateEvents()
        {
            eventElements = new Dictionary<Enum, IEventElement>();
        }

        public void Add<T1, T2>(T1 state, StateEventHandler<T2> action)
            where T1 : Enum
            where T2 : class, IStateChangedEventArgs<T1, T2>
        {
            if (eventElements.ContainsKey(state))
            {
                EventElement<T2> eventElement = (EventElement<T2>)eventElements[state];
                eventElement += action;
            }
            else
            {
                EventElement<T2> eventElement = new EventElement<T2>();
                eventElement += action;
                eventElements.Add(state, eventElement);
            }
        }

        internal bool ContainsKey<T>(T state) where T:Enum
        {
            return eventElements.ContainsKey(state);
        }

        internal void Invoke<T1, T2>(T1 state, T2 eventArgs)
            where T1 : Enum
            where T2 : IStateChangedEventArgs<T1,T2>
        {
            if (eventElements.ContainsKey(state))
                ((EventElement<T2>)eventElements[state]).Invoke(new StateGroup(state), eventArgs);
        }

        internal void Remove<T1, T2>(T1 state, StateEventHandler<T2> action)
            where T1 : Enum
            where T2 : IStateChangedEventArgs<T1, T2>
        {
            if (!eventElements.ContainsKey(state)) return;
            EventElement<T2> eventElement = (EventElement<T2>)eventElements[state];
            eventElement -= action;
        }
    }
}