﻿using BG.API;
using System;


namespace BG.States
{
    public static class EntityStateExtensions
    {
        public static void SetInitialStates(this Entity self, StateGroup templateStates)
        {
            self.StateManager.SetInitialStates(templateStates);
        }
        public static void SetStateAsChild<T1, T2>(this Entity self, T1 state, T2 eventArgs) where T1 : Enum where T2 : class, IStateChangedEventArgs<T1, T2>
        {
            self.StateManager.SetStateAsChild(state, eventArgs);
        }
        public static void SetState<T1, T2>(this Entity self, T1 state, T2 eventArgs) where T1 : Enum where T2 : class, IStateChangedEventArgs<T1, T2>
        {
            self.StateManager.SetState(state, eventArgs);
        }
        public static void SetState(this Entity self, Enum state, IStateChangedEventArgs eventArgs, Type flag)
        {
            self.StateManager.SetState(state, eventArgs, flag);
        }
        public static void SetState<T1, T2>(this ContainerEntity self, T1 state, T2 eventArgs) where T1 : Enum where T2 : class, IStateChangedEventArgs<T1, T2>
        {
            self.StateManager.SetState(state, eventArgs);
        }
        public static void SetState(this ContainerEntity self, Enum state, IStateChangedEventArgs eventArgs, Type flag)
        {
            self.StateManager.SetState(state, eventArgs, flag);
        }
        public static bool IsStateActive(this Entity self, StateGroup stateGroup)
        {
            return self.StateManager.IsStateActive(stateGroup);
        }
        public static bool IsStateActive(this Entity self, Enum state)
        {
            return self.StateManager.IsStateActive(state);
        }
        public static Enum GetActiveState(this Entity self, Type stateType)
        {
            return self.StateManager.GetActiveState(stateType);
        }
        public static void AddStateActivateAction(this Entity self, StateGroup states, StateEventHandler<IStateChangedEventArgs> action)
        {
            self.StateManager.AddStateActivateAction(states, action);
        }
        public static void AddStateDeactivateAction(this Entity self, StateGroup states, StateEventHandler<IStateChangedEventArgs> action)
        {
            self.StateManager.AddStateDeactivateAction(states, action);
        }
        public static void AddSingleStateActivateAction<T1, T2>(this Entity self, T1 state, StateEventHandler<T2> action) where T1 : Enum where T2 : class, IStateChangedEventArgs<T1, T2>
        {
            self.StateManager.AddSingleStateActivateAction(state, action);
        }
        public static void AddSingleStateDeactivateAction<T1, T2>(this Entity self, T1 state, StateEventHandler<T2> action) where T1 : Enum where T2 : class, IStateChangedEventArgs<T1, T2>
        {
            self.StateManager.AddSingleStateDeactivateAction(state, action);
        }
        public static void Dispose(this Entity self, StateGroup states, StateEventHandler<IStateChangedEventArgs> action)
        {
            self.StateManager.Dispose(states, action);
        }
        public static void Dispose<T1, T2>(this Entity self, T1 state, StateEventHandler<T2> action) where T1 : Enum where T2 : class, IStateChangedEventArgs<T1, T2>
        {
            self.StateManager.Dispose(state, action);
        }

        internal static void SetStateAsParent<T1, T2>(this ContainerEntity self, T1 state, T2 eventArgs) where T1 : Enum where T2 : class, IStateChangedEventArgs<T1, T2>
        {
            ((ContainerEntityStateManager)self.StateManager).SetStateAsParent(state, eventArgs);
        }
    }
}