﻿using BG.States;
using BG.Extensions;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BG.API
{
    public interface IContainerEntity : ISet<Entity>
    {

    }

    public class ContainerEntity : Entity, IContainerEntity
    {
        private int capacity = 1;
        public int Capacity { get => capacity; set => capacity = value; }

        public HashSet<Entity> Entities { get; internal set; } = new HashSet<Entity>();

        public int Count => Entities.Count;

        public bool IsReadOnly => ((ICollection<Entity>)Entities).IsReadOnly;

        public bool AffectContents { get; set; }
        public bool GetAffectedByContents { get; set; } //??!

        // Lightweight pattern event args
        private ContainerEntityStateChagedEventArgs containerChangeEventArgs = new ContainerEntityStateChagedEventArgs();

        protected override void Awake()
        {
            StateManager = new ContainerEntityStateManager(this);
        }


        Dictionary<Type, IStateSetter> cachedStates = new Dictionary<Type, IStateSetter>();

        //private void SubEntityStateChanged<T1,T2>(T1 state, T2 e)
        //    where T1 : Enum
        //    where T2 : class, IStateChangedEventArgs<T1,T2>
        //{
        //    if (IsStateActive(state)) return;
        //    if (!cachedStates.ContainsKey(typeof(T2)))
        //        cachedStates.Add(typeof(T2), new CacheState<T1, T2>(state, e.TakeCopy(this)));
            
        //    cachedStates[typeof(T2)].SetState((Entity)this);
        //}

        //private void SubEntityStateChanged(StateGroup o, IStateChangedEventArgs e) 
        //{
        //    if (!IsStateActive(e.NewState)) SetState(e.NewState, e, e.NewState.GetType());
        //}

        public void Add(Entity entity)
        {
            if (StateManager.IsStateActive(ContainerEntityState.Full))
            {
                Debug.LogError($"{this.name} is full");
                return;
            }

            if (!Entities.Contains(entity))
            {
                //if (GetAffectedByContents)
                //{
                //    entity.StateManager.StateChanged += SubEntityStateChanged;
                //}
                Entities.Add(entity);
            }

            if (entity.Parent == null || !entity.Parent.Equals(this))
                entity.Parent = this;

            //if (GetAffectedByContents) entity.StateChanged += SubEntityStateChanged;
            //entities.Add(entity);

            IncreaseStateCheck();
        }

        bool ISet<Entity>.Add(Entity entity)
        {
            if (StateManager.IsStateActive(ContainerEntityState.Full))
            {
                Debug.LogError($"{this.name} is full");
                return false;
            }
            bool result = false;
            if (!Entities.Contains(entity))
            {
                // if (GetAffectedByContents) entity.StateManager.StateChanged += SubEntityStateChanged;
                result = Entities.Add(entity);
            }
            else
                result = true;

            if (entity.Parent == null || !entity.Parent.Equals(this))
                entity.Parent = this;

            IncreaseStateCheck();
            return result;

        }

        public void Clear()
        {
            //Entities.ForEach(e => { e.StateManager.StateChanged -= SubEntityStateChanged; });
            Entities.Clear();
            this.SetState(ContainerEntityState.Empty, containerChangeEventArgs);
        }

        public bool Contains(Entity item)
        {
            return Entities.Contains(item);
        }

        public void CopyTo(Entity[] array, int arrayIndex)
        {
            Entities.CopyTo(array, arrayIndex);
        }

        public void ExceptWith(IEnumerable<Entity> other)
        {
            // e.StateChanged -= SubEntityStateChanged; Should be added
            Entities.ExceptWith(other);
            DecreaseStateCheck();
        }


        public IEnumerator<Entity> GetEnumerator()
        {
            return Entities.GetEnumerator();
        }

        public void IntersectWith(IEnumerable<Entity> other)
        {
            // e.StateChanged -= SubEntityStateChanged; Should be added
            Entities.IntersectWith(other);
            DecreaseStateCheck();
        }

        public bool IsProperSubsetOf(IEnumerable<Entity> other)
        {
            return Entities.IsProperSubsetOf(other);
        }

        public bool IsProperSupersetOf(IEnumerable<Entity> other)
        {
            return Entities.IsProperSupersetOf(other);
        }

        public bool IsSubsetOf(IEnumerable<Entity> other)
        {
            return Entities.IsSubsetOf(other);
        }

        public bool IsSupersetOf(IEnumerable<Entity> other)
        {
            return Entities.IsSupersetOf(other);
        }

        public bool Overlaps(IEnumerable<Entity> other)
        {
            return Entities.Overlaps(other);
        }

        public bool Remove(Entity entity)
        {
            // entity.StateManager.StateChanged -= SubEntityStateChanged;
            if (entity.Parent != null && entity.Parent.Equals(this)) entity.Parent = null;
            bool result = Entities.Remove(entity);
            DecreaseStateCheck();
            return result;
        }

        public bool SetEquals(IEnumerable<Entity> other)
        {
            return Entities.SetEquals(other);
        }

        public void SymmetricExceptWith(IEnumerable<Entity> other)
        {
            // if (GetAffectedByContents) entity.StateChanged += SubEntityStateChanged; Should be added
            // e.StateChanged -= SubEntityStateChanged; Should be added


            int count = Count;
            Entities.SymmetricExceptWith(other);
            if (Count > Capacity)
            {
                Debug.LogError("SymmetricExceptWith gone over the entity's capacity");
                return;
            }
            if (Count > count)
                IncreaseStateCheck();
            else if(Count<count)
                DecreaseStateCheck();
        }

        public void UnionWith(IEnumerable<Entity> other)
        {
            // if (GetAffectedByContents) entity.StateChanged += SubEntityStateChanged; Should be added


            Entities.UnionWith(other);
            if (Count > Capacity)
            {
                Debug.LogError("UnionWith gone over the entity's capacity");
                return;
            }
            IncreaseStateCheck();
        }


        IEnumerator IEnumerable.GetEnumerator()
        {
            return Entities.GetEnumerator();
        }


        private void IncreaseStateCheck()
        {
            if (Count > 0)
            {
                if (Count >= Capacity && !StateManager.IsStateActive(ContainerEntityState.Full))
                    StateManager.SetState(ContainerEntityState.Full, containerChangeEventArgs);
                else if (!StateManager.IsStateActive(ContainerEntityState.NotFullNotEmpty))
                    StateManager.SetState(ContainerEntityState.NotFullNotEmpty, containerChangeEventArgs);
            }
        }
        private void DecreaseStateCheck()
        {
            if (Count < Capacity)
            {
                if (Count <= 0 && !StateManager.IsStateActive(ContainerEntityState.Empty))
                    StateManager.SetState(ContainerEntityState.Empty, containerChangeEventArgs);
                else if (!StateManager.IsStateActive(ContainerEntityState.NotFullNotEmpty))
                    StateManager.SetState(ContainerEntityState.NotFullNotEmpty, containerChangeEventArgs);
            }
        }
    }
}