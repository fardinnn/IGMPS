﻿using BG.States;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


namespace BG.API
{
    public abstract class Entity : MonoBehaviour, IDisposable
    {
        public Options Options { get; set; }

        private ContainerEntity parent;
        public ContainerEntity Parent { get => parent;
            set
            {
                if (parent == value) return;
                var previousParent = parent;
                parent = value;
                if(parent!=null && !parent.Contains(this)) parent?.Add(this);
                previousParent?.Remove(this);
            }
        }

        public IEntityStateManager StateManager { get; protected set; }
        protected virtual void Awake()
        {
            StateManager = new EntityStatesManager(this);
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }


        // Just for show
        public string ActiveStates;
    }
}