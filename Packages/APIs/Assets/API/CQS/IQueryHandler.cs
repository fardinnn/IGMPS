﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;


namespace BG.API
{
    public interface IQueryHandler<TQuery, TResult> where TQuery: IQuery
    {
        TResult Query(TQuery parameters);
    }

    public interface IAsyncQueryHandler<TQuery, TResult> : IQueryHandler<TQuery, TResult> where TQuery : IQuery
    {
        Task<TResult> AsyncQuery(TQuery parameters);
    }
}