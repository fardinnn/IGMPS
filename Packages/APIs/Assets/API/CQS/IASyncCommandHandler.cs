﻿using System.Threading.Tasks;

namespace BG.API
{
    public interface IASyncCommandHandler<TCommand> : ICommandHandler<TCommand> where TCommand : ICommand
    {
        Task AsyncHandle(TCommand command);
    }
}
