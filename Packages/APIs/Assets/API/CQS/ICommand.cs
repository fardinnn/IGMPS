﻿
using System;

namespace BG.API
{
    public interface ICommand
    {
        Guid Id { get; set; }
    }
}