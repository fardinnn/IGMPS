﻿namespace BG.API
{
    public interface ISearchIdQuery : IQuery
    {
        System.Guid TargetId { get; set; }
    }
}