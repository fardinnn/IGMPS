﻿using System;
using UnityEditor;

namespace BG.API
{
    public interface IQuery
    {
        Guid Id { get; set; }
    }
}