﻿using BG.API;
using BG.Extensions;
using BG.States;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BG.Transitions.Domain
{
    public class SimpleTransitionSystem : ITransitionSystem
    {
        private SimpleTransitionParameters command;


        public SimpleTransitionSystem(SimpleTransitionParameters command)
        {
            this.command = command;
            IsReady = true;
        }

        public Guid Id { get; set; }
        public bool IsReady { get; private set; }

        public void Visit(List<Entity> entities)
        {
            entities.ForEach(entity =>
            {
                entity.AddSingleStateActivateAction<TransitionState, LinearTransitionChangedEventArgs>
                (TransitionState.Begin, Transit);
            });
        }


        Dictionary<Entity, Task> transitionCoroutines = new Dictionary<Entity, Task>();

        public void Transit(StateGroup states, LinearTransitionChangedEventArgs linearTransition)
        {
            try
            {
                Entity entity = linearTransition.Entity;
                if (transitionCoroutines.ContainsKey(entity))
                {
                    transitionCoroutines[entity].Stop();
                    transitionCoroutines[entity] = new Task(WaitForTransition(entity, linearTransition));
                }
                else
                {
                    transitionCoroutines.Add(entity, new Task(WaitForTransition(entity, linearTransition)));
                }
            }
            catch (Exception ex)
            {
                Debug.LogError(ex.Message);
                throw;
            }
        }

        IEnumerator WaitForTransition(Entity entity, LinearTransitionChangedEventArgs linearTransition)
        {
            yield return TransitionEntityToCell(entity, linearTransition.EndPosition, linearTransition.Threshold);

            entity.transform.position = linearTransition.EndPosition;
            entity.SetState(TransitionState.End, linearTransition);

            transitionCoroutines.Remove(entity);
        }

        IEnumerator TransitionEntityToCell(Entity entity, Vector3 targetPosition, float distanceThreshold)
        {
            float distance = (entity.transform.position - targetPosition).magnitude;
            float maxDistanceDelta = distance / 20.0f;

            while ((entity.transform.position - targetPosition).sqrMagnitude > distanceThreshold)
            {
                entity.transform.position = Vector3.MoveTowards(entity.transform.position, targetPosition, maxDistanceDelta);
                yield return new WaitForSeconds(Time.deltaTime);
            }
        }


        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}
