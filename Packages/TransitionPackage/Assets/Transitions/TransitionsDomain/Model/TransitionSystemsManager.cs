﻿using BG.API;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace BG.Transitions.Domain
{
    public class TransitionSystemsManager : Singleton<TransitionSystemsManager>
    {
        private static Dictionary<Guid, ITransitionSystem> transitionSystems;

        protected override void Awake()
        {
            IsDestroyOnLoad = true;
            base.Awake();

            transitionSystems = new Dictionary<Guid, ITransitionSystem>();
        }

        public void Add(ITransitionSystem transitionSystem)
        {
            if (transitionSystem.Id == default) throw new ArgumentException("transitionSystem's id in not assigned");
            transitionSystems.Add(transitionSystem.Id, transitionSystem);
        }

        public static ITransitionSystem Get(Guid id)
        {
            if (transitionSystems == null) throw new NullReferenceException("transitionSystems can not be null!");
            if (!transitionSystems.ContainsKey(id))
                Debug.LogError("There is no transition system with the provided id!");
            return transitionSystems[id];
        }

        public static T Get<T>(Guid id) where T : class, ITransitionSystem
        {
            if (transitionSystems == null) throw new NullReferenceException("transitionSystems can not be null!");
            if (!transitionSystems.ContainsKey(id))
                Debug.LogError("There is no transition system with the provided id!");
            var target = transitionSystems[id] as T;
            if (target == null)
                Debug.LogError("Provided type is not compatible with target system id!");
            return target;
        }
    }
}
