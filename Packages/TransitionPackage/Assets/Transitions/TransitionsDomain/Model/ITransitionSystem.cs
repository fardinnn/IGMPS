﻿using BG.API;
using BG.States;
using System;

namespace BG.Transitions.Domain
{
    public interface ITransitionSystem : IComponentSystem, IDisposable
    {
        Guid Id { get; set; }

        void Transit(StateGroup states, LinearTransitionChangedEventArgs linearTransition);
    }
}
