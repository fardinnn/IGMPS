using BG.API;
using System;

namespace BG.Transitions.Domain
{
    public abstract class TransitionCommandParameters : ICommand
    {
        public Guid Id { get; set; }
        public Guid SystemId { get; set; }
    }
}
