﻿namespace BG.Transitions.Domain
{
    public class SetUpSimpleTransitionSystemHandler : SetupTransitionCommandHandler<SimpleTransitionParameters>
    {
        public override void Handle(SimpleTransitionParameters command)
        {
            base.Handle(command);

            var system = new SimpleTransitionSystem(command);
            system.Id = command.SystemId;
            Manager.Add(system);
        }
    }
}
