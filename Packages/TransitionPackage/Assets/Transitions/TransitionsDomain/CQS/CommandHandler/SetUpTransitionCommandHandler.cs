using BG.API;
using UnityEngine;

namespace BG.Transitions.Domain
{
    public abstract class SetupTransitionCommandHandler<TCommand> : ICommandHandler<TCommand>
            where TCommand : TransitionCommandParameters
    {
        protected TransitionSystemsManager Manager;
        public virtual void Handle(TCommand command)
        {
            Manager = Object.FindObjectOfType<TransitionSystemsManager>();
            if (Manager == null)
            {
                new GameObject("TransitionSystemManager").AddComponent<TransitionSystemsManager>();
                Manager = Object.FindObjectOfType<TransitionSystemsManager>();
            }
        }
    }
}
