﻿using BG.API;
using System;
using BG.Transitions.Domain;

namespace BG.Transitions
{
    public class SimpleTransitionController : TransitionController<SimpleTransitionView, SimpleTransitionModel>
    {
        private SimpleTransitionSystem transitionSystem;
        public SimpleTransitionController(SimpleTransitionView view = null, SimpleTransitionModel model = null) : base(view, model)
        {

        }

        public override IComponentSystem ProvideComponentSystem(Options options = null)
        {
            if (transitionSystem == null)
            {
                    Guid systemId = Guid.NewGuid();
                    SimpleTransitionParameters command = Model.ToCommand();
                    command.SystemId = systemId;
                    new SetUpSimpleTransitionSystemHandler().Handle(command);
                    transitionSystem = TransitionSystemsManager.Get<SimpleTransitionSystem>(systemId);
            }
            options?.AlterOn(transitionSystem);
            return transitionSystem;
        }
    }
}
