﻿using BG.API;
using UnityEngine;

namespace BG.Transitions
{
    public abstract class TransitionModel : IModel
    {
        public Transform Parent { get; set; }
        public Options DefaultOptions { get; set; }
    }
}
