﻿using BG.API;
using System;
using UnityEngine;

namespace BG.Transitions
{
    public abstract class TransitionView : MonoBehaviour, IView
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public IController<IView, IModel> Controller { get; internal set; }
        public void Dispose()
        {
            Destroy(gameObject);
        }
    }
}
