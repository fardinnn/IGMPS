﻿using UnityEngine;
using Fardin.Audio.Sfx;
using NUnit.Framework;

public class SfxSoundTestsa
{
    class SfxSoundTest : SfxSound
    {

    }

    [Test]
    public void Set_SfxSound_Clip_For_First_Time()
    {
        SfxSound sound = new SfxSoundTest();

        sound.Clip = AudioClip.Create("TestClip", 1, 1, 1001, false);

        Assert.IsNotNull(sound.Clip);
    }

    [Test]
    public void SfxSounds_OnClipChanged_Event_Invoked_On_Setting_Clip()
    {
        SfxSound sound = new SfxSoundTest();
        bool Invoked = false;
        sound.OnClipChanged += (s, e)=>
        {
            Invoked = true;
        };

        sound.Clip = AudioClip.Create("TestClip", 1, 1, 1001, false);

        Assert.IsTrue(Invoked);
    }
}


