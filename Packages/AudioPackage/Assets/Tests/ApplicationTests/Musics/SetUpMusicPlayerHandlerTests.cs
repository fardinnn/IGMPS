﻿using System;
using Fardin.Audio.Musics;
using NUnit.Framework;
using UnityEngine;
using Object = System.Object;

namespace Assets.Tests.ApplicationTests.Musics
{
    public class SetUpMusicPlayerHandlerTests
    {
        [Test]
        public void Default_SetUp_MusicPlayer()
        {
            var musicPlayerObject = new GameObject("MusicPlayer");
            var parameters = new SetUpMusicPlayer {MusicPlayerObject = musicPlayerObject};
            var handler = new SetUpMusicPlayerHandler();

            handler.Handle(parameters);

            //Assert.IsNotNull(musicPlayerObject.GetComponent<MusicPlayer<>>());
            Assert.IsNotNull(musicPlayerObject.GetComponent<AudioSource>());
        }

        //[Test]
        //public void SetUp_MusicPlayer_Without_Object_Parameters()
        //{
        //    var allMusicPlayers = UnityEngine.Object.FindObjectsOfType<MusicPlayer>();
        //    foreach (var musicPlayer in allMusicPlayers)
        //    {
        //        var audioSource = musicPlayer.GetComponent<AudioSource>();
        //        UnityEngine.Object.Destroy(musicPlayer);
        //        UnityEngine.Object.Destroy(audioSource);
        //    }
        //    var parameters = new SetUpMusicPlayer { MusicPlayerObject = null};
        //    var handler = new SetUpMusicPlayerHandler();

        //    handler.Handle(parameters);

        //    Assert.IsNotNull(UnityEngine.Object.FindObjectOfType<MusicPlayer>());
        //    Assert.IsNotNull(UnityEngine.Object.FindObjectOfType<MusicPlayer>().GetComponent<AudioSource>());
        //}

        [Test]
        public void SetUp_Singleton_MusicPlayer()
        {
            var musicPlayerObject = new GameObject("MusicPlayer");
            var parameters = new SetUpMusicPlayer { MusicPlayerObject = musicPlayerObject , IsSingleton = true};
            var handler = new SetUpMusicPlayerHandler();

            handler.Handle(parameters);
            handler.Handle(parameters);
            handler.Handle(parameters);

            //Assert.AreEqual(1, musicPlayerObject.GetComponents<MusicPlayer<>>().Length);
        }
    }
}
