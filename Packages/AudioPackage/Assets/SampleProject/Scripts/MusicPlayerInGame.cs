﻿using System;
using Fardin.Audio.Musics;
using UnityEngine;

namespace Assets.SampleProject.Scripts
{
    public class MusicPlayerInGame : MonoBehaviour
    {
#pragma warning disable 649
        [SerializeField] private FakeMusicPlayerUi _fakeMusicPlayerUi;
#pragma warning restore 649

        [SerializeField] private AudioClip _clip;
        //private MusicPlayer<> _musicPlayer;

        public void SetUpMusicPlayer()
        {
            try
            {
                new SetUpMusicPlayerHandler().Handle(new SetUpMusicPlayer{IsSingleton = true});
            }
            catch (Exception e)
            {
                Debug.Log(e);
            }

            //_musicPlayer = FindObjectOfType<MusicPlayer>(); ///??????????

            var musicPlayerUi = Instantiate(_fakeMusicPlayerUi, transform);

            //musicPlayerUi.AddMusicPlayer(_musicPlayer);
        }
    }
}
