﻿using System;
using Fardin.API.Abilities;
using Fardin.Audio.Musics;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.SampleProject.Scripts
{
    public class FakeMusicPlayerUi : MonoBehaviour
    {

#pragma warning disable 649
        [SerializeField] private Button _playButton;
        [SerializeField] private Button _addItemButton;
        [SerializeField] private Button _pauseButton;
        [SerializeField] private Button _stopButton;
        [SerializeField] private Button _muteButton;

        [SerializeField] private AudioClip _music;
#pragma warning restore 649

        //public void AddMusicPlayer(MusicPlayer controller)
        //{
        //    _playButton.onClick.AddListener(() => controller.Play(_playButton, new PlayEventArgs()));
        //    _pauseButton.onClick.AddListener(() => controller.Pause(this, new PauseEventArgs()));
        //    _stopButton.onClick.AddListener(() => controller.Stop(this, new StopEventArgs()));
        //    _muteButton.onClick.AddListener(() => controller.Mute(_muteButton, null));
        //}

        private AudioClip GetAudioClip()
        {
            return _music;
        }
    }
}
