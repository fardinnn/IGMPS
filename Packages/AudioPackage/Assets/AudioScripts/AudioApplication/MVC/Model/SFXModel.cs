﻿using BG.API;
using System;
using UnityEngine;

namespace BG.Audios
{
    public abstract class SFXModel : IModel
    {
        public Transform Parent { get; set; }
        public Options DefaultOptions { get; set; }
        public Guid GridSysetmId { get; set; }
    }

}
