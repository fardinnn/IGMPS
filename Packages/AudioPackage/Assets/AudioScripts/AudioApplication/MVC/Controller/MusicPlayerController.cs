using BG.API;
using System;
using UnityEngine;

namespace BG.Audios
{
    public abstract class MusicPlayerController<TModel> : IEntityController<IDynamicView, TModel>, IDisposable
        where TModel : MusicPlayerModel
    {
        public IDynamicView View { get; }
        public TModel Model { get; }

        public Guid Id { get; } = Guid.NewGuid();

        public abstract Guid SystemId { get; }

        public MusicPlayerController(IDynamicView view = null, TModel model = null)
        {
            Model = model ?? CreateDefaultModel();
            View = view ?? new GameObject("MusicPlayer").AddComponent<EmptyView>();
            //View.Controller = this;
            if (Model.Parent == null && (View is Component ViewComp))
                Model.Parent = ViewComp.transform;
        }

        protected virtual TModel CreateDefaultModel()
        {
            return (TModel)Activator.CreateInstance(typeof(TModel));
        }


        public virtual void SetActivate(bool state)
        {
            if (View is Component ViewComp) ViewComp.gameObject.SetActive(state);
        }
        public abstract void OnEnable();
        public abstract void OnDisable();

        public virtual void Dispose()
        {
            View.Dispose();
        }

        public abstract void Generate(Options options = null, bool activale = true);


    }

}
