﻿using BG.API;
using BG.API.Abilities;
using BG.Audios.Domain;
using BG.Extensions;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace BG.Audios
{
    public class SimpleMusicPlayerController : MusicPlayerController<SimpleMusicPlayerModel>,
        IInputReceiver
    {
        private SimpleMusicPlayer _musicPlayer;

        public SimpleMusicPlayerController(IDynamicView view = null,
            SimpleMusicPlayerModel model = null) : base(view, model)
        {
            InitializeInputs();
            InitializeView();
        }

       
        public override Guid SystemId => _musicPlayer == null ? Guid.Empty : _musicPlayer.Id;

        public override void Generate(Options options = null, bool activate = true)
        {
            var command = Model.ToCommand();
            new SetUpSimpleMusicPlayerHandler().Handle(command);
            _musicPlayer = MusicPlayersManager.Get<SimpleMusicPlayer>(command.SystemId);
            _musicPlayer.Generate();
        }

        public void OnCellShapeChange(object o, ChangeEventArgs e)
        {
            // _grid.UpdateGrid();
        }


        /****************************************/
        /*             SetUp Inputs             */
        /****************************************/

        public event EventHandler Enabled;
        public event EventHandler Disabled;

        public string InputMapName { get; private set; }
        public Dictionary<string, EventHandler<IInputValue>> Actions { get; private set; }
        public Dictionary<string, EventHandler<IInputValue>> CancelActions { get; private set; }
        public string JSONInputs { get; private set; }
        public string CoExistGroup { get; set; } = "Cameras";


        private void InitializeInputs()
        {
            InputMapName = "Simple Music Player";
            Actions = new Dictionary<string, EventHandler<IInputValue>>
            {
                {"Next_Buttons",  (object o, IInputValue e)=>OnSteppingInput(this, new StepEventArgs{ StepCount = 1}) },
                {"Previous_Buttons",  (object o, IInputValue e)=>OnSteppingInput(this, new StepEventArgs{ StepCount = -1}) },
                {"Play_Buttons",  (object o, IInputValue e)=>OnPlayInput(this, new PlayEventArgs()) },
                {"Pause_Buttons",  (object o, IInputValue e)=>OnPauseInput(this, new PauseEventArgs()) },
                {"Stop_Buttons",  (object o, IInputValue e)=>OnStopInput(this, new StopEventArgs()) },
                {"SpeedUp_Buttons",  (object o, IInputValue e)=>OnSpeedChangeInput(this, new SpeedChangeEventArgs{ Value = 1.6f}) },
                {"SlowDown_Buttons",  (object o, IInputValue e)=>OnSpeedChangeInput(this, new SpeedChangeEventArgs{ Value = 0.625f}) },
                {"IncreaseVolume_Buttons",  (object o, IInputValue e)=>OnVolumeChangeInput(this, new VolumeChangeEventArgs{Value = 0.1f }) },
                {"DecreaseVolume_Buttons",  (object o, IInputValue e)=>OnVolumeChangeInput(this, new VolumeChangeEventArgs{Value = -0.1f }) },
                {"Mute_Buttons",  (object o, IInputValue e)=>OnMuteInput(this, new MuteEventArgs()) },
            };

            JSONInputs = new SimpleMusicPlayerInput().SimpleMusicPlayer.Get().ToJson();
        }


        private bool isEnable = true;
        public override void OnEnable()
        {
            isEnable = true;
            Enabled?.Invoke(this, null);

        }

        public override void OnDisable()
        {
            isEnable = false;
            Disabled?.Invoke(this, null);
            //if (rotateCoroutine!=null)
            //{
            //    if (rotateCoroutine.Running) rotateCoroutine.Stop();
            //    rotateCoroutine = null;
            //}
        }

        /****************************************/
        /*             SetUp View             */
        /****************************************/

        public void InitializeView()
        {
            if (View is EmptyView) return;
            View.GetAbilities<IPlayable>().ForEach(playable => playable.Played += OnPlayInput);
            View.GetSubscribers<IPlaySubscriber>().ForEach(playSubscriber => Played += playSubscriber.OnPlayed);

            View.GetAbilities<IPausable>().ForEach(pausable => pausable.Paused += OnPauseInput);
            View.GetSubscribers<IPauseSubscriber>().ForEach(pauseSubscriber => Paused += pauseSubscriber.OnPaused);

            View.GetAbilities<IStoppable>().ForEach(stppable => stppable.Stopped += OnStopInput);
            View.GetSubscribers<IStopSubscriber>().ForEach(stopSubscriber => Stopped += stopSubscriber.OnStopped);

            View.GetAbilities<IMutable>().ForEach(mutable => mutable.Muted += OnMuteInput);
            View.GetSubscribers<IMuteSubscriber>().ForEach(muteSubscriber => Muted += muteSubscriber.OnMuted);

            View.GetAbilities<ISteppable>().ForEach(steppable => steppable.Stepped += OnSteppingInput);
            View.GetSubscribers<IStepSubscriber>().ForEach(stepSubscriber => Stepped += stepSubscriber.OnStepped);

            View.GetAbilities<ISpeedChangeable>().ForEach(changeable => changeable.Changed += OnSpeedChangeInput);
            View.GetSubscribers<ISpeedChangeSubscriber>().ForEach(changeSubscriber => SpeedChanged += changeSubscriber.OnChanged);

            View.GetAbilities<IVolumeChangeable>().ForEach(changeable => changeable.Changed += OnVolumeChangeInput);
            View.GetSubscribers<IVolumeChangeSubscriber>().ForEach(changeSubscriber => VolumeChanged += changeSubscriber.OnChanged);
        }

        /****************************************/
        /*             SetUp Input Actions            */
        /****************************************/
        private void OnSteppingInput(object o, StepEventArgs e)
        {
            Debug.Log("Next / Previous");
        }

        private void OnPlayInput(object o, PlayEventArgs e)
        {
            Debug.Log("Play");
        }
        private void OnPauseInput(object o, PauseEventArgs e)
        {
            Debug.Log("Pause");
        }
        private void OnStopInput(object o, StopEventArgs e)
        {
            Debug.Log("Stop");
        }

        private void OnSpeedChangeInput(object o, SpeedChangeEventArgs e)
        {
            Debug.Log("Speed Up / Slow Down");
        }
        private void OnVolumeChangeInput(object o, VolumeChangeEventArgs e)
        {
            Debug.Log("IncreaseVolume / Decrease");
        }
        private void OnMuteInput(object o, MuteEventArgs e)
        {
            Debug.Log("Mute");
        }


        /****************************************/
        /*             Actions Results          */
        /****************************************/
        public event EventHandler<StepEventArgs> Stepped;
        private void OnSteppingResult(int stepCount)
        {
            Debug.Log("Next / Previous");
        }

        public event EventHandler<PlayEventArgs> Played;
        private void OnPlayResult(object o, PlayEventArgs e)
        {
            Debug.Log("Play");
        }

        public event EventHandler<PauseEventArgs> Paused;
        private void OnPauseResult()
        {
            Debug.Log("Pause");
        }

        public event EventHandler<StopEventArgs> Stopped;
        private void OnStopResult()
        {
            Debug.Log("Stop");
        }

        public event EventHandler<SpeedChangeEventArgs> SpeedChanged;
        private void OnSpeedChangeResult(float factor)
        {
            Debug.Log("SpeedUp");
        }

        public event EventHandler<VolumeChangeEventArgs> VolumeChanged;
        private void OnVolumeChanageResult(float amount)
        {
            Debug.Log("IncreaseVolume");
        }

        public event EventHandler<MuteEventArgs> Muted;
        private void OnMuteResult()
        {
            Debug.Log("Mute");
        }
    }
}
