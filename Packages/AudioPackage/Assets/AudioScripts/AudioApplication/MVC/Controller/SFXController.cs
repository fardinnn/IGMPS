﻿using BG.API;
using BG.API.Abilities;
using BG.Audios.Domain;
using BG.States;
using System;
using UnityEngine;

namespace BG.Audios
{
    public class SimpleSFXController : SFXController<SimpleSFXView, SimpleSFXModel>
    {
        private SimpleSFXSystem sfxSystem;

        public SimpleSFXController(SimpleSFXView view = null, SimpleSFXModel model = null) : base(view, model)
        {

        }

        public override IComponentSystem ProvideComponentSystem(Options options = null)
        {
            if (sfxSystem == null)
            {
                //if (movesFactory.Has(Model.MoveSystemName))
                //{
                //    var factory = movesFactory.Get(Model.MoveSystemName);
                //    moveSystem = factory.Create<SimpleMoveSystem>();
                //}
                //else
                //{
                Guid systemId = Guid.NewGuid();
                var command = Model.ToCommand();
                command.SystemId = systemId;
                new SetUpSimpleSFXSystemHandler().Handle(command);
                sfxSystem = SFXSystemsManager.Get<SimpleSFXSystem>(systemId);
                //}
            }
            options?.AlterOn(sfxSystem);
            return sfxSystem;
        }

    }



    public abstract class SFXController<TView, TModel> : IComponentController<TView, TModel>, IDisposable
        where TView : SFXView
        where TModel : SFXModel
    {
        public TView View { get; }
        public TModel Model { get; }

        public Guid Id { get; } = Guid.NewGuid();


        public SFXController(TView view = null, TModel model = null)
        {
            Model = model ?? CreateDefaultModel();
            View = view ?? CreateDefaultView();
            this.requiredStates = requiredStates ?? new CompositeStatesSet();
            View.Controller = this;
        }

        protected CompositeStatesSet requiredStates;
        public event EventHandler<RequiredStatesEventArgs> RequiredStatesUpdated;
        public void AddStatesGroup(params Enum[] groupStates)
        {
            if (requiredStates.Contains(groupStates)) return;
            requiredStates.Add(groupStates);
            RequiredStatesUpdated?.Invoke(this, new RequiredStatesEventArgs { StatesSet = requiredStates });
        }

        protected virtual TModel CreateDefaultModel()
        {
            return (TModel)Activator.CreateInstance(typeof(TModel));
        }

        protected virtual TView CreateDefaultView()
        {
            var view = (Model.Parent ? Model.Parent.gameObject :
                (Model.Parent = new GameObject(typeof(TView).Name.Replace("View", "")).transform).gameObject)
                .AddComponent<TView>();
            return view;
        }

        public virtual void Dispose()
        {
            UnityEngine.Object.Destroy(View.gameObject);
        }

        public abstract IComponentSystem ProvideComponentSystem(Options options);
    }

}
