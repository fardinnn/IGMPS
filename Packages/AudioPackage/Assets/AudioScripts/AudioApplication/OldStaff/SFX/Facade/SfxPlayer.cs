﻿using System;
using System.Threading.Tasks;
using BG.API.Abilities;
using UnityEngine;

namespace BG.Audio.Sfx
{
    public abstract class SfxPlayer: MonoBehaviour, IPlayable, IStoppable, IPausable
    {
        protected SfxSound sfxSound { get; set; }

        public event EventHandler<PlayEventArgs> Played;
        public abstract void Play(object sender, PlayEventArgs e);

        public event EventHandler<StopEventArgs> Stopped;
        public abstract void Stop(object o, StopEventArgs e);

        public event EventHandler<PauseEventArgs> Paused;
        public abstract void Pause(object o, PauseEventArgs e);


        internal virtual void Bind(SfxSound sfx)
        {
            UnBind();
            sfxSound = sfx ?? throw new ArgumentNullException("Sfxsound can not be null!");
            sfxSound.OnClipChanged += SetClip;
        }

        private void UnBind()
        {
            if (sfxSound != null) sfxSound.OnClipChanged -= SetClip;
        }

        protected virtual void OnDisable()
        {
            if (sfxSound != null) sfxSound.OnClipChanged -= SetClip;
        }

        protected virtual void OnEnable()
        {
            if (sfxSound != null) sfxSound.OnClipChanged -= SetClip;
        }

        protected abstract void SetClip(object o, ClipChangeEventArgs e);
    }
}
