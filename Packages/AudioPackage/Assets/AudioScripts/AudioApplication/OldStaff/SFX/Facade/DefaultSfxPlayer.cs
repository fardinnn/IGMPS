﻿using System.Threading.Tasks;
using BG.API.Abilities;
using UnityEngine;

namespace BG.Audio.Sfx
{
    [RequireComponent(typeof(AudioSource))]
    public class DefaultSfxPlayer : SfxPlayer
    {
        private readonly AudioSource audioSource;
        internal DefaultSfxPlayer()
        {
            audioSource = GetComponent<AudioSource>();
        }
        
        public override void Play(object o, PlayEventArgs e)
        {
            audioSource.Play();
        }

        public override void Stop(object o, StopEventArgs e)
        {
            audioSource.Stop();
        }

        public override void Pause(object o, PauseEventArgs e)
        {
            audioSource.Pause();
        }

        protected override void SetClip(object o, ClipChangeEventArgs e)
        {
            audioSource.clip = e.Clip;
        }
    }
}