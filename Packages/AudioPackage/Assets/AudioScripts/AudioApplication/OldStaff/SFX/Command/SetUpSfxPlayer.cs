﻿using System;
using BG.API;
using UnityEngine;

namespace BG.Audio.Sfx
{
    public class SetUpSfxPlayer : ICommand
    {
        public Guid Id { get; set; }
        public SfxSound Sfx { get; set; }
        public GameObject TargetObject { get; set; }
    }
}