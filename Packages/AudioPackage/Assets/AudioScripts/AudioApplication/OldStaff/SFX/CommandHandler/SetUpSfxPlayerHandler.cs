﻿using System;
using BG.API;

namespace BG.Audio.Sfx
{
    public class SetUpSfxPlayerHandler : ICommandHandler<SetUpSfxPlayer>
    {
        public void Handle(SetUpSfxPlayer command)
        {
            var sfxPlayer = command.TargetObject.AddComponent<DefaultSfxPlayer>();
            sfxPlayer.Bind(command.Sfx);
            //command.Sfx.OnClipChanged
        }
    }
}