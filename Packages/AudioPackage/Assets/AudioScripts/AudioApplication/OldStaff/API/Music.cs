﻿using BG.Audio.Musics.Domain;
using UnityEngine;

namespace BG.Audio.Musics
{
    public class Music
    {
        public Music(AudioClip musicClip, MusicInfo info)
        {
            Clips = new []{musicClip};
            Info = info;
            Composition = new byte[100];//!!?? //new ClipsComposition(musicClip);
        }

        public Music(AudioClip[] clips, MusicInfo info, byte[] composition)
        {
            Clips = clips;
            Info = info;
            //Composition = deserialize(composition);
        }

        // ReSharper disable twice MemberCanBePrivate.Global UnusedAutoPropertyAccessor.Global
        public AudioClip[] Clips { get; }
        public MusicInfo Info { get; }
        internal byte[] Composition { get; }
    }
}