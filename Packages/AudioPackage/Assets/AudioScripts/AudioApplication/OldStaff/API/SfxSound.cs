﻿using System;
using UnityEngine;

namespace BG.Audio.Sfx
{
    public abstract class SfxSound
    {
        private AudioClip _audioClip;
        public AudioClip Clip
        {
            get => _audioClip;
            set
            {
                if (_audioClip == value) return;
                _audioClip = value;
                OnClipChanged?.Invoke(this, new ClipChangeEventArgs { Clip = value });
            }
        }

        public event EventHandler<ClipChangeEventArgs> OnClipChanged;
    }

    public class ClipChangeEventArgs : EventArgs
    {
        public AudioClip Clip { get; set; }
    }
}
