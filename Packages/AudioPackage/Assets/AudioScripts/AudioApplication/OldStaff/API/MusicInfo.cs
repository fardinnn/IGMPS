﻿using System;
using System.Collections.Generic;

namespace BG.Audio.Musics
{
    public class MusicInfo
    {
        public MusicInfo(Guid musicId, string name, float length, Dictionary<string, string> otherInfo)
        {
            MusicId = musicId;
            Name = name;
            Length = length;
            OtherInfo = otherInfo ?? new Dictionary<string, string>();
        }

        // ReSharper disable twice MemberCanBePrivate.Global UnusedAutoPropertyAccessor.Global
        public Guid MusicId { get; }
        public string Name { get; }
        public float Length { get; }
        public Dictionary<string, string> OtherInfo { get; }
    }
}