﻿namespace BG.Audio.Musics.Model
{
    internal class MusicPlayerSettings
    {
        public int Volume;
        public bool Mute;
    }
}