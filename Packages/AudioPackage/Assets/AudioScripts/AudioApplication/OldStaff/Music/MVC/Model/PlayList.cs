﻿using System.Collections.Generic;
using JetBrains.Annotations;

namespace BG.Audio.Musics.Model
{
    internal class PlayList
    {
        public PlayList()
        {
            MusicsInfos = new List<MusicInfo>();
        }

        [ItemNotNull]
        public List<MusicInfo> MusicsInfos { get; }
    }
}