﻿using BG.API;
using UnityEngine;

namespace BG.Audio.Musics.Model
{
    public class MusicPlayerModelOld : IModel
    {
        internal AudioClip MusicClip { get; set; }
        internal int ActiveMusicIndex { get; set; }
        internal MusicPlayerSettings Settings { get; set; }
        internal PlayList PlayList { get; set; }
    }

    // ReSharper disable once ClassNeverInstantiated.Global
}