﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BG.API.Abilities;
using BG.Audio.Musics.Domain;
using BG.Audio.Musics.Model;
using UnityEngine;
using BG.API;
// ReSharper disable once CheckNamespace
// ReSharper disable once IdentifierTypo
namespace BG.Audio.Musics
{
    public class MusicPlayer<TView> : IController<TView, MusicPlayerModelOld>, IPlayable, IPausable, IStoppable, IMutable, IAddableDirectory,
        IAddableItem<MusicInfo>, IRemovableItem, ISelectableItem, IResetable, IReversible, ICrossable, IInformative where TView: IView
    {
        public MusicPlayerModelOld Model { get; private set; }
        public TView View { get; set; }
        private TView _view;

        internal ActiveMusicPlayer DomainPlayer { get; private set; }

        public Guid Id => throw new NotImplementedException();

        public MusicPlayer(TView view, MusicPlayerModelOld model)
        {
            View = view;
            Model = model;
            DomainPlayer = new ActiveMusicPlayer(new List<Guid>());
        }

        public event EventHandler<PlayEventArgs> Played;

        public void Play(object o, PlayEventArgs e)
        {
            //Task task1 = await new PlayMusicHandler().Handle(new PlayMusic { AudioSource = AudioSource});
            throw new ArgumentNullException();
        }

        public event EventHandler<PauseEventArgs> Paused;

        public void Pause(object o, PauseEventArgs e)
        {
            //new PauseMusicHandler().Handle(new PauseMusic { AudioSource = AudioSource });
        }

        public event EventHandler<StopEventArgs> Stopped;

        public void Stop(object o, StopEventArgs e)
        {

        }

        public event EventHandler<MuteEventArgs> Muted;

        public void Mute(object o, MuteEventArgs e)
        {
            
        }

        public event EventHandler<AddDirectoryEventArgs> DirectoryAdded;

        public void AddDirectory(object o, AddDirectoryEventArgs e)
        {
            
        }


        public event EventHandler<RemoveItemEventArgs> ItemRemoved;

        public void RemoveItem(object o, RemoveItemEventArgs e)
        {
            
        }

        public event EventHandler<SelectItemEventArgs> ItemSelected;

        public void SelectItem(object o, SelectItemEventArgs e)
        {
            
        }

        public event EventHandler<ResetEventArgs> Reseted;

        public void Reset(object o, ResetEventArgs e)
        {
            
        }

        public event EventHandler<ReverseEventArgs> Reversed;

        public void Reverse(object o, ReverseEventArgs e)
        {
            
        }

        public event EventHandler<CrossEventArgs> Crossed;

        public void Cross(object o, CrossEventArgs e)
        {
            
        }

        public event EventHandler<GetInformationEventArgs> GotInformation;

        public void GetInformation(object o, GetInformationEventArgs e)
        {
            
        }

        public event EventHandler<AddItemEventArgs<MusicInfo>> ItemAdded;
        public event EventHandler<ResetEventArgs> Resetting;

        public void AddItem(object o, AddItemEventArgs<MusicInfo> e)
        {
            new AddSimpleMusicHandler().Handle(new AddSimpleMusic()
            {
                MusicId = e.Item.MusicId
            });
            Model.PlayList.MusicsInfos.Add(e.Item);
        }
    }
}
