﻿using System;
using BG.API;
using System.Threading;
using System.Threading.Tasks;
using BG.Audio.Musics.Domain;

namespace BG.Audio.Musics
{
    public class PlayMusicHandler : ICommandHandler<PlayMusic>
    {
        public void Handle(PlayMusic command)
        {

            if (command==null)
                throw new ArgumentNullException($"PlayMusicHandler command can not be null");
            if (command.AudioSource.isPlaying)
                return;

            //CancellationToken ct = new CancellationToken();

            //command.AudioSource.Play();
            //var task1 = Task.Delay((int) (command.AudioSource.clip.length * 1000));
            //var task2 = Task.FromCanceled()


            ////command.AudioSource.clip = ...
            //command.AudioSource.Play(); //???

        }
    }
}