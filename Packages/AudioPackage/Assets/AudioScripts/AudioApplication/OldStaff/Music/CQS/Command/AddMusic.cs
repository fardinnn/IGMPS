﻿using System;
using BG.API;

namespace BG.Audio.Musics
{
    public abstract class AddMusic : ICommand
    {
        public Guid Id { get; set; }
        public Guid MusicId { get; set; }
    }
}