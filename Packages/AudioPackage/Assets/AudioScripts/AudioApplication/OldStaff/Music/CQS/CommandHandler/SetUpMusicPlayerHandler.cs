﻿using System;
using BG.API;
using UnityEngine;

namespace BG.Audio.Musics
{
    public class SetUpMusicPlayerHandler : ICommandHandler<SetUpMusicPlayer>
    {
        public void Handle(SetUpMusicPlayer command)
        {
            if (command==null)
                throw new ArgumentNullException("SetUpMusicPlayer command can't be null");

            CheckObject(command);
            CheckSingleton(command.IsSingleton);

            //var musicPlayer = command.MusicPlayerObject.AddComponent<MusicPlayer>();
            //musicPlayer.AudioSource.loop = false;
        }

        private void CheckObject(SetUpMusicPlayer command)
        {
            if (command.MusicPlayerObject == null)
                command.MusicPlayerObject = new GameObject("MusicPlayer");
        }

        private void CheckSingleton(bool isSingleton)
        {
            //if (isSingleton)
            //{
            //    var musicPlayers = UnityEngine.Object.FindObjectsOfType<MusicPlayer>();
            //    foreach (var player in musicPlayers)
            //    {
            //        UnityEngine.Object.DestroyImmediate(player);
            //    }
            //}
        }
    }
}
