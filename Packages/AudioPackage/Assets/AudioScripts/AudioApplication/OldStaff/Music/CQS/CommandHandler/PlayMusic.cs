﻿using System;
using BG.API;
using UnityEngine;

namespace BG.Audio.Musics
{
    public class PlayMusic : ICommand
    {
        public Guid Id { get; set; }
        public AudioSource AudioSource { get; set; }
    }
}