﻿using System;
using BG.API;

namespace BG.Audio.Musics
{
    public class RemoveMusic : ICommand
    {
        public Guid Id { get; set; }
    }
}