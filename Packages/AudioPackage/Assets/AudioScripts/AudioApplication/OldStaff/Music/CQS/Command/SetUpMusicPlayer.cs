﻿using System;
using BG.API;
using UnityEngine;

namespace BG.Audio.Musics
{
    public class SetUpMusicPlayer : ICommand
    {
        public Guid Id { get; set; }
        public GameObject MusicPlayerObject { get; set; }
        public bool IsSingleton { get; set; }
    }
}
