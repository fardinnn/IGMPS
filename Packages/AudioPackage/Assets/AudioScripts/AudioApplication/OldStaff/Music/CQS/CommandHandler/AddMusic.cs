﻿using System;
using BG.API;
using BG.Audio.Musics.Model;
using UnityEngine;

namespace BG.Audio.Musics
{
    public abstract class AddMusicHandler<T> : ICommandHandler<T> where T : AddMusic
    {
        public abstract void Handle(T command);
    }

    public class Audio<T>
    {
        public Guid AudioId { get; }
        internal Func<T> LoadAudio { get; }
        internal string Name { get; }
        internal int Length { get; }

        public Audio(Guid audioId, Func<T> LoadAudioData, string name, int length)
        {
            AudioId = audioId;
            LoadAudio = LoadAudioData;
            Name = name;
            Length = length;
        }

    }

    internal class AudioLoader
    {
        internal Music LoadMusicData<T>(Audio<T> audio)
        {
            var data = audio.LoadAudio();
            // deserialize data
            return new Music(null, null, null);
        }
    }

}