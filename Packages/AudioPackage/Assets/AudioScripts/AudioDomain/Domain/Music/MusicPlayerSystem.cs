﻿using System;

namespace BG.Audios.Domain
{
    public abstract class MusicPlayerSystem
    {
        public Guid Id { get; set; }

        public virtual void Generate()
        {

        }
    }
}
