﻿using BG.API;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace BG.Audios.Domain
{
    public class MusicPlayersManager : Singleton<MusicPlayersManager>
    {
        private static Dictionary<Guid, MusicPlayerSystem> musicPlayers;

        protected override void Awake()
        {
            IsDestroyOnLoad = true;
            base.Awake();

            musicPlayers = new Dictionary<Guid, MusicPlayerSystem>();
        }

        public void Add(MusicPlayerSystem musicPlayer)
        {
            if (musicPlayer.Id == default) throw new ArgumentException("Music Player's id in not assigned");
            musicPlayers.Add(musicPlayer.Id, musicPlayer);
        }

        public static MusicPlayerSystem Get(Guid id)
        {
            if (musicPlayers == null) throw new NullReferenceException("musicPlayers can not be null!");
            if (!musicPlayers.ContainsKey(id))
                Debug.LogError("There is no 'Music Player' with the provided id!");
            return musicPlayers[id];
        }

        public static T Get<T>(Guid id) where T : MusicPlayerSystem
        {
            if (musicPlayers == null) throw new NullReferenceException("musicPlayers can not be null!");
            if (!musicPlayers.ContainsKey(id))
                Debug.LogError("There is no 'Music Player' with the provided id!");
            var target = musicPlayers[id] as T;
            if (target == null)
                Debug.LogError("Provided type is not compatible with target system id!");
            return target;
        }
    }
}
