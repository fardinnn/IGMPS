﻿using BG.API;
using BG.API.Audios;
using System;
using System.Collections.Generic;

namespace BG.Audios.Domain
{
    public class SimpleSFXSystem : ISFXSystem
    {
        private SimpleSFXParameters command;

        public SimpleSFXSystem(SimpleSFXParameters command)
        {
            this.command = command;
            IsReady = true;
        }

        public Guid Id { get; set; }
        public bool IsReady { get; private set; }

        public void Visit(List<Entity> entities)
        {
            entities.ForEach(entity =>
            {
                SetSFXs(entity.GetComponentsInChildren<SFX>(true));
                SetMusicSFXs(entity.GetComponentsInChildren<Music>(true));
                SetVoices(entity.GetComponentsInChildren<Voice>(true));

                //entity.SetState(MoveState.NotMoved);

                //foreach (var state in command.StatesToActivatePotentialMove)
                //{
                //    entity.AddStateActivateAction(state, OnActivateStateMove);
                //    entity.AddStateDeactivateAction(state, OnDeactivateStateMove);
                //}

                //entity.AddStateActivateAction(new StateGroup(PlacementStageState.Begin), OnBeginPlacement);
                //entity.AddStateDeactivateAction(new StateGroup(PlacementStageState.Begin), OnChangedPlacement);
            });
        }

        private void SetSFXs(SFX[] sFXes)
        {
            foreach (var sfx in sFXes)
            {
                sfx.Played += OnSFXPlayed;
                sfx.Stopped += OnSFXStopped;
                sfx.Paused += OnSFXPaused;
            }
        }

        private void SetMusicSFXs(Music[] musics)
        {

        }

        private void SetVoices(Voice[] voices)
        {

        }



        private void OnSFXPlayed(object o, EventArgs e)
        {

        }
        private void OnSFXStopped(object o, EventArgs e)
        {

        }
        private void OnSFXPaused(object o, EventArgs e)
        {

        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}
