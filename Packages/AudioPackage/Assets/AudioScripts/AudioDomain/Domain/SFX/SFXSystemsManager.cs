﻿using BG.API;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace BG.Audios.Domain
{
    public class SFXSystemsManager : Singleton<SFXSystemsManager>
    {
        private static Dictionary<Guid, ISFXSystem> sfxSystems;

        protected override void Awake()
        {
            IsDestroyOnLoad = true;
            base.Awake();

            sfxSystems = new Dictionary<Guid, ISFXSystem>();
        }

        public void Add(ISFXSystem sfxSystem)
        {
            if (sfxSystem.Id == default) throw new ArgumentException("sfxSystem's id in not assigned");
            sfxSystems.Add(sfxSystem.Id, sfxSystem);
        }

        public static ISFXSystem Get(Guid id)
        {
            if (sfxSystems == null) throw new NullReferenceException("sfxSystems can not be null!");
            if (!sfxSystems.ContainsKey(id))
                Debug.LogError("There is no sfx system with the provided id!");
            return sfxSystems[id];
        }

        public static T Get<T>(Guid id) where T : class, ISFXSystem
        {
            if (sfxSystems == null) throw new NullReferenceException("sfxSystems can not be null!");
            if (!sfxSystems.ContainsKey(id))
                Debug.LogError("There is no sfx system with the provided id!");
            var target = sfxSystems[id] as T;
            if (target == null)
                Debug.LogError("Provided type is not compatible with target system id!");
            return target;
        }
    }
}
