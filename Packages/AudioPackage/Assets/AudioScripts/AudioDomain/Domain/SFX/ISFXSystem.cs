﻿using BG.API;
using System;

namespace BG.Audios.Domain
{
    public interface ISFXSystem : IComponentSystem
    {
        Guid Id { get; set; }
    }
}
