﻿
using System;
using System.Collections.Generic;

namespace BG.Audio.Musics.Domain{
    public class ActiveMusicPlayer
    {

        public ActiveMusicPlayer(List<Guid> musicIds, int activeMusicIndex = 0)
        {
            MusicIds = musicIds;
            ActiveMusicIndex = activeMusicIndex;
        }

        // ReSharper disable once MemberCanBePrivate.Global UnusedAutoPropertyAccessor.Global
        public List<Guid> MusicIds { get; }
        // ReSharper disable once MemberCanBePrivate.Global UnusedAutoPropertyAccessor.Global
        public int ActiveMusicIndex { get; }
    }

    public class ActiveMusic
    {
        internal ClipsComposition Composition { get; }
        internal float Progress { get; }
    }

    public class PlayMusic
    {
        public void BeginPlaying(Func<ActiveMusic, MusicClipEndArgs, ClipInfo> musicClipEndFunction)
        {
            musicClipEndFunction = OnMusicClipEnd;
        }

        private ClipInfo OnMusicClipEnd(ActiveMusic music, MusicClipEndArgs e)
        {
            return music.Composition.CompositionRows[e.Row].Next();
        }

    }
    public struct MusicClipEndArgs
    {
        public int Row { get; }

        public MusicClipEndArgs(int row)
        {
            Row = row;
        }
    }

    public struct ClipInfo
    {
        // ReSharper disable once UnusedAutoPropertyAccessor.Global
        public int ClipIndex { get; set; }
        // ReSharper disable once UnusedAutoPropertyAccessor.Global
        public float Begin { get; set; }
        // ReSharper disable once UnusedAutoPropertyAccessor.Global
        public float End { get; set; }
    }

}


