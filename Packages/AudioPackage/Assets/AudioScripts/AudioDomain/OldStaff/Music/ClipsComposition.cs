﻿using UnityEngine;

namespace BG.Audio.Musics.Domain
{
    internal class ClipsComposition
    {
        internal CompositionRow[] CompositionRows { get; }


        public ClipsComposition(CompositionRow[] compositionRows)
        {
            CompositionRows = compositionRows;
        }

        public ClipsComposition(AudioClip clip)
        {
            CompositionRows = new[] {new CompositionRow(new[] {0}, new[] {0.0f}, new[] {clip.length})};
        }
    }
}