﻿using UnityEngine;

namespace BG.Audio.Musics.Domain
{
    internal class CompositionRow
    {

        private int _progress = 0;

        // ReSharper disable twice MemberCanBePrivate.Global UnusedAutoPropertyAccessor.Global
        internal int[] ClipsIndices { get; }
        internal float[] ClipsPlayBegin { get; }
        internal float[] ClipsPlayEnd { get; }

        public CompositionRow(int[] clipsIndices, float[] clipsPlayBegin, float[] clipsPlayEnd)
        {
            ClipsIndices = clipsIndices;
            ClipsPlayBegin = clipsPlayBegin;
            ClipsPlayEnd = clipsPlayEnd;
        }

        internal ClipInfo Next()
        {
            _progress++;
            return new ClipInfo
                {ClipIndex = ClipsIndices[_progress], Begin = ClipsPlayBegin[_progress], End = ClipsPlayEnd[_progress]};
        }
    }
}