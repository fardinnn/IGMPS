﻿using BG.API;
using UnityEngine;

namespace BG.Audios.Domain
{
    public abstract class SetUpSFXSystemHandler<TCommand> : ICommandHandler<TCommand>
            where TCommand : SFXParameters
    {
        protected SFXSystemsManager Manager;
        public virtual void Handle(TCommand command)
        {
            Manager = UnityEngine.Object.FindObjectOfType<SFXSystemsManager>();
            if (Manager == null)
            {
                new GameObject("SFXSystemManager").AddComponent<SFXSystemsManager>();
                Manager = UnityEngine.Object.FindObjectOfType<SFXSystemsManager>();
            }
        }
    }
}
