﻿namespace BG.Audios.Domain
{
    public class SetUpSimpleSFXSystemHandler : SetUpSFXSystemHandler<SimpleSFXParameters>
    {
        public override void Handle(SimpleSFXParameters command)
        {
            base.Handle(command);

            var system = new SimpleSFXSystem(command);
            system.Id = command.SystemId;
            Manager.Add(system);
        }
    }
}
