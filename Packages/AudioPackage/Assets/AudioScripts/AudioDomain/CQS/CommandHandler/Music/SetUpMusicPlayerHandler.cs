using BG.API;
using UnityEngine;

namespace BG.Audios.Domain
{
    public abstract class SetUpMusicPlayerHandler<TCommandParameter> : ICommandHandler<TCommandParameter> where TCommandParameter : SetUpMusicPlayerParameters
    {
        protected MusicPlayersManager Manager;
        public virtual void Handle(TCommandParameter command)
        {
            Manager = Object.FindObjectOfType<MusicPlayersManager>();
            if (Manager == null)
            {
                new GameObject("MusicPlayersManager").AddComponent<MusicPlayersManager>();
                Manager = Object.FindObjectOfType<MusicPlayersManager>();
            }
        }
    }
}
