﻿namespace BG.Audios.Domain
{
    public class SetUpSimpleMusicPlayerHandler : SetUpMusicPlayerHandler<SimpleMusicPlayerParameters>
    {
        public override void Handle(SimpleMusicPlayerParameters command)
        {
            base.Handle(command);

            var musicPlayer = new SimpleMusicPlayer(command);
            musicPlayer.Id = command.SystemId;
            Manager.Add(musicPlayer);
        }
    }
}
