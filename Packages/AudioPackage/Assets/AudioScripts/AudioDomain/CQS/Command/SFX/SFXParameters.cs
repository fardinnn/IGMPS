﻿using BG.API;
using System;

namespace BG.Audios.Domain
{
    public abstract class SFXParameters : ICommand
    {
        public Guid Id { get; set; }
        public Guid SystemId { get; set; }
    }
}
