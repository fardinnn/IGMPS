﻿using BG.API;
using System;
using UnityEngine;

namespace BG.Audios.Domain
{
    public abstract class SetUpMusicPlayerParameters : ICommand
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public Guid SystemId { get; set; } = Guid.NewGuid();
    }
}
