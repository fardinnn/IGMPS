
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BG.GameMechanism
{
    [CreateAssetMenu(fileName ="Game Template",menuName = "Templates/GameTemplates", order = 1)]
    public class TemplateAccess : ScriptableObject
    {
        [SerializeField]
        private PrefabTemplate[] _templates;

        public PrefabTemplate[] Templates { get => _templates; set => _templates = value; }
    }

    [Serializable]
    public class PrefabTemplate
    {
        [SerializeField]
        private GameObject _template;
        [SerializeField]
        private Guid _id;

        public GameObject Template { get => _template; set => _template = value; }
        public Guid Id { get => _id; set => _id = value; }
    }
}
