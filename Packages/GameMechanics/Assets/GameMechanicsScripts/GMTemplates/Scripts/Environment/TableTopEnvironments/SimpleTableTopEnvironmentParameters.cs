﻿using System.Collections.Generic;
using UnityEngine;

namespace BG.GameMechanism.Templates
{
    public class SimpleTableTopEnvironmentParameters : EnvironmentParameters
    {
        public Transform EnvironmentAesthetics { get; set; }
        public List<Transform> Chairs { get; set; }
        public Transform Table { get; set; }
        public float TableHeight { get; set; }
        public Transform TableTop { get; set; }
    }
}