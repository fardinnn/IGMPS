using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BG.GameMechanism.Templates
{
    public class SimpleTableTopEnvironmentTemplate : EnvironmentTempalte<SimpleTableTopEnvironmentParameters>
    {
        [SerializeField]
        private Transform _aesthetics;

        [SerializeField]
        private Transform _table;

        [SerializeField]
        private Transform _onTable;

        [SerializeField]
        private Transform _chairs;

        public override void SetUp(SimpleTableTopEnvironmentParameters parameters)
        {
            SetLocalHeight(_onTable, parameters.TableHeight);
            ResetInParent(parameters.Table, _table);
            ResetInParent(parameters.EnvironmentAesthetics, _aesthetics);
            parameters.Chairs.ForEach(c => ResetInParent(c, _chairs));
            ResetInParent(parameters.TableTop, _onTable);
        }

        private void ResetInParent(Transform target, Transform parent)
        {
            target.SetParent(parent);
            target.localScale = Vector3.one;
            target.localPosition = Vector3.zero;
            target.localRotation = Quaternion.identity;
        }

        private void SetLocalHeight(Transform target, float height)
        {
            var localposition = target.localPosition;
            localposition.y = height;
            target.localPosition = localposition;
        }
    }
}