using UnityEngine;

namespace BG.GameMechanism.Templates
{
    public class SimpleSquareCell : CellTempalte<SimpleCellParameters>
    {
        [SerializeField]
        private Transform _cellObject;
        public override void SetUp(SimpleCellParameters parameters)
        {
            _cellObject.GetComponent<MeshFilter>().mesh = parameters.CellMesh;
            _cellObject.GetComponent<Renderer>().material.mainTexture = parameters.CellTexture;
            parameters.Aesthetics.SetParent(_cellObject);
        }
    }
}