﻿using UnityEngine;

namespace BG.GameMechanism.Templates
{
    public class SimpleCellParameters : CellParameters
    {
        public Mesh CellMesh { get; set; }
        public Texture2D CellTexture { get; set; }
        public Transform Aesthetics { get; set; }
    }
}