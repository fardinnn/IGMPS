
using System;
using UnityEngine;

namespace BG.GameMechanism.Templates
{
    public class SimplePieceTemplate : PieceTempalte<SimplePieceParameters>
    {
        [SerializeField]
        private MeshFilter _model3DMesh;
        [SerializeField]
        private MeshRenderer _model3DRenderer;
        [SerializeField]
        private MeshRenderer _model2DRenderer;
        public override void SetUp(SimplePieceParameters parameters)
        {
            _model3DMesh.mesh = parameters.Geometry;
            _model3DRenderer.material.mainTexture = parameters.Texture;
            _model2DRenderer.material.mainTexture = parameters.Model2D;

            _model2DRenderer.gameObject.SetActive(false);
        }

        private void On2DActivated(object o, EventArgs e)
        {
            _model3DMesh.gameObject.SetActive(false);
            _model2DRenderer.gameObject.SetActive(true);
        }

        private void On3DActivated(object o, EventArgs e)
        {
            _model3DMesh.gameObject.SetActive(true);
            _model2DRenderer.gameObject.SetActive(false);
        }
    }
}
