﻿using UnityEngine;

namespace BG.GameMechanism.Templates
{
    public class SimplePieceParameters : PieceParameters
    {
        public Mesh Geometry { get; set; }
        public Texture2D Texture { get; set; }
        public Texture2D Model2D { get; set; }
    }
}
