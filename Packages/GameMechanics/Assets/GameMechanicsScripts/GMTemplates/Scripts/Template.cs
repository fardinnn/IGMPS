using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BG.GameMechanism.Templates
{
    public abstract class Template<TParam> : MonoBehaviour where TParam: Parameters
    {
        public abstract void SetUp(TParam parameters);
    }
}
