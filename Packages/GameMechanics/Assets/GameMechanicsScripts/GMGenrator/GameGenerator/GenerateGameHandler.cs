﻿using BG.API;

namespace BG.GameMechanism.Generator
{
    public abstract class GenerateGameHandler<TCommand> : ICommandHandler<TCommand> where TCommand : GenerateGame
    {
        public abstract void Handle(TCommand command);
    }
}
