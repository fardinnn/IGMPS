using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

namespace BG.GameMechanism.Generator
{
    public class GenerateBoardGameHandler : GenerateGameHandler<GenerateBoardGame>
    {
        public override void Handle(GenerateBoardGame command)
        {
            var gameTransform = new GameObject(command.GameGeneratorInstruction.Title).AddComponent<BoardGameView>().transform;

            // Generate Pieces
            var piecesParent = new GenerateBoardGamePieces(command.GameGeneratorInstruction.PlyerPiecesInstruction).Generate();
            piecesParent.SetParent(gameTransform);

            // Generate Players
            var playersParent = new GenerateBoardGamePlayers(command.GameGeneratorInstruction.PlayersGeneratorInstruction).Generate();
            playersParent.SetParent(gameTransform);

            // Generate Board
            var boardParent = new GenerateBoardGameBoards(command.GameGeneratorInstruction.BoardsGeneratorInstruction).Generate();
            boardParent.SetParent(gameTransform);

            //Cameras
            var cameraParent = new GenerateBoardGameCameras(command.GameGeneratorInstruction.CamerasGeneratorInstruction).Generate();
            cameraParent.SetParent(gameTransform);

            // Generate Environment


            // Generate HUD
        }
    }
}
