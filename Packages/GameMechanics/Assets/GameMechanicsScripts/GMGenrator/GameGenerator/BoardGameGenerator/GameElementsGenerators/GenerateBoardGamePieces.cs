﻿using System;
using UnityEngine;

namespace BG.GameMechanism.Generator
{
    internal class GenerateBoardGamePieces : GenerateBoardGameElements
    {
        private readonly PlayerPiecesInstruction[] _piecesInstruction;
        public GenerateBoardGamePieces(PlayerPiecesInstruction[] piecesInstruction)
        {
            _piecesInstruction = piecesInstruction ?? throw new NullReferenceException("piecesInstruction can't be null");
        }

        public override Transform Generate()
        {
            var pieces = new GameObject("Pieces").transform; // should add pieces component
            for (int i = 0; i < _piecesInstruction.Length; i++)
            {
                var playerPieces = new GameObject(_piecesInstruction[i].Title).transform;
                for (int j = 0; j < _piecesInstruction[i].SinglePieceInstructions.Length; j++)
                {
                    for (int k = 0; k < _piecesInstruction[i].SinglePieceInstructions[j].Count; k++)
                    {
                        var singlePieceGenrator = new GenerateSinglePiece(_piecesInstruction[i].SinglePieceInstructions[j]);
                        var singlePiece = singlePieceGenrator.Generate();
                        singlePiece.SetParent(playerPieces);
                    }
                }
                playerPieces.SetParent(pieces);
            }
            
            return pieces;
        }
    }
}
