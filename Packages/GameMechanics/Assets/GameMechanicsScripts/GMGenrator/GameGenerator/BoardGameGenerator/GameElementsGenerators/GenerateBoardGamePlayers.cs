﻿using System;
using UnityEngine;

namespace BG.GameMechanism.Generator
{
    internal class GenerateBoardGamePlayers : GenerateBoardGameElements
    {
        private readonly PlayerGeneratorInstruction[] _playersInstruction;
        public GenerateBoardGamePlayers(PlayerGeneratorInstruction[] piecesInstruction)
        {
            _playersInstruction = piecesInstruction ?? throw new NullReferenceException("piecesInstruction can't be null");
        }

        public override Transform Generate()
        {
            var players = new GameObject("Players").transform; // should add players component
            for (int i = 0; i < _playersInstruction.Length; i++)
            {
                var player = new GameObject(_playersInstruction[i].Title).transform;
                //Set up player

                player.SetParent(players);
            }
            return players;
        }
    }
}
