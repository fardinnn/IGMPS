﻿using System;
using UnityEngine;

namespace BG.GameMechanism.Generator
{
    internal class GenerateBoardGameBoards : GenerateBoardGameElements
    {
        private readonly BoardGeneratorInstruction[] _boardsInstruction;
        public GenerateBoardGameBoards(BoardGeneratorInstruction[] boardsInstruction)
        {
            _boardsInstruction = boardsInstruction ?? throw new NullReferenceException("boardsInstruction can't be null");
        }

        public override Transform Generate()
        {
            var boards = new GameObject("Boards").transform; // should add pieces component
            for (int i = 0; i < _boardsInstruction.Length; i++)
            {
                var board = new GameObject(_boardsInstruction[i].Title).transform;
                //SetUpBoard

                board.SetParent(boards);
            }
            return boards;
        }
    }
}
