﻿using System;
using UnityEngine;

namespace BG.GameMechanism.Generator
{
    internal class GenerateSinglePiece
    {
        private readonly PieceGeneratorInstruction _pieceInstruction;
        public GenerateSinglePiece(PieceGeneratorInstruction pieceInstruction)
        {
            _pieceInstruction = pieceInstruction ?? throw new NullReferenceException("pieceInstruction can't be null");
        }

        public Transform Generate()
        {
            //_pieceInstruction.Model3DPrefab.PrefabId
            var piece = new GameObject(_pieceInstruction.Title).transform;

            return piece;
        }
    }
}
