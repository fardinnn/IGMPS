﻿using UnityEngine;

namespace BG.GameMechanism.Generator
{
    public abstract class GenerateGameElements
    {
        public abstract Transform Generate();
    }

}
