﻿using BG.API;
using System;
using UnityEngine;

namespace BG.GameMechanism.Generator
{
    public abstract class GenerateGame : ICommand
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public Guid GameId { get; set; } = Guid.NewGuid();
        public Transform Parent { get; set; }
    }
}
