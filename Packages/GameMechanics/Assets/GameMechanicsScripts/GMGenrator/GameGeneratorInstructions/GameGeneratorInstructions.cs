﻿using System;
using UnityEngine;

namespace BG.GameMechanism.Generator
{
    public abstract class GameGeneratorInstructions : ScriptableObject
    {
        public Guid Id { get; set; }
        public Guid GameTemplateId { get; set; }
        public string Title { get; set; }
    }
}
