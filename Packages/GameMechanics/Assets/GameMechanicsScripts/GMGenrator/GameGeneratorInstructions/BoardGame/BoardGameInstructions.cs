﻿using UnityEngine;

namespace BG.GameMechanism.Generator
{
    [CreateAssetMenu(fileName = "BoardGame", menuName = "Templates/BoardGameInstruction", order = 1)]
    public class BoardGameInstructions : GameGeneratorInstructions
    {
        // pieces
        public PlayerPiecesInstruction[] PlyerPiecesInstruction { get; set; }

        // players
        public PlayerGeneratorInstruction[] PlayersGeneratorInstruction { get; set; }

        // board
        public BoardGeneratorInstruction[] BoardsGeneratorInstruction { get; set; }

        // Environment
        public EnvironmentGeneratorInstruction EnvironmentGeneratorInstruction { get; set; }

        // Camera
        public BoardGameCameraInstruction CamerasGeneratorInstruction { get; set; }

        // GameAlgorithm
        public BoardGameAlgorith GameAlgorith { get; set; }
    }
}
