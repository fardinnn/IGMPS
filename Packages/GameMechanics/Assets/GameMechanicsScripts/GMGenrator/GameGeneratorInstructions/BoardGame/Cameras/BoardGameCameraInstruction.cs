﻿using System;
using UnityEngine;

namespace BG.GameMechanism.Generator
{
    public class BoardGameCameraInstruction : ICameraGeneratorInstruction
    {
        public Guid TemplateId { get; set; }
    }
}