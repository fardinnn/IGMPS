﻿using System;


namespace BG.GameMechanism.Generator
{
    public class EnvironmentGeneratorInstruction
    {
        public Guid TemplateId { get; set; }
        public string Title { get; set; }
    }
}
