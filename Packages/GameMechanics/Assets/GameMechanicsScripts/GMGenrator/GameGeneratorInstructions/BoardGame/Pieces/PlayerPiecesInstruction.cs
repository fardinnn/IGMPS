﻿using System;
using UnityEngine;

namespace BG.GameMechanism.Generator
{
    public class PlayerPiecesInstruction
    {
        public string Title { get; set; }
        public Guid PlayerId { get; set; }
        public Color SharedColor { get; set; }
        public PieceGeneratorInstruction[] SinglePieceInstructions { get; set; }
    }
}
