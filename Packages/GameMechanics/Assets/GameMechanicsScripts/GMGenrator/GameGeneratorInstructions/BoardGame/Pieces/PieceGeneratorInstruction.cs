﻿using System;

namespace BG.GameMechanism.Generator
{
    [Serializable]
    public class PieceGeneratorInstruction
    {
        public string Title { get; set; }
        public int Count { get; set; }
        public Guid[] TemplateId { get; set; }
        public Prefab3DGeneratorInstruction Model3DPrefab { get; set; }
        public Graphics2DGeneratorInstruction Model2DInstructions { get; set; }
    }
}
