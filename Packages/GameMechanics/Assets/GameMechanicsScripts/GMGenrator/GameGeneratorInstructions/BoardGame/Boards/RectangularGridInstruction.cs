﻿namespace BG.GameMechanism.Generator
{
    public class RectangularGridInstruction : IGridGeneratorInstruction
    {
        public int Row { get; set; }
        public int Column { get; set; }
    }
}
