﻿namespace BG.GameMechanism.Generator
{
    public class CellGeneratorInstruction
    {
        public GridCellPositionRange[] UsedRanges { get; set; }
        public Prefab3DGeneratorInstruction CellPrefab { get; set; }
    }
}
