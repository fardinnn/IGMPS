﻿namespace BG.GameMechanism.Generator
{
    public struct GridCellPositionRange
    {
        public int RowBegin { get; set; }
        public int RowEnd { get; set; }
        public int ColumnBegin { get; set; }
        public int ColumnEnd { get; set; }
    }
}
