﻿using System;


namespace BG.GameMechanism.Generator
{
    public class BoardGeneratorInstruction
    {
        public Guid TemplateId { get; set; }
        public string Title { get; set; }
        public IGridGeneratorInstruction GridInstruction { get; set; }
    }
}
