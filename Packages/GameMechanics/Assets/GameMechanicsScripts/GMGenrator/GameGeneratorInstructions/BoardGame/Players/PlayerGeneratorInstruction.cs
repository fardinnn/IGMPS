﻿using System;


namespace BG.GameMechanism.Generator
{
    public class PlayerGeneratorInstruction
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
    }
}
