﻿using System;

namespace BG.GameMechanism.Generator
{
    public class Graphics2DGeneratorInstruction
    {
        public Guid[] GraphicIds { get; set; }
    }
}
