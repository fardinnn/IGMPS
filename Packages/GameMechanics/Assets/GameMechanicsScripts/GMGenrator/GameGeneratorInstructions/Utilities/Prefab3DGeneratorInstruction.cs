﻿using System;
using UnityEngine;

namespace BG.GameMechanism.Generator
{
    [Serializable]
    public class Prefab3DGeneratorInstruction
    {
        public Guid PrefabId { get; set; }
        public Guid[] GeometryIds { get; set; }
        public Graphics2DGeneratorInstruction TextureInstructions { get; set; }
        public Color[] Colors{ get; set; }
    }
}
