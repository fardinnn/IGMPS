﻿using BG.API;
using UnityEngine;

namespace BG.GameMechanism
{
    public abstract class SetUpGameMechanismHandler<TCommand> : ICommandHandler<TCommand> where TCommand: SetUpGameMechanism
    {
        public abstract void Handle(TCommand command);
    }
}