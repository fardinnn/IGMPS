﻿using BG.API;
using System;

namespace BG.Games.Chess
{
    public class SelectPieceCommand : IActionCommand
    {
        public Guid Id { get; set; }

        public Guid PieceId { get; set; }
    }
}
