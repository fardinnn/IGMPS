﻿using System;
using BG.API;
using UnityEngine;

// ReSharper disable once CheckNamespace IdentifierTypo
namespace BG.GameMechanism
{
    public abstract class SetUpGameMechanism : ICommand
    {
        public Guid Id { get; set; } = Guid.NewGuid();

        public Guid GameId { get; set; } = Guid.NewGuid();
        public string Title { get; set; } = "Game";
        public Transform Parent { get; set; }

    }
}
