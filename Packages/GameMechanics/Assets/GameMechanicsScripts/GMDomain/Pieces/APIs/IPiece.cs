using BG.API.Abilities;
using System.Collections.Generic;

namespace BG.Games.Chess
{
    public interface IPiece
    {
        List<IAbility> Abilities { get; set; }
    }
}
