﻿namespace BG.Games.Chess
{
    public interface IDrawable
    {
        void Draw();
        void UnDraw();
    }
}