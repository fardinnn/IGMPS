﻿using BG.API.Abilities;
using System;

namespace BG.Games.Chess
{
    internal class HoverablePiece : IHoverable
    {
        public event EventHandler<HoverEventArgs> Hovered;
        public event EventHandler<HoverEventArgs> Entered;
        public event EventHandler<HoverEventArgs> Exited;

        public void Enter(object sender, HoverEventArgs e)
        {
            throw new NotImplementedException();
        }

        public void Exit(object sender, HoverEventArgs e)
        {
            throw new NotImplementedException();
        }

        public void Hover(object sender, HoverEventArgs e)
        {
            ///pointer enter
            // Check if it belongs to the player
            // If it was highlight the piece

            ///pointer exit
            // Check if it highlited
            // If it was remove the highlight
        }
    }
}
