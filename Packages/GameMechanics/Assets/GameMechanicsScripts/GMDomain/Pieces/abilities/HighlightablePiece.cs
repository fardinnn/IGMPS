﻿using BG.API.Abilities;
using System;

namespace BG.Games.Chess
{
    internal class HighlightablePiece : IHighlightable
    {
        public event EventHandler<HighlightEventArgs> Highlighted;

        public void Highlight(object sender, HighlightEventArgs e)
        {
        }
    }
}
