﻿using BG.API.Abilities;
using System;

namespace BG.Games.Chess
{
    internal class SelectablePiece : ISelectable
    {
        public event EventHandler<SelectEventArgs> Selected;

        public void Select(object sender, SelectEventArgs e)
        {
            // Check if it is highlighted
            // If it was highlighted select the piece by changing highlights and its cell and calculating its potential moves
            // Give type to potential move calculator to get data
            // Give data to draw potential moves
        }
    }
}
