﻿using BG.API.Abilities;
using System.Collections.Generic;
using UnityEngine;

namespace BG.Games.Chess
{
    internal abstract class PieceTemplate : MonoBehaviour, IPiece
    {
        public List<IAbility> Abilities { get; set; }

        private void Awake()
        {
            Abilities = new List<IAbility>();
            //Abilities.Add(new DraggablePiece());
            Abilities.Add(new HoverablePiece());
            Abilities.Add(new HighlightablePiece());
            Abilities.Add(new SelectablePiece());
        }
    }
}
