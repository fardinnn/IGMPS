﻿
using System;
using BG.API;
using BG.API.Abilities;

// ReSharper disable once CheckNamespace IdentifierTypo
namespace BG.GameMechanism
{

    public abstract class GameAbility : IAbility
    {
        //???
    }

    public abstract class GameMechanism<TView, TModel> : IController<TView, TModel>
        where TModel : GameMechanismModel
        where TView : IView
    {
        public TView View { get;}
        public TModel Model { get; }

        public Guid Id => throw new NotImplementedException();

        //public InstanceContainer<IAbility> Abilities { get; set; }

        protected GameMechanism(TView view, TModel model)
        {
            Model = model;
            View = view;
        }
    }

    public class DefaultGameMechanism : GameMechanism<IView, DefaultGameMechanismModel>
    {
        public DefaultGameMechanism(IView view, DefaultGameMechanismModel model) : base(view, model)
        {

        }
    }

    public class DefaultGameMechanismModel : GameMechanismModel
    {

    }
}
