﻿using System;
using UnityEngine;

namespace BG.GameMechanism
{
    public class BoardGameView : MonoBehaviour, IGameView
    {
        public Guid Id { get; set; }

        private BoardGameController chessController;

        void Awake()
        {
            chessController = new BoardGameController(this, new BoardGameModel());
        }

        public void Dispose()
        {
            Destroy(gameObject);
        }
    }
}