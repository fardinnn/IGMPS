﻿using BG.API;
using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace BG.GameMechanism
{
    public class PieceView : MonoBehaviour, IView, IPointerClickHandler, IBeginDragHandler, IDragHandler, IPointerEnterHandler, IPointerExitHandler
    {
        public Guid Id { get; set; }

        private PieceController piecePlayerControl;

        private void Awake()
        {
            piecePlayerControl = new PieceController(this, new PieceModel());
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            piecePlayerControl.SelectPiece();
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            //throw new NotImplementedException();
        }

        public void OnDrag(PointerEventData eventData)
        {
            //throw new NotImplementedException();
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            //throw new NotImplementedException();
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            //throw new NotImplementedException();
        }
        public void Dispose()
        {
            Destroy(gameObject);
        }
    }

}
