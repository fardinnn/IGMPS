using BG.API;
using BG.Games.Chess;
using System;

namespace BG.GameMechanism
{
    public class PieceController : IController<PieceView, PieceModel>
    {
        public PieceView View { get; }

        public PieceModel Model { get; }

        public Guid Id => throw new NotImplementedException();

        internal PieceController(PieceView view, PieceModel model)
        {
            View = view;
            Model = model;
        }

        internal void SelectPiece()
        {
            new SelectPieceCommandHandler().Handle(new SelectPieceCommand { PieceId = View.Id });
        }

        internal void DragPiece()
        {

        }
    }
}