﻿using BG.API;
using System;

namespace BG.GameMechanism
{
    internal class BoardGameController : IController<BoardGameView, BoardGameModel>
    {
        public BoardGameView View { get; }

        public BoardGameModel Model { get; }

        public Guid Id => throw new NotImplementedException();

        public BoardGameController(BoardGameView view, BoardGameModel model)
        {
            View = view;
            Model = model;
        }
    }
}