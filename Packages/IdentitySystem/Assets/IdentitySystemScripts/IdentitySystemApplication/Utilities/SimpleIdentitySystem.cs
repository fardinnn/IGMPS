using BG.API;
using BG.API.Abilities;
using System;
using System.Collections.Generic;
using Unity.Collections;

namespace BG.IdentitySystem
{
    //public abstract class IdentitySystem : SystemBase, ISizable, IGameEntitiesVisitor
    //{
    //    public List<Type> EntityComponentTypes { get; private set; }

    //    public virtual event EventHandler<SizeEventArgs> SizeSetted;

    //    public virtual void Sizing(object sender, SizeEventArgs e)
    //    {
    //        SizeSetted?.Invoke(this, e);
    //    }


    //    public abstract void Visit(Type groupTag);

    //    //public abstract void Visit(Entity entity);
    //}

    //public abstract class IdentitySystem<T> : IdentitySystem where T : IdentitySystem<T>
    //{ }

    //[DisableAutoCreation]
    //public class SimpleIdentitySystem : IdentitySystem<SimpleIdentitySystem>, IIdentifiable
    //{
    //    public Guid Id { get; set; } = Guid.NewGuid();
    //    public string Name { get; set; } = "Name";

    //    //private List<Entity> targetEntities = new List<Entity>();

    //    //public override void Visit(Entity entity)
    //    //{
    //    //    entity.Id = Id;
    //    //    entity.name = Name;
    //    //    targetEntities.Add(entity);
    //    //}

    //    public event EventHandler<IdentityChangeEventArgs> IdentityChanged;
    //    public void ChangeIdentity(object sender, IdentityChangeEventArgs e)
    //    {
    //        if (e.Name != null) Name = e.Name;
    //        if (e.Id != default) Id = e.Id;

    //        //targetEntities.ForEach(entity =>
    //        //{
    //        //    entity.Id = Id;
    //        //    entity.name = Name;
    //        //});
    //        IdentityChanged?.Invoke(this, e);
    //    }

    //    public override void Visit(Type groupTag)
    //    {

    //    }

    //    protected override void OnUpdate()
    //    {
    //        //Entities.ForEach(() =>
    //        //{

    //        //});
    //    }
    //}
}