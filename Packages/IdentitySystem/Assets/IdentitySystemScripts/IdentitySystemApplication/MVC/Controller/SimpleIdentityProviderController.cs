﻿using BG.API;
using BG.States;
using System;
using System.Collections.Generic;
using System.Linq;
using Unity.Collections;

namespace BG.IdentitySystem
{
    public class SimpleIdentityProviderController :
        IdentityProviderController<SimpleIdentityProviderView, SimpleIdentityProviderModel>
    {
        public SimpleIdentityProviderController(SimpleIdentityProviderView view = null,
            SimpleIdentityProviderModel model = null, CompositeStatesSet requiredStates = null) : base(view, model, requiredStates)
        {

        }

        public override IComponentSystem ProvideComponentSystem(Options options)
        {
            throw new NotImplementedException();
        }

        //public Dictionary<ComponentType, List<IdentitySystem>> IdentitySystem { get; set; }
        //    = new Dictionary<ComponentType, List<IdentitySystem>>();
        //public IdentitySystem ProvideIdentitySystem<T>(ComponentType groupTag, Options options = null)
        //    where T : IdentitySystem<T>
        //{
        //    var system = World.DefaultGameObjectInjectionWorld.GetOrCreateSystem(typeof(T)) as T;
        //    options?.AlterOn(system, groupTag);
        //    return system;
        //}

        //public void Change<T>(ComponentType groupTag, Options changes)
        //{
        //    var components = IdentitySystem[groupTag].Where(c => c.GetType() == typeof(T));

        //    foreach (var component in components)
        //        changes.AlterOn(component, groupTag);
        //}

        //public void Change(ComponentType groupTag, Options changes)
        //{
        //    foreach (var component in IdentitySystem[groupTag])
        //        changes.AlterOn(component, groupTag);
        //}
    }
}
