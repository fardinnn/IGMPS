﻿using BG.API;
using UnityEngine;

namespace BG.IdentitySystem
{
    public abstract class IdentityProviderModel : IModel
    {
        public Transform Parent { get; set; }
        public Options DefaultOptions { get; set; }
    }
}
