﻿using System;

namespace BG.PointerSystem.Domain
{
    public class SimpleDropRegionSystemCommand : PointerSystemCommand
    {
        public Guid SystemId { get; set; }
    }
}
