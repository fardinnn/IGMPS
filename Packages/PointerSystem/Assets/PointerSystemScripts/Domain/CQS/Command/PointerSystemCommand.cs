using BG.API;
using System;

namespace BG.PointerSystem.Domain
{
    public abstract class PointerSystemCommand : ICommand
    {
        public Guid Id { get; set; }
    }
}
