﻿using System;

namespace BG.PointerSystem.Domain
{
    public class SimplePointerSystemCommand : PointerSystemCommand
    {
        public Guid SystemId { get; set; }
    }
}
