﻿using System;

namespace BG.PointerSystem.Domain
{
    public class SimpleClickSystemCommand : PointerSystemCommand
    {
        public Guid SystemId { get; set; }
    }
}
