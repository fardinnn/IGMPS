﻿using System;

namespace BG.PointerSystem.Domain
{
    public class SimpleHoverSystemCommand : PointerSystemCommand
    {
        public Guid SystemId { get; set; }
    }
}
