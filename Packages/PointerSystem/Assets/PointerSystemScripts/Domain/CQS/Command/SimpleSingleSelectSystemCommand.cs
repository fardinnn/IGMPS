﻿using System;

namespace BG.PointerSystem.Domain
{
    public class SimpleSingleSelectSystemCommand : PointerSystemCommand
    {
        public Guid SystemId { get; set; }
    }
}
