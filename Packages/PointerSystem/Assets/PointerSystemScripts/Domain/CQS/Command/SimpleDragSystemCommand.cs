﻿using System;

namespace BG.PointerSystem.Domain
{
    public class SimpleDragSystemCommand : PointerSystemCommand
    {
        public Guid SystemId { get; set; }
    }
}
