﻿namespace BG.PointerSystem.Domain
{
    public class SetUpSimplePointerSystemHandler : SetUpPointerSystemHandler<SimplePointerSystemCommand>
    {
        public override void Handle(SimplePointerSystemCommand command)
        {
            base.Handle(command);

            var system = new SimplePointerSystem(command);
            system.Id = command.SystemId;
            Manager.Add(system);
        }
    }
}
