﻿namespace BG.PointerSystem.Domain
{
    public class SetUpSimpleSingleSelectSystemHandler : SetUpPointerSystemHandler<SimpleSingleSelectSystemCommand>
    {
        public override void Handle(SimpleSingleSelectSystemCommand command)
        {
            base.Handle(command);

            var system = new SimpleSingleSelectSystem(command);
            system.Id = command.SystemId;
            Manager.Add(system);
        }
    }
}
