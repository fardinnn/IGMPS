﻿namespace BG.PointerSystem.Domain
{
    public class SetUpSimpleClickSystemHandler : SetUpPointerSystemHandler<SimpleClickSystemCommand>
    {
        public override void Handle(SimpleClickSystemCommand command)
        {
            base.Handle(command);

            var system = new SimpleClickSystem(command);
            system.Id = command.SystemId;
            Manager.Add(system);
        }
    }
}
