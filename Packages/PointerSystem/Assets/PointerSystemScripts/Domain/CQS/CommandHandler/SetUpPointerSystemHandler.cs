using BG.API;
using UnityEngine;

namespace BG.PointerSystem.Domain
{
    public abstract class SetUpPointerSystemHandler<TCommand> : ICommandHandler<TCommand>
            where TCommand : PointerSystemCommand
    {
        protected PointerSystemManager Manager;
        public virtual void Handle(TCommand command)
        {
            Manager = UnityEngine.Object.FindObjectOfType<PointerSystemManager>();
            if (Manager == null)
            {
                new GameObject("PointerSystemManager").AddComponent<PointerSystemManager>();
                Manager = UnityEngine.Object.FindObjectOfType<PointerSystemManager>();
            }
        }
    }
}
