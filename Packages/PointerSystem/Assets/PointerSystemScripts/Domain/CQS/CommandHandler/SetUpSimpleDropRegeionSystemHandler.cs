﻿namespace BG.PointerSystem.Domain
{
    public class SetUpSimpleDropRegionSystemHandler : SetUpPointerSystemHandler<SimpleDropRegionSystemCommand>
    {
        public override void Handle(SimpleDropRegionSystemCommand command)
        {
            base.Handle(command);

            var system = new SimpleDropRegionSystem(command);
            system.Id = command.SystemId;
            Manager.Add(system);
        }
    }
}
