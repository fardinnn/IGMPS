﻿using BG.API;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace BG.PointerSystem.Domain
{
    public class PointerSystemManager : Singleton<PointerSystemManager>
    {
        private static Dictionary<Guid, IPointerSystem> pointerSystems;

        protected override void Awake()
        {
            IsDestroyOnLoad = true;
            base.Awake();

            pointerSystems = new Dictionary<Guid, IPointerSystem>();
        }

        public void Add(IPointerSystem pointerSystem)
        {
            if (pointerSystem.Id == default) throw new ArgumentException("pointerSystem's id in not assigned");
            pointerSystems.Add(pointerSystem.Id, pointerSystem);
        }

        public static T Get<T>(Guid id) where T : class, IPointerSystem
        {
            if (pointerSystems == null) throw new NullReferenceException("pointerSystems can not be null!");
            if (!pointerSystems.ContainsKey(id))
                Debug.LogError("There is no pointer system with the provided id!");
            var target = pointerSystems[id] as T;
            if (target == null)
                Debug.LogError("Provided type is not compatible with target system id!");
            return target;
        }
    }
}
