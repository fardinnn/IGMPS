﻿namespace BG.PointerSystem.Domain
{
    public class SetUpSimpleDragSystemHandler : SetUpPointerSystemHandler<SimpleDragSystemCommand>
    {
        public override void Handle(SimpleDragSystemCommand command)
        {
            base.Handle(command);

            var system = new SimpleDragSystem(command);
            system.Id = command.SystemId;
            Manager.Add(system);
        }
    }
}
