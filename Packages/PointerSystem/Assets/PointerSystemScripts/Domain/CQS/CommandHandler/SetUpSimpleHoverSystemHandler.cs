﻿namespace BG.PointerSystem.Domain
{
    public class SetUpSimpleHoverSystemHandler : SetUpPointerSystemHandler<SimpleHoverSystemCommand>
    {
        public override void Handle(SimpleHoverSystemCommand command)
        {
            base.Handle(command);

            var system = new SimpleHoverSystem(command);
            system.Id = command.SystemId;
            Manager.Add(system);
        }
    }
}
