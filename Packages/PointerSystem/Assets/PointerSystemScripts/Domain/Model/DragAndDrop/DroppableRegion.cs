﻿using BG.API;
using BG.States;
using System;
using UnityEngine;

namespace BG.PointerSystem.Domain
{
    //public abstract class DroppableRegion : MonoBehaviour
    //{
    //    public Entity Entity { get; set; }
    //    private Entity droppedEntity;

    //    // public event EventHandler<DropRegionEventArgs> DragEntered;
    //    public virtual void DragEnter(/*Entity entity*/)
    //    {
    //        Entity.SetState(DroppableRegionState.DragEnter);
    //        // DragEntered?.Invoke(this, new DropRegionEventArgs { RegionEntity = Entity, DroppableEntity = entity });
    //    }

    //    // public event EventHandler<DropRegionEventArgs> DragExited;
    //    public virtual void DragExit(/*Entity entity*/)
    //    {
    //        Entity.SetState(DroppableRegionState.DragExit);
    //        // DragExited?.Invoke(this, new DropRegionEventArgs { RegionEntity = Entity, DroppableEntity = entity });
    //    }

    //    // public event EventHandler<DropRegionEventArgs> DroppedIn;
    //    public virtual void DropIn(Entity entity)
    //    {
    //        droppedEntity = entity;
    //        Entity.SetState(DroppableRegionState.DropIn);
    //        // DroppedIn?.Invoke(this, new DropRegionEventArgs { RegionEntity = Entity, DroppableEntity = droppedEntity });
    //    }

    //    // public event EventHandler<DropRegionEventArgs> TookOut;
    //    public virtual void TakenOut(Entity entity)
    //    {
    //        if (droppedEntity==entity)
    //        {
    //            Entity.SetState(DroppableRegionState.TakenOut);
    //            droppedEntity = null;
    //        }
    //        // TookOut?.Invoke(this, new DropRegionEventArgs { RegionEntity = Entity, DroppableEntity = entity });
    //    }

    //    public virtual bool IsEmpty()
    //    {
    //        return droppedEntity == null;
    //    }
    //}

    public class DropRegionEventArgs : EventArgs
    {
        public Entity RegionEntity { get; set; }
        public Entity DroppableEntity { get; set; }
    }
}
