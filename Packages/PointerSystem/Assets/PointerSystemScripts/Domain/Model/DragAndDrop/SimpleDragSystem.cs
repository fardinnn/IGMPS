﻿using BG.API;
using BG.API.Abilities;
using BG.States;
using BG.Extensions;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace BG.PointerSystem.Domain
{
    public class SimpleDragSystem : IPointerSystem<SimpleDragSystem>, IColorable
    {
        public event EventHandler<ColorChangeEventArgs> Colored;


        private Dictionary<Entity, SimpleDraggableComponent> draggableEntities;
        private Options options;

        public Guid Id { get; set; }

        public bool IsReady { get; private set; }

        public SimpleDragSystem(SimpleDragSystemCommand command)
        {
            options = new Options();
            draggableEntities = new Dictionary<Entity, SimpleDraggableComponent>();
            IsReady = true;
        }

        public void Visit(List<Entity> entities)
        {
            entities.ForEach(entity =>
            {
                entity.AddSingleStateActivateAction<DragAndDropState, DragAndDropChagedEventArgs>
                (DragAndDropState.Dragging, OnDragBeginned);
                entity.AddSingleStateActivateAction<DragAndDropState, DragAndDropChagedEventArgs>
                (DragAndDropState.Dragging, OnDragged);
                entity.AddSingleStateActivateAction<DragAndDropState, DragAndDropChagedEventArgs>
                (DragAndDropState.Drop, OnDragEnded);

                SimpleDraggableComponent draggableObject = entity.GetComponent<SimpleDraggableComponent>();
                if (draggableObject == null)
                    draggableObject = entity.gameObject.AddComponent<SimpleDraggableComponent>();
                draggableObject.Entity = entity;
                draggableEntities.AddIfNotExist(entity, draggableObject);
            });
        }


        public void OnDragBeginned(StateGroup stateGroup, DragAndDropChagedEventArgs e)
        {
            DragBeginned?.Invoke(this, new DragEventArgs { Entity = e.Entity/*, InputModule =*/ });
        }
        public void OnDragged(StateGroup stateGroup, DragAndDropChagedEventArgs e)
        {
            Dragging?.Invoke(this, new DragEventArgs { Entity = e.Entity/*, InputModule =*/ });
        }
        public void OnDragEnded(StateGroup stateGroup, DragAndDropChagedEventArgs e)
        {
            DragEnded?.Invoke(this, new DragEventArgs { Entity = e.Entity/*, InputModule =*/ });
        }


        public void Coloring(object sender, ColorChangeEventArgs e)
        {
            ColorOption option = !options.Contains(typeof(ColorOption)) ? new ColorOption() :
                options[typeof(ColorOption)] as ColorOption;
            option.Value = e.Color;
            AlteredOption(option);
        }

        private void AlteredOption<T>(T option) where T : Option
        {
            options[typeof(T)] = option;
        }


        public event EventHandler<DragEventArgs> DragBeginned;
        public event EventHandler<DragEventArgs> Dragging;
        public event EventHandler<DragEventArgs> DragEnded;

        public void Dispose()
        {
            //entities.ForEach
        }


        HashSet<Entity> dragEntities;
        Dictionary<Entity, int> entityLayers;
        LayerMask ignoreMask = LayerMask.NameToLayer("Ignore Raycast");
        int ignoreLayer = ~(1 << LayerMask.NameToLayer("Ignore Raycast"));

        private Vector2 startPointerPosition;
        private bool dragBeginned;

        public void BeginDrag(Vector2 beginScreenPosition, HashSet<Entity> targetEntities)
        {
            try
            {
                dragBeginned = false;
                startPointerPosition = beginScreenPosition;
                dragEntities = new HashSet<Entity>();
                entityLayers = new Dictionary<Entity, int>();
                foreach (var entity in targetEntities)
                {
                    if (!draggableEntities.ContainsKey(entity)) return;
                    dragEntities.Add(entity);
                    entityLayers.Add(entity, entity.gameObject.layer);
                    entity.SetLayer<Collider>(ignoreMask);
                    draggableEntities[entity].OnBeginDrag();
                }
            }
            catch (Exception ex)
            {
                Debug.LogError($"Catch Error: {ex.Message}");
            }
        }

        public void ContinueDragging(Vector2 pointerPosition)
        {
            if (!(dragBeginned || (pointerPosition - startPointerPosition).sqrMagnitude > 10.0f)) return;
            if (!dragBeginned) dragBeginned = true;
            foreach (var entity in dragEntities)
            {
                if (!draggableEntities.ContainsKey(entity)) return;
                var position = GetDragPosition(pointerPosition);
                entity.transform.position = position;
                draggableEntities[entity].OnDragging(droppableRegion);
            }
        }

        public void EndDrag(Vector2 pointerPosition)
        {
            try
            {
                if (!dragBeginned)
                {
                    dragEntities.Clear();
                    return;
                }
                foreach (var entity in dragEntities)
                {
                    if (!draggableEntities.ContainsKey(entity)) return;
                    entity.SetLayer<Collider>(entityLayers[entity]);
                    entityLayers.Remove(entity);

                    draggableEntities[entity].OnEndDrag(droppableRegion);
                    //// Check for droppable
                    //if (droppable)
                    //{
                    //    // If True
                    //    if (droppable.IsEmpty())
                    //    {
                    //        droppable.DropIn(entity);
                    //        entity.SetState(PlacementState.NotPlaced); //???
                    //    }
                    //    else
                    //    {
                    //        droppable.DragExit();
                    //    }
                    //}
                }
                dragEntities.Clear();
            }
            catch (Exception ex)
            {
                Debug.LogError($"Catch Error: {ex.Message} \n" +
                    $"dragEntities: {dragEntities}\n" +
                    $"draggableEntities: {draggableEntities}\n" +
                    $"entityLayers: {entityLayers}\n" +
                    $"droppableRegion: {droppableRegion}\n");

            }
        }

        GameObject lastHitObject;
        IDroppableRegion droppableRegion;
        private Vector3 GetDragPosition(Vector2 pointerPosition)
        {
            var mainCamera = Camera.main;
            Ray ray = mainCamera.ScreenPointToRay(pointerPosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, Mathf.Infinity, ignoreLayer))
            {
                if(lastHitObject != hit.collider.gameObject)
                {
                    droppableRegion?.DragExit();
                    droppableRegion = hit.collider.GetComponentInParent<IDroppableRegion>();
                    droppableRegion?.DragEnter();
                    lastHitObject = hit.collider.gameObject;
                }

                return hit.point;
            }
            else
            {
                // We want to find a point on the floor plane
                // The floor plane that cross P0=(0, 0, 0) and N=(0, 1, 0) => y = 0
                // Line equation is P = origin+rayDir*t
                // origin.y + rayDir.y*t = y = 0
                float t = -ray.origin.y / ray.direction.y;
                // X = CPos.x + rayDir.x*t
                float x = ray.origin.x + ray.direction.x * t;
                // Z = CPos.z + rayDir.z*t
                float z = ray.origin.z + ray.direction.z * t;

                if (droppableRegion!=null)
                {
                    droppableRegion?.DragExit();
                    lastHitObject = null;
                    droppableRegion = null;
                }
                return new Vector3(x, 0, z);
            }
        }
    }
}
