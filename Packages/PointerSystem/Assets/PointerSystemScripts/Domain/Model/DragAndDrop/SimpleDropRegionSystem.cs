﻿using BG.API;
using BG.API.Abilities;
using BG.States;
using BG.Extensions;
using System;
using System.Collections.Generic;

namespace BG.PointerSystem.Domain
{
    public class SimpleDropRegionSystem : IPointerSystem<SimpleDropRegionSystem>
    {
        public event EventHandler<ColorChangeEventArgs> Colored;

        private Dictionary<Entity, SimpleDroppableRegion> droppableRegions;
        private HashSet<Entity> regionEntities;
        private Options options;

        //private void CheckForDroppable()
        //{
        //    //RaycastHit hit;
        //    //if (Physics.Raycast(transform.position, Vector3.down, out hit, 25.0f, raycastMask))
        //    //{
        //    //    var newDroppable = hit.transform.GetComponent<SimpleDroppableRegion>();
        //    //    if (newDroppable != droppable)
        //    //    {
        //    //        droppable?.Entity.SetState(DroppableRegionState.DragExit);
        //    //        droppable = newDroppable;
        //    //        droppable?.Entity.SetState(DroppableRegionState.DragEnter);
        //    //        Debug.Log("Hit the new floor");
        //    //    }
        //    //}
        //}

        public Guid Id { get; set; }

        public bool IsReady { get; private set; }

        private StateGroup droppedInState = new StateGroup(DroppableRegionState.DropIn);


        public SimpleDropRegionSystem(SimpleDropRegionSystemCommand command)
        {
            options = new Options();
            regionEntities = new HashSet<Entity>();
            droppableRegions = new Dictionary<Entity, SimpleDroppableRegion>();
            IsReady = true;
        }

        public void Visit(List<Entity> entities)
        {
            entities.ForEach(entity =>
            {
                regionEntities.Add(entity);
                var droppableRegion = entity.GetComponent<SimpleDroppableRegion>();
                if (droppableRegion == null)
                    droppableRegion = entity.gameObject.AddComponent<SimpleDroppableRegion>();
                droppableRegion.Entity = entity;
                droppableRegions.Add(entity, droppableRegion);

                //droppableRegion.DragEntered += OnDragEntered;
                //droppableRegion.DragExited += OnDragExited;
                //droppableRegion.DroppedIn += OnDroppedIn;
                //droppableRegion.TookOut += OnTookOut;

                entity.AddStateActivateAction(droppedInState, DropIn);
            });
        }

        private void OnTookOut(object sender, DropRegionEventArgs e)
        {
            throw new NotImplementedException();
        }

        private void OnDroppedIn(object sender, DropRegionEventArgs e)
        {
        }

        private void OnDragExited(object sender, DropRegionEventArgs e)
        {

        }

        private void OnDragEntered(object sender, DropRegionEventArgs e)
        {

        }

        //public event EventHandler<DropEventArgs> DroppedIn;
        private void DropIn(StateGroup stateGroup, IStateChangedEventArgs e)
        {
            // Debug.Log($"Dropped {droppableRegions[e.Entity]} In {e.Entity}");
            // DroppedIn?.Invoke(this, new DropEventArgs { DroppedEntity = droppableRegions[e.Entity].droppedEntity, DropRegionEntity = e.Entity });
        }


        private void AlteredOption<T>(T option) where T : Option
        {
            options[typeof(T)] = option;
        }

        public void Dispose()
        {
            regionEntities.ForEach(entity =>
            {
                entity.Dispose(droppedInState, DropIn);
            });
        }
    }
}
