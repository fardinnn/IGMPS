﻿using BG.API;
using UnityEngine;
using BG.API.Abilities;
using BG.States;

namespace BG.PointerSystem.Domain
{
    public class SimpleDroppableRegion : MonoBehaviour, IDroppableRegion
    {
        public Entity Entity { get; set; }
        public IDroppable DroppedEntity { get; set; }

        // Lightweight pattern event args
        private DroppableRegionChagedEventArgs droppableRegionChaged = new DroppableRegionChagedEventArgs();
        private HoverStateChagedEventArgs hoverChagedEventArgs = new HoverStateChagedEventArgs();
        private SelectStateChagedEventArgs selectChagedEventArgs = new SelectStateChagedEventArgs();


        // public event EventHandler<DropRegionEventArgs> DragEntered;
        public virtual void DragEnter(/*Entity entity*/)
        {
            Entity.SetState(DroppableRegionState.DragEnter, droppableRegionChaged);
            Entity.SetState(HoverState.Entered, hoverChagedEventArgs);
            // DragEntered?.Invoke(this, new DropRegionEventArgs { RegionEntity = Entity, DroppableEntity = entity });
        }

        // public event EventHandler<DropRegionEventArgs> DragExited;
        public virtual void DragExit(/*Entity entity*/)
        {
            Entity.SetState(DroppableRegionState.DragExit, droppableRegionChaged);
            Entity.SetState(HoverState.Exited, hoverChagedEventArgs);
            // DragExited?.Invoke(this, new DropRegionEventArgs { RegionEntity = Entity, DroppableEntity = entity });
        }

        public virtual void DragFailed()
        {
            Entity.SetState(DroppableRegionState.DragExit, droppableRegionChaged);
        }

        // public event EventHandler<DropRegionEventArgs> DroppedIn;
        public virtual void DropIn(IDroppable droppedEntity, bool select = true)
        {
            DroppedEntity = droppedEntity;
            Entity.SetState(DroppableRegionState.DropIn, droppableRegionChaged);
            if(select) Entity.SetState(SelectState.Selected, selectChagedEventArgs);

            if (Entity is ContainerEntity cEntity)
                cEntity.Add(droppedEntity.Entity);

            // DroppedIn?.Invoke(this, new DropRegionEventArgs { RegionEntity = Entity, DroppableEntity = droppedEntity });
        }

        // public event EventHandler<DropRegionEventArgs> TookOut;
        public virtual void TakenOut(Entity entity)
        {
            if (DroppedEntity.Entity == entity)
            {
                if (Entity is ContainerEntity cEntity)
                    cEntity.Remove(entity);
                Entity.SetState(DroppableRegionState.TakenOut, droppableRegionChaged);
                Entity.SetState(HoverState.Exited, hoverChagedEventArgs);
                DroppedEntity = null;

            }
            // TookOut?.Invoke(this, new DropRegionEventArgs { RegionEntity = Entity, DroppableEntity = entity });
        }

        public virtual bool IsEmpty()
        {
            return DroppedEntity == null;
        }
    }
}
