﻿using BG.API;
using BG.API.Abilities;
using BG.States;
using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace BG.PointerSystem.Domain
{
    public class SimpleDraggableComponent : MonoBehaviour, IDraggable
    {
        public Entity Entity { get; set; }
        public IDroppableRegion DroppableRegion { get; set; }

        // Lightweight pattern event args
        private DragAndDropChagedEventArgs dragChagedEventArgs = new DragAndDropChagedEventArgs();
        private SelectStateChagedEventArgs selectChagedEventArgs = new SelectStateChagedEventArgs();
        private HoverStateChagedEventArgs hoverStateChaged = new HoverStateChagedEventArgs();

        public void OnBeginDrag()
        {
            dragChagedEventArgs.TargetRegion = null;
            Entity.SetState(DragAndDropState.DragBeginned, dragChagedEventArgs);
            Entity.SetState(SelectState.Selected, selectChagedEventArgs);
        }

        public void OnDragging(IDroppableRegion hoveredRegion)
        {
            dragChagedEventArgs.TargetRegion = hoveredRegion;
            Entity.SetState(DragAndDropState.Dragging, dragChagedEventArgs);
        }
         
        public void OnEndDrag(IDroppableRegion droppedInRegion)
        {
            //if (droppedInRegion != null && DroppableRegion != droppedInRegion && droppedInRegion.Entity.IsStateActive(PlaceState.Ready))
            //{
            //    DroppableRegion?.TakenOut(Entity);
            //    DroppableRegion = droppedInRegion;
            //    DroppableRegion.DropIn(this);
            //}
            //else
            //{
            //    droppedInRegion?.DragFailed();
            //}


            dragChagedEventArgs.TargetRegion = droppedInRegion;
            Entity.SetState(DragAndDropState.Drop, dragChagedEventArgs);
        }
    } 
}
