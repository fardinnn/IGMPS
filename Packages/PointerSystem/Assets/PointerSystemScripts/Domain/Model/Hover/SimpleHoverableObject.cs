﻿using BG.API;
using UnityEngine;
using BG.States;
using UnityEngine.EventSystems;

namespace BG.PointerSystem.Domain
{
    public class SimpleHoverableObject : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        public Entity Entity { get; internal set; }
        // Lightweight pattern event args
        private HoverStateChagedEventArgs hoverChaged = new HoverStateChagedEventArgs();

        //public event EventHandler<HoverEventArgs> HoverBeginned;
        public void OnPointerEnter(PointerEventData eventData)
        {
            //Debug.Log($"Hovered {name}");
            if (Entity == null)
            {
                Debug.LogError($"The entity property of entity named: '{gameObject.name}' inside component: 'SimpleHoverableObject' not assigned yet");
                return;
            }
            Entity.SetState(HoverState.Entered, hoverChaged);
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            //Debug.Log($"Un-hovered {name}");
            Entity.SetState(HoverState.Exited, hoverChaged);
        }
    }
}
