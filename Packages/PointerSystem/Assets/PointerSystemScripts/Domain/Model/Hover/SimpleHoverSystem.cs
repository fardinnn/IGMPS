﻿using BG.API;
using BG.API.Abilities;
using BG.States;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace BG.PointerSystem.Domain
{
    public interface IHoverSystem: IPointerSystem
    {
        HashSet<Entity> HoveredEntities { get; }
    }

    public interface IHoverSystem<T> : IHoverSystem, IPointerSystem<T> where T: IPointerSystem<T> { }
    public class SimpleHoverSystem : IHoverSystem<SimpleHoverSystem>
    {
        public event EventHandler<HoverEventArgs> Entered;
        public event EventHandler<HoverEventArgs> Exited;

        private Dictionary<Entity, SimpleHoverableObject> targetHoverables;
        private Options options;

        public HashSet<Entity> HoveredEntities { get; }

        public Guid Id { get; set; }

        public bool IsReady { get; private set; }

        public SimpleHoverSystem(SimpleHoverSystemCommand command)
        {
            HoveredEntities = new HashSet<Entity>();
            options = new Options();
            targetHoverables = new Dictionary<Entity, SimpleHoverableObject>();
            IsReady = true;
        }


        private StateGroup hoverEnterState = new StateGroup(HoverState.Entered);
        private StateGroup hoverExitState = new StateGroup(HoverState.Exited);
        public void Visit(List<Entity> entities)
        {
            entities.ForEach(entity =>
            {
                var hoverable = entity.GetComponent<SimpleHoverableObject>();
                if (hoverable == null) hoverable = entity.gameObject.AddComponent<SimpleHoverableObject>();
                hoverable.Entity = entity;

                if (!targetHoverables.ContainsKey(entity))
                {
                    entity.AddSingleStateActivateAction<HoverState, HoverStateChagedEventArgs>
                    (HoverState.Entered, Enter);
                    entity.AddSingleStateActivateAction<HoverState, HoverStateChagedEventArgs>
                    (HoverState.Exited, Exit);

                    targetHoverables.Add(entity, hoverable);
                }
            });
        }

        private void AlteredOption<T>(T option) where T : Option
        {
            options[typeof(T)] = option;
        }

        public void OnPointerPositionChanged(Vector2 PointerPosition)
        {
            //var mainCamera = Camera.main;
            //mainCamera.ScreenPointToRay;
        }


        public void Enter(StateGroup stateGroup, HoverStateChagedEventArgs e)
        {
            if (!HoveredEntities.Contains(e.Entity))
            {
                HoveredEntities.Add(e.Entity);
            }
            Entered?.Invoke(this, new HoverEventArgs {Entity = e.Entity/*, Position = ?*/ });
        }

        public void Exit(StateGroup stateGroup, HoverStateChagedEventArgs e)
        {
            if (HoveredEntities.Contains(e.Entity))
            {
                HoveredEntities.Remove(e.Entity);
            }
            Exited?.Invoke(this, new HoverEventArgs { Entity = e.Entity/*, Position = ?*/ });
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}
