﻿using BG.API;
using System;

namespace BG.PointerSystem.Domain
{
    public interface IPointerSystem : IComponentSystem
    {
        Guid Id { get; set; }
    }

    public interface IPointerSystem<T> : IPointerSystem where T : IPointerSystem<T>
    {

    }
}
