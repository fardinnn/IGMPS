﻿using BG.API;
using BG.API.Abilities;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace BG.PointerSystem.Domain
{
    public class SimpleClickSystem : IPointerSystem<SimpleClickSystem>, IClickable
    {

        private HashSet<Transform> targetEntities;
        private Options options;


        public Guid Id { get; set; }

        public bool IsReady { get; private set; }

        private HashSet<SimpleClickableObject> targetClickables;

        public SimpleClickSystem(SimpleClickSystemCommand command)
        {

            options = new Options();
            targetEntities = new HashSet<Transform>();
            targetClickables = new HashSet<SimpleClickableObject>();
            IsReady = true;
        }
        public void Visit(List<Entity> entities)
        {
            for (int i = 0; i < entities.Count; i++)
            {
                var clickable = entities[i].GetComponent<SimpleClickableObject>();
                if (clickable == null)
                {
                    clickable = entities[i].gameObject.AddComponent<SimpleClickableObject>();
                    clickable.Entity = entities[i];
                }
                if (targetClickables.Contains(clickable)) continue;

                clickable.Clicked += (o, e) =>
                {
                    switch (e.Button)
                    {
                        case UnityEngine.EventSystems.PointerEventData.InputButton.Left:
                            OnClick0(this, new ClickEventArgs { ClickedEntity = e.ClickedEntity, Position = e.Position });
                            break;
                        case UnityEngine.EventSystems.PointerEventData.InputButton.Right:
                            OnClick1(this, new ClickEventArgs { ClickedEntity = e.ClickedEntity, Position = e.Position });
                            break;
                        case UnityEngine.EventSystems.PointerEventData.InputButton.Middle:
                            OnClick2(this, new ClickEventArgs { ClickedEntity = e.ClickedEntity, Position = e.Position });
                            break;
                    }
                };


                clickable.PointerDowned += (o, e) =>
                {
                    switch (e.Button)
                    {
                        case UnityEngine.EventSystems.PointerEventData.InputButton.Left:
                            ClickedDown0(this, new ClickEventArgs { ClickedEntity = e.ClickedEntity, Position = e.Position });
                            break;
                        case UnityEngine.EventSystems.PointerEventData.InputButton.Right:
                            ClickedDown1(this, new ClickEventArgs { ClickedEntity = e.ClickedEntity, Position = e.Position });
                            break;
                        case UnityEngine.EventSystems.PointerEventData.InputButton.Middle:
                            ClickedDown2(this, new ClickEventArgs { ClickedEntity = e.ClickedEntity, Position = e.Position });
                            break;
                    }
                };

                clickable.PointerUped += (o, e) =>
                {
                    switch (e.Button)
                    {
                        case UnityEngine.EventSystems.PointerEventData.InputButton.Left:
                            ClickedUp0(this, new ClickEventArgs { ClickedEntity = e.ClickedEntity, Position = e.Position });
                            break;
                        case UnityEngine.EventSystems.PointerEventData.InputButton.Right:
                            ClickedUp1(this, new ClickEventArgs { ClickedEntity = e.ClickedEntity, Position = e.Position });
                            break;
                        case UnityEngine.EventSystems.PointerEventData.InputButton.Middle:
                            ClickedUp2(this, new ClickEventArgs { ClickedEntity = e.ClickedEntity, Position = e.Position });
                            break;
                    }
                };

                targetClickables.Add(clickable);
            }
        }

        private void AlteredOption<T>(T option) where T : Option
        {
            options[typeof(T)] = option;
        }

        public event EventHandler<ClickEventArgs> Clicked0;
        public event EventHandler<ClickEventArgs> Clicked1;
        public event EventHandler<ClickEventArgs> Clicked2;
        public event EventHandler<ClickEventArgs> ClickedDown0;
        public event EventHandler<ClickEventArgs> ClickedDown1;
        public event EventHandler<ClickEventArgs> ClickedDown2;
        public event EventHandler<ClickEventArgs> ClickedUp0;
        public event EventHandler<ClickEventArgs> ClickedUp1;
        public event EventHandler<ClickEventArgs> ClickedUp2;

        public void OnClick0(object sender, ClickEventArgs e)
        {
            Clicked0?.Invoke(this, e);
        }

        public void OnClick1(object sender, ClickEventArgs e)
        {
            Clicked1?.Invoke(this, e);
        }

        public void OnClick2(object sender, ClickEventArgs e)
        {
            Clicked2?.Invoke(this, e);
        }

        public void OnPointerDown0(object sender, ClickEventArgs e)
        {
            ClickedDown0?.Invoke(this, e);
        }

        public void OnPointerDown1(object sender, ClickEventArgs e)
        {
            ClickedDown1?.Invoke(this, e);
        }

        public void OnPointerDown2(object sender, ClickEventArgs e)
        {
            ClickedDown2?.Invoke(this, e);
        }

        public void OnPointerUp0(object sender, ClickEventArgs e)
        {
            ClickedUp0?.Invoke(this, e);
        }

        public void OnPointerUp1(object sender, ClickEventArgs e)
        {
            ClickedUp1?.Invoke(this, e);
        }

        public void OnPointerUp2(object sender, ClickEventArgs e)
        {
            ClickedUp2?.Invoke(this, e);
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}
