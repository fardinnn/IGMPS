﻿using BG.API;
using BG.API.Abilities;
using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace BG.PointerSystem.Domain
{
    internal class SimpleClickableObject : MonoBehaviour, IPointerClickHandler, IPointerUpHandler, IPointerDownHandler
    {
        public event EventHandler<LocalClickEventArgs> Clicked;
        public event EventHandler<LocalClickEventArgs> PointerDowned;
        public event EventHandler<LocalClickEventArgs> PointerUped;
        public event EventHandler<SelectEventArgs> Selected;

        public Entity Entity { get; set; }

        public void OnPointerClick(PointerEventData eventData)
        {
            Clicked?.Invoke(this, new LocalClickEventArgs { ClickedEntity = Entity, Position = eventData.position, Button = eventData.button });
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            PointerDowned?.Invoke(this, new LocalClickEventArgs { ClickedEntity = Entity, Position = eventData.position, Button = eventData.button });
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            PointerUped?.Invoke(this, new LocalClickEventArgs { ClickedEntity = Entity, Position = eventData.position, Button = eventData.button });
        }
    }
    internal class LocalClickEventArgs : ClickEventArgs
    {
        public PointerEventData.InputButton Button { get; set; }
    }
}
