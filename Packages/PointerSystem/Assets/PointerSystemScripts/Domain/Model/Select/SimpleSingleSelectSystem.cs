﻿using BG.API;
using BG.API.Abilities;
using BG.States;
using BG.Extensions;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace BG.PointerSystem.Domain
{
    public class SimpleSingleSelectSystem : IPointerSystem<SimpleSingleSelectSystem>
    {

        private Options options;

        public Guid Id { get; set; }

        public bool IsReady { get; private set; }

        private HashSet<Entity> targetSelectables;
        private Entity SelectedEntity;


        public SimpleSingleSelectSystem(SimpleSingleSelectSystemCommand command)
        {
            if (command.SystemId == Guid.Empty)
            {
                Debug.LogError("The 'SystemId' mustn't be empty!");
                return;
            }
            Id = command.SystemId;
            options = new Options();
            targetSelectables = new HashSet<Entity>();
            IsReady = true;
        }
        private readonly StateGroup selectedState = new StateGroup(SelectState.Selected);
        private readonly StateGroup unselectedState = new StateGroup(SelectState.Unselected);
        public void Visit(List<Entity> entities)
        {
            entities.ForEach(entity =>
            {
                if (!targetSelectables.Contains(entity))
                {
                    entity.AddSingleStateActivateAction<SelectState, SelectStateChagedEventArgs>
                    (SelectState.Selected, Select);
                    entity.AddSingleStateDeactivateAction<SelectState, SelectStateChagedEventArgs>
                    (SelectState.Selected, Unselect);

                    targetSelectables.Add(entity);
                }
            });
        }

        private void AlteredOption<T>(T option) where T : Option
        {
            options[typeof(T)] = option;
        }

        public event EventHandler<SelectEventArgs> Selected;
        public event EventHandler<SelectEventArgs> Unselected;

        // Lightweight pattern event args
        private SelectStateChagedEventArgs selectChaged = new SelectStateChagedEventArgs();
        private SelectStateChagedEventArgs unselectChaged = new SelectStateChagedEventArgs();

        public void Select(StateGroup stateGroup, SelectStateChagedEventArgs e)
        {
            if (SelectedEntity != null && SelectedEntity != e.Entity)
                SelectedEntity.SetState(SelectState.Unselected, unselectChaged);
            SelectedEntity = e.Entity;
            Selected?.Invoke(this, new SelectEventArgs { Entity = e.Entity/*, InputModule =*/ });
        }
        public void Unselect(StateGroup stateGroup, SelectStateChagedEventArgs e)
        {
            Unselected?.Invoke(this, new SelectEventArgs { Entity = e.Entity/*, InputModule =*/ });
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        private HashSet<Entity> unselectReserved = new HashSet<Entity>();
        private HashSet<Entity> selectReserved = new HashSet<Entity>();
        public void PrepareSelection(HashSet<Entity> targetEntities)
        {
            foreach (var entity in targetEntities)
            {
                if(!targetSelectables.Contains(entity)) continue;
                if (!entity.IsStateActive(SelectState.Selected))
                {
                    if(!selectReserved.Contains(entity))
                        selectReserved.Add(entity);
                    //entity.SetState(SelectState.Reserved);
                }
                else if (!unselectReserved.Contains(entity)) unselectReserved.Add(entity);
            }
        }

        public void FinalizeSelection(HashSet<Entity> targetEntities)
        {
            foreach (var entity in selectReserved)
            {
                if (targetEntities.Contains(entity))
                    entity.SetState(SelectState.Selected, selectChaged);
            }

            foreach (var entity in unselectReserved)
            {
                if (targetEntities.Contains(entity))
                    entity.SetState(SelectState.Unselected, selectChaged);
            }

            selectReserved.Clear();
            unselectReserved.Clear();
        }
    }
}
