﻿using BG.API;
using BG.API.Abilities;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace BG.PointerSystem.Domain
{

    public class SimplePointerSystem : IPointerSystem<SimplePointerSystem>, IColorable
    {
        public event EventHandler<ColorChangeEventArgs> Colored;


        private HashSet<Transform> targetEntities;
        private Options options;

        public Guid Id { get; set; }

        public bool IsReady { get; private set; }

        public SimplePointerSystem(SimplePointerSystemCommand command)
        {

            options = new Options();
            targetEntities = new HashSet<Transform>();
            IsReady = true;
        }
        public void Visit(List<Entity> gameObjects)
        {

        }

        public void Coloring(object sender, ColorChangeEventArgs e)
        {
            ColorOption option = !options.Contains(typeof(ColorOption)) ? new ColorOption() :
                options[typeof(ColorOption)] as ColorOption;
            option.Value = e.Color;
            AlteredOption(option);
        }

        private void AlteredOption<T>(T option) where T : Option
        {
            options[typeof(T)] = option;
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}
