using BG.API;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BG.PointerSystem
{
    public abstract class PointerModel : IModel
    {
        public Transform Parent { get; set; }
        public Options DefaultOptions { get; set; }
    }
}
