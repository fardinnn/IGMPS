﻿namespace BG.PointerSystem
{
    public class SimplePointerView : PointerView
    {
        protected override void OnDisable()
        {
            (Controller as SimplePointertController)?.OnDisable();
        }

        protected override void OnEnable()
        {
            (Controller as SimplePointertController)?.OnEnable();
        }
    }
}
