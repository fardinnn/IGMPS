using BG.API;
using System;
using UnityEngine;

namespace BG.PointerSystem
{
    public abstract class PointerView : MonoBehaviour, IView
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public IController<IView, IModel> Controller { get; internal set; }

        protected abstract void OnEnable();
        protected abstract void OnDisable();
        public void Dispose()
        {
            Destroy(gameObject);
        }

    }
}
