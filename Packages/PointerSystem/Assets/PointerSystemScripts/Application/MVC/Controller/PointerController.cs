using BG.API;
using BG.API.Abilities;
using BG.States;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace BG.PointerSystem
{
    public interface IPointerController : IComponentController, IInputReceiver
    {

    }

    public abstract class PointerController<TView, TModel> : IComponentController<TView, TModel>, IPointerController, IDisposable
        where TView : PointerView
        where TModel : PointerModel
    {
        public TView View { get; }
        public TModel Model { get; }

        public Guid Id { get; } = Guid.NewGuid();


        public PointerController(TView view = null, TModel model = null, CompositeStatesSet requiredStates = null)
        {
            Model = model ?? CreateDefaultModel();
            View = view ?? CreateDefaultView();
            this.requiredStates = requiredStates ?? new CompositeStatesSet();
            View.Controller = this;
        }

        protected CompositeStatesSet requiredStates;
        public event EventHandler<RequiredStatesEventArgs> RequiredStatesUpdated;

        public void AddStatesGroup(params Enum[] groupStates)
        {
            if (requiredStates.Contains(groupStates)) return;
            requiredStates.Add(groupStates);
            RequiredStatesUpdated?.Invoke(this, new RequiredStatesEventArgs { StatesSet = requiredStates });
        }

        protected virtual TModel CreateDefaultModel()
        {
            return (TModel)Activator.CreateInstance(typeof(TModel));
        }

        protected virtual TView CreateDefaultView()
        {
            var view = (Model.Parent ? Model.Parent.gameObject :
                (Model.Parent = new GameObject(typeof(TView).Name.Replace("View", "")).transform).gameObject)
                .AddComponent<TView>();
            return view;
        }

        public virtual void Dispose()
        {
            UnityEngine.Object.Destroy(View.gameObject);
        }



        public abstract IComponentSystem ProvideComponentSystem(Options options);



        /************************************/
        /*              Inputs              */
        /*                                  */
        /************************************/

        public abstract string InputMapName { get; protected set; }
        public abstract Dictionary<string, EventHandler<IInputValue>> Actions { get; protected set; }
        public abstract Dictionary<string, EventHandler<IInputValue>> CancelActions { get; protected set; }
        public abstract string JSONInputs { get; protected set; }
        public abstract string CoExistGroup { get; set; }
        public abstract event EventHandler Enabled;
        public abstract event EventHandler Disabled;
        internal abstract void OnEnable();
        internal abstract void OnDisable();
    }
}
