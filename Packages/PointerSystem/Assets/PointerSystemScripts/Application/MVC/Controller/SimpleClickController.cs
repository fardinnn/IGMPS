﻿using BG.API;
using BG.API.Abilities;
using BG.PointerSystem.Domain;
using BG.States;
using System;
using System.Collections.Generic;

namespace BG.PointerSystem
{
    public class SimpleClickController :
        PointerController<SimplePointerView, SimplePointerModel>, IClickable
    {
        public SimpleClickController(SimplePointerView view = null,
            SimplePointerModel model = null, CompositeStatesSet requiredStates = null) : base(view, model, requiredStates)
        {

        }
        /************************************/
        /*              Inputs              */
        /*                                  */
        /************************************/
        //private PointerInputs inputActions;

        public override string InputMapName { get; protected set; }
        public override Dictionary<string, EventHandler<IInputValue>> Actions { get; protected set; }
        public override Dictionary<string, EventHandler<IInputValue>> CancelActions { get; protected set; }
        public override string JSONInputs { get; protected set; }
        public override string CoExistGroup { get; set; } = "Pointers";
        public override event EventHandler Enabled;
        public override event EventHandler Disabled;

        internal override void OnEnable()
        {
            Enabled?.Invoke(this, null);
        }


        internal override void OnDisable()
        {
            Disabled?.Invoke(this, null);
        }


        /************************************/
        /*              Click               */
        /*                                  */
        /************************************/

        public event EventHandler<ClickEventArgs> Clicked0;
        public event EventHandler<ClickEventArgs> Clicked1;
        public event EventHandler<ClickEventArgs> Clicked2;
        public event EventHandler<ClickEventArgs> ClickedDown0;
        public event EventHandler<ClickEventArgs> ClickedDown1;
        public event EventHandler<ClickEventArgs> ClickedDown2;
        public event EventHandler<ClickEventArgs> ClickedUp0;
        public event EventHandler<ClickEventArgs> ClickedUp1;
        public event EventHandler<ClickEventArgs> ClickedUp2;

        private SimpleClickSystem clickSystem;

        public override IComponentSystem ProvideComponentSystem(Options options = null)
        {
            if (clickSystem == null)
            {
                Guid systemId = Guid.NewGuid();
                new SetUpSimpleClickSystemHandler().Handle(new SimpleClickSystemCommand
                {
                    SystemId = systemId
                });
                clickSystem = PointerSystemManager.Get<SimpleClickSystem>(systemId);
                clickSystem.Clicked0 += OnClick0;
                clickSystem.Clicked1 += OnClick1;
                clickSystem.Clicked2 += OnClick2;
                clickSystem.ClickedDown0 += OnPointerDown0;
                clickSystem.ClickedDown1 += OnPointerDown1;
                clickSystem.ClickedDown2 += OnPointerDown2;
                clickSystem.ClickedUp0 += OnPointerUp0;
                clickSystem.ClickedUp1 += OnPointerUp1;
                clickSystem.ClickedUp2 += OnPointerUp2;
            }
            options?.AlterOn(clickSystem);
            return clickSystem;
        }
        public void OnClick0(object sender, ClickEventArgs e)
        {
            Clicked0?.Invoke(this, e);
        }

        public void OnClick1(object sender, ClickEventArgs e)
        {
            Clicked1?.Invoke(this, e);
        }

        public void OnClick2(object sender, ClickEventArgs e)
        {
            Clicked2?.Invoke(this, e);
        }

        public void OnPointerDown0(object sender, ClickEventArgs e)
        {
            ClickedDown0?.Invoke(this, e);
        }

        public void OnPointerDown1(object sender, ClickEventArgs e)
        {
            ClickedDown1?.Invoke(this, e);
        }

        public void OnPointerDown2(object sender, ClickEventArgs e)
        {
            ClickedDown2?.Invoke(this, e);
        }

        public void OnPointerUp0(object sender, ClickEventArgs e)
        {
            ClickedUp0?.Invoke(this, e);
        }

        public void OnPointerUp1(object sender, ClickEventArgs e)
        {
            ClickedUp1?.Invoke(this, e);
        }

        public void OnPointerUp2(object sender, ClickEventArgs e)
        {
            ClickedUp2?.Invoke(this, e);
        }

    }

}
