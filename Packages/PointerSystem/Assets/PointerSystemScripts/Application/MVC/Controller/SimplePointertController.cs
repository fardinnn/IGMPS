﻿using BG.API;
using BG.API.Abilities;
using BG.PointerSystem.Domain;
using BG.States;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace BG.PointerSystem
{
    public class SimplePointertController :
        PointerController<SimplePointerView, SimplePointerModel>, IClickable, IHoverable
    {
        public SimplePointertController(SimplePointerView view = null,
            SimplePointerModel model = null, CompositeStatesSet requiredStates = null) : base(view, model, requiredStates)
        {

        }

        /************************************/
        /*              Inputs              */
        /*                                  */
        /************************************/
        //private PointerInputs inputActions;

        public override string InputMapName { get; protected set; }
        public override Dictionary<string, EventHandler<IInputValue>> Actions { get; protected set; }
        public override Dictionary<string, EventHandler<IInputValue>> CancelActions { get; protected set; }
        public override string JSONInputs { get; protected set; }
        public override string CoExistGroup { get; set; } = "Pointers";
        public override event EventHandler Enabled;
        public override event EventHandler Disabled;

        internal override void OnEnable()
        {
            Enabled?.Invoke(this, null);
        }


        internal override void OnDisable()
        {
            Disabled?.Invoke(this, null);
        }


        /************************************/
        /*              Hover               */
        /*                                  */
        /************************************/
        public event EventHandler<HoverEventArgs> Hovered;
        public event EventHandler<HoverEventArgs> Entered;
        public event EventHandler<HoverEventArgs> Exited;
        public void Hover(object sender, HoverEventArgs e)
        {
            Hovered?.Invoke(this, e);
        }
        public void Enter(object sender, HoverEventArgs e)
        {
            Entered?.Invoke(this, e);
        }

        public void Exit(object sender, HoverEventArgs e)
        {
            Exited?.Invoke(this, e);
        }

        private SimpleHoverSystem hoverSystem;
        public IComponentSystem ProvideHoverSystem(Options options = null)
        {
            if (hoverSystem == null)
            {
                Guid systemId = Guid.NewGuid();
                new SetUpSimpleHoverSystemHandler().Handle(new SimpleHoverSystemCommand
                {
                    SystemId = systemId
                });
                hoverSystem = PointerSystemManager.Get<SimpleHoverSystem>(systemId);
                hoverSystem.Entered += Enter;
                hoverSystem.Exited += Exit;
            }
            options?.AlterOn(hoverSystem);
            return hoverSystem;
        }

        /************************************/
        /*              Click               */
        /*                                  */
        /************************************/
        public event EventHandler<ClickEventArgs> Clicked0;
        public event EventHandler<ClickEventArgs> Clicked1;
        public event EventHandler<ClickEventArgs> Clicked2;
        public event EventHandler<ClickEventArgs> ClickedDown0;
        public event EventHandler<ClickEventArgs> ClickedDown1;
        public event EventHandler<ClickEventArgs> ClickedDown2;
        public event EventHandler<ClickEventArgs> ClickedUp0;
        public event EventHandler<ClickEventArgs> ClickedUp1;
        public event EventHandler<ClickEventArgs> ClickedUp2;

        private SimpleClickSystem clickSystem;
        public IComponentSystem ProvideClickSystem(Options options = null)
        {
            if (clickSystem == null)
            {
                Guid systemId = Guid.NewGuid();
                new SetUpSimpleClickSystemHandler().Handle(new SimpleClickSystemCommand
                {
                    SystemId = systemId
                });
                clickSystem = PointerSystemManager.Get<SimpleClickSystem>(systemId);
                clickSystem.Clicked0 += OnClick0;
                clickSystem.Clicked1 += OnClick1;
                clickSystem.Clicked2 += OnClick2;
                clickSystem.ClickedDown0 += OnPointerDown0;
                clickSystem.ClickedDown1 += OnPointerDown1;
                clickSystem.ClickedDown2 += OnPointerDown2;
                clickSystem.ClickedUp0 += OnPointerUp0;
                clickSystem.ClickedUp1 += OnPointerUp1;
                clickSystem.ClickedUp2 += OnPointerUp2;
            }
            options?.AlterOn(clickSystem);
            return clickSystem;
        }
        public void OnClick0(object sender, ClickEventArgs e)
        {
            Clicked0?.Invoke(this, e);
        }

        public void OnClick1(object sender, ClickEventArgs e)
        {
            Clicked1?.Invoke(this, e);
        }

        public void OnClick2(object sender, ClickEventArgs e)
        {
            Clicked2?.Invoke(this, e);
        }

        public void OnPointerDown0(object sender, ClickEventArgs e)
        {
            ClickedDown0?.Invoke(this, e);
        }

        public void OnPointerDown1(object sender, ClickEventArgs e)
        {
            ClickedDown1?.Invoke(this, e);
        }

        public void OnPointerDown2(object sender, ClickEventArgs e)
        {
            ClickedDown2?.Invoke(this, e);
        }

        public void OnPointerUp0(object sender, ClickEventArgs e)
        {
            ClickedUp0?.Invoke(this, e);
        }

        public void OnPointerUp1(object sender, ClickEventArgs e)
        {
            ClickedUp1?.Invoke(this, e);
        }

        public void OnPointerUp2(object sender, ClickEventArgs e)
        {
            ClickedUp2?.Invoke(this, e);
        }

        /************************************/
        /*               Drag               */
        /*                                  */
        /************************************/
        public event EventHandler<DragEventArgs> Dragged;

        public void Drag(object sender, DragEventArgs e)
        {
            throw new NotImplementedException();
        }

        private SimpleDragSystem dragSystem;
        public IComponentSystem ProvideDragSystem(Options options = null)
        {
            if (dragSystem == null)
            {
                Guid systemId = Guid.NewGuid();
                new SetUpSimpleDragSystemHandler().Handle(new SimpleDragSystemCommand
                {
                    SystemId = systemId
                });
                dragSystem = PointerSystemManager.Get<SimpleDragSystem>(systemId);
            }
            options?.AlterOn(dragSystem);
            return dragSystem;
        }

        public override IComponentSystem ProvideComponentSystem(Options options)
        {
            if (hoverSystem == null)
            {
                Guid systemId = Guid.NewGuid();
                new SetUpSimpleHoverSystemHandler().Handle(new SimpleHoverSystemCommand
                {
                    SystemId = systemId
                });
                hoverSystem = PointerSystemManager.Get<SimpleHoverSystem>(systemId);
                hoverSystem.Entered += Enter;
                hoverSystem.Exited += Exit;
            }
            options?.AlterOn(hoverSystem);
            return hoverSystem;
        }
    }

}
