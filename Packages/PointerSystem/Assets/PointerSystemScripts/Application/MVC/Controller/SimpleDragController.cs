﻿using BG.API;
using BG.API.Abilities;
using BG.Extensions;
using BG.PointerSystem.Domain;
using BG.States;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace BG.PointerSystem
{
    public class SimpleDragController :
        PointerController<SimplePointerView, SimplePointerModel>, IInputReceiver
    {
        public SimpleDragController(SimplePointerView view = null,
            SimplePointerModel model = null, CompositeStatesSet requiredStates = null) : base(view, model, requiredStates)
        {
            if (Model.HoverController == null)
            {
                Debug.LogError("HoverController can not be null!");
                return;
            }
            InitializeInputs();
        }

        /************************************/
        /*              Inputs              */
        /*                                  */
        /************************************/
        //private PointerInputs inputActions;

        public override string InputMapName { get; protected set; }
        public override Dictionary<string, EventHandler<IInputValue>> Actions { get; protected set; }
        public override Dictionary<string, EventHandler<IInputValue>> CancelActions { get; protected set; }
        public override string JSONInputs { get; protected set; }
        public override string CoExistGroup { get; set; } = "Pointers";
        public override event EventHandler Enabled;
        public override event EventHandler Disabled;

        private Task dragCoroutine;

        private void InitializeInputs()
        {
            //Debug.Log($"this: {this.GetType().Name}");
            InputMapName = "Simple Pointer";
            Actions = new Dictionary<string, EventHandler<IInputValue>>
            {
                {"Drag_Actuators", (object o, IInputValue e) =>  OnBeginHold() },
                //{"Click0", (object o, IInputValue e) => OnHoldClickNew() }
            };
            CancelActions = new Dictionary<string, EventHandler<IInputValue>>
            {
                {"Drag_Actuators", (object o, IInputValue e) => OnEndHold() },
                //{"Click0", (object o, IInputValue e) => OnReleaseClickNew() }
            };

            JSONInputs = new SimplePointerInputs().SimplePointer.Get().ToJson();//DefaultInputs.SimplePerspectiveCamera;

            dragCoroutine = new Task(DragCoroutine(), false);
        }

        private bool isEnable = true;
        internal override void OnEnable()
        {
            isEnable = true;
            Enabled?.Invoke(this, null);
        }


        internal override void OnDisable()
        {
            isEnable = false;
            Disabled?.Invoke(this, null);
        }


        /************************************/
        /*               Drag               */
        /*                                  */
        /************************************/


        private Pointer currentPointer;
        private void OnBeginHold()
        {
            if (!isEnable || dragCoroutine.Running || Model.HoverController.HoveredEntities.Count < 1) return;
            currentPointer = Pointer.current;
            if (dragSystem == null) return;
            dragSystem.BeginDrag(currentPointer.position.ReadValue(), Model.HoverController.HoveredEntities);

            dragCoroutine.Start();
        }


        IEnumerator DragCoroutine()
        {
            while (isEnable)
            {
                dragSystem.ContinueDragging(currentPointer.position.ReadValue());
                yield return null;
            }
        }

        private void OnEndHold()
        {
            try
            {
                if (!(isEnable && dragCoroutine.Running)) return;
                dragSystem.EndDrag(currentPointer.position.ReadValue());
                dragCoroutine.Stop();
            }
            catch (Exception ex)
            {
                Debug.LogError($"Catch Error: {ex.Message}");
            }
        }




        //public void OnHoldClick(Vector2 pointerPosition)
        //{
        //    if (!dragBeginned)
        //    {
        //        if (dragSystem == null) return;
        //        dragSystem.BeginDrag(Model.HoverController.HoveredEntities);
        //        dragBeginned = true;
        //        return;
        //    }
        //    dragSystem.ContinueDragging(pointerPosition);
        //}

        //public void OnReleaseClick(Vector2 pointerPosition)
        //{
        //    if (!dragBeginned) return;
        //    dragSystem.EndDrag(pointerPosition);
        //    dragBeginned = false;
        //}


        //private bool clickHolded;
        //private WaitForSeconds waitForDeltaTime;
        //private Task holdClickTask;
        ////private Pointer currentPointer;
        //public void OnHoldClickNew()
        //{
        //    Debug.Log("One");
        //    waitForDeltaTime = new WaitForSeconds(Time.deltaTime);
        //    currentPointer = Pointer.current;
        //    if (holdClickTask != null && holdClickTask.Running) holdClickTask.Stop();
        //    holdClickTask = new Task(HoldClick());
        //    holdClickTask.Start();
        //}

        //IEnumerator HoldClick()
        //{
        //    while (true)
        //    {
        //        yield return waitForDeltaTime;
        //        Debug.Log($"currentPointer.position: {currentPointer.position.ReadValue()}");
        //    }
        //}

        //public void OnReleaseClickNew()
        //{
        //    Debug.Log("Two");
        //    holdClickTask.Stop();
        //    holdClickTask = null;
        //}


        //public event EventHandler<DragEventArgs> Dragged;

        //public void Drag(object sender, DragEventArgs e)
        //{
        //    throw new NotImplementedException();
        //}

        private SimpleDragSystem dragSystem;
        public override IComponentSystem ProvideComponentSystem(Options options = null)
        {
            if (dragSystem == null)
            {
                Guid systemId = Guid.NewGuid();
                new SetUpSimpleDragSystemHandler().Handle(new SimpleDragSystemCommand
                {
                    SystemId = systemId
                });
                dragSystem = PointerSystemManager.Get<SimpleDragSystem>(systemId);
            }
            options?.AlterOn(dragSystem);
            return dragSystem;
        }
    }

}
