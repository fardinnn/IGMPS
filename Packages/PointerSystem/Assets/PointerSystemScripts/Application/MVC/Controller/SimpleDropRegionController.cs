﻿using BG.API;
using BG.API.Abilities;
using BG.PointerSystem.Domain;
using BG.States;
using System;
using System.Collections.Generic;

namespace BG.PointerSystem
{
    public class SimpleDropRegionController : PointerController<SimplePointerView, SimplePointerModel>
    {
        public SimpleDropRegionController(SimplePointerView view = null,
                    SimplePointerModel model = null, CompositeStatesSet requiredStates = null) : base(view, model, requiredStates)
        {

        }

        /************************************/
        /*              Click               */
        /*                                  */
        /************************************/

        public event EventHandler<DropEventArgs> DroppedIn;

        private SimpleDropRegionSystem clickSystem;
        public override IComponentSystem ProvideComponentSystem(Options options = null)
        {
            if (clickSystem == null)
            {
                Guid systemId = Guid.NewGuid();
                new SetUpSimpleDropRegionSystemHandler().Handle(new SimpleDropRegionSystemCommand
                {
                    SystemId = systemId
                });
                clickSystem = PointerSystemManager.Get<SimpleDropRegionSystem>(systemId);
                //clickSystem.DroppedIn += OnDroppedIn;
            }
            options?.AlterOn(clickSystem);
            return clickSystem;
        }

        private void OnDroppedIn(object sender, DropEventArgs e)
        {
            DroppedIn?.Invoke(this, e);
        }

        /************************************/
        /*              Inputs              */
        /*                                  */
        /************************************/
        //private PointerInputs inputActions;

        public override string InputMapName { get; protected set; }
        public override Dictionary<string, EventHandler<IInputValue>> Actions { get; protected set; }
        public override Dictionary<string, EventHandler<IInputValue>> CancelActions { get; protected set; }
        public override string JSONInputs { get; protected set; }
        public override string CoExistGroup { get; set; } = "Pointers";
        public override event EventHandler Enabled;
        public override event EventHandler Disabled;

        internal override void OnEnable()
        {
            Enabled?.Invoke(this, null);
        }


        internal override void OnDisable()
        {
            Disabled?.Invoke(this, null);
        }

    }

}
