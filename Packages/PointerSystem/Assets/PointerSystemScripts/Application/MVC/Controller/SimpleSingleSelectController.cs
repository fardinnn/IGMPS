﻿using BG.API;
using BG.API.Abilities;
using BG.PointerSystem.Domain;
using BG.States;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace BG.PointerSystem
{
    public class SimpleSingleSelectController :
        PointerController<SimplePointerView, SimplePointerModel>, IInputReceiver
    {
        public SimpleSingleSelectController(SimplePointerView view = null,
            SimplePointerModel model = null, CompositeStatesSet requiredStates = null) : base(view, model, requiredStates)
        {
            if(Model.HoverController == null)
            {
                Debug.LogError("HoverController can not be null!");
                return;
            }
            InitializeInputs();
        }
        /************************************/
        /*              Inputs              */
        /*                                  */
        /************************************/
        //private PointerInputs inputActions;

        public override string InputMapName { get; protected set; }
        public override Dictionary<string, EventHandler<IInputValue>> Actions { get; protected set; }
        public override Dictionary<string, EventHandler<IInputValue>> CancelActions { get; protected set; }
        public override string JSONInputs { get; protected set; }
        public override string CoExistGroup { get; set; } = "Pointers";
        public override event EventHandler Enabled;
        public override event EventHandler Disabled;

        private void InitializeInputs()
        {
            var inputActions = new SimplePointerInputs();
            InputMapName = inputActions.asset.actionMaps[0].name;
                //"Simple Pointer";
            Actions = new Dictionary<string, EventHandler<IInputValue>>
            {
                {inputActions.SimplePointer.Click0.name, (object o, IInputValue e) => OnClickDown(Model.HoverController.HoveredEntities) }
            };
            CancelActions = new Dictionary<string, EventHandler<IInputValue>>
            {
                {inputActions.SimplePointer.Click0.name, (object o, IInputValue e) => OnClickUp(Model.HoverController.HoveredEntities) }
            };

            JSONInputs = inputActions.SimplePointer.Get().ToJson();//DefaultInputs.SimplePerspectiveCamera;
        }

        internal override void OnEnable()
        {
            Enabled?.Invoke(this, null);
        }


        internal override void OnDisable()
        {
            Disabled?.Invoke(this, null);
        }


        /************************************/
        /*              Select               */
        /*                                  */
        /************************************/
        public event EventHandler<SelectEventArgs> Selected;
        public event EventHandler<SelectEventArgs> Unselected;

        private SimpleSingleSelectSystem selectSystem;

        public override IComponentSystem ProvideComponentSystem(Options options = null)
        {
            if (selectSystem == null)
            {
                Guid systemId = Guid.NewGuid();
                new SetUpSimpleSingleSelectSystemHandler().Handle(new SimpleSingleSelectSystemCommand
                {
                    SystemId = systemId
                });
                selectSystem = PointerSystemManager.Get<SimpleSingleSelectSystem>(systemId);
                selectSystem.Selected += Select;
                selectSystem.Unselected += Unselect;
            }
            options?.AlterOn(selectSystem);
            return selectSystem;
        }


        public void OnClickDown(HashSet<Entity> targetEntities)
        {
            if (selectSystem == null) return;
            //Debug.Log("ClickedDown");
            selectSystem.PrepareSelection(targetEntities);
        }

        public void OnClickUp(HashSet<Entity> targetEntities)
        {
            //Debug.Log("ClickedUp");
            if (selectSystem == null) return;
            selectSystem.FinalizeSelection(targetEntities);
        }


        public void Select(object sender, SelectEventArgs e)
        {
            Selected?.Invoke(this, e);
        }
        public void Unselect(object sender, SelectEventArgs e)
        {
            Unselected?.Invoke(this, e);
        }

    }

}
