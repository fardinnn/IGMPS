﻿using BG.API;
using BG.API.Abilities;
using BG.PointerSystem.Domain;
using BG.States;
using System;
using System.Collections.Generic;

namespace BG.PointerSystem
{
    public interface IHoverController : IComponentController
    { public HashSet<Entity> HoveredEntities { get;} }

    public abstract class HoverController<TView, TModel> : PointerController<SimplePointerView, SimplePointerModel>, IHoverController
        where TView : SimplePointerView
        where TModel : SimplePointerModel
    {
        protected HoverController(SimplePointerView view, SimplePointerModel model, CompositeStatesSet requiredStates) : base(view, model, requiredStates)
        {
            if (Model.HoverController == null) Model.HoverController = this;
        }

        public abstract HashSet<Entity> HoveredEntities { get; }
    }

    public class SimpleHoverController :
        HoverController<SimplePointerView, SimplePointerModel>, IHoverable
    {
        public SimpleHoverController(SimplePointerView view = null,
            SimplePointerModel model = null, CompositeStatesSet requiredStates = null) : base(view, model, requiredStates)
        {

        }


        /************************************/
        /*              Hover               */
        /*                                  */
        /************************************/
        public event EventHandler<HoverEventArgs> Hovered;
        public event EventHandler<HoverEventArgs> Entered;
        public event EventHandler<HoverEventArgs> Exited;
        public void Hover(object sender, HoverEventArgs e)
        {
            Hovered?.Invoke(this, e);
        }
        public void Enter(object sender, HoverEventArgs e)
        {
            Entered?.Invoke(this, e);
        }

        public void Exit(object sender, HoverEventArgs e)
        {
            Exited?.Invoke(this, e);
        }

        private SimpleHoverSystem hoverSystem;

        public override HashSet<Entity> HoveredEntities
        {
            get
            {
                if (hoverSystem == null) return null;
                return hoverSystem?.HoveredEntities;
            }
        }

        public override IComponentSystem ProvideComponentSystem(Options options = null)
        {
            if (hoverSystem == null)
            {
                Guid systemId = Guid.NewGuid();
                new SetUpSimpleHoverSystemHandler().Handle(new SimpleHoverSystemCommand
                {
                    SystemId = systemId
                });
                hoverSystem = PointerSystemManager.Get<SimpleHoverSystem>(systemId);
                hoverSystem.Entered += Enter;
                hoverSystem.Exited += Exit;
            } 
            options?.AlterOn(hoverSystem);
            return hoverSystem;
        }


        /************************************/
        /*              Inputs              */
        /*                                  */
        /************************************/

        public override string InputMapName { get; protected set; }
        public override Dictionary<string, EventHandler<IInputValue>> Actions { get; protected set; }
        public override Dictionary<string, EventHandler<IInputValue>> CancelActions { get; protected set; }
        public override string JSONInputs { get; protected set; }
        public override string CoExistGroup { get; set; }
        public override event EventHandler Enabled;
        public override event EventHandler Disabled;
        internal override void OnEnable()
        {
            Enabled?.Invoke(this, null);
        }
        internal override void OnDisable()
        {
            Disabled?.Invoke(this, null);
        }
    }

}
