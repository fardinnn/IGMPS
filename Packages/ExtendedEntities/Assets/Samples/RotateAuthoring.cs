using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

public class RotateAuthoring : MonoBehaviour, IConvertGameObjectToEntity
{
    [SerializeField] private float degreePerSecond;
    public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
    {
        dstManager.AddComponentData(entity,
            new Rotate { RadiansPerSecond = degreePerSecond * math.radians(degreePerSecond) });
        dstManager.AddComponentData(entity, new RotationEulerXYZ());
        dstManager.AddComponentData(entity, new EntityName { Name = this.name });
    }
}
