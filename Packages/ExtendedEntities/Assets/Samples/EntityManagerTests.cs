using System.Collections.Generic;
using Unity.Collections;
using Unity.Entities;
using Unity.Rendering;
using Unity.Transforms;
using UnityEngine;

public class EntityManagerTests : MonoBehaviour
{
    [SerializeField]
    private Mesh mesh;
    [SerializeField]
    private UnityEngine.Material material;
    NativeArray<Entity> entities;
    // Start is called before the first frame update
    void Start()
    {
        CreatGrid(Vector3.zero, World.DefaultGameObjectInjectionWorld.EntityManager);
        //entities = new NativeArray<Entity>(2, Allocator.Persistent, NativeArrayOptions.UninitializedMemory);
        //var entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
        //var entityArchetype = entityManager.CreateArchetype(
        //    typeof(EntityName),
        //    typeof(LocalToWorld),
        //    typeof(Translation),
        //    typeof(Rotation)
        //    );
        //var entity1 = entityManager.CreateEntity(entityArchetype);
        //var entity2 = entityManager.CreateEntity(entityArchetype);
        //entities[0] = entity1;
        //entities[1] = entity1;

        //Debug.Log($"is {entities[0]} equals to {entity1} : {entities[0] == entity1}");
        //PassNativeArrayAsParameter(entities, entity1, entityManager);

        //entities.Dispose();
    }


    void PassNativeArrayAsParameter(NativeArray<Entity> myEntities, Entity entity1, EntityManager entityManager)
    {
        List<NativeArray<Entity>> entitiesList = new List<NativeArray<Entity>>();
        entitiesList.Add(myEntities);
        Debug.Log($"is {entitiesList[0]} equals to {entities} : {entitiesList[0] == entities}");
        Debug.Log($"is {entitiesList[0][0]} equals to {entity1} : {entitiesList[0][0] == entity1}");

        entityManager.AddComponent<Rotate>(entitiesList[0][0]);
        entityManager.AddComponent<RotationEulerXYZ>(entitiesList[0][0]);
    }

    private void CreatGrid(Vector3 offset, EntityManager entityManager)
    {

        var entityArchetype = entityManager.CreateArchetype(
            typeof(LocalToParent),
            typeof(LocalToWorld),
            typeof(Translation),
            typeof(RenderBounds),
            typeof(Parent),
            typeof(RenderMesh),
            //typeof(WorldRenderBounds),
            typeof(BuiltinMaterialPropertyUnity_LightData),
            typeof(BuiltinMaterialPropertyUnity_RenderingLayer),
            typeof(BuiltinMaterialPropertyUnity_WorldTransformParams),
            typeof(BlendProbeTag),
            typeof(PerInstanceCullingTag),
            typeof(Rotation),
            typeof(WorldToLocal_Tag),

            typeof(EntityTag),
            typeof(EntityName),

            //typeof(Rotate),
            typeof(RotationEulerXYZ)
            );

        var parent = entityManager.CreateEntity(typeof(Rotate),
            typeof(LocalToWorld),
            typeof(Translation),
            typeof(Rotation),
            typeof(Child)
            );

        entityManager.SetComponentData(parent, new Translation { Value = new Unity.Mathematics.float3 { x = 0.0f, y = 0.0f, z = 0.0f } });


        for (int i = 0; i < 10; i++)
        {
            for (int j = 0; j < 100; j++)
            {
                var entity = entityManager.CreateEntity(entityArchetype);
                entityManager.SetComponentData(entity, new Parent { Value = parent });
                entityManager.SetComponentData(entity, new EntityTag
                {
                    Tag = $"{i}-{j}"
                });
                entityManager.SetSharedComponentData(entity, new RenderMesh
                {
                    mesh = mesh,
                    material = material,
                    needMotionVectorPass = true,
                    castShadows = UnityEngine.Rendering.ShadowCastingMode.On,
                    receiveShadows = true
                });
                entityManager.SetComponentData(entity, new Translation
                {
                    Value = new Unity.Mathematics.float3
                    {
                        x = i * 1.5f + offset.x,
                        y = 0.0f + offset.y,
                        z = j * 1.5f + offset.z
                    }
                });

                entityManager.SetComponentData(entity, new RenderBounds
                {
                    Value = new Unity.Mathematics.AABB
                    {
                        Extents = new Unity.Mathematics.float3
                        {
                            x = 0.5f,
                            y = 0.5f,
                            z = 0.5f
                        }
                    }
                });


                //entityManager.SetComponentData(entity, new WorldRenderBounds
                //{
                //    Value = new Unity.Mathematics.AABB
                //    {
                //        Center = new Unity.Mathematics.float3
                //        {
                //            x = i * 1.5f + offset.x,
                //            y = 0.0f + offset.y,
                //            z = j * 1.5f + offset.z
                //        },
                //        Extents = new Unity.Mathematics.float3
                //        {
                //            x = 0.5f,
                //            y = 0.5f,
                //            z = 0.5f
                //        }
                //    }
                //});

                entityManager.SetComponentData(entity,
                    new BuiltinMaterialPropertyUnity_LightData
                    {
                        Value = new Unity.Mathematics.float4
                        {
                            x = 0.0f,
                            y = 0.0f,
                            z = 1.0f,
                            w = 0.0f
                        }
                    });
                entityManager.SetComponentData(entity,
                     new BuiltinMaterialPropertyUnity_RenderingLayer
                     {
                         Value = new Unity.Mathematics.uint4
                         {
                             x = 1,
                             y = 0,
                             z = 0,
                             w = 0
                         }
                     });
                entityManager.SetComponentData(entity,
                     new BuiltinMaterialPropertyUnity_WorldTransformParams
                     {
                         Value = new Unity.Mathematics.float4
                         {
                             x = 0.0f,
                             y = 0.0f,
                             z = 0.0f,
                             w = 1.0f
                         }
                     });
                //entityManager.SetComponentData<Rotate>(entity,
                //     new Rotate
                //     {
                //         RadiansPerSecond = 0.45f
                //     });
                if (entityManager.HasComponent<Rotate>(entity))
                {
                    var rot = entityManager.GetComponentData<Rotate>(entity);
                    Debug.Log($"rot == null: {rot.RadiansPerSecond}");
                }
            }
        }

    }
}
