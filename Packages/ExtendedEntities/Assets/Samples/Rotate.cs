using Unity.Entities;
using Unity.Collections;

public struct Rotate : IComponentData
{
    public float RadiansPerSecond;
}

public struct EntityName : IComponentData
{
    public FixedString32 Name;
}

public struct EntityTag : IComponentData
{
    public FixedString32 Tag;
}
