using Unity.Collections;
using Unity.Entities;
using Unity.Transforms;
using UnityEngine;

public class RotateSystem : SystemBase
{
    protected override void OnUpdate()
    {
        //float deltaTime = Time.DeltaTime;
        //Entities.ForEach((ref Rotate rotate, ref RotationEulerXYZ euler) =>
        //{
        //    euler.Value.y += rotate.RadiansPerSecond * deltaTime;
        //}).Schedule();
    }
}

public class TestArrays : SystemBase
{
    NativeArray<FixedString32> allTags = new NativeArray<FixedString32>(10, Allocator.Persistent);
    protected override void OnCreate()
    {
        base.OnCreate();
        SetArray();
    }
    protected override void OnUpdate()
    {
        //var begin = UnityEngine.Time.realtimeSinceStartup;
        //Entities.ForEach((ref EntityName entityName, in EntityTag entityTag) =>
        //{
        //    if (allTags.Contains(entityTag.Tag))
        //    {
        //        entityName.Name = entityTag.Tag;
        //    }
        //}).WithoutBurst().Run();
        //var end = UnityEngine.Time.realtimeSinceStartup;
        //Debug.Log($"elapsed time: {end - begin}.");
    }

    void SetArray()
    {
        for (int i = 0; i < 10; i++)
            //allTags.Add($"{i}-{i}");
            allTags[i] = $"{i}-{i}";
            //allTags.Add($"{i}-{i}", $"{i}-{i}");
    }

    protected override void OnDestroy()
    {
        allTags.Dispose();
        base.OnDestroy();

    }
}
