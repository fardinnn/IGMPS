@echo off
IF EXIST %UserProfile%\currentPath.txt goto :SetUp_Current_Path
ECHO %cd%>%UserProfile%\currentPath.txt

:FileExists
setlocal EnableDelayedExpansion & cd /d "%~dp0"
%1 %2
mshta vbscript:createobject("shell.application").shellexecute("%~s0","goto :start","","runas",0)(window.close)&exit

:start
SET /P currentPath=<%UserProfile%\currentPath.txt
SET /P actualPath=<%UserProfile%\takeLinkDirectory.txt
CALL :get_last_folder "%actualPath%" _result
SET "linkPath=%currentPath%\%_result%"

CALL :GetRelativePath "%actualPath%" "%linkPath%" relativePath

mklink /D "%linkPath%" "%relativePath%" && (
	ECHO "was successfull">%UserProfile%\takeLinkResult.txt
	)||(
	ECHO "wasn't successfull">%UserProfile%\takeLinkResult.txt
	)
type nul >%UserProfile%\currentPath.txt
exit /b

:SetUp_Current_Path
	SET /P currentPath=<%UserProfile%\currentPath.txt
	IF "%currentPath%"=="" ECHO %cd%>%UserProfile%\currentPath.txt
goto :FileExists

::-----------------------------------------------------------------------------
:: Subroutine
::-----------------------------------------------------------------------------
:get_last_folder <path> [return]
:: Extracts the last folder in a file system directory path.
:: Path may include zero, one, or more trailing slashes, but not a file name.

REM Use SETLOCAL to keep our subroutine variables from affecting the parent.
REM It also allows us to limit delayed variable expansion to where it's needed.
setlocal enableDelayedExpansion

REM Add a trailing slash to ensure the last element is seen as a folder.
REM If it already had a trailing slash, that's okay: extras are ignored.
set "_full_path=%~1\"

REM The caller can provide a variable name that we'll set to the return value.
REM If no return variable is given, we'll just ECHO the value before exiting.
set "_return=%~2"

for /f "delims=" %%F in ("%_full_path%") do (
    REM Treat the path as a string to avoid "file not found" error.
    REM Use loop variable expansion to get the "path" part of the path.
    REM The resulting string will always have exactly one trailing slash.
    set "_path=%%~pF"

    REM Use substring substitution to remove the trailing slash.
    REM Delayed expansion lets us access the new value while inside the loop.
    set "_path=!_path:~0,-1!"

    REM Without a trailing slash, the last element is now seen as a file.
    REM Use the "name" substring to get the value we came for.
    for /f "delims=" %%D in ("!_path!") do (
        set "_name=%%~nD"
    )
)

REM 
if defined _return (
    set "_command=set %_return%=%_name%"
) else (
    set "_command=echo\%_name%"
)

REM The "ENDLOCAL &" trick allows setting variables in the parent environment.
REM See https://ss64.com/nt/syntax-functions.html for more details.
endlocal & %_command%

goto :eof


:GetRelativePath
CALL :strlen lenght1 %~1
CALL :strlen lenght2 %~2
SETLOCAL StartLenght = 0
IF lenght1>lenght2 SET StartLenght = lenght2
ELSE SET StartLenght = lenght1

SETLOCAL LinkPathRest = %%~1:~StartLenght%
SETLOCAL ActualRest = %%~1:~StartLenght%

FOR /L %%targetChar IN(0,1,%StartLenght%) DO 

SET %~3 = %ActualRest%
EXIT /B 0

REM ********* function *****************************
:strlen <resultVar> <stringVar>
(   
    setlocal EnableDelayedExpansion
    (set^ tmp=!%~2!)
    if defined tmp (
        set "len=1"
        for %%P in (4096 2048 1024 512 256 128 64 32 16 8 4 2 1) do (
            if "!tmp:~%%P,1!" NEQ "" ( 
                set /a "len+=%%P"
                set "tmp=!tmp:~%%P!"
            )
        )
    ) ELSE (
        set len=0
    )
)
( 
    endlocal
    set "%~1=%len%"
    exit /b
)