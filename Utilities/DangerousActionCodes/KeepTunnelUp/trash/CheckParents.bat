
:check_node_as_child
	echo off 
	SetLocal EnableExtensions, EnableDelayedExpansion

	set /A i=0
	for /F "tokens=2" %%R in ('
		tasklist /fi "WINDOWTITLE eq ConnectToLocalTunnel" /fo list ^| findstr /B "PID:"
	') do (
		set /A i+=1
		set /A PID[!i!]=%%R
	)
	timeout 1 > NUL
	wmic process where ParentProcessId^="%PID[1]%" get Caption | find /v "" > "children.txt"
	timeout 1 > NUL
	echo children.txt
	findstr /m "node" children.txt > NUL
	echo %errorlevel%
	if %errorlevel%==0 (
		SET "%~1 = 1"
		echo "node.exe" was found within the children.txt file!
	) else (
		SET "%~1 = 0"
		echo "node.exe" wasn't found within the children.txt file!
	)
