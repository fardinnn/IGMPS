@echo off
cls
SETLOCAL EnableDelayedExpansion

:begin
pause
SET process_name=ConnectToLocalTunnel.bat
echo process_name1:%process_name%
:: start cmd /k call %process_name% 18136

CALL :is_process_running %process_name%, result
echo %result%
pause
goto begin
EXIT /B %ERRORLEVEL%


:is_process_running
SET process_name=N:\IGMPS\Utilities\DangerousActionCodes\%~1
SET /A result = 0

echo process_name: %process_name%
tasklist /NH /FI "imagename eq %process_name%" 2>nul |find /i "%process_name%" >nul
If not errorlevel 1 (Echo my process started) else (Echo my process is not started)
IF not ERRORLEVEL 1 (SET /A result = 0) ELSE (SET /A result = 1)
echo result: %result%
endlocal & SET "%~2 = %result%"
pause
EXIT /B 0
