findstr /m "node.exe" children.txt > NUL
if %errorlevel%==0 (
	echo "node.exe" was found within the children.txt file!
)

if %errorlevel%==1 (
	echo "node.exe" wasn't found within the children.txt file!
)
pause