@echo off & SetLocal EnableExtensions, EnableDelayedExpansion
SET file_name=ConnectToLocalTunnel.bat
::ConnectToLocalTunnel.bat
SET file_title=ConnectToLocalTunnel

:run_fule
	start "ConnectToLocalTunnel" %file_name%


:is_file_running
	timeout 5 > NUL
	tasklist /fi "WINDOWTITLE eq %file_title%" | find /I "cmd.exe">NUL
	if "%ERRORLEVEL%"=="0" (
		timeout 5 > NUL
		call :check_node_as_child result
		echo result is %result%
		goto is_file_running
	) ELSE (
	echo run_fule
	goto run_fule
	)
	

:check_node_as_child
	set /A i=0
	for /F "tokens=2" %%R in ('
		tasklist /fi "WINDOWTITLE eq ConnectToLocalTunnel" /fo list ^| findstr /B "PID:"
	') do (
		set /A i+=1
		set /A PID[!i!]=%%R
	)
	timeout 5 > NUL
	wmic process where ParentProcessId^="%PID[1]%" get Caption | find /v "" > "children.txt"
	timeout 1 > NUL
	findstr /m "node" children.txt > NUL
	echo %errorlevel%
	if %errorlevel%==0 (
		SET /A "%~1 = 1"
		echo "node.exe" was found within the children.txt file!
	) else (
		taskkill /PID %PID[1]% /T /F
		SET /A "%~1 = 0"
		echo "node.exe" wasn't found within the children.txt file!
	)