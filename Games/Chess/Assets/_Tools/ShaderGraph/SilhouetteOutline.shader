Shader "Unlit/SilhouetteOutline"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _Color("Outline Color", Color) = (0,0,0,0)
		_Outline ("Outline width", Range (0.0, 10.0)) = .005
    }
    SubShader
    {
        Tags { "Queue"="Transparent" }
        LOD 300
        //Cull back
        //Blend one one

        Pass
        {
            Name "OUTLINE"
            Tags { 
                "RenderType" = "Opaque"
                "RenderPipeline" = "UniversalPipeline"
            }

            Cull Back
            ZWrite Off
            ZTest Always
            //Blend SrcAlpha OneMinusSrcAlpha // Normal
            Blend One One

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
	            float3 normal : NORMAL;
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
                float4 color : Color;
            };
            
            uniform float _Outline;
            uniform float4 _Color;
            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                float3 norm   = mul ((float3x3)UNITY_MATRIX_IT_MV, v.normal);
	            float2 offset = normalize(TransformViewToProjection(norm.xy));
                float dist = distance(_WorldSpaceCameraPos, mul(unity_ObjectToWorld, v.vertex));
                
	            o.vertex.xy += offset * _Outline * dist / _ScreenParams.xy ;
	            o.color = _Color;
                return o;
            }
            fixed4 frag (v2f i) : SV_Target
            {
                return i.color;
            }
            ENDCG
        }
        Pass
        {
            Name "Diffuse"
            Tags { 
                "RenderType" = "Opaque"
                "RenderPipeline" = "UniversalPipeline"
                "LightMode" = "UniversalForward"
            }
            Cull Back
            Blend One OneMinusDstColor
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                // sample the texture
                fixed4 col = tex2D(_MainTex, i.uv);
                // apply fog
                UNITY_APPLY_FOG(i.fogCoord, col);
                return col;
            }
            ENDCG
        }
    }
}
