﻿using BG.API;
using UnityEngine;
using BG.EISystem;
using System.Collections;

public class GenerateInputs : Singleton<GenerateInputs>
{
    private SimpleInputController inputController;

    protected override void Awake()
    {
        base.Awake();
        inputController = new SimpleInputController(model: new SimpleInputModel
        {

        });
    }

    public void AddInputReceiver(IInputReceiver inputReceiver)
    {
        if (inputReceiver == null)
        {
            Debug.LogError("'inputReceiver' can not be null!");
            return;
        }

        if (inputController==null)
        {
            StartCoroutine(AddInputReceiverAsync(inputReceiver));
            return;
        }

        inputController.Add(inputReceiver);
    }

    IEnumerator AddInputReceiverAsync(IInputReceiver inputReceiver)
    {
        int counter = 0;
        while (inputController == null && counter<2000)
        {
            counter++;
            yield return new WaitForFixedUpdate();
        }
        if (counter>=2000)
        {
            Debug.LogError("'inputController' doesn't created");
            yield break;
        }
        inputController.Add(inputReceiver);
    }
}