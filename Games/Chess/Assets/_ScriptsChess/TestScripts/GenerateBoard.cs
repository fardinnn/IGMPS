using System;
using BG.API;
using BG.Grids;
using UnityEngine;
using BG.API.Shapes;
using BG.API.Abilities;
using BG.PointerSystem;
using BG.ShapeProviders;
using BG.HighlightSystem;
using System.Collections;
using BG.States;
using System.Collections.Generic;
using BG.GameMechanism.BoardGames;
using BG.ArtAssets;

public class GenerateBoard : MonoBehaviour
{
    /*************************************************/
    /*                Dependencies                   */
    /*************************************************/
    [SerializeField]
    private Transform _parent;

    /*************************************************/
    /*              Define Controllers               */
    /*************************************************/
    private IGridController board;
    private Simple3DShapeProviderController shapeProvider;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(StartAfterRequirements());

    }

    IEnumerator StartAfterRequirements()
    {
        yield return new WaitUntil(() =>
        {
            return true;
        });

        /*************************************************/
        /*              Declare Controllers              */
        /*************************************************/
        //shapeProvider = new Simple3DShapeProviderController(model:
        //    new Simple3DShapeProviderModel
        //    {
        //        DefaultOptions = new Options { new ScaleFactorOption { ScaleFactor = 0.0635f } },
        //    });

        var hoverController = new SimpleHoverController();
        var clickController = new SimpleClickController(model: new SimplePointerModel { HoverController = hoverController });
        var selectController = new SimpleSingleSelectController(model: new SimplePointerModel { HoverController = hoverController });
        var dropRegionController = new SimpleDropRegionController(model: new SimplePointerModel { HoverController = hoverController });


        var highlightController = new SimpleHighlightController(model: new SimpleHighlightModel
        {
            RequiredStates = new HashSet<Enum>
            {
                LockState.Unlocked
            },
            StateColors = new Dictionary<StateGroup, Color>
            {
                {new StateGroup( PointerState.PointerDown), Color.red },
                {new StateGroup( DroppableRegionState.DragEnter), Color.gray },
                {new StateGroup( HoverState.Entered, SelectState.Selected), Color.magenta },
                {new StateGroup( SelectState.Selected), Color.green },
                {new StateGroup( HoverState.Entered) , Color.blue },
                { new StateGroup( ReserveState.Reserved), Color.gray },
                { new StateGroup( PlaceState.Ready), Color.cyan }
            }
        });

        highlightController.AddStatesGroup(LockState.Unlocked);

        /*************************************************/
        /*            Create Main Controller             */
        /*************************************************/

        board = new SimpleRectangularGridController(model:
                    new SimpleRectangularGridModel
                    {
                        Parent = _parent,
                        Size = new Vector2Int(8, 8),
                        GenerateOnBegin = true,
                        CellsAffectContents = true,
                        CellsGetAffectedByContents = true,
                        GenerateAlgorithm = new CheckerFillAlgorithmModel
                        {
                            Cell1Systems = new GameEntitiesVisitors<IComponentSystem> {
                                { LockState.Unlocked ,  new LockStateChangedEventArgs()},
                                { SelectState.NotSelected ,  new SelectStateChagedEventArgs()},
                                { ContainerEntityState.Empty ,  new ContainerEntityStateChagedEventArgs()},

                                //shapeProvider.ProvideComponentSystem(new Options{
                                //        new FormingOption{ShapeName = ShapeNames.SimpleSquareCell,
                                //            Package = ShapePackages.DefaultShapesPackage,
                                //            ShapeType = typeof(CellShape)
                                //        },
                                //        new ColorOption{Value = Color.white},
                                //        new SizeOption{KeepAspects = true, Size = Vector3.one * 0.0635f }
                                //    }
                                //),
                                highlightController.ProvideComponentSystem(), 
                                hoverController.ProvideComponentSystem(), 
                                clickController.ProvideComponentSystem(),
                                selectController.ProvideComponentSystem(),
                                dropRegionController.ProvideComponentSystem()
                            },

                            Cell2Systems = new GameEntitiesVisitors<IComponentSystem> {
                                { LockState.Unlocked ,  new LockStateChangedEventArgs()},
                                { SelectState.NotSelected ,  new SelectStateChagedEventArgs()},
                                { ContainerEntityState.Empty ,  new ContainerEntityStateChagedEventArgs()},

                                //shapeProvider.ProvideComponentSystem(new Options{
                                //        new FormingOption{ShapeName = ShapeNames.SimpleSquareCell,
                                //        Package = ShapePackages.DefaultShapesPackage,
                                //        ShapeType = typeof(CellShape)
                                //        },
                                //        new ColorOption{Value = Color.black},
                                //        new SizeOption{KeepAspects = true, Size = Vector3.one * 0.0635f }
                                //        }
                                //    ),
                                highlightController.ProvideComponentSystem(),
                                hoverController.ProvideComponentSystem(),
                                clickController.ProvideComponentSystem(),
                                selectController.ProvideComponentSystem(),
                                dropRegionController.ProvideComponentSystem()
                            },
                            CellSize = new Vector2(0.0635f, 0.0635f)
                        }
                    }
                    );

        GenerateInputs.Instance.AddInputReceiver(selectController);

        /*************************************************/
        /*          Prepare Actions and Events           */
        /*************************************************/

        //hoverController.Entered += (object o, HoverEventArgs e) =>
        //{
        //    highlightController.BeginHighlight("Entered", e.HoveredEntity);
        //};

        //hoverController.Exited += (object o, HoverEventArgs e) =>
        //{
        //    highlightController.EndHighlight("Entered", e.HoveredEntity);
        //};

        //clickController.ClickedDown0 += (object o, ClickEventArgs e) =>
        //{
        //    highlightController.BeginHighlight("Clicked", e.ClickedEntity);
        //};
        //clickController.ClickedUp0 += (object o, ClickEventArgs e) =>
        //{
        //    highlightController.EndHighlight("Clicked", e.ClickedEntity);
        //};

        //selectController.Selected += (object o, SelectEventArgs e) =>
        //{
        //    Debug.Log($"Selected {e.Entity.name}");
        //    highlightController.BeginHighlight("Selected", e.Entity);
        //};
        //selectController.Unselected += (object o, SelectEventArgs e) =>
        //{
        //    Debug.Log($"Unselected {e.Entity.name}");
        //    highlightController.EndHighlight("Selected", e.Entity);
        //};
    }


    public void OnChangeToSquareShape()
    {
        //shapeProvider.ChangeShapes<SimpleSquareCellEntity>();
    }

    public void OnChangeToCircleShape()
    {
        //shapeProvider.ChangeShapes<SimpleCircularCellEntity>();
    }

    public IGridController GetBoard()
    {
        return board;
    }
}

