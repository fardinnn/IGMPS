using UnityEngine;

public class InstantiatePrefab : MonoBehaviour
{
    [SerializeField]
    private GameObject myPrefab;

    private void Start()
    {
        Instantiate(myPrefab);
    }
}
