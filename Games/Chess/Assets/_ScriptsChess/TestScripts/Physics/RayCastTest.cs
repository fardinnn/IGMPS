using Unity.Entities;
using Unity.Physics;
using Unity.Physics.Systems;
using UnityEngine;
using Unity.Rendering;


public class RayCastTest : MonoBehaviour
{
    public static readonly float RAYCAST_DISTANCE = 1000;

    PhysicsWorld physicsWorld => World.DefaultGameObjectInjectionWorld.GetExistingSystem<BuildPhysicsWorld>().PhysicsWorld;
    EntityManager entityManager => World.DefaultGameObjectInjectionWorld.EntityManager;

    private Camera camera;
    void LateUpdate()
    {

        if (!Input.GetMouseButtonDown(0)) return;

        Debug.Log("2");
        if (camera==null || !camera.gameObject.activeSelf)
            camera = FindObjectOfType<Camera>();
        if (camera == null || !camera.gameObject.activeSelf)
        {
            Debug.Log("Couldn't find camera!");
            return;
        }
        Debug.Log("3");

        var screenPointToRay = camera.ScreenPointToRay(Input.mousePosition);

        Debug.Log("4");
        var rayInput = new RaycastInput
        {
            Start = screenPointToRay.origin,
            End = screenPointToRay.GetPoint(RAYCAST_DISTANCE),
            Filter = CollisionFilter.Default
        };

        Debug.Log("5");
        if (!physicsWorld.CastRay(rayInput, out Unity.Physics.RaycastHit hit)) return;

        Debug.Log("6");
        var selectedEntity = physicsWorld.Bodies[hit.RigidBodyIndex].Entity;

        Color color = UnityEngine.Random.ColorHSV();
        //entityManager.AddComponentData(selectedEntity, new MaterialColor { Value = new float4(color.r, color.g, color.b, color.a) });
        var renderMesh = entityManager.GetSharedComponentData<RenderMesh>(selectedEntity);
        //var mat = new UnityEngine.Material(renderMesh.material);
        Debug.Log($"6.5: color is {color}");
        //mat.SetColor("_Color", color);
        //Debug.Log($"6.75: color is {mat.color}");
        //renderMesh.material = mat;

        Debug.Log("7");

        var material = new UnityEngine.Material(renderMesh.material);
        material.color = UnityEngine.Random.ColorHSV();

        entityManager.SetSharedComponentData(selectedEntity, new RenderMesh
        {
            mesh = renderMesh.mesh,
            material = material,
            castShadows = renderMesh.castShadows,
            layer = renderMesh.layer,
            needMotionVectorPass = renderMesh.needMotionVectorPass,
            receiveShadows = renderMesh.receiveShadows,
            subMesh = renderMesh.subMesh
        });
        Debug.Log($"8: {selectedEntity.Index} : {renderMesh.material.color}");
    }
}
