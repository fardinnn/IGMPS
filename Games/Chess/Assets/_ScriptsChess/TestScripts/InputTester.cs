using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using BG.EISystem;
using BG.PointerSystem;
using BG.CameraSystems;

public class InputTester : MonoBehaviour
{

    //private ChessInputActions controls;
    private SimplePerspectiveCameraInput inputActions;

    private void Awake()
    {
        inputActions = new SimplePerspectiveCameraInput();
        //inputActions.SimplePointer.Click0.canceled += ctx => { Debug.Log("Click0"); };
        Debug.Log("AAA");
        inputActions.SimplePerspectiveCamera.Rotate_Actuators.performed += ctx => { 
            Debug.Log($"PointerDrag0: {ctx.ReadValueAsObject()}");
        };
        Debug.Log("BBB");
        //inputActions.Map1.PointerDrag1.performed += ctx => { Debug.Log("PointerDrag1"); };
        //inputActions.Map1.PointerDrag2.performed += ctx => { Debug.Log("PointerDrag2"); };
        //controls = new ChessInputActions();
    }

    private void Start()
    {
        //controls.Camera.Rotate.performed += ctx => Rotate(ctx.ReadValue<Vector2>());
        //controls.Camera.Pan.performed += ctx => Pan(ctx.ReadValue<Vector2>());
        //controls.Camera.Zoom.performed += ctx => Zoom(ctx.ReadValue<float>());
    }

    private void Rotate(Vector2 deltaRotation)
    {
        if (deltaRotation == default) return;
        Debug.Log("Rotate worked : " + deltaRotation.ToString());
    }

    private void Pan(Vector2 deltaPosition)
    {
        if (deltaPosition == default) return;
        Debug.Log("Performed :" + deltaPosition.ToString());
    }

    private void Zoom(float deltaScale)
    {
        if (deltaScale == default) return;
        Debug.Log("Performed :" + deltaScale.ToString());
    }

    private void OnEnable()
    {
        inputActions.Enable();
        //    controls.Enable();
    }

    private void OnDisable()
    {
        inputActions.Disable();
    //    controls.Disable();
    }
}
