using BG.CameraSystems;
using System;
using UnityEngine;

public class GenerateCamera : MonoBehaviour
{
    [SerializeField]
    private Transform TargetField;

    private SimpleMultiCameraController simpleBoardCamera;

    private Guid perspectiveCamera;
    private Guid topViewCamera;

    void Start()
    {
        simpleBoardCamera = new SimpleMultiCameraController(model: new SimpleMultiCameraModel { CameraTag = "" });
        var simplePerspectiveCamera = new SimplePerspectiveCameraController(null,
            new SimplePerspectiveCameraModel
            {
                TargetField = this.TargetField,
                DefaultRelativePosition = new Vector3(0, 0.75f, -0.5f),
                DefaultEulerAnglesRange = new Range<Vector3> { Min = new Vector3(20, 0), Max = new Vector3(80, 360) },
                RotationConstraintsType = ParameterSettingType.Default,
                HasRayCaster = true
            });
        var simpleTopViewCamera = new SimpleTopViewCameraController(null,
            new SimpleTopViewCameraModel
            {
                TargetField = TargetField,
                DefaultRelativePosition = Vector3.up * 1,
                Size = 0.5f,
                DefaultEulerAngles = Vector3.zero,
                HasRayCaster = true
            });

        simpleBoardCamera.AddCamera(simplePerspectiveCamera.View, true);
        simpleBoardCamera.AddCamera(simpleTopViewCamera.View);

        simpleBoardCamera.Generate();

        perspectiveCamera = simplePerspectiveCamera.Id;
        topViewCamera = simpleTopViewCamera.Id;


        GenerateInputs.Instance.AddInputReceiver(simplePerspectiveCamera);
        GenerateInputs.Instance.AddInputReceiver(simpleTopViewCamera);
    }

    public void ActivatePerspectiveCamera()
    {
        simpleBoardCamera.Activate(perspectiveCamera);
    }

    public void ActivateTopViewCamera()
    {
        simpleBoardCamera.Activate(topViewCamera);
    }
}
