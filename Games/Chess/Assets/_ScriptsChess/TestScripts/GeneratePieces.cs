using System;
using BG.API;
using BG.Grids;
using UnityEngine;
using BG.API.Shapes;
using BG.API.Abilities;
using BG.ShapeProviders;
using BG.BoardGamePieces;
using System.Collections;
using BG.States;
using System.Collections.Generic;
using BG.PointerSystem;
using BG.HighlightSystem;
using BG.Transitions;
using BG.ArtAssets;
using BG.Themes;

public class GeneratePieces : MonoBehaviour
{
    /*************************************************/
    /*                Dependencies                   */
    /*************************************************/
    [SerializeField]
    private GenerateBoard boardGenerator;

    /*************************************************/
    /*              Define Controllers               */
    /*************************************************/
    IGridController board;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(StartAfterRequirements());
    }


    IEnumerator StartAfterRequirements()
    {
        yield return new WaitUntil(() => { board = boardGenerator.GetBoard(); return board != null; });

        // Taking Players Ids
        var player1Id = Guid.NewGuid();
        var player2Id = Guid.NewGuid();


        var sfxAssetProvider = GetSFXProviderController();

        //sfxAssetProvider.Get();

        /*************************************************/
        /*              Declare Controllers              */
        /*************************************************/

        //var shapeProvider = GetPieceShapeController();
        var hoverController = GetHoverController();
        var clickController = GetClickController(hoverController);
        var selectController = GetSelectController(hoverController);
        var dragController = GetDragController(hoverController);
        var placementController = GetPlacementController();
        var moveController = GetMoveController(placementController);
        var highlightController = GetHighlightController();
        var transitionController = GetTransitionController();

        var sfxAssetController = new SimpleSFXAssetsController(model: new SimpleSFXAssetsModel
        {

        });

        var themeController = new DefaultThemeController();

        /*************************************************/
        /*            Create Main Controller             */
        /*************************************************/

        var pawn = new SimplePieceController(model:
            new SimplePieceModel
            {
                Components = new GameEntitiesVisitors<IComponentSystem>
                {
                    { LockState.Unlocked ,  new LockStateChangedEventArgs()},
                    { PlacementState.NotPlaced, new PlacementStateChagedEventArgs()},
                    { GameState.Beginning, new GameStateChangedEventArgs()},

                    //shapeProvider.ProvideComponentSystem(new Options{
                    //        new FormingOption{ShapeName = ShapeNames.SimpleBoardGamePiece,
                    //            Package = ShapePackages.DefaultShapesPackage,
                    //            ShapeType = typeof(PieceShape)
                    //        },
                    //        new ColorOption{Value = Color.white},
                    //        new SizeOption{KeepAspects = true, Size = Vector3.one * 0.0635f }
                    //    }
                    //),
                    highlightController.ProvideComponentSystem(),
                    hoverController.ProvideComponentSystem(),
                    clickController.ProvideComponentSystem(),
                    selectController.ProvideComponentSystem(),
                    dragController.ProvideComponentSystem(),
                    placementController.ProvideComponentSystem(),
                    moveController.ProvideComponentSystem(),
                    transitionController.ProvideComponentSystem(),
                    sfxAssetController.ProvideComponentSystem(),
                    themeController.ProvideComponentSystem()
                },
                PlayerId = player1Id
            }
            );

        GenerateInputs.Instance.AddInputReceiver(selectController);
        GenerateInputs.Instance.AddInputReceiver(dragController);

        //var player1Pieces = CreatePlayerPieces(player1Id, Color.black);
        //var player2Pieces = CreatePlayerPieces(player2Id, Color.black);
    }



    private IComponentController GetPieceShapeController()
    {
        return new Simple3DShapeProviderController(model:
            new Simple3DShapeProviderModel
            {
                DefaultOptions = new Options { new ScaleFactorOption { ScaleFactor = 0.0635f } },
            });
    }

    private ISFXAssetsController GetSFXProviderController()
    {
        return new SimpleSFXAssetsController();
    }

    private IHoverController GetHoverController()
    {
        return new SimpleHoverController();
    }

    private IPointerController GetClickController(IHoverController hoverController)
    {
        return new SimpleClickController(model: new SimplePointerModel { HoverController = hoverController });
    }

    private IPointerController GetSelectController(IHoverController hoverController)
    {
        return new SimpleSingleSelectController(model: new SimplePointerModel { HoverController = hoverController });
    }

    private IPointerController GetDragController(IHoverController hoverController)
    {
        return new SimpleDragController(model: new SimplePointerModel { HoverController = hoverController });
    }

    private IPlacementController GetPlacementController()
    {
        return new SimplePlacementController(model: new SimplePlacementModel
        {
            GridSysetmId = board.SystemId,
            PlacementStates = new PlacementStateSet
            {
                {  new GridPositionSet(8, 8).AddColumnRange(1, 0, 8), new StateGroup(GameState.Beginning) }
            },
            UnplacingStates = new CompositeStatesSet
            {
                new StateGroup(PlacementState.Unplaced)
            },
            ReplacementStates = new CompositeStatesSet
            {
                new StateGroup(PlacementState.Placed, LockState.Locked)
            },
            Positions = new GridPositionSet(8, 8).AddColumnRange(1, 0, 8),
            CellConstraints = new HashSet<StateGroup> { new StateGroup(PlaceState.Ready) }

        });
    }

    private IComponentController GetMoveController(IPlacementController placementController)
    {
        return new SimpleMoveController(model: new SimpleMoveModel
        {
            //MoveSystemName = BuiltinMoves.Pawn,
            ActivatePotentialMoves = new CompositeStatesSet
            {
                new StateGroup(SelectState.Selected)
            },
            MoveToCellStates = new CompositeStatesSet
            {
                new StateGroup(SelectState.Selected),
                new StateGroup(PointerState.PointerReleased)
            },

            StateBasedMoveRules = new Dictionary<StateGroup, HashSet<IMoveRuleFactory>>
            {
                {new StateGroup(MoveState.NotMoved),
                    new HashSet<IMoveRuleFactory>{BuiltinMoves.OneForward, BuiltinMoves.MaxTwoForward } },
                {new StateGroup(MoveState.Moved),
                    new HashSet<IMoveRuleFactory>{BuiltinMoves.OneForward, BuiltinMoves.OneDiagonalForward } },
                //{new StateGroup(),
                //    new HashSet<IMoveRuleFactory>{BuiltinMoves.OneDiagonalForward}}
            },
            StatesToActivatePotentialMove = new HashSet<StateGroup>
            {
                new StateGroup(SelectState.Selected)
            },
            //StatesToDeactivatePotentialMove = new HashSet<StateGroup>
            //{
            //    new StateGroup(SelectState.Selected)
            //},
            PlacementSystem = placementController.ProvideComponentSystem()
        });
    }

    private IComponentController GetHighlightController()
    {
        var highlightController = new SimpleHighlightController(model: new SimpleHighlightModel
        {
            RequiredStates = new HashSet<Enum>
            {
                LockState.Unlocked
            },
            StateColors = new Dictionary<StateGroup, Color>
            {
                { new StateGroup( SafetyState.Endangered), Color.red },
                { new StateGroup( PointerState.PointerDown), Color.red },
                { new StateGroup( HoverState.Entered, SelectState.Selected), Color.magenta },
                { new StateGroup( SelectState.Selected), Color.green },
                { new StateGroup( HoverState.Entered) , Color.blue },
                { new StateGroup( ReserveState.Reserved), Color.gray },
                { new StateGroup( PlaceState.Ready), Color.cyan }
            }
        });
        highlightController.AddStatesGroup(LockState.Unlocked);
        return highlightController;
    }

    private IComponentController GetTransitionController()
    {
        return new SimpleTransitionController();
    }
}