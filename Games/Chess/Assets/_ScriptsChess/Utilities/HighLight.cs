using BG.API;
using UnityEngine;

public static class DrawBorderExtension
{
    public static void DrawBorder(this Transform targetObject)
    {
        DrawBorder(targetObject.gameObject);
    }
    public static void RemoveBorder(this Transform targetObject)
    {
        RemoveBorder(targetObject.gameObject);
    }

    public static void DrawBorder(this GameObject targetObject)
    {

    }
    public static void RemoveBorder(this GameObject targetObject)
    {

    }
}


public class HighLight : IVisitor
{
    //public void Visit(PieceTemplate target)
    //{
    //    target.transform.DrawBorder();

    //}
    //public void Visit(ICell target)
    //{

    //}
}