using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameElementIds
{
    public static Guid GameId = Guid.NewGuid();
    public static Guid AlgorithmId = Guid.NewGuid();
    public static Guid BoardId = Guid.NewGuid();
    public static Guid EnvironmentId = Guid.NewGuid();
    public static Guid[] CameraIds = new[] { Guid.NewGuid() };
    public static Guid[][] CellIds = new Guid[][] {
            new Guid[] { Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid() },
            new Guid[] { Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid() },
            new Guid[] { Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid() },
            new Guid[] { Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid() },
            new Guid[] { Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid() },
            new Guid[] { Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid() },
            new Guid[] { Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid() },
            new Guid[] { Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid() }
    };

    public static Dictionary<string, Guid>[] PieceIds = new Dictionary<string, Guid>[] {
        new Dictionary<string, Guid> {
            { "pawn1",Guid.NewGuid()} ,{ "pawn2",Guid.NewGuid()} ,{ "pawn3",Guid.NewGuid()} ,{ "pawn4",Guid.NewGuid()} ,
            { "pawn5",Guid.NewGuid()} ,{ "pawn6",Guid.NewGuid()} ,{ "pawn7",Guid.NewGuid()} ,{ "pawn8",Guid.NewGuid()} ,
            { "rook1",Guid.NewGuid()} ,{ "rook2",Guid.NewGuid()} ,{ "bishop1",Guid.NewGuid()} ,{ "bishop2",Guid.NewGuid()} ,
            { "knight1",Guid.NewGuid()} ,{ "knight2",Guid.NewGuid()} ,{ "queen",Guid.NewGuid()} ,{ "king",Guid.NewGuid()} ,
        },
        new Dictionary<string, Guid> {
            { "pawn1",Guid.NewGuid()} ,{ "pawn2",Guid.NewGuid()} ,{ "pawn3",Guid.NewGuid()} ,{ "pawn4",Guid.NewGuid()} ,
            { "pawn5",Guid.NewGuid()} ,{ "pawn6",Guid.NewGuid()} ,{ "pawn7",Guid.NewGuid()} ,{ "pawn8",Guid.NewGuid()} ,
            { "rook1",Guid.NewGuid()} ,{ "rook2",Guid.NewGuid()} ,{ "bishop1",Guid.NewGuid()} ,{ "bishop2",Guid.NewGuid()} ,
            { "knight1",Guid.NewGuid()} ,{ "knight2",Guid.NewGuid()} ,{ "queen",Guid.NewGuid()} ,{ "king",Guid.NewGuid()} ,
        }
    };

    public static Dictionary<string, Guid> GeometryIds = new Dictionary<string, Guid> {
        { "pawn",Guid.NewGuid()} ,{ "rook",Guid.NewGuid()} ,{ "bishop",Guid.NewGuid()} ,{ "knight",Guid.NewGuid()} ,{ "queen",Guid.NewGuid()} ,{ "king",Guid.NewGuid()}
    };
    public static Dictionary<string, Guid> Graphics2DIds = new Dictionary<string, Guid> {
        { "pawn",Guid.NewGuid()} ,{ "rook",Guid.NewGuid()} ,{ "bishop",Guid.NewGuid()} ,{ "knight",Guid.NewGuid()} ,{ "queen",Guid.NewGuid()} ,{ "king",Guid.NewGuid()}
    };
    public static Guid[] Prefab3DIds = new Guid[] { Guid.NewGuid() };

    public static Guid[] PlayerIds = new Guid[] { Guid.NewGuid(), Guid.NewGuid() };
}
